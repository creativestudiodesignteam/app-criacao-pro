// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'report_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$ReportController on _ReportController, Store {
  final _$reportsAtom = Atom(name: '_ReportController.reports');

  @override
  List<ReportModel> get reports {
    _$reportsAtom.reportRead();
    return super.reports;
  }

  @override
  set reports(List<ReportModel> value) {
    _$reportsAtom.reportWrite(value, super.reports, () {
      super.reports = value;
    });
  }

  final _$onTapBackAsyncAction = AsyncAction('_ReportController.onTapBack');

  @override
  Future<void> onTapBack() {
    return _$onTapBackAsyncAction.run(() => super.onTapBack());
  }

  final _$readAllReportsAsyncAction =
      AsyncAction('_ReportController.readAllReports');

  @override
  Future<void> readAllReports() {
    return _$readAllReportsAsyncAction.run(() => super.readAllReports());
  }

  @override
  String toString() {
    return '''
reports: ${reports}
    ''';
  }
}
