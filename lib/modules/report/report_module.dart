import 'package:flutter_modular/flutter_modular.dart';
import '../bird/bird_module.dart';
import 'cage/report_cage_module.dart';
import 'filter/report_filter_module.dart';
import 'report_controller.dart';
import 'report_repository.dart';
import 'report_view.dart';

class ReportModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind(
          (i) => ReportController(
            reportRepository: i.get<ReportRepository>(),
          ),
        ),
        Bind(
          (i) => ReportRepository(),
        ),
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(
          '/',
          child: (ctx, args) => ReportView(),
        ),
        ModularRouter(
          '/bird',
          module: BirdModule(),
        ),
        ModularRouter(
          '/filter',
          module: ReportFilterModule(),
        ),
        ModularRouter(
          '/cage',
          module: ReportCageModule(),
        ),
      ];
}
