import 'package:asuka/asuka.dart' as asuka;
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:intl/intl.dart';

import '../../../report_pdf_filters.dart';
import '../../app/app_appbar_widget.dart';
import '../../app/app_button_widget.dart';
import '../../app/app_dropdown_widget.dart';
import '../../app/app_header_widget.dart';
import '../../app/app_message_widget.dart';
import '../../app/app_scaffold_widget.dart';
import '../../app/app_text_widget.dart';
import 'report_filter_controller.dart';

class ReportFilterView extends StatefulWidget {
  @override
  _ReportFilterViewState createState() => _ReportFilterViewState();
}

class _ReportFilterViewState
    extends ModularState<ReportFilterView, ReportFilterController> {
  ScrollController _sc;
  bool isLoading = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget appBar(BuildContext context) {
    return AppAppBarWidget(
      titles: <Widget>[
        AppTextWidget(
          text: 'Buscar aves',
          size: Theme.of(context).textTheme.subtitle2.fontSize,
        ),
      ],
      actions: <Widget>[
        IconButton(
          icon: Icon(
            Icons.chevron_left,
          ),
          onPressed: controller.onTapBack,
        ),
      ],
    );
  }

  Widget body(BuildContext context) {
    return Column(
      children: <Widget>[
        AppHeaderWidget(
          title: 'Vários filtros',
          subtitle: 'para você ',
          highlight: 'buscar a sua ave',
        ),
        SizedBox(
          height: 20,
        ),
        Observer(
          builder: (_) {
            if (controller.birdController.birds == null) {
              return AppTextWidget(
                text: 'Buscando aves...',
              );
            } else if (controller.birdController.birds.isEmpty) {
              return AppTextWidget(
                text: 'Nenhuma ave encontrada.',
              );
            } else {
              return Column(
                children: <Widget>[
                  AppTextWidget(
                    size: 15,
                    text: 'Filtros',
                    weight: FontWeight.bold,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Observer(
                    builder: (_) {
                      var loading = controller.categories == null;
                      return AppDropdownWidget(
                        label: 'Categoria',
                        value: loading ? ' ... ' : controller.category.name,
                        items: loading
                            ? [
                                ' ... ',
                              ]
                            : controller.categories.map((e) => e.name).toList(),
                        onChanged: (value) {
                          controller.category = controller.categories
                              .firstWhere((e) => e.name == value);
                          controller.race =
                              controller.category.races.elementAt(0);
                          controller.color =
                              controller.race.colors.elementAt(0);
                        },
                      );
                    },
                  ),
                  Observer(
                    builder: (_) {
                      var loading = controller.categories == null;
                      return AppDropdownWidget(
                        label: 'Raça',
                        value: loading ? ' ... ' : controller.race.name,
                        items: loading
                            ? [
                                ' ... ',
                              ]
                            : controller.category.races
                                .map((e) => e.name)
                                .toList(),
                        onChanged: (value) {
                          controller.race = controller.category.races
                              .firstWhere((e) => e.name == value);
                          controller.color =
                              controller.race.colors.elementAt(0);
                        },
                      );
                    },
                  ),
                  Observer(
                    builder: (_) {
                      var loading = controller.categories == null;
                      return AppDropdownWidget(
                        label: 'Cor',
                        value: loading ? ' ... ' : controller.color.name,
                        items: loading
                            ? [
                                ' ... ',
                              ]
                            : controller.race.colors
                                .map((e) => e.name)
                                .toList(),
                        onChanged: (value) {
                          controller.color = controller.race.colors
                              .firstWhere((e) => e.name == value);
                        },
                      );
                    },
                  ),
                  Row(
                    children: <Widget>[
                      Flexible(
                        child: Observer(
                          builder: (_) {
                            var loading =
                                controller.birdCreateController.genders == null;
                            return AppDropdownWidget(
                              label: 'Gênero',
                              value: loading
                                  ? ' ... '
                                  : controller.birdCreateController.gender.name,
                              items: loading
                                  ? [
                                      ' ... ',
                                    ]
                                  : controller.birdCreateController.genders
                                      .map((e) => e.name)
                                      .toList(),
                              onChanged: (value) {
                                controller.birdCreateController.gender =
                                    controller.birdCreateController.genders
                                        .firstWhere((e) => e.name == value);
                              },
                            );
                          },
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Flexible(
                        child: Observer(
                          builder: (_) {
                            var loading =
                                controller.birdCreateController.statuses ==
                                    null;
                            return AppDropdownWidget(
                              label: 'Status',
                              value: loading
                                  ? ' ... '
                                  : controller.birdCreateController.statuses
                                      .elementAt(0)
                                      .name,
                              items: loading
                                  ? [
                                      ' ... ',
                                    ]
                                  : controller.birdCreateController.statuses
                                      .map((e) => e.name)
                                      .toList(),
                              onChanged: (value) {
                                controller.birdCreateController.status =
                                    controller.birdCreateController.statuses
                                        .firstWhere((e) => e.name == value);
                              },
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Flexible(
                        child: Observer(
                          builder: (_) {
                            var loading = controller.birthDateList == null;
                            return AppDropdownWidget(
                              label: 'Ano de Nascimento',
                              value: loading
                                  ? ' ... '
                                  : controller.birthDateList.elementAt(0).name,
                              items: loading
                                  ? [
                                      ' ... ',
                                    ]
                                  : controller.birthDateList
                                      .map((e) => e.name)
                                      .toList(),
                              onChanged: (value) {
                                controller.birthDate = controller.birthDateList
                                    .firstWhere((e) => e.name == value);
                              },
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      AppButtonWidget(
                        text: 'Gerar Relatório',
                        color: Theme.of(context).primaryColor,
                        onPressed: () {
                          controller.onTapApplyFilters(false).then((value) {
                            if (controller.birds.length > 0) {
                              asuka.showSnackBar(
                                SnackBar(
                                  content: AppMessageWidget(
                                    message:
                                        'Aguarde enquanto o relatório é gerado!',
                                  ),
                                  duration: Duration(
                                    milliseconds: 2000,
                                  ),
                                ),
                              );
                              reportViewFilters(context, controller.birds);
                            } else {
                              asuka.showSnackBar(
                                SnackBar(
                                  content: AppMessageWidget(
                                    message:
                                        'Nenhuma ave encontrada para o relatório!',
                                  ),
                                  duration: Duration(
                                    milliseconds: 2000,
                                  ),
                                ),
                              );
                            }
                          });
                        },
                      ),
                      AppButtonWidget(
                        text: 'Todas Aves',
                        color: Theme.of(context).primaryColor,
                        onPressed: () {
                          asuka.showSnackBar(
                            SnackBar(
                              content: AppMessageWidget(
                                message:
                                    'Aguarde enquanto o relatório é gerado!',
                              ),
                              duration: Duration(
                                milliseconds: 2000,
                              ),
                            ),
                          );
                          controller.onTapApplyFilters(true).then((value) {
                            reportViewFilters(context, controller.birds);
                          });
                        },
                      ),
                    ],
                  ),
                ],
              );
              // return BirdListWidget(
              //   birds: controller.birdController.birds,
              //   isReport: true,
              //   sc: _sc,
              // );
            }
          },
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    controller.birdController.readAllBirdsCage(null, null).then((value) {
      controller.birdCreateController.readAllCategories().then((value) {
        controller.loadFilters();
      });
    });

    controller.birdCreateController.readAllGenders();
    controller.birdCreateController.readAllStatuses();

    controller.birdCreateController.birthController.text =
        DateFormat('dd/MM/yyyy').format(DateTime.now()).toString();
    return AppScaffoldWidget(
      appBar: appBar(context),
      body: body(context),
    );
  }
}
