import 'package:flutter_modular/flutter_modular.dart';
import 'package:dio/dio.dart';
import '../../account/account_controller.dart';
import '../../bird/bird_controller.dart';
import '../../bird/bird_repository.dart';
import '../../bird/create/bird_create_controller.dart';
import '../../cage/baby/cage_baby_controller.dart';
import '../../cage/baby/cage_baby_repository.dart';
import '../../cage/cage_controller.dart';
import '../../cage/cage_repository.dart';
import 'report_filter_controller.dart';
import 'report_filter_view.dart';

class ReportFilterModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind(
          (i) => ReportFilterController(
            birdCreateController: i.get<BirdCreateController>(),
            birdController: i.get<BirdController>(),
          ),
        ),
        Bind(
          (i) => BirdController(
            birdRepository: i.get<BirdRepository>(),
            accountController: i.get<AccountController>(),
            birdCreateController: i.get<BirdCreateController>(),
          ),
        ),
        Bind(
          (i) => BirdCreateController(
            birdRepository: i.get<BirdRepository>(),
            cageController: i.get<CageController>(),
            accountController: i.get<AccountController>(),
            cageBabyController: i.get<CageBabyController>(),
          ),
        ),
        Bind(
          (i) => CageBabyController(
            cageBabyRepository: i.get<CageBabyRepository>(),
          ),
        ),
        Bind(
          (i) => CageController(
            cageRepository: i.get<CageRepository>(),
            accountController: i.get<AccountController>(),
          ),
        ),
        Bind(
          (i) => BirdRepository(
            dio: i.get<Dio>(),
          ),
        ),
        Bind(
          (i) => CageBabyRepository(
            dio: i.get<Dio>(),
          ),
        ),
        Bind(
          (i) => CageRepository(
            dio: i.get<Dio>(),
          ),
        ),
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(
          '/',
          child: (ctx, args) => ReportFilterView(),
        ),
      ];
}
