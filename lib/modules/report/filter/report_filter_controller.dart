import 'package:asuka/asuka.dart' as asuka;
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:intl/intl.dart';
import 'package:mobx/mobx.dart';

import '../../app/app_message_widget.dart';
import '../../bird/bird_category_model.dart';
import '../../bird/bird_color_model.dart';
import '../../bird/bird_controller.dart';
import '../../bird/bird_race_model.dart';
import '../../bird/create/bird_create_controller.dart';

part 'report_filter_controller.g.dart';

class ReportFilterController = _ReportFilterController
    with _$ReportFilterController;

abstract class _ReportFilterController with Store {
  _ReportFilterController({
    this.birdCreateController,
    this.birdController,
  });

  final BirdCreateController birdCreateController;
  final BirdController birdController;

  @observable
  List<BirdCategoryModel> categories;

  @observable
  BirdCategoryModel category;

  @observable
  BirdRaceModel race;

  @observable
  BirdColorModel color;

  @observable
  int idade;

  @observable
  List<YearModel> birthDateList;

  @observable
  YearModel birthDate;

  @observable
  dynamic birds;

  Future<void> onTapBack() async {
    Modular.to.pop();
  }

  Future<void> onTapBirthDate(DateTime date) async {
    birdCreateController.birthController.text =
        DateFormat('dd/MM/yyyy').format(date).toString();
  }

  Future<void> loadFilters() async {
    categories = List<BirdCategoryModel>();
    birthDateList = List<YearModel>();
    await categories.add(birdCreateController.categories[0]);
    await birthDateList.add(YearModel.fromJson({'name': ''}));
    // birthDateList.add(YearModel.fromJson({'name': '2018'}));
    // birthDateList.add(YearModel.fromJson({'name': '2019'}));
    // birthDateList.add(YearModel.fromJson({'name': '2020'}));

    for (var item in birdController.birds) {
      for (var itemCategory in birdCreateController.categories) {
        if (itemCategory.name == item.category) {
          if (!categories.contains(itemCategory)) {
            categories.add(itemCategory);
          }
        }
      }
    }

    for (var i = 0; i < categories.length; i++) {
      var forRaces = new List<BirdRaceModel>();
      for (var itemRace in categories[i].races) {
        for (var item in birdController.birds) {
          var whereExists =
              forRaces.indexWhere((element) => element.name == item.race);
          if (item.race == itemRace.name) {
            if (whereExists == -1) {
              forRaces.add(itemRace);
            }
          }
        }
      }
      if (forRaces.length > 0) {
        categories[i].races = forRaces;
      }
    }

    for (var i = 0; i < categories.length; i++) {
      for (var j = 0; j < categories[i].races.length; j++) {
        var forColors = new List<BirdColorModel>();
        for (var itemColor in categories[i].races[j].colors) {
          for (var item in birdController.birds) {
            var whereExists =
                forColors.indexWhere((element) => element.name == item.color);
            if (item.color == itemColor.name) {
              if (whereExists == -1) {
                forColors.add(itemColor);
              }
            }
          }
          if (forColors.length > 0) {
            categories[i].races[j].colors = forColors;
          }
        }
      }
    }

    var birthDateListFor = [];
    birthDateListFor.addAll(birthDateList);
    for (var item in birdController.birds) {
      for (var itemBirthDate in birthDateListFor) {
        var itemYear = true;
        var birthDateBird = DateTime.fromMillisecondsSinceEpoch(item.birthDate);
        var birthDateFormattedBird =
            DateFormat('yyyy').format(birthDateBird).toString();
        var itemAdd = YearModel.fromJson({'name': birthDateFormattedBird});
        if (itemBirthDate.name == birthDateFormattedBird) {
          itemYear = false;
        }

        var whereExists =
            birthDateList.indexWhere((element) => element.name == itemAdd.name);
        if (itemYear == true) {
          if (whereExists == -1) {
            await birthDateList.add(itemAdd);
          }
        }
      }
    }

    birthDate = birthDateList.elementAt(0);
    category = birdCreateController.categories.elementAt(0);
    race = category.races.elementAt(0);
    color = race.colors[0];
  }

  @action
  Future<void> onTapApplyFilters(bool all) async {
    var filtered = [];
    birds = [];

    if (all == false) {
      if (category.name == '' &&
          race.name == '' &&
          color.name == '' &&
          birthDate.name == '' &&
          birdCreateController.status.name == '' &&
          birdCreateController.gender.name == '') {
        asuka.showSnackBar(
          SnackBar(
            content: AppMessageWidget(
              message: 'Selecione algum filtro para o relatório!',
            ),
            duration: Duration(
              milliseconds: 2000,
            ),
          ),
        );
      } else {
        if (category.name != '') {
          var categoryBirds = birdController.birds.where((b) {
            return b.category == category.name;
          });
          filtered.addAll(categoryBirds);
        }
        if (race.name != '') {
          var raceBirds;
          var filteredTemp = filtered;
          filtered = [];
          if (filteredTemp.length > 0) {
            raceBirds = filteredTemp.where((b) {
              return b.race == race.name;
            });
            filtered.addAll(raceBirds);
          } else {
            raceBirds = birdController.birds.where((b) {
              return b.race == race.name;
            });
            filtered.addAll(raceBirds);
          }
        }
        if (color.name != '') {
          var colorBirds;
          var filteredTemp = filtered;
          filtered = [];
          if (filteredTemp.length > 0) {
            colorBirds = filteredTemp.where((b) {
              return b.color == color.name;
            });
            filtered.addAll(colorBirds);
          } else {
            colorBirds = birdController.birds.where((b) {
              return b.color == color.name;
            });
            filtered.addAll(colorBirds);
          }
        }
        if (birdCreateController.gender.name != '') {
          var genderBirds;
          var filteredTemp = filtered;
          filtered = [];
          if (filteredTemp.length > 0) {
            genderBirds = filteredTemp.where((b) {
              return b.gender == birdCreateController.gender.name;
            });
            filtered.addAll(genderBirds);
          } else {
            genderBirds = birdController.birds.where((b) {
              return b.gender == birdCreateController.gender.name;
            });
            filtered.addAll(genderBirds);
          }
        }
        if (birdCreateController.status.name != '') {
          var statusBirds;
          var filteredTemp = filtered;
          filtered = [];
          if (filteredTemp.length > 0) {
            statusBirds = filteredTemp.where((b) {
              return b.status == birdCreateController.status.name;
            });
            filtered.addAll(statusBirds);
          } else {
            statusBirds = birdController.birds.where((b) {
              return b.status == birdCreateController.status.name;
            });
            filtered.addAll(statusBirds);
          }
        }

        for (var item in filtered) {
          var birthDateBird =
              DateTime.fromMillisecondsSinceEpoch(item.birthDate);
          var birthDateFormattedBird =
              DateFormat('yyyy').format(birthDateBird).toString();

          if (birthDate.name != '') {
            if (birthDate.name == birthDateFormattedBird) {
              birds.add({
                'id': item.id,
                'category': item.category,
                'race': item.race,
                'color': item.color,
                'gender': item.gender,
                'status': item.status,
                'birthDate': item.birthDate,
                'rightRing': item.rightRing != '' ? item.rightRing : 0,
                'leftRing': item.leftRing != '' ? item.leftRing : 0,
                'registry': item.registry,
                'name': item.name,
                'yearBirth': birthDateFormattedBird,
              });
            }
          } else {
            birds.add({
              'id': item.id,
              'category': item.category,
              'race': item.race,
              'color': item.color,
              'gender': item.gender,
              'status': item.status,
              'birthDate': item.birthDate,
              'rightRing': item.rightRing != '' ? item.rightRing : 0,
              'leftRing': item.leftRing != '' ? item.leftRing : 0,
              'registry': item.registry,
              'name': item.name,
              'yearBirth': birthDateFormattedBird,
            });
          }
        }
      }
    } else if (all == true) {
      for (var item in birdController.birds) {
        var birthDateBird = DateTime.fromMillisecondsSinceEpoch(item.birthDate);
        var birthDateFormattedBird =
            DateFormat('yyyy').format(birthDateBird).toString();

        birds.add({
          'id': item.id,
          'category': item.category,
          'race': item.race,
          'color': item.color,
          'gender': item.gender,
          'status': item.status,
          'birthDate': item.birthDate,
          'rightRing': item.rightRing != '' ? item.rightRing : 0,
          'leftRing': item.leftRing != '' ? item.leftRing : 0,
          'registry': item.registry,
          'name': item.name,
          'yearBirth': birthDateFormattedBird,
        });
      }
    }
  }
}
