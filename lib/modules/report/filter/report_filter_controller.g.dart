// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'report_filter_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$ReportFilterController on _ReportFilterController, Store {
  final _$birdsAtom = Atom(name: '_BirdController.birds');

  @override
  dynamic get birds {
    _$birdsAtom.reportRead();
    return super.birds;
  }

  @override
  set birds(dynamic value) {
    _$birdsAtom.reportWrite(value, super.birds, () {
      super.birds = value;
    });
  }

  final _$categoryAtom = Atom(name: '_BirdCreateController.category');

  @override
  BirdCategoryModel get category {
    _$categoryAtom.reportRead();
    return super.category;
  }

  @override
  set category(BirdCategoryModel value) {
    _$categoryAtom.reportWrite(value, super.category, () {
      super.category = value;
    });
  }

  final _$raceAtom = Atom(name: '_BirdCreateController.race');

  @override
  BirdRaceModel get race {
    _$raceAtom.reportRead();
    return super.race;
  }

  @override
  set race(BirdRaceModel value) {
    _$raceAtom.reportWrite(value, super.race, () {
      super.race = value;
    });
  }

  final _$colorAtom = Atom(name: '_BirdCreateController.color');

  @override
  BirdColorModel get color {
    _$colorAtom.reportRead();
    return super.color;
  }

  @override
  set color(BirdColorModel value) {
    _$colorAtom.reportWrite(value, super.color, () {
      super.color = value;
    });
  }

  final _$birthDateListAtom = Atom(name: '_BirdCreateController.birthDateList');

  @override
  List<YearModel> get birthDateList {
    _$birthDateListAtom.reportRead();
    return super.birthDateList;
  }

  @override
  set birthDateList(List<YearModel> value) {
    _$colorAtom.reportWrite(value, super.birthDateList, () {
      super.birthDateList = value;
    });
  }

  final _$birthDateAtom = Atom(name: '_BirdCreateController.birthDate');

  @override
  YearModel get birthDate {
    _$birthDateAtom.reportRead();
    return super.birthDate;
  }

  @override
  set birthDate(YearModel value) {
    _$colorAtom.reportWrite(value, super.birthDate, () {
      super.birthDate = value;
    });
  }

  final _$onTapApplyFiltersAsyncAction =
      AsyncAction('_ReportFilterController.onTapApplyFilters');

  @override
  Future<void> onTapApplyFilters(bool all) {
    return _$onTapApplyFiltersAsyncAction
        .run(() => super.onTapApplyFilters(all));
  }

  final _$loadFiltersAsyncAction =
      AsyncAction('_ReportFilterController.onTapApplyFilters');

  @override
  Future<void> loadFilters() {
    return _$loadFiltersAsyncAction.run(() => super.loadFilters());
  }

  @override
  String toString() {
    return '''
category: ${category},
race: ${race},
color: ${color},
birthDateList: ${birthDateList},
birthDate: ${birthDate},
    ''';
  }
}
