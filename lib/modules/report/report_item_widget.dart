import 'package:flutter/material.dart';
import '../app/app_card_widget.dart';
import '../app/app_text_widget.dart';
import 'report_model.dart';

class ReportItemWidget extends StatelessWidget {
  const ReportItemWidget({
    Key key,
    this.report,
    this.onPressed,
  }) : super(key: key);

  final ReportModel report;
  final Function onPressed;

  @override
  Widget build(BuildContext context) {
    return AppCardWidget(
      children: <Widget>[
        AppTextWidget(
          text: '${report.title}',
          size: Theme.of(context).textTheme.subtitle1.fontSize,
          weight: FontWeight.bold,
        ),
        AppTextWidget(
          text: '${report.subtitle}',
          size: Theme.of(context).textTheme.bodyText1.fontSize,
        ),
      ],
      image: report.image,
      color: Colors.white,
      onPressed: onPressed,
    );
  }
}
