import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'report_model.dart';
import 'report_repository.dart';
part 'report_controller.g.dart';

class ReportController = _ReportController with _$ReportController;

abstract class _ReportController with Store {
  _ReportController({
    this.reportRepository,
  });

  final ReportRepository reportRepository;

  @observable
  List<ReportModel> reports;

  @action
  Future<void> onTapBack() async {
    Modular.to.pop();
  }

  @action
  Future<void> readAllReports() async {
    reports = await reportRepository.readAllReports();
  }
}
