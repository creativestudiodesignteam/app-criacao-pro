import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import '../app/app_appbar_widget.dart';
import '../app/app_header_widget.dart';
import '../app/app_scaffold_widget.dart';
import '../app/app_text_widget.dart';
import 'report_controller.dart';
import 'report_list_widget.dart';

class ReportView extends StatefulWidget {
  @override
  _ReportViewState createState() => _ReportViewState();
}

class _ReportViewState extends ModularState<ReportView, ReportController> {
  Widget appBar(BuildContext context) {
    return AppAppBarWidget(
      titles: <Widget>[
        AppTextWidget(
          text: 'Relatórios',
          size: Theme.of(context).textTheme.subtitle2.fontSize,
        ),
      ],
      actions: <Widget>[
        IconButton(
          icon: Icon(
            Icons.chevron_left,
          ),
          onPressed: controller.onTapBack,
        ),
      ],
    );
  }

  Widget body(BuildContext context) {
    return Column(
      children: <Widget>[
        AppHeaderWidget(
          title: 'Escolha uma opcão',
          subtitle: 'para gerar um ',
          highlight: 'relatório',
        ),
        SizedBox(
          height: 20,
        ),
        Observer(
          builder: (_) {
            if (controller.reports == null) {
              return AppTextWidget(
                text: 'Buscando relatórios...',
              );
            } else if (controller.reports.isEmpty) {
              return AppTextWidget(
                text: 'Nenhum relatório encontrado.',
              );
            } else {
              return ReportListWidget(
                reports: controller.reports,
              );
            }
          },
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    controller.readAllReports();
    return AppScaffoldWidget(
      appBar: appBar(context),
      body: body(context),
    );
  }
}
