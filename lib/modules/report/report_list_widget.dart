import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'report_item_widget.dart';
import 'report_model.dart';

class ReportListWidget extends StatelessWidget {
  const ReportListWidget({
    Key key,
    this.reports,
  }) : super(key: key);

  final List<ReportModel> reports;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemCount: reports.length,
      itemBuilder: (_, i) {
        return ReportItemWidget(
          onPressed: () {
            if (reports.elementAt(i).id == 1) {
              Modular.to.pushNamed(
                '/report/bird',
                arguments: {
                  'bird': null,
                  'isReport': true,
                  'isSelect': false,
                },
              );
            }
            if (reports.elementAt(i).id == 2) {
              Modular.to.pushNamed(
                '/report/filter',
                arguments: {
                  'bird': null,
                  'isReport': true,
                  'isSelect': false,
                },
              );
            }
            if (reports.elementAt(i).id == 3) {
              Modular.to.pushNamed(
                '/report/cage',
                arguments: {
                  'bird': null,
                  'isReport': true,
                  'isSelect': false,
                  'isTwoOptions': true
                },
              );
            }
          },
          report: reports.elementAt(i),
        );
      },
    );
  }
}
