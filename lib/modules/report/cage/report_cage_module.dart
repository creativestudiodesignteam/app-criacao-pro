import 'package:flutter_modular/flutter_modular.dart';
import 'package:dio/dio.dart';
import '../../account/account_controller.dart';
import '../../cage/cage_controller.dart';
import '../../cage/cage_repository.dart';
import 'report_cage_controller.dart';
import 'report_cage_view.dart';

class ReportCageModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind(
          (i) => ReportCageController(
            cageController: i.get<CageController>(),
          ),
        ),
        Bind(
          (i) => CageController(
            cageRepository: i.get<CageRepository>(),
            accountController: i.get<AccountController>(),
          ),
        ),
        Bind(
          (i) => CageRepository(
            dio: i.get<Dio>(),
          ),
        ),
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(
          '/',
          child: (ctx, args) => ReportCageView(),
        ),
      ];
}
