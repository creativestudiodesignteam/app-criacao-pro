import 'package:criacao_pro/modules/cage/cage_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../app/app_appbar_widget.dart';
import '../../app/app_header_widget.dart';
import '../../app/app_scaffold_widget.dart';
import '../../app/app_text_widget.dart';
import '../../cage/cage_list_widget.dart';
import 'report_cage_controller.dart';

class ReportCageView extends StatefulWidget {
  @override
  _ReportCageViewState createState() => _ReportCageViewState();
}

class _ReportCageViewState
    extends ModularState<ReportCageView, ReportCageController> {
  ScrollController _sc;
  bool isLoading = false;

  void asyncInit() async {
    await controller.cageController.readAllCages('', false, true);
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    _sc = new ScrollController()..addListener(_scrollListener);
    asyncInit();
  }

  @override
  void dispose() {
    _sc.removeListener(_scrollListener);
    super.dispose();
  }

  void _scrollListener() {
    if (_sc.position.maxScrollExtent == _sc.position.pixels) {
      if (int.parse(controller.cageController.totalEggs) >
          controller.cageController.cages.length) {
        controller.cageController.readAllCages('', false, false);
      }
    }
  }

  Widget appBar(BuildContext context) {
    return AppAppBarWidget(
      titles: <Widget>[
        AppTextWidget(
          text: 'Buscar aves',
          size: Theme.of(context).textTheme.subtitle2.fontSize,
        ),
      ],
      actions: <Widget>[
        IconButton(
          icon: Icon(
            Icons.chevron_left,
          ),
          onPressed: controller.onTapBack,
        ),
      ],
    );
  }

  Widget body(BuildContext context) {
    return Column(
      children: <Widget>[
        Observer(
          builder: (_) {
            return AppHeaderWidget(
              title: 'Você possui',
              subtitle: 'um total de ',
              highlight: '${controller.cageController.totalEggs} gaiolas',
            );
          },
        ),
        SizedBox(
          height: 20,
        ),
        Observer(
          builder: (_) {
            if (controller.cageController.cages == null) {
              return Column(
                children: [
                  CircularProgressIndicator(),
                  AppTextWidget(
                    text: 'Buscando gaiolas.\nIsso pode demorar um pouco',
                    textAlign: TextAlign.center,
                  ),
                ],
              );
            } else if (controller.cageController.cages.isEmpty) {
              return AppTextWidget(
                text: 'Nenhuma gaiola encontrada.',
              );
            } else {
              print("controller.cageController.cages");
              print(controller.cageController.cages.length);
              return CageListWidget(
                  cages: controller.cageController.cages,
                  isReport: true,
                  function: null,
                  totalEggs: controller.cageController.totalEggs,
                  sc: _sc,
                  isTwoOptions: true);
            }
          },
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    controller.cageController.readAllCages('', false, true);
    return AppScaffoldWidget(
      appBar: appBar(context),
      body: body(context),
    );
  }
}
