import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import '../../cage/cage_controller.dart';
part 'report_cage_controller.g.dart';

class ReportCageController = _ReportCageController with _$ReportCageController;

abstract class _ReportCageController with Store {
  _ReportCageController({
    this.cageController,
  });

  final CageController cageController;

  Future<void> onTapBack() async {
    Modular.to.pop();
  }
}
