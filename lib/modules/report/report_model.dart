class ReportModel {
  ReportModel({
    this.id,
    this.image,
    this.title,
    this.subtitle,
  });

  final int id;
  final String image;
  final String title;
  final String subtitle;
}
