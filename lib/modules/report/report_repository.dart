import 'report_model.dart';

class ReportRepository {
  Future<List<ReportModel>> readAllReports() async {
    return [
      ReportModel(
        id: 1,
        image: 'lib/assets/report-1.png',
        title: 'Relatório da ave',
        subtitle: 'Extrai as informações da ave escolhida.',
      ),
      ReportModel(
        id: 2,
        image: 'lib/assets/report-2.png',
        title: 'Listagem das aves',
        subtitle: 'Lista as informações das aves de acordo com o filtro.',
      ),
      ReportModel(
        id: 3,
        image: 'lib/assets/report-3.png',
        title: 'Listagem de casais',
        subtitle: 'Lista as informações do casal na gaiola selecionada.',
      ),
    ];
  }
}
