import 'dart:convert';
import 'package:dio/dio.dart';
import '../../constants.dart' as constants;
import 'cage_model.dart';

class CageRepository {
  CageRepository({
    this.dio,
  });

  final Dio dio;

  final String path = '${constants.appUrl}/cage';
  final Options options = Options(
    headers: {
      'X-Token': constants.appSecret,
    },
  );

  Future<Map<String, dynamic>> createCage(CageModel cage) async {
    var formData = FormData.fromMap(cage.toJson());
    var response = await dio.post(
      '${path}/create',
      data: formData,
      options: options,
    );
    return json.decode(response.data);
  }

  Future<Map<String, dynamic>> readCage(int cageId) async {
    var formData = FormData.fromMap(
      {
        'cage': cageId,
      },
    );
    var response = await dio.post(
      '${path}/read',
      data: formData,
      options: options,
    );
    return json.decode(response.data);
  }

  Future<Map<String, dynamic>> readAllCages(
      int accountId, String lastId) async {
    var formData = FormData.fromMap(
      {'account': accountId, 'lastId': lastId},
    );
    var response = await dio.post(
      '${path}/read-all-by-account-pagination',
      data: formData,
      options: options,
    );
    return json.decode(response.data);
  }

  Future<Map<String, dynamic>> readAllCagesForSelect(int accountId) async {
    var formData = FormData.fromMap(
      {
        'account': accountId,
      },
    );
    var response = await dio.post(
      '${path}/read-all-by-account',
      data: formData,
      options: options,
    );
    return json.decode(response.data);
  }

  Future<Map<String, dynamic>> updateCage(CageModel cage) async {
    var formData = FormData.fromMap(cage.toJson());
    var response = await dio.post(
      '${path}/update',
      data: formData,
      options: options,
    );
    return json.decode(response.data);
  }

  Future<Map<String, dynamic>> deleteCage(int cageId) async {
    var formData = FormData.fromMap(
      {
        'id': cageId,
      },
    );
    var response = await dio.post(
      '${path}/delete',
      data: formData,
      options: options,
    );
    return json.decode(response.data);
  }
}
