// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cage_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$CageController on _CageController, Store {
  final _$cagesAtom = Atom(name: '_CageController.cages');

  @override
  List<CageModel> get cages {
    _$cagesAtom.reportRead();
    return super.cages;
  }

  @override
  set cages(List<CageModel> value) {
    _$cagesAtom.reportWrite(value, super.cages, () {
      super.cages = value;
    });
  }

  final _$totalEggsAtom = Atom(name: '_CageEggController.totalEggs');

  @override
  String get totalEggs {
    _$totalEggsAtom.reportRead();
    return super.totalEggs;
  }

  @override
  set totalEggs(String value) {
    _$totalEggsAtom.reportWrite(value, super.totalEggs, () {
      super.totalEggs = value;
    });
  }

  final _$lastNameAtom = Atom(name: '_CageEggController.lastName');

  @override
  String get lastName {
    _$lastNameAtom.reportRead();
    return super.lastName;
  }

  @override
  set lastName(String value) {
    _$lastNameAtom.reportWrite(value, super.lastName, () {
      super.lastName = value;
    });
  }

  final _$imageAtom = Atom(name: '_CageController.image');

  @override
  String get image {
    _$imageAtom.reportRead();
    return super.image;
  }

  @override
  set image(String value) {
    _$imageAtom.reportWrite(value, super.image, () {
      super.image = value;
    });
  }

  final _$onTapAddCageAsyncAction = AsyncAction('_CageController.onTapAddCage');

  @override
  Future<void> onTapAddCage() {
    return _$onTapAddCageAsyncAction.run(() => super.onTapAddCage());
  }

  final _$readAllCagesAsyncAction = AsyncAction('_CageController.readAllCages');

  @override
  Future<void> readAllCages(String filter, bool dropdown, bool reloadCages) {
    return _$readAllCagesAsyncAction
        .run(() => super.readAllCages(filter, dropdown, reloadCages));
  }

  final _$readAllCagesForSelectAsyncAction =
      AsyncAction('_CageController.readAllCagesForSelect');

  @override
  Future<void> readAllCagesForSelect(String filter) {
    return _$readAllCagesForSelectAsyncAction
        .run(() => super.readAllCagesForSelect(filter));
  }

  final _$readAllCagesListAsyncAction =
      AsyncAction('_CageController.readAllCagesList');

  @override
  Future<void> readAllCagesList(String filter) {
    return _$readAllCagesListAsyncAction
        .run(() => super.readAllCagesList(filter));
  }

  final _$onTapCleanFilterAsyncAction =
      AsyncAction('_BirdController.onTapCleanFilter');

  @override
  Future<void> onTapCleanFilter() {
    return _$onTapCleanFilterAsyncAction.run(() => super.onTapCleanFilter());
  }

  @override
  String toString() {
    return '''
cages: ${cages},
totalEggs: ${totalEggs},
cagesAll: ${cagesAll},
    ''';
  }
}
