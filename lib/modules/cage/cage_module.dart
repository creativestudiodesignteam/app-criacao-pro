import 'package:flutter_modular/flutter_modular.dart';
import 'package:dio/dio.dart';
import '../account/account_controller.dart';
import 'baby/cage_baby_module.dart';
import 'bird/cage_bird_module.dart';
import 'cage_controller.dart';
import 'cage_repository.dart';
import 'cage_view.dart';
import 'detail/cage_detail_module.dart';
import 'egg/cage_egg_module.dart';

class CageModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind(
          (i) => CageController(
            cageRepository: i.get<CageRepository>(),
            accountController: i.get<AccountController>(),
          ),
        ),
        Bind(
          (i) => CageRepository(
            dio: i.get<Dio>(),
          ),
        ),
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(
          '/',
          child: (ctx, args) => CageView(
            isSelect: args.data['isSelect'],
          ),
        ),
        ModularRouter(
          '/detail',
          module: CageDetailModule(),
        ),
        ModularRouter(
          '/bird',
          module: CageBirdModule(),
        ),
        ModularRouter(
          '/baby',
          module: CageBabyModule(),
        ),
        ModularRouter(
          '/egg',
          module: CageEggModule(),
        ),
      ];
}
