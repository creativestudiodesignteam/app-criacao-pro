import 'dart:convert';
import 'dart:developer';

import 'package:asuka/asuka.dart' as asuka;
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:mobx/mobx.dart';

import '../../constants.dart' as constants;
import '../account/account_controller.dart';
import '../app/app_message_widget.dart';
import 'cage_model.dart';
import 'cage_repository.dart';
part 'cage_controller.g.dart';

class CageController = _CageController with _$CageController;

abstract class _CageController with Store {
  _CageController({
    this.cageRepository,
    this.accountController,
  }) {}

  final CageRepository cageRepository;
  final AccountController accountController;
  Dio dio = Dio();
  final String path = '${constants.appUrl}/egg';
  final Options options = Options(
    headers: {
      'X-Token': constants.appSecret,
    },
  );

  @observable
  List<CageModel> cages;

  @observable
  List<CageModel> cagesAll;

  @observable
  List<CageModel> cagesAllList;

  @observable
  List<CageModel> cagesAllPagination;

  @observable
  String image;

  @observable
  String lastId = '0';

  @observable
  int pagination = 0;

  @observable
  String totalEggs = '0';

  @observable
  String lastName = '0';

  bool firstAcessInFunction = true;
  List<CageModel> cagesList;
  List<CageModel> tempCages;

  @action
  Future<void> onTapAddCage() async {
    var nameCage;
    List<CageModel> cagesListOrder = List<CageModel>();
    for (var itemCage in cagesAll) {
      if (!itemCage.name.contains(RegExp(r'[A-Z]', caseSensitive: false))) {
        cagesListOrder.add(itemCage);
      }
    }

    if (cagesListOrder.length == 0) {
      nameCage = '1';
    } else {
      for (var i = 0; i < (cagesListOrder.length + 1); i++) {
        if (cagesListOrder[0].name != '1') {
          nameCage = '1';
        } else if (lastName == cagesListOrder[i].name) {
          nameCage = (int.parse(lastName) + 1).toString();
          break;
        } else if ((int.parse(cagesListOrder[i + 1].name) -
                int.parse(cagesListOrder[i].name)) >
            1) {
          nameCage = (i + 2).toString();
          break;
        }
      }
    }
    var cage = CageModel(
        account: accountController.account.id, state: 'Ativa', name: nameCage);
    asuka.showSnackBar(
      SnackBar(
        content: AppMessageWidget(
          isLoading: true,
          message: 'Por favor, aguarde ...',
        ),
        duration: Duration(
          milliseconds: 800,
        ),
      ),
    );
    var response = await cageRepository.createCage(cage);
    readAllCagesList('');
    readAllCages('', false, true);
    asuka.showSnackBar(
      SnackBar(
        content: AppMessageWidget(
          message: response['message'],
        ),
        duration: Duration(
          milliseconds: 1000,
        ),
      ),
    );
    return response;
  }

  Future<Map<String, dynamic>> readAllEggs(int id) async {
    var formData = FormData.fromMap({
      'cage': id,
    });
    var response = await dio.post(
      '${path}/read-all-by-cage',
      data: formData,
      options: options,
    );
    return json.decode(response.data);
  }

  @action
  Future<void> readAllCages(
      String filter, bool dropdown, bool reloadCages) async {
    if (reloadCages ||
        cages == null ||
        int.parse(totalEggs) > (cages.length - 1) ||
        dropdown) {
      if (reloadCages) {
        lastId = '0';
        pagination = 0;
        cages = null;
        cagesList = List<CageModel>();
        cagesAllPagination = List<CageModel>();
      }
      if (firstAcessInFunction) {
        pagination = 0;
        cagesList = List<CageModel>();
        cagesAllPagination = List<CageModel>();
      }

      if (pagination > 0) {
        if (cagesAllList.length < pagination + 10) {
          if (cagesAllList.length < 10) {
            pagination = (cagesAllList.length - 1) - (pagination);
            for (var i = 0; i <= pagination; i++) {
              cagesAllPagination.add(cagesAllList[i]);
            }
          } else {
            int paginationFor = pagination;
            pagination =
                ((cagesAllList.length - 1) - (pagination)) + pagination;
            for (var i = paginationFor; i <= pagination; i++) {
              cagesAllPagination.add(cagesAllList[i]);
            }
          }
        } else {
          pagination += 10;
          for (var i = (pagination - 10); i < pagination; i++) {
            cagesAllPagination.add(cagesAllList[i]);
          }
        }
        if (cagesAllList.length < 10) {
        } else {}

        cages = cagesAllPagination;
        pagination += 5;
      } else {
        var response = await cageRepository.readAllCages(
            accountController.account.id, lastId);
        var data = json.decode(response['data']);
        lastName = response['lastName'] == '' ? '0' : response['lastName'];
        if (firstAcessInFunction && filter == '') {
          totalEggs = response['total'] == '' ? '0' : response['total'];
          firstAcessInFunction = false;
        }

        if (filter == '' || filter == 'Total') {
          totalEggs = response['total'] == '' ? '0' : response['total'];
        }

        if (filter == 'Ativas') {
          totalEggs =
              response['totalActive'] == '' ? '0' : response['totalActive'];
        }

        if (filter == 'Inativas') {
          totalEggs =
              response['totalInactive'] == '' ? '0' : response['totalInactive'];
        }

        List<dynamic> list = data;
        var filteredList = [];
        if (filter == 'Ativas') {
          for (var i in list) {
            if (i['state'] == 'Ativa') {
              filteredList.add(i);
            }
          }
        } else if (filter == 'Inativas') {
          for (var i in list) {
            if (i['state'] == 'Inativa') {
              filteredList.add(i);
            }
          }
        } else {
          filteredList = list;
        }
        filteredList.sort((a, b) {
          if (double.tryParse(a['name']) == null &&
              double.tryParse(b['name']) == null) {
            return a['name'].toLowerCase().compareTo(b['name'].toLowerCase());
          } else if (double.tryParse(a['name']) != null &&
              double.tryParse(b['name']) != null) {
            return double.parse(a['name']).compareTo(double.parse(b['name']));
          } else {
            return a['name'].toLowerCase().compareTo(b['name'].toLowerCase());
          }
        });
        tempCages = List<CageModel>.from(filteredList.map((e) {
          return CageModel.fromJson(e);
        }).toList());
        for (var i = 0; i <= tempCages.length - 1; i++) {
          var fertilize = false;
          var tempImage = '';
          var t = await readAllEggs(tempCages[i].id);
          var s = t['data'];
          if (s.toString().contains('Fertilizar')) {
            fertilize = true;
          }
          if (tempCages[i].babies > 0) {
            tempImage = 'baby';
          } else if (tempCages[i].eggs > 0) {
            if (fertilize == true) {
              tempImage = 'hatch';
            } else {
              tempImage = 'egg';
            }
          } else if (tempCages[i].birds > 0) {
            tempImage = 'bird';
          } else {
            tempImage = 'empty';
          }
          tempCages[i].image = tempImage;
        }

        await tempCages.forEach((element) async {
          var addCage = true;
          if (element.id > int.parse(lastId)) {
            if (element.name != '') {
              if (!element.name
                  .contains(RegExp(r'[A-Z]', caseSensitive: false))) {
                lastId = element.name;
              }
            }
          }
          cagesList.forEach((elementCage) {
            if (element.id == elementCage.id) {
              addCage = false;
            }
          });
          if (addCage == true) {
            await cagesList.add(element);
          }
        });

        if (dropdown) {
          cages = tempCages;
        } else {
          List<CageModel> cagesListOrder = List<CageModel>();
          var cagesNumber = [];
          var cagesString = [];
          for (var itemCage in cagesList) {
            if (itemCage.name
                .contains(RegExp(r'[A-Z]', caseSensitive: false))) {
              cagesString.add(itemCage);
            } else {
              cagesNumber.add(itemCage);
            }
          }

          for (var itemCage in cagesString) {
            cagesListOrder.add(itemCage);
          }
          for (var itemCage in cagesNumber) {
            cagesListOrder.add(itemCage);
          }
          cagesAllList = cagesListOrder;
          if (cagesAllList.length < pagination + 10) {
            if (cagesAllList.length < 10) {
              pagination = (cagesAllList.length - 1) - (pagination);
              for (var i = 0; i <= pagination; i++) {
                cagesAllPagination.add(cagesAllList[i]);
              }
            } else {
              int paginationFor = pagination;
              pagination =
                  ((cagesAllList.length - 1) - (pagination)) + pagination;
              for (var i = paginationFor; i <= pagination; i++) {
                cagesAllPagination.add(cagesAllList[i]);
              }
            }
          } else {
            pagination += 10;
            for (var i = (pagination - 10); i < pagination; i++) {
              cagesAllPagination.add(cagesAllList[i]);
            }
          }

          cages = cagesAllPagination;
        }
      }
    }
  }

  @action
  Future<void> onTapCleanFilter() async {
    await readAllCages('', false, true);
  }

  @action
  Future<void> readAllCagesForSelect(String filter) async {
    cages = null;
    var response = await cageRepository
        .readAllCagesForSelect(accountController.account.id);
    var data = json.decode(response['data']);
    List<dynamic> list = data;
    var filteredList = [];
    if (filter == 'Ativas') {
      for (var i in list) {
        if (i['state'] == 'Ativa') {
          filteredList.add(i);
        }
      }
    } else if (filter == 'Inativas') {
      for (var i in list) {
        if (i['state'] == 'Inativa') {
          filteredList.add(i);
        }
      }
    } else {
      filteredList = list;
    }
    filteredList.sort((a, b) {
      if (double.tryParse(a['name']) == null &&
          double.tryParse(b['name']) == null) {
        return a['name'].toLowerCase().compareTo(b['name'].toLowerCase());
      } else if (double.tryParse(a['name']) != null &&
          double.tryParse(b['name']) != null) {
        return double.parse(a['name']).compareTo(double.parse(b['name']));
      } else {
        return a['name'].toLowerCase().compareTo(b['name'].toLowerCase());
      }
    });
    var tempCages = List<CageModel>.from(filteredList.map((e) {
      return CageModel.fromJson(e);
    }).toList());
    for (var i = 0; i <= tempCages.length - 1; i++) {
      var fertilize = false;
      var tempImage = '';
      var t = await readAllEggs(tempCages[i].id);
      var s = t['data'];
      if (s.toString().contains('Fertilizar')) {
        fertilize = true;
      }
      if (tempCages[i].eggs > 0) {
        if (fertilize == true) {
          tempImage = 'hatch';
        } else {
          tempImage = 'egg';
        }
      } else if (tempCages[i].babies > 0) {
        tempImage = 'baby';
      } else if (tempCages[i].birds > 0) {
        tempImage = 'bird';
      } else {
        tempImage = 'empty';
      }
      tempCages[i].image = tempImage;
    }
    totalEggs = tempCages.length.toString();
    cages = tempCages;
  }

  @action
  Future<void> readAllCagesList(String filter) async {
    var response = await cageRepository
        .readAllCagesForSelect(accountController.account.id);
    var data = json.decode(response['data']);
    var tempCages = List<CageModel>.from(data.map((e) {
      return CageModel.fromJson(e);
    }).toList());
    cagesAll = tempCages;
  }
}
