class CageModel {
  CageModel({
    this.id,
    this.account,
    this.state,
    this.name,
    this.birds,
    this.babies,
    this.eggs,
    this.image,
  });

  int id;
  int account;
  String state;
  String name;
  int birds;
  int babies;
  int eggs;
  String image;

  CageModel.fromJson(Map<String, dynamic> json) {
    id = int.parse(json['id']);
    account = int.parse(json['account']);
    state = json['state'];
    name = json['name'];
    birds = int.parse(json['birds']);
    babies = int.parse(json['babies']);
    eggs = int.parse(json['eggs']);
    image = json['image'];
  }

  Map<String, dynamic> toJson() {
    var data = {
      'id': id,
      'account': account,
      'state': state,
      'name': name,
      'birds': birds,
      'babies': babies,
      'eggs': eggs,
      'image': image,
    };
    return data;
  }
}
