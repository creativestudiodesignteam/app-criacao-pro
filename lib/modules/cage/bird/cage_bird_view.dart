import 'package:criacao_pro/modules/app/app_message_widget.dart';
import 'package:criacao_pro/report_pdf_cages.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:asuka/asuka.dart' as asuka;

import '../../app/app_action_button_widget.dart';
import '../../app/app_appbar_widget.dart';
import '../../app/app_header_widget.dart';
import '../../app/app_scaffold_widget_multi.dart';
import '../../app/app_sub_menu_widget.dart';
import '../../app/app_text_widget.dart';
import '../../bird/bird_list_widget.dart';
import '../cage_model.dart';
import 'cage_bird_controller.dart';

class CageBirdView extends StatefulWidget {
  const CageBirdView({
    Key key,
    this.cage,
    this.isReport = false,
    this.isSelect = true,
    this.isTwoOptions = false,
  }) : super(key: key);

  final CageModel cage;
  final bool isReport;
  final bool isSelect;
  final bool isTwoOptions;

  @override
  _CageBirdViewState createState() => _CageBirdViewState();
}

class _CageBirdViewState
    extends ModularState<CageBirdView, CageBirdController> {
  ScrollController _sc;
  bool isLoading = true;

  @override
  void initState() {
    super.initState();
    if (widget.isTwoOptions == true) {
      controller.isTransfer = widget.isTwoOptions;
    }
    _sc = new ScrollController()..addListener(_scrollListener);
  }

  @override
  void dispose() {
    _sc.removeListener(_scrollListener);
    super.dispose();
  }

  void _scrollListener() {}

  Widget appBar(BuildContext context) {
    return AppAppBarWidget(
      titles: <Widget>[
        AppTextWidget(
          text: 'Aves na gaiola',
          size: Theme.of(context).textTheme.subtitle2.fontSize,
        ),
      ],
      actions: <Widget>[
        IconButton(
          icon: Icon(
            Icons.chevron_left,
          ),
          onPressed: () => Navigator.pop(context),
        ),
      ],
    );
  }

  Widget subMenu() {
    if (!widget.isReport) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          AppSubMenuWidget(
            image: 'lib/assets/cage-sub-1.png',
            text: 'Detalhes',
            selected: false,
            onPressed: () {
              Navigator.pop(context);
              Navigator.pushNamed(
                context,
                '/cage/detail',
                arguments: {'cage': widget.cage},
              );
            },
          ),
          AppSubMenuWidget(
            image: 'lib/assets/cage-sub-2.png',
            text: 'Aves',
            selected: true,
          ),
          AppSubMenuWidget(
            image: 'lib/assets/cage-sub-4.png',
            text: 'Ovos',
            selected: false,
            onPressed: () {
              Navigator.pop(context);
              Navigator.pushNamed(
                context,
                '/cage/egg',
                arguments: {'cage': widget.cage},
              );
            },
          ),
          AppSubMenuWidget(
            image: 'lib/assets/cage-sub-3.png',
            text: 'Filhotes',
            selected: false,
            onPressed: () {
              Navigator.pop(context);
              Navigator.pushNamed(
                context,
                '/cage/baby',
                arguments: {'cage': widget.cage},
              );
            },
          ),
        ],
      );
    } else {
      return SizedBox();
    }
  }

  Widget body(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        subMenu(),
        SizedBox(
          height: widget.isReport ? 0 : 20,
        ),
        Observer(
          builder: (_) {
            var isEmpty = controller.birds == null;
            return AppHeaderWidget(
              title: 'Você tem um total',
              subtitle: 'de ',
              highlight:
                  '${isEmpty ? 0 : controller.birds.length} aves na gaiola',
            );
          },
        ),
        SizedBox(
          height: 20,
        ),
        Observer(
          builder: (_) {
            return Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                RaisedButton.icon(
                  elevation: 0,
                  textColor: Colors.white,
                  color: Theme.of(context).primaryColor,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8),
                  ),
                  icon: Icon(
                    widget.isTwoOptions == true
                        ? Icons.description
                        : controller.isTransfer
                            ? Icons.done
                            : Icons.edit,
                  ),
                  onPressed: () async {
                    if (widget.isTwoOptions == true) {
                      if (controller.selectedBirds.length == 2) {
                        asuka.showSnackBar(
                          SnackBar(
                            content: AppMessageWidget(
                              message: 'Aguarde enquanto o relatório é gerado!',
                            ),
                            duration: Duration(
                              milliseconds: 2000,
                            ),
                          ),
                        );
                        return reportViewCages(
                            context, controller.selectedBirds);
                      } else {
                        return asuka.showSnackBar(
                          SnackBar(
                            content: AppMessageWidget(
                              message: 'Selecione duas aves!',
                            ),
                            duration: Duration(
                              milliseconds: 2000,
                            ),
                          ),
                        );
                      }
                    } else {
                      if (controller.isTransfer) {
                        await controller.onPressedTransferDone(context);
                      } else {
                        await controller.onPressedTransfer();
                      }
                    }
                    await controller.readBirds(widget.cage.id);
                  },
                  label: AppTextWidget(
                    text: widget.isTwoOptions == true
                        ? 'Relatório Casal'
                        : controller.isTransfer
                            ? 'Mover aves'
                            : 'Selecionar aves',
                  ),
                ),
              ],
            );
          },
        ),
        SizedBox(
          height: 20,
        ),
        Observer(
          builder: (_) {
            if (controller.birds == null) {
              return AppTextWidget(
                text: 'Buscando aves...',
              );
            } else if (controller.birds.isEmpty) {
              return AppTextWidget(
                text: 'Nenhuma ave encontrada.',
              );
            } else {
              return BirdListWidget(
                birds: controller.birds,
                isReport: widget.isTwoOptions == true ? true : false,
                isTwoOptions: widget.isTwoOptions,
                isSelect: false,
                isTransfer: controller.isTransfer,
                onChangedCheckbox: controller.onChangedCheckbox,
                totalBirds: controller.totalBirds,
                totalSelectBirds: controller.totalSelectBirds,
                selectGender: controller.selectGender,
                sc: _sc,
              );
            }
          },
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    print("widget.isTwoOptions");
    print(widget.isTwoOptions);
    controller.readBirds(widget.cage.id);
    return AppScaffoldWidgetMulti(
      appBar: appBar(context),
      body: body(context),
      sc: _sc,
      floatingActionButton: Observer(
        builder: (_) {
          return widget.isReport || controller.isTransfer
              ? SizedBox(
                  width: MediaQuery.of(context).size.width * 0.58,
                )
              : AppActionAppButtonWidget(
                  label: 'Adicionar ave',
                  icon: Icons.add,
                  color: Colors.green,
                  onPressed: () async {
                    var bird = await Modular.to.pushNamed(
                      '/bird',
                      arguments: {
                        'isReport': false,
                        'isSelect': true,
                      },
                    );
                    if (bird != null) {
                      controller.onTapAddBird(widget.cage, bird);
                    }
                  },
                );
        },
      ),
    );
  }
}
