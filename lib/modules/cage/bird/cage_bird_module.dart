import 'package:dio/dio.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../account/account_controller.dart';
import '../../bird/bird_controller.dart';
import '../../bird/bird_repository.dart';
import '../../bird/create/bird_create_controller.dart';
import '../baby/cage_baby_controller.dart';
import '../baby/cage_baby_repository.dart';
import '../cage_controller.dart';
import '../cage_repository.dart';
import 'cage_bird_controller.dart';
import 'cage_bird_view.dart';

class CageBirdModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind(
          (i) => CageBirdController(
            birdController: i.get<BirdController>(),
            birdRepository: i.get<BirdRepository>(),
            cageController: i.get<CageController>(),
          ),
        ),
        Bind(
          (i) => BirdController(
            birdRepository: i.get<BirdRepository>(),
            accountController: i.get<AccountController>(),
            birdCreateController: i.get<BirdCreateController>(),
          ),
        ),
        Bind(
          (i) => BirdCreateController(
            birdRepository: i.get<BirdRepository>(),
            cageBabyController: i.get<CageBabyController>(),
            cageController: i.get<CageController>(),
            accountController: i.get<AccountController>(),
          ),
        ),
        Bind(
          (i) => CageBabyController(
            cageBabyRepository: i.get<CageBabyRepository>(),
          ),
        ),
        Bind(
          (i) => CageController(
            cageRepository: i.get<CageRepository>(),
            accountController: i.get<AccountController>(),
          ),
        ),
        Bind(
          (i) => BirdRepository(
            dio: i.get<Dio>(),
          ),
        ),
        Bind(
          (i) => CageBabyRepository(
            dio: i.get<Dio>(),
          ),
        ),
        Bind(
          (i) => CageRepository(
            dio: i.get<Dio>(),
          ),
        ),
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(
          '/',
          child: (ctx, args) => CageBirdView(
            cage: args.data['cage'],
            isReport: args.data['isReport'],
            isTwoOptions: args.data['isTwoOptions'],
            isSelect: true,
          ),
        ),
      ];
}
