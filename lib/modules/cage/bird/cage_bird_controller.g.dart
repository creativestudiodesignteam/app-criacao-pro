// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cage_bird_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$CageBirdController on _CageBirdController, Store {
  final _$isTransferAtom = Atom(name: '_CageBirdController.isTransfer');

  @override
  bool get isTransfer {
    _$isTransferAtom.reportRead();
    return super.isTransfer;
  }

  @override
  set isTransfer(bool value) {
    _$isTransferAtom.reportWrite(value, super.isTransfer, () {
      super.isTransfer = value;
    });
  }

  final _$selectedBirdsAtom = Atom(name: '_CageBirdController.selectedBirds');

  @override
  List<BirdModel> get selectedBirds {
    _$selectedBirdsAtom.reportRead();
    return super.selectedBirds;
  }

  @override
  set selectedBirds(List<BirdModel> value) {
    _$selectedBirdsAtom.reportWrite(value, super.selectedBirds, () {
      super.selectedBirds = value;
    });
  }

  final _$birdsAtom = Atom(name: '_CageBirdController.birds');

  @override
  List<BirdModel> get birds {
    _$birdsAtom.reportRead();
    return super.birds;
  }

  @override
  set birds(List<BirdModel> value) {
    _$birdsAtom.reportWrite(value, super.birds, () {
      super.birds = value;
    });
  }

  final _$onPressedTransferAsyncAction =
      AsyncAction('_CageBirdController.onPressedTransfer');

  @override
  Future<void> onPressedTransfer() {
    return _$onPressedTransferAsyncAction.run(() => super.onPressedTransfer());
  }

  final _$onPressedTransferDoneAsyncAction =
      AsyncAction('_CageBirdController.onPressedTransferDone');

  @override
  Future<void> onPressedTransferDone(BuildContext context) {
    return _$onPressedTransferDoneAsyncAction
        .run(() => super.onPressedTransferDone(context));
  }

  final _$onChangedCheckboxAsyncAction =
      AsyncAction('_BirdController.onChangedCheckbox');

  @override
  Future<void> onChangedCheckbox(BirdModel bird, bool isTwoOptions) {
    return _$onChangedCheckboxAsyncAction
        .run(() => super.onChangedCheckbox(bird, isTwoOptions));
  }

  final _$readBirdsAsyncAction = AsyncAction('_CageBirdController.readBirds');

  @override
  Future<void> readBirds(int cageId) {
    return _$readBirdsAsyncAction.run(() => super.readBirds(cageId));
  }

  final _$onTapAddBirdAsyncAction =
      AsyncAction('_CageBirdController.onTapAddBird');

  @override
  Future<void> onTapAddBird(CageModel cage, BirdModel bird) {
    return _$onTapAddBirdAsyncAction.run(() => super.onTapAddBird(cage, bird));
  }

  final _$totalBirdsAtom = Atom(name: '_CageEggController.totalBirds');

  @override
  String get totalBirds {
    _$totalBirdsAtom.reportRead();
    return super.totalBirds;
  }

  @override
  set totalBirds(String value) {
    _$totalBirdsAtom.reportWrite(value, super.totalBirds, () {
      super.totalBirds = value;
    });
  }

  final _$totalSelectBirdsAtom =
      Atom(name: '_CageEggController.totalSelectBirds');

  @override
  int get totalSelectBirds {
    _$totalSelectBirdsAtom.reportRead();
    return super.totalSelectBirds;
  }

  @override
  set totalSelectBirds(int value) {
    _$totalSelectBirdsAtom.reportWrite(value, super.totalSelectBirds, () {
      super.totalSelectBirds = value;
    });
  }

  final _$selectGenderAtom = Atom(name: '_CageEggController.selectGender');

  @override
  String get selectGender {
    _$selectGenderAtom.reportRead();
    return super.selectGender;
  }

  @override
  set selectGender(String value) {
    _$totalSelectBirdsAtom.reportWrite(value, super.selectGender, () {
      super.selectGender = value;
    });
  }

  @override
  String toString() {
    return '''
birds: ${birds},
totalBirds: ${totalBirds}
totalSelectBirds: ${totalSelectBirds}
selectGender: ${selectGender}
    ''';
  }
}
