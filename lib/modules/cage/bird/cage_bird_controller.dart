import 'package:asuka/asuka.dart' as asuka;
import 'package:criacao_pro/modules/cage/cage_controller.dart';
import 'package:flutter/material.dart';
import 'package:mobx/mobx.dart';

import '../../app/app_message_widget.dart';
import '../../bird/bird_controller.dart';
import '../../bird/bird_model.dart';
import '../../bird/bird_repository.dart';
import '../cage_model.dart';

part 'cage_bird_controller.g.dart';

class CageBirdController = _CageBirdController with _$CageBirdController;

abstract class _CageBirdController with Store {
  _CageBirdController({
    this.birdController,
    this.birdRepository,
    this.cageController,
  });

  final BirdController birdController;
  final BirdRepository birdRepository;
  final CageController cageController;

  @observable
  bool isTransfer = false;

  @observable
  List<BirdModel> selectedBirds = [];

  @observable
  List<BirdModel> birds;

  @observable
  String totalBirds = '0';

  @observable
  int totalSelectBirds = 0;

  @observable
  String selectGender = '';

  @action
  Future<void> onPressedTransfer() async {
    isTransfer = !isTransfer;
    selectedBirds = [];
  }

  @action
  Future<void> onPressedTransferDone(BuildContext context) async {
    isTransfer = !isTransfer;
    if (selectedBirds.isEmpty) {
      asuka.showSnackBar(
        SnackBar(
          content: AppMessageWidget(
            message: 'Você não selecionou nenhuma ave.',
          ),
          duration: Duration(
            milliseconds: 800,
          ),
        ),
      );
      return;
    }
    var response = await Navigator.pushNamed(
      context,
      '/cage',
      arguments: {
        'isSelect': true,
      },
    );
    CageModel cage = response;
    if (cage != null) {
      for (var bird in selectedBirds) {
        bird.cage = cage.id;
        await birdRepository.updateBird(bird);
      }
      selectedBirds = [];
      asuka.showSnackBar(
        SnackBar(
          content: AppMessageWidget(
            message: 'As aves foram movidas com sucesso!',
          ),
          duration: Duration(
            milliseconds: 800,
          ),
        ),
      );
      // asuka.showSnackBar(
      //   SnackBar(
      //     content: AppMessageWidget(
      //       message:
      //           'Agora você está na gaiola que \n acabou de mover as aves!',
      //       sizeHeight: 80,
      //     ),
      //     duration: Duration(
      //       milliseconds: 2000,
      //     ),
      //   ),
      // );
      // showDialog(
      //   context: context,
      //   child: AlertDialog(
      //     content:
      //         Text("Você foi direcionado para a gaiola que moveu as aves!"),
      //   ),
      // );
    }
  }

  @action
  Future<void> onChangedCheckbox(BirdModel bird, bool isTwoOptions) async {
    if (isTwoOptions == null) {
      isTwoOptions = false;
    }
    if (isTwoOptions) {
      if (selectedBirds.length <= 2) {
        if (!selectedBirds.contains(bird)) {
          selectedBirds.add(bird);
        } else {
          selectedBirds.remove(bird);
        }
      }
    } else {
      if (!selectedBirds.contains(bird)) {
        selectedBirds.add(bird);
      } else {
        selectedBirds.remove(bird);
      }
    }
    if (selectedBirds.length == 1) {
      selectGender = selectedBirds[0].gender;
    } else if (selectedBirds.length == 0) {
      selectGender = '';
    }
    totalSelectBirds = selectedBirds.length;
  }

  @action
  Future<void> readBirds(int cageId) async {
    await birdController.readAllBirdsCage(null, null);
    birds = await birdController.birds.where((e) => e.cage == cageId).toList();
    totalBirds = birds.length.toString();
  }

  @action
  Future<void> onTapAddBird(CageModel cage, BirdModel bird) async {
    if (cage.id == bird.cage) {
      return;
    }
    asuka.showSnackBar(
      SnackBar(
        content: AppMessageWidget(
          isLoading: true,
          message: 'Por favor, aguarde ...',
        ),
        duration: Duration(
          milliseconds: 800,
        ),
      ),
    );
    bird.cage = cage.id;
    if (bird.mother == 0) {
      bird.mother = -1;
    }
    if (bird.father == 0) {
      bird.father = -1;
    }
    var response = await birdController.birdRepository.updateBird(bird);
    readBirds(cage.id);
    asuka.showSnackBar(
      SnackBar(
        content: AppMessageWidget(
          message: response['message'],
        ),
        duration: Duration(
          milliseconds: 1000,
        ),
      ),
    );
  }
}
