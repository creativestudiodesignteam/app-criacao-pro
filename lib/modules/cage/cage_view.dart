import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../account/account_controller.dart';
import '../app/app_action_button_widget.dart';
import '../app/app_appbar_widget.dart';
import '../app/app_button_widget.dart';
import '../app/app_dropdown_widget.dart';
import '../app/app_header_widget.dart';
import '../app/app_scaffold_widget_multi.dart';
import '../app/app_text_widget.dart';
import 'cage_controller.dart';
import 'cage_list_widget.dart';

class CageView extends StatefulWidget {
  const CageView({
    Key key,
    this.isSelect = false,
  }) : super(key: key);

  final bool isSelect;

  @override
  _CageViewState createState() => _CageViewState();
}

class _CageViewState extends ModularState<CageView, CageController> {
  AccountController accountController = AccountController();
  SharedPreferences prefs;
  String cageFilter;
  String tempCageFilter;
  ScrollController _sc;
  bool isLoading = false;

  void asyncInit() async {
    // if (!controller.firstAcessInFunction) {
    //   controller.firstAcessInFunction = null;
    //   controller.cages = null;
    // }
    await controller.readAllCagesList('');
    await controller.readAllCages('', false, true);
    prefs = await SharedPreferences.getInstance();
    // setState(() {});
  }

  @override
  void initState() {
    super.initState();
    _sc = new ScrollController()..addListener(_scrollListener);
    tempCageFilter = null;
    cageFilter = null;
    asyncInit();
  }

  @override
  void dispose() {
    _sc.removeListener(_scrollListener);
    super.dispose();
  }

  void _scrollListener() {
    if (_sc.position.maxScrollExtent == _sc.position.pixels) {
      if (int.parse(controller.totalEggs) > controller.cages.length) {
        controller.readAllCages('', false, false);
      }
    }
  }

  Widget appBar(BuildContext context) {
    return AppAppBarWidget(
      titles: <Widget>[
        AppTextWidget(
          text: widget.isSelect ? 'Selecione uma gaiola' : 'Gaiolas',
          size: Theme.of(context).textTheme.subtitle2.fontSize,
        ),
      ],
      actions: <Widget>[
        Observer(builder: (_) {
          return controller.cages != null
              ? IconButton(
                  icon: Icon(
                    Icons.sort,
                  ),
                  onPressed: () {
                    showModalBottomSheet(
                      context: context,
                      builder: (_) {
                        cageFilter = prefs.getString('cageFilter');
                        tempCageFilter = cageFilter;
                        return SingleChildScrollView(
                          padding: EdgeInsets.all(30),
                          child: Container(
                            decoration: BoxDecoration(),
                            child: Column(
                              children: <Widget>[
                                AppTextWidget(
                                  size: 15,
                                  text: 'Filtros',
                                  weight: FontWeight.bold,
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                AppDropdownWidget(
                                  label: 'Status',
                                  value:
                                      cageFilter != null ? cageFilter : 'Todas',
                                  items: ['Todas', 'Ativas', 'Inativas'],
                                  onChanged: (value) {
                                    tempCageFilter = value;
                                  },
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: <Widget>[
                                    AppButtonWidget(
                                      text: 'Aplicar filtro',
                                      color: Theme.of(context).primaryColor,
                                      onPressed: () {
                                        prefs.setString(
                                            'cageFilter', tempCageFilter);
                                        controller.readAllCagesForSelect(
                                            tempCageFilter);
                                        Navigator.pop(context);
                                      },
                                    ),
                                    InkWell(
                                      child: Text(
                                        'Limpar Filtro',
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      onTap: () {
                                        controller.onTapCleanFilter();
                                        setState(() {
                                          cageFilter = null;
                                        });
                                      },
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    );
                  },
                )
              : Container();
        }),
        IconButton(
          icon: Icon(
            Icons.chevron_left,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ],
    );
  }

  Widget body(BuildContext context) {
    return Column(
      children: <Widget>[
        Observer(builder: (_) {
          if (controller.cages == null) {
            return AppHeaderWidget(
              title: 'Por favor',
              subtitle: 'aguarde ',
              highlight: ' ... ',
            );
          } else {
            return AppHeaderWidget(
              title: 'Você possui',
              subtitle: 'um total de ',
              highlight: '${controller.totalEggs} gaiolas',
            );
          }
        }),
        SizedBox(
          height: 20,
        ),
        Observer(
          builder: (_) {
            if (controller.cages == null) {
              return Column(
                children: [
                  CircularProgressIndicator(),
                  SizedBox(
                    height: 20,
                  ),
                  AppTextWidget(
                    text: 'Buscando gaiolas.\nIsso pode demorar um pouco',
                    textAlign: TextAlign.center,
                  ),
                ],
              );
            } else if (controller.cages.isEmpty) {
              return AppTextWidget(
                text: 'Nenhuma gaiola encontrada.',
              );
            } else {
              return CageListWidget(
                cages: controller.cages,
                isReport: false,
                isSelect: widget.isSelect,
                function: asyncInit,
                readAllCages: controller.readAllCages,
                totalEggs: controller.totalEggs,
                sc: _sc,
                firstAcessInFunction: controller.firstAcessInFunction,
              );
            }
          },
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    // Observer(
    //   builder: (_) {
    //     controller.firstAcessInFunction
    //         ? controller.readAllCages('', false, true)
    //         : null;
    //     return Container();
    //   },
    // );
    return AppScaffoldWidgetMulti(
      appBar: appBar(context),
      body: body(context),
      sc: _sc,
      floatingActionButton: AppActionAppButtonWidget(
        label: 'Adicionar gaiola',
        icon: Icons.add,
        color: Colors.green,
        onPressed: controller.onTapAddCage,
      ),
    );
  }
}
