// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cage_baby_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$CageBabyController on _CageBabyController, Store {
  final _$babiesAtom = Atom(name: '_CageBabyController.babies');

  @override
  List<CageBabyModel> get babies {
    _$babiesAtom.reportRead();
    return super.babies;
  }

  @override
  set babies(List<CageBabyModel> value) {
    _$babiesAtom.reportWrite(value, super.babies, () {
      super.babies = value;
    });
  }

  final _$selectedBabiesAtom = Atom(name: '_CageBabyController.selectedBabies');

  @override
  List<CageBabyModel> get selectedBabies {
    _$selectedBabiesAtom.reportRead();
    return super.selectedBabies;
  }

  @override
  set selectedBabies(List<CageBabyModel> value) {
    _$selectedBabiesAtom.reportWrite(value, super.selectedBabies, () {
      super.selectedBabies = value;
    });
  }

  final _$isTransferAtom = Atom(name: '_CageBabyController.isTransfer');

  @override
  bool get isTransfer {
    _$isTransferAtom.reportRead();
    return super.isTransfer;
  }

  @override
  set isTransfer(bool value) {
    _$isTransferAtom.reportWrite(value, super.isTransfer, () {
      super.isTransfer = value;
    });
  }

  final _$onChangedCheckboxAsyncAction =
      AsyncAction('_CageBabyController.onChangedCheckbox');

  @override
  Future<void> onChangedCheckbox(CageBabyModel baby) {
    return _$onChangedCheckboxAsyncAction
        .run(() => super.onChangedCheckbox(baby));
  }

  final _$onPressedTransferAsyncAction =
      AsyncAction('_CageBabyController.onPressedTransfer');

  @override
  Future<void> onPressedTransfer() {
    return _$onPressedTransferAsyncAction.run(() => super.onPressedTransfer());
  }

  final _$onPressedTransferDoneAsyncAction =
      AsyncAction('_CageBabyController.onPressedTransferDone');

  @override
  Future<void> onPressedTransferDone(BuildContext context) {
    return _$onPressedTransferDoneAsyncAction
        .run(() => super.onPressedTransferDone(context));
  }

  final _$onTapDeleteBabyAsyncAction =
      AsyncAction('_CageBabyController.onTapDeleteBaby');

  @override
  Future<void> onTapDeleteBaby(int cageId, int babyId) {
    return _$onTapDeleteBabyAsyncAction
        .run(() => super.onTapDeleteBaby(cageId, babyId));
  }

  final _$readAllBabiesAsyncAction =
      AsyncAction('_CageBabyController.readAllBabies');

  @override
  Future<void> readAllBabies(int cageId) {
    return _$readAllBabiesAsyncAction.run(() => super.readAllBabies(cageId));
  }

  @override
  String toString() {
    return '''
babies: ${babies},
selectedBabies: ${selectedBabies},
isTransfer: ${isTransfer}
    ''';
  }
}
