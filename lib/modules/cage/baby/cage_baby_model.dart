class CageBabyModel {
  CageBabyModel({
    this.id,
    this.account,
    this.birthDate,
    this.ringDate,
    this.separateDate,
    this.cage,
    this.mother,
    this.father,
    this.ringRight,
    this.ringLeft,
  });

  int id;
  int account;
  int birthDate;
  int ringDate;
  int separateDate;
  int cage;
  int mother;
  int father;
  String ringRight;
  String ringLeft;

  CageBabyModel.fromJson(Map<String, dynamic> json) {
    id = int.parse(json['id']);
    account = int.parse(json['account']);
    birthDate = int.parse(json['birth_date']);
    ringDate = int.parse(json['ring_date']);
    separateDate = int.parse(json['separate_date']);
    cage = int.parse(json['cage']);
    mother = int.parse(json['mother']);
    father = int.parse(json['father']);
    ringRight = json['right_ring'];
    ringLeft = json['left_ring'];
  }

  Map<String, dynamic> toJson() {
    var data = {
      'id': id,
      'account': account,
      'birthDate': birthDate,
      'ringDate': ringDate,
      'separateDate': separateDate,
      'cage': cage,
      'mother': mother,
      'father': father,
      'ringRight': ringRight,
      'ringLeft': ringLeft,
    };
    return data;
  }
}
