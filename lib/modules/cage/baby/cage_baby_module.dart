import 'package:criacao_pro/modules/cage/baby/ring/cage_baby_ring_module.dart';
import 'package:dio/dio.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'cage_baby_controller.dart';
import 'cage_baby_repository.dart';
import 'cage_baby_view.dart';

class CageBabyModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind(
          (i) => CageBabyController(
            cageBabyRepository: i.get<CageBabyRepository>(),
          ),
        ),
        Bind(
          (i) => CageBabyRepository(
            dio: i.get<Dio>(),
          ),
        ),
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(
          '/',
          child: (ctx, args) => CageBabyView(
            cage: args.data['cage'],
          ),
        ),
        ModularRouter(
          '/ring',
          module: CageBabyRingModule(),
        ),
      ];
}
