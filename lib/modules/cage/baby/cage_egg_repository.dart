import 'dart:convert';
import 'package:dio/dio.dart';
import '../../../constants.dart' as constants;
import 'cage_baby_model.dart';

class CageBabyRepository {
  CageBabyRepository({
    this.dio,
  });

  final Dio dio;

  final String path = '${constants.appUrl}/baby';
  final Options options = Options(
    headers: {
      'X-Token': constants.appSecret,
    },
  );

  Future<Map<String, dynamic>> readAllBabies(int cageId) async {
    var formData = FormData.fromMap({
      'cage': cageId,
    });
    var response = await dio.post(
      '${path}/read-all-by-cage',
      data: formData,
      options: options,
    );
    return json.decode(response.data);
  }

  Future<Map<String, dynamic>> createBaby(CageBabyModel baby) async {
    var formData = FormData.fromMap(baby.toJson());
    var response = await dio.post(
      '${path}/create',
      data: formData,
      options: options,
    );
    return json.decode(response.data);
  }

  Future<Map<String, dynamic>> updateBaby(CageBabyModel baby) async {
    var formData = FormData.fromMap(baby.toJson());
    var response = await dio.post(
      '${path}/update',
      data: formData,
      options: options,
    );
    return json.decode(response.data);
  }

  Future<Map<String, dynamic>> deleteBaby(int babyId) async {
    var formData = FormData.fromMap(
      {
        'id': babyId,
      },
    );
    var response = await dio.post(
      '${path}/delete',
      data: formData,
      options: options,
    );
    return json.decode(response.data);
  }
}
