import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:asuka/asuka.dart' as asuka;
import '../../app/app_message_widget.dart';
import '../cage_model.dart';
import 'cage_baby_model.dart';
import 'cage_baby_repository.dart';
part 'cage_baby_controller.g.dart';

class CageBabyController = _CageBabyController with _$CageBabyController;

abstract class _CageBabyController with Store {
  _CageBabyController({
    this.cageBabyRepository,
  });

  final CageBabyRepository cageBabyRepository;

  @observable
  List<CageBabyModel> babies;

  @observable
  List<CageBabyModel> selectedBabies = [];

  @observable
  bool isTransfer = false;

  @action
  Future<void> onChangedCheckbox(CageBabyModel baby) async {
    if (!selectedBabies.contains(baby)) {
      selectedBabies.add(baby);
    }
  }

  @action
  Future<void> onPressedTransfer() async {
    isTransfer = !isTransfer;
    selectedBabies = [];
  }

  @action
  Future<void> onPressedTransferDone(BuildContext context) async {
    isTransfer = !isTransfer;
    if (selectedBabies.isEmpty) {
      asuka.showSnackBar(
        SnackBar(
          content: AppMessageWidget(
            message: 'Você não selecionou nenhum filhote.',
          ),
          duration: Duration(
            milliseconds: 800,
          ),
        ),
      );
      return;
    }
    var cage = await Modular.to.pushNamed(
      '/cage',
      arguments: {
        'isSelect': true,
      },
    ) as CageModel;
    if (cage != null) {
      for (var baby in selectedBabies) {
        baby.cage = cage.id;
        await cageBabyRepository.updateBaby(baby);
      }
      selectedBabies = [];
      asuka.showSnackBar(
        SnackBar(
          content: AppMessageWidget(
            message: 'Os filhotes foram movidos com sucesso!',
          ),
          duration: Duration(
            milliseconds: 800,
          ),
        ),
      );
    }
  }

  @action
  Future<void> onTapDeleteBaby(int cageId, int babyId) async {
    Modular.to.pop();
    asuka.showSnackBar(
      SnackBar(
        content: AppMessageWidget(
          isLoading: true,
          message: 'Por favor, aguarde ...',
        ),
        duration: Duration(
          milliseconds: 800,
        ),
      ),
    );
    var response = await cageBabyRepository.deleteBaby(babyId);
    asuka.showSnackBar(
      SnackBar(
        content: AppMessageWidget(
          message: response['message'],
        ),
        duration: Duration(
          milliseconds: 1000,
        ),
      ),
    );
    Modular.to.pop();
    readAllBabies(cageId);
  }

  @action
  Future<void> readAllBabies(int cageId) async {
    var response = await cageBabyRepository.readAllBabies(cageId);
    var data = json.decode(response['data']);
    babies = List<CageBabyModel>.from(data.map((e) {
      return CageBabyModel.fromJson(e);
    }).toList());
  }
}
