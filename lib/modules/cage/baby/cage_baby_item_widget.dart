import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../../app/app_button_widget.dart';
import '../../app/app_card_widget.dart';
import '../../app/app_text_widget.dart';
import 'cage_baby_model.dart';

class CageBabyItemWidget extends StatelessWidget {
  const CageBabyItemWidget({
    Key key,
    this.baby,
    this.isTransfer = false,
    this.onChangedCheckbox,
    this.onPressedDelete,
    this.onPressedUpgrade,
    this.ring,
  }) : super(key: key);

  final CageBabyModel baby;
  final bool isTransfer;
  final bool ring;
  final void Function(CageBabyModel) onChangedCheckbox;
  final Function onPressedDelete;
  final Function onPressedUpgrade;

  @override
  Widget build(BuildContext context) {
    var birthDate = DateTime.fromMillisecondsSinceEpoch(baby.birthDate);
    var birthDateFormatted =
        DateFormat('dd/MM/yyyy').format(birthDate).toString();
    return AppCardWidget(
      isTransfer: isTransfer,
      onChangedCheckbox: (value) => onChangedCheckbox(baby),
      color: Colors.white,
      image: 'lib/assets/baby.png',
      children: [
        AppTextWidget(
          text: 'Nascido em',
          size: Theme.of(context).textTheme.subtitle1.fontSize,
          weight: FontWeight.bold,
        ),
        AppTextWidget(
          text: '${birthDateFormatted}',
          size: Theme.of(context).textTheme.bodyText1.fontSize,
        ),
        SizedBox(
          height: 10,
        ),
        SizedBox(
          height: 10,
        ),
        Row(
          children: <Widget>[
            AppButtonWidget(
              color: Colors.white,
              textColor: Colors.black,
              paddingX: 0,
              paddingY: 0,
              text: 'Morreu',
              onPressed: onPressedDelete,
            ),
            SizedBox(
              width: 10,
            ),
            AppButtonWidget(
              color: Colors.green,
              textColor: Colors.white,
              paddingX: 0,
              paddingY: 0,
              text: ring ? 'Anilhar' : 'Cadastra-lo',
              onPressed: onPressedUpgrade,
            ),
          ],
        ),
      ],
    );
  }
}
