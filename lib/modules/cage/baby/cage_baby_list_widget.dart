import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import '../../app/app_modal_widget.dart';
import 'cage_baby_controller.dart';
import 'cage_baby_item_widget.dart';
import 'cage_baby_model.dart';

class CageBabyListWidget extends StatelessWidget {
  CageBabyListWidget(
      {Key key,
      this.babies,
      this.isTransfer = false,
      this.onChangedCheckbox,
      this.sc})
      : super(key: key);

  final List<CageBabyModel> babies;
  final CageBabyController cageBabyController =
      Modular.get<CageBabyController>();
  final bool isTransfer;
  final void Function(CageBabyModel) onChangedCheckbox;
  final ScrollController sc;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      // physics: NeverScrollableScrollPhysics(),
      controller: sc,
      itemCount: babies.length,
      itemBuilder: (_, i) {
        var ringBool = false;
        if (babies.elementAt(i).ringLeft == null ||
            babies.elementAt(i).ringLeft == '' ||
            babies.elementAt(i).ringRight == null ||
            babies.elementAt(i).ringRight == '') {
          ringBool = true;
        }
        return CageBabyItemWidget(
          baby: babies.elementAt(i),
          isTransfer: isTransfer,
          onChangedCheckbox: onChangedCheckbox,
          ring: ringBool,
          onPressedDelete: () {
            showModalBottomSheet(
              isDismissible: false,
              context: context,
              builder: (_) {
                return AppModalWidget(
                  title: 'Quer mesmo deletar o filhote?',
                  textYes: 'Deletar',
                  textNo: 'Cancelar',
                  onPressedYes: () => cageBabyController.onTapDeleteBaby(
                      babies.elementAt(i).cage, babies.elementAt(i).id),
                  onPressedNo: () => Navigator.pop(context),
                );
              },
            );
          },
          onPressedUpgrade: () {
            if (babies.elementAt(i).ringLeft == null ||
                babies.elementAt(i).ringLeft == '' ||
                babies.elementAt(i).ringRight == null ||
                babies.elementAt(i).ringRight == '') {
              Modular.to.pushNamed(
                '/cage/baby/ring',
                arguments: {
                  'baby': babies.elementAt(i),
                },
              );
            } else {
              var ring = {
                'left': babies.elementAt(i).ringLeft,
                'right': babies.elementAt(i).ringRight
              };
              Modular.to.pushNamed(
                '/bird/create',
                arguments: {
                  'baby': babies.elementAt(i),
                  'ring': ring,
                  'cage': babies.elementAt(i).cage
                },
              );
            }
          },
        );
      },
    );
  }
}
