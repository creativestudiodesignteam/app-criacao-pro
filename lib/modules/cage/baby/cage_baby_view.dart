import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../app/app_appbar_widget.dart';
import '../../app/app_header_widget.dart';
import '../../app/app_scaffold_widget_multi.dart';
import '../../app/app_sub_menu_widget.dart';
import '../../app/app_text_widget.dart';
import '../cage_model.dart';
import 'cage_baby_controller.dart';
import 'cage_baby_list_widget.dart';

class CageBabyView extends StatefulWidget {
  const CageBabyView({
    Key key,
    this.cage,
  }) : super(key: key);

  final CageModel cage;

  @override
  _CageBabyViewState createState() => _CageBabyViewState();
}

class _CageBabyViewState
    extends ModularState<CageBabyView, CageBabyController> {
  ScrollController _sc;
  bool isLoading = true;

  @override
  void initState() {
    super.initState();
    _sc = new ScrollController()..addListener(_scrollListener);
  }

  @override
  void dispose() {
    _sc.removeListener(_scrollListener);
    super.dispose();
  }

  void _scrollListener() {}

  Widget appBar(BuildContext context) {
    return AppAppBarWidget(
      titles: <Widget>[
        AppTextWidget(
          text: 'Filhotes na gaiola',
          size: Theme.of(context).textTheme.subtitle2.fontSize,
        ),
      ],
      actions: <Widget>[
        IconButton(
          icon: Icon(
            Icons.chevron_left,
          ),
          onPressed: () => Navigator.pop(context),
        ),
      ],
    );
  }

  Widget body(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            AppSubMenuWidget(
              image: 'lib/assets/cage-sub-1.png',
              text: 'Detalhes',
              selected: false,
              onPressed: () {
                Navigator.pop(context);
                Navigator.pushNamed(
                  context,
                  '/cage/detail',
                  arguments: {'cage': widget.cage},
                );
              },
            ),
            AppSubMenuWidget(
              image: 'lib/assets/cage-sub-2.png',
              text: 'Aves',
              selected: false,
              onPressed: () {
                Navigator.pop(context);
                Navigator.pushNamed(
                  context,
                  '/cage/bird',
                  arguments: {'cage': widget.cage, 'isReport': false},
                );
              },
            ),
            AppSubMenuWidget(
              image: 'lib/assets/cage-sub-4.png',
              text: 'Ovos',
              selected: false,
              onPressed: () {
                Navigator.pop(context);
                Navigator.pushNamed(
                  context,
                  '/cage/egg',
                  arguments: {'cage': widget.cage},
                );
              },
            ),
            AppSubMenuWidget(
              image: 'lib/assets/cage-sub-3.png',
              text: 'Filhotes',
              selected: true,
            ),
          ],
        ),
        SizedBox(
          height: 20,
        ),
        Observer(
          builder: (_) {
            var isEmpty = controller.babies == null;
            var length = isEmpty ? 0 : controller.babies.length;
            return AppHeaderWidget(
              title: 'Você tem um total',
              subtitle: 'de ',
              highlight: '${length} filhotes na gaiola',
            );
          },
        ),
        SizedBox(
          height: 20,
        ),
        Observer(
          builder: (_) {
            return Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                RaisedButton.icon(
                  elevation: 0,
                  textColor: Colors.white,
                  color: controller.isTransfer
                      ? Theme.of(context).primaryColor
                      : Theme.of(context).primaryColor,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8),
                  ),
                  icon: Icon(
                    controller.isTransfer ? Icons.done : Icons.edit,
                  ),
                  onPressed: () async {
                    if (controller.isTransfer) {
                      await controller.onPressedTransferDone(context);
                      controller.readAllBabies(widget.cage.id);
                    } else {
                      await controller.onPressedTransfer();
                    }
                  },
                  label: AppTextWidget(
                    text: controller.isTransfer
                        ? 'Mover filhotes'
                        : 'Selecionar filhotes',
                  ),
                ),
              ],
            );
          },
        ),
        SizedBox(
          height: 20,
        ),
        Observer(
          builder: (_) {
            if (controller.babies == null) {
              return AppTextWidget(
                text: 'Buscando filhotes...',
              );
            } else if (controller.babies.isEmpty) {
              return AppTextWidget(
                text: 'Nenhum filhote encontrado.',
              );
            } else {
              return CageBabyListWidget(
                babies: controller.babies,
                isTransfer: controller.isTransfer,
                onChangedCheckbox: controller.onChangedCheckbox,
                sc: _sc,
              );
            }
          },
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    controller.readAllBabies(widget.cage.id);
    return AppScaffoldWidgetMulti(
      sc: _sc,
      mainAxisAlignment: MainAxisAlignment.end,
      floatingActionButton: SizedBox(
        width: MediaQuery.of(context).size.width * 0.58,
      ),
      appBar: appBar(context),
      body: body(context),
    );
  }
}
