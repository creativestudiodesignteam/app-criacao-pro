import 'package:asuka/asuka.dart' as asuka;
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';

import '../../../app/app_message_widget.dart';
import '../cage_baby_controller.dart';
import '../cage_baby_model.dart';

part 'cage_baby_ring_controller.g.dart';

class CageBabyRingController = _CageBabyRingController
    with _$CageBabyRingController;

abstract class _CageBabyRingController with Store {
  _CageBabyRingController({
    this.cageBabyController,
  });

  final CageBabyController cageBabyController;
  final TextEditingController ringLeftController = TextEditingController();
  final TextEditingController ringRightController = TextEditingController();

  @action
  Future<void> onTapRing(CageBabyModel baby) async {
    var ringLeft = ringRightController.text;
    var ringRight = ringLeftController.text;
    Modular.to.pop();

    if (ringRight == '' || ringLeft == '') {}

    asuka.showSnackBar(
      SnackBar(
        content: AppMessageWidget(
          isLoading: true,
          message: 'Por favor, aguarde ...',
        ),
        duration: Duration(
          milliseconds: 800,
        ),
      ),
    );

    var response = await cageBabyController.cageBabyRepository
        .updateBabyRing(baby.id, ringLeft, ringRight);
    await cageBabyController.readAllBabies(baby.cage);
    if (response['type'] == 'success') {
      Modular.to.pop();
    }
    showAsukaSnackBar(response['message']);
  }

  void showAsukaSnackBar(String message) async {
    asuka.showSnackBar(
      SnackBar(
        content: AppMessageWidget(
          message: message,
        ),
        duration: Duration(
          milliseconds: 1000,
        ),
      ),
    );
  }
}
