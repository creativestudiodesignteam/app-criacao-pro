import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../../app/app_appbar_widget.dart';
import '../../../app/app_button_widget.dart';
import '../../../app/app_header_widget.dart';
import '../../../app/app_modal_widget.dart';
import '../../../app/app_scaffold_widget.dart';
import '../../../app/app_text_widget.dart';
import '../../../app/app_textfield_widget.dart';
import '../cage_baby_model.dart';
import 'cage_baby_ring_controller.dart';

class CageBabyRingView extends StatefulWidget {
  const CageBabyRingView({
    Key key,
    this.baby,
  }) : super(key: key);

  final CageBabyModel baby;

  @override
  _CageBabyRingViewState createState() => _CageBabyRingViewState();
}

class _CageBabyRingViewState
    extends ModularState<CageBabyRingView, CageBabyRingController> {
  @override
  void initState() {
    super.initState();
  }

  Widget appBar(BuildContext context) {
    return AppAppBarWidget(
      titles: <Widget>[
        AppTextWidget(
          text: 'Anilhar filhote',
          size: Theme.of(context).textTheme.subtitle2.fontSize,
        ),
      ],
      actions: <Widget>[
        IconButton(
          icon: Icon(
            Icons.chevron_left,
          ),
          onPressed: () => Navigator.pop(context),
        ),
      ],
    );
  }

  Widget body(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        AppHeaderWidget(
          title: 'Informe os',
          subtitle: 'detalhes do ',
          highlight: 'anilhamento',
        ),
        SizedBox(
          height: 20,
        ),
        AppTextFieldWidget(
          controller: controller.ringLeftController,
          label: 'Anel esquerdo',
          keyboardType: TextInputType.number,
        ),
        AppTextFieldWidget(
          controller: controller.ringRightController,
          label: 'Anel direito',
          keyboardType: TextInputType.number,
        ),
        SizedBox(
          height: 20,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            AppButtonWidget(
              text: 'Anilhar',
              color: Theme.of(context).primaryColor,
              onPressed: () {
                showModalBottomSheet(
                  isDismissible: false,
                  context: context,
                  builder: (_) {
                    return AppModalWidget(
                      title: 'Quer mesmo anilhar o filhote?',
                      textYes: 'Anilhar',
                      textNo: 'Cancelar',
                      onPressedYes: () => controller.onTapRing(widget.baby),
                      onPressedNo: () => Modular.to.pop(),
                    );
                  },
                );
              },
            ),
            AppTextWidget(
              text: 'Voltar',
              onTap: () => Navigator.pop(context),
            ),
          ],
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return AppScaffoldWidget(
      appBar: appBar(context),
      body: body(context),
    );
  }
}
