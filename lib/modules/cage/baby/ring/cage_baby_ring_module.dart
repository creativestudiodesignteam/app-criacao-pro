import 'package:criacao_pro/modules/cage/baby/ring/cage_baby_ring_controller.dart';
import 'package:dio/dio.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../../account/account_controller.dart';
import '../../../bird/bird_controller.dart';
import '../../../bird/bird_repository.dart';
import '../../../bird/create/bird_create_controller.dart';
import '../../cage_controller.dart';
import '../../cage_repository.dart';
import '../cage_baby_controller.dart';
import '../cage_baby_repository.dart';
import 'cage_baby_ring_view.dart';

class CageBabyRingModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind(
          (i) => CageBabyRingController(
            cageBabyController: i.get<CageBabyController>(),
          ),
        ),
        Bind(
          (i) => BirdController(
            birdRepository: i.get<BirdRepository>(),
            accountController: i.get<AccountController>(),
            birdCreateController: i.get<BirdCreateController>(),
          ),
        ),
        Bind(
          (i) => BirdCreateController(
            birdRepository: i.get<BirdRepository>(),
            accountController: i.get<AccountController>(),
            cageController: i.get<CageController>(),
            cageBabyController: i.get<CageBabyController>(),
          ),
        ),
        Bind(
          (i) => CageController(
            cageRepository: i.get<CageRepository>(),
            accountController: i.get<AccountController>(),
          ),
        ),
        Bind(
          (i) => CageBabyController(
            cageBabyRepository: i.get<CageBabyRepository>(),
          ),
        ),
        Bind(
          (i) => CageBabyRepository(
            dio: i.get<Dio>(),
          ),
        ),
        Bind(
          (i) => BirdRepository(
            dio: i.get<Dio>(),
          ),
        ),
        Bind(
          (i) => CageRepository(
            dio: i.get<Dio>(),
          ),
        ),
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(
          '/',
          child: (ctx, args) => CageBabyRingView(
            baby: args.data['baby'],
          ),
        ),
      ];
}
