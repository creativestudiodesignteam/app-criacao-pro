class CageEggModel {
  CageEggModel({
    this.id,
    this.account,
    this.state,
    this.layingDate,
    this.hatchingDate,
    this.birthDate,
    this.candlingDate,
    this.cage,
    this.mother,
    this.father,
  });

  int id;
  int account;
  String state;
  int layingDate;
  int hatchingDate;
  int birthDate;
  int candlingDate;
  int cage;
  int mother;
  int father;
  bool select = true;

  CageEggModel.fromJson(Map<String, dynamic> json) {
    id = int.parse(json['id']);
    account = int.parse(json['account']);
    state = json['state'];
    layingDate = int.parse(json['laying_date']);
    hatchingDate = int.parse(json['hatching_date']);
    birthDate = int.parse(json['birth_date']);
    candlingDate = int.parse(json['candling_date']);
    cage = int.parse(json['cage']);
    mother = int.parse(json['mother']);
    father = int.parse(json['father']);
  }

  Map<String, dynamic> toJson() {
    var data = {
      'id': id,
      'account': account,
      'state': state,
      'layingDate': layingDate,
      'hatchingDate': hatchingDate,
      'birthDate': birthDate,
      'candlingDate': candlingDate,
      'cage': cage,
      'mother': mother,
      'father': father,
      'select': select
    };
    return data;
  }
}
