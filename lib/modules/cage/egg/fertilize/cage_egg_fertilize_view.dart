import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:intl/intl.dart';
import '../../../app/app_appbar_widget.dart';
import '../../../app/app_button_widget.dart';
import '../../../app/app_header_widget.dart';
import '../../../app/app_modal_widget.dart';
import '../../../app/app_scaffold_widget.dart';
import '../../../app/app_text_widget.dart';
import '../cage_egg_model.dart';
import 'cage_egg_fertilize_controller.dart';

class CageEggFertilizeView extends StatefulWidget {
  const CageEggFertilizeView({
    Key key,
    this.egg,
  }) : super(key: key);

  final CageEggModel egg;

  @override
  _CageEggFertilizeViewState createState() => _CageEggFertilizeViewState();
}

class _CageEggFertilizeViewState
    extends ModularState<CageEggFertilizeView, CageEggFertilizeController> {
  Widget appBar(BuildContext context) {
    return AppAppBarWidget(
      titles: <Widget>[
        AppTextWidget(
          text: 'Fertilizar ovo',
          size: Theme.of(context).textTheme.subtitle2.fontSize,
        ),
      ],
      actions: <Widget>[
        IconButton(
          icon: Icon(
            Icons.chevron_left,
          ),
          onPressed: () => Navigator.pop(context),
        ),
      ],
    );
  }

  Widget body(BuildContext context) {
    var layingDate = DateTime.fromMillisecondsSinceEpoch(widget.egg.layingDate);
    var layingDateFormatted =
        DateFormat('dd/MM/yyyy').format(layingDate).toString();
    var hatchingDate =
        DateTime.fromMillisecondsSinceEpoch(widget.egg.hatchingDate);
    var hatchingDateFormatted =
        DateFormat('dd/MM/yyyy').format(hatchingDate).toString();
    var birthDate = DateTime.fromMillisecondsSinceEpoch(widget.egg.birthDate);
    var birthDateFormatted =
        DateFormat('dd/MM/yyyy').format(birthDate).toString();
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        AppHeaderWidget(
          title: 'Confirme ',
          subtitle: 'a ',
          highlight: 'fertilização',
        ),
        SizedBox(
          height: 20,
        ),
        AppTextWidget(
          text: 'Postura efetuada',
          size: Theme.of(context).textTheme.subtitle1.fontSize,
          weight: FontWeight.bold,
        ),
        AppTextWidget(
          text: '${layingDateFormatted}',
          size: Theme.of(context).textTheme.bodyText1.fontSize,
        ),
        SizedBox(
          height: 10,
        ),
        AppTextWidget(
          text: 'Chôco iniciado em',
          size: Theme.of(context).textTheme.subtitle1.fontSize,
          weight: FontWeight.bold,
        ),
        AppTextWidget(
          text: '${hatchingDateFormatted}',
          size: Theme.of(context).textTheme.bodyText1.fontSize,
        ),
        SizedBox(
          height: 10,
        ),
        AppTextWidget(
          text: 'Nascimento previsto',
          size: Theme.of(context).textTheme.subtitle1.fontSize,
          weight: FontWeight.bold,
        ),
        AppTextWidget(
          text: '${birthDateFormatted}',
          size: Theme.of(context).textTheme.bodyText1.fontSize,
        ),
        SizedBox(
          height: 20,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            AppButtonWidget(
              text: 'Fertilizar',
              color: Theme.of(context).primaryColor,
              onPressed: () {
                showModalBottomSheet(
                  isDismissible: false,
                  context: context,
                  builder: (_) {
                    return AppModalWidget(
                      title: 'Quer mesmo fertilizar o ovo?',
                      textYes: 'Fertilizar',
                      textNo: 'Cancelar',
                      onPressedYes: () => controller.onTapFertilize(widget.egg),
                      onPressedNo: () => Modular.to.pop(),
                    );
                  },
                );
              },
            ),
            AppTextWidget(
              text: 'Voltar',
              onTap: () => Navigator.pop(context),
            ),
          ],
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return AppScaffoldWidget(
      appBar: appBar(context),
      body: body(context),
    );
  }
}
