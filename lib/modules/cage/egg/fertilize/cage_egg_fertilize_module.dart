import 'package:flutter_modular/flutter_modular.dart';
import 'package:dio/dio.dart';
import '../cage_egg_controller.dart';
import '../cage_egg_repository.dart';
import 'cage_egg_fertilize_controller.dart';
import 'cage_egg_fertilize_view.dart';

class CageEggFertilizeModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind(
          (i) => CageEggFertilizeController(
            cageEggController: i.get<CageEggController>(),
          ),
        ),
        Bind(
          (i) => CageEggController(
            cageEggRepository: i.get<CageEggRepository>(),
          ),
        ),
        Bind(
          (i) => CageEggRepository(
            dio: i.get<Dio>(),
          ),
        ),
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(
          '/',
          child: (ctx, args) => CageEggFertilizeView(
            egg: args.data['egg'],
          ),
        ),
      ];
}
