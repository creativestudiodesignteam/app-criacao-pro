import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:asuka/asuka.dart' as asuka;
import '../../../app/app_message_widget.dart';
import '../cage_egg_controller.dart';
import '../cage_egg_model.dart';
part 'cage_egg_fertilize_controller.g.dart';

class CageEggFertilizeController = _CageEggFertilizeController
    with _$CageEggFertilizeController;

abstract class _CageEggFertilizeController with Store {
  _CageEggFertilizeController({
    this.cageEggController,
  });

  final CageEggController cageEggController;

  Future<void> onTapFertilize(CageEggModel egg) async {
    Modular.to.pop();
    asuka.showSnackBar(
      SnackBar(
        content: AppMessageWidget(
          isLoading: true,
          message: 'Por favor, aguarde ...',
        ),
        duration: Duration(
          milliseconds: 800,
        ),
      ),
    );
    egg.state = 'Nasceu';
    var response = await cageEggController.cageEggRepository.updateEgg(egg);
    cageEggController.firstAcessInFunction = true;
    cageEggController.eggs = null;
    cageEggController.readAllEggs(egg.cage);
    Modular.to.pop();
    asuka.showSnackBar(
      SnackBar(
        content: AppMessageWidget(
          message: response['message'],
        ),
        duration: Duration(
          milliseconds: 1000,
        ),
      ),
    );
  }
}
