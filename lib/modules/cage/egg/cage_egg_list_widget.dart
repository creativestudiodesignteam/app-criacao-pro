import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import '../../app/app_modal_widget.dart';
import 'cage_egg_controller.dart';
import 'cage_egg_item_widget.dart';
import 'cage_egg_model.dart';

class CageEggListWidget extends StatefulWidget {
  CageEggListWidget({
    Key key,
    this.eggs,
    this.selectedEggs,
    this.isTransfer = false,
    this.isAction = false,
    this.onChangedCheckbox,
    this.sc,
    this.totalEggs,
    this.stateEgg,
  }) : super(key: key);

  final List<CageEggModel> eggs;
  final List<CageEggModel> selectedEggs;
  final bool isTransfer;
  final bool isAction;
  final void Function(CageEggModel) onChangedCheckbox;
  final ScrollController sc;
  final String stateEgg;
  final String totalEggs;

  bool isLoading = false;

  @override
  _CageEggListWidgetState createState() => _CageEggListWidgetState();
}

class _CageEggListWidgetState extends State<CageEggListWidget> {
  final CageEggController cageEggController = Modular.get<CageEggController>();

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.60,
      child: ListView.builder(
        shrinkWrap: true,
        itemCount: widget.eggs.length + 1,
        controller: widget.sc,
        itemBuilder: (_, i) {
          if (i == widget.eggs.length) {
            if (widget.totalEggs != '') {
              if (int.parse(widget.totalEggs) == widget.eggs.length) {
                widget.isLoading = false;
              } else {
                widget.isLoading = true;
              }
            }
            return _buildProgressIndicator();
          } else {
            widget.isLoading = false;
            return CageEggItemWidget(
              stateEgg: widget.stateEgg,
              isTransfer: widget.isTransfer,
              isAction: widget.isAction,
              onChangedCheckbox: widget.onChangedCheckbox,
              egg: widget.eggs.elementAt(i),
              onPressedDelete: () {
                showModalBottomSheet(
                  isDismissible: false,
                  context: context,
                  builder: (_) {
                    return AppModalWidget(
                      title: 'Quer mesmo deletar o ovo?',
                      textYes: 'Deletar',
                      textNo: 'Cancelar',
                      onPressedYes: () => cageEggController.onTapDeleteEgg(
                          widget.eggs.elementAt(i).cage,
                          widget.eggs.elementAt(i).id,
                          true,
                          true),
                      onPressedNo: () => Modular.to.pop(),
                    );
                  },
                );
              },
              onPressedUpgrade: () {
                if (widget.eggs.elementAt(i).state == 'Chocar') {
                  Modular.to.pushNamed(
                    '/cage/egg/hatch',
                    arguments: {
                      'egg': widget.eggs.elementAt(i),
                    },
                  );
                }
                if (widget.eggs.elementAt(i).state == 'Fertilizar') {
                  Modular.to.pushNamed(
                    '/cage/egg/fertilize',
                    arguments: {
                      'egg': widget.eggs.elementAt(i),
                    },
                  );
                }
                if (widget.eggs.elementAt(i).state == 'Nasceu') {
                  Modular.to.pushNamed(
                    '/cage/egg/birth',
                    arguments: {
                      'egg': widget.eggs.elementAt(i),
                    },
                  );
                }
              },
            );
          }
        },
      ),
    );
  }

  Widget _buildProgressIndicator() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Center(
        child: Opacity(
          opacity: widget.isLoading ? 1.0 : 00,
          child: CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(
              Color(0xffe9ae24),
            ),
          ),
        ),
      ),
    );
  }
}
