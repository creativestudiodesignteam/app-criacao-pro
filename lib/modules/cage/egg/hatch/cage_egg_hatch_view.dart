import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import '../../../app/app_appbar_widget.dart';
import '../../../app/app_button_widget.dart';
import '../../../app/app_header_widget.dart';
import '../../../app/app_modal_widget.dart';
import '../../../app/app_scaffold_widget.dart';
import '../../../app/app_text_widget.dart';
import '../../../app/app_textfield_widget.dart';
import '../../../bird/bird_item_widget.dart';
import '../cage_egg_model.dart';
import 'cage_egg_hatch_controller.dart';

class CageEggHatchView extends StatefulWidget {
  const CageEggHatchView({
    Key key,
    this.egg,
    this.eggs,
  }) : super(key: key);

  final CageEggModel egg;
  final List<CageEggModel> eggs;

  @override
  _CageEggHatchViewState createState() => _CageEggHatchViewState();
}

class _CageEggHatchViewState
    extends ModularState<CageEggHatchView, CageEggHatchController> {
  @override
  void initState() {
    super.initState();
  }

  Widget appBar(BuildContext context) {
    return AppAppBarWidget(
      titles: <Widget>[
        AppTextWidget(
          text: 'Chocar ovo',
          size: Theme.of(context).textTheme.subtitle2.fontSize,
        ),
      ],
      actions: <Widget>[
        IconButton(
          icon: Icon(
            Icons.chevron_left,
          ),
          onPressed: () => Navigator.pop(context),
        ),
      ],
    );
  }

  Widget body(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        AppHeaderWidget(
          title: 'Informe os',
          subtitle: 'detalhes do ',
          highlight: 'ovo',
        ),
        SizedBox(
          height: 20,
        ),
        AppTextFieldWidget(
          controller: controller.birthDateController,
          label: 'Quantos dias para o chôco?',
          keyboardType: TextInputType.number,
        ),
        AppTextFieldWidget(
          controller: controller.candlingDateController,
          label: 'Em quantos dias será realizada a ovoscopia?',
          keyboardType: TextInputType.number,
        ),
        SizedBox(
          height: 20,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            AppButtonWidget(
              text: 'Chocar',
              color: Theme.of(context).primaryColor,
              onPressed: () {
                showModalBottomSheet(
                  isDismissible: false,
                  context: context,
                  builder: (_) {
                    return AppModalWidget(
                      title: 'Quer mesmo chocar o ovo?',
                      textYes: 'Chocar',
                      textNo: 'Cancelar',
                      onPressedYes: () =>
                          controller.onTapHatch(widget.egg, widget.eggs),
                      onPressedNo: () => Modular.to.pop(),
                    );
                  },
                );
              },
            ),
            AppTextWidget(
              text: 'Voltar',
              onTap: () => Navigator.pop(context),
            ),
          ],
        ),
        SizedBox(
          height: 20,
        ),
        widget.eggs == null
            ? Column(
                children: <Widget>[
                  AppHeaderWidget(
                    title: 'Quem é ',
                    subtitle: 'a ',
                    highlight: 'Mãe?',
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Observer(
                    builder: (_) {
                      var isEmpty = controller.mother == null;
                      if (isEmpty) {
                        return AppTextWidget(
                          text: 'Selecione a mãe do ovo',
                        );
                      } else {
                        return BirdItemWidget(
                          bird: controller.mother,
                          isReport: true,
                          isSelect: true,
                        );
                      }
                    },
                  ),
                  Observer(
                    builder: (_) {
                      var isEmpty = controller.mother == null;
                      var tag = isEmpty ? 'Adicionar' : 'Trocar';
                      return AppButtonWidget(
                        onPressed: () async {
                          showModalBottomSheet(
                            isDismissible: true,
                            context: context,
                            builder: (_) {
                              return AppModalWidget(
                                title:
                                    '${tag} ${isEmpty ? '' : 'ou deletar '}mãe?',
                                textYes: tag,
                                textNo: isEmpty ? 'Cancelar' : 'Deletar',
                                onPressedYes: () async {
                                  Modular.to.pop();
                                  var mother = await Modular.to.pushNamed(
                                    '/bird',
                                    arguments: {
                                      'isReport': true,
                                      'isSelect': true,
                                      'gender': 'Fêmea',
                                    },
                                  );
                                  if (mother != null) {
                                    controller.onTapAddMother(
                                        widget.egg, mother, 1);
                                  }
                                },
                                onPressedNo: () {
                                  if (isEmpty) {
                                    Modular.to.pop();
                                  } else {
                                    controller.onTapAddMother(
                                        widget.egg, null, 0);
                                  }
                                },
                              );
                            },
                          );
                        },
                        color: Colors.white,
                        text: isEmpty ? 'Adicionar mãe' : 'Trocar mãe',
                        textColor: Colors.black,
                      );
                    },
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  AppHeaderWidget(
                    title: 'Quem é ',
                    subtitle: 'o ',
                    highlight: 'Pai?',
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Observer(
                    builder: (_) {
                      var isEmpty = controller.father == null;
                      if (isEmpty) {
                        return AppTextWidget(
                          text: 'Selecione o pai do ovo',
                        );
                      } else {
                        return BirdItemWidget(
                          bird: controller.father,
                          isReport: true,
                          isSelect: true,
                        );
                      }
                    },
                  ),
                  Observer(
                    builder: (_) {
                      var isEmpty = controller.father == null;
                      var tag = isEmpty ? 'Adicionar' : 'Trocar';
                      return AppButtonWidget(
                        onPressed: () async {
                          showModalBottomSheet(
                            isDismissible: true,
                            context: context,
                            builder: (_) {
                              return AppModalWidget(
                                title:
                                    '${tag} ${isEmpty ? '' : 'ou deletar '}pai?',
                                textYes: tag,
                                textNo: isEmpty ? 'Cancelar' : 'Deletar',
                                onPressedYes: () async {
                                  Modular.to.pop();
                                  var father = await Modular.to.pushNamed(
                                    '/bird',
                                    arguments: {
                                      'isReport': true,
                                      'isSelect': true,
                                      'gender': 'Macho',
                                    },
                                  );
                                  if (father != null) {
                                    controller.onTapAddFather(
                                        widget.egg, father, 1);
                                  }
                                },
                                onPressedNo: () {
                                  if (isEmpty) {
                                    Modular.to.pop();
                                  } else {
                                    controller.onTapAddFather(
                                        widget.egg, null, 0);
                                  }
                                },
                              );
                            },
                          );
                        },
                        color: Colors.white,
                        text: isEmpty ? 'Adicionar pai' : 'Trocar pai',
                        textColor: Colors.black,
                      );
                    },
                  ),
                ],
              )
            : Container(),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    controller.birthDateController.text = '13';
    controller.candlingDateController.text = '7';
    print('AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA');
    print(widget.egg);
    if (widget.egg != null) {
      if (widget.egg.mother != null) {
        controller.readMother(widget.egg.mother);
      }
      if (widget.egg.father != null) {
        controller.readFather(widget.egg.father);
      }
    }

    return AppScaffoldWidget(
      appBar: appBar(context),
      body: body(context),
    );
  }
}
