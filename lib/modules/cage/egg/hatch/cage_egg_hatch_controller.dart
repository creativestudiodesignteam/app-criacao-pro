import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:asuka/asuka.dart' as asuka;
import '../../../app/app_message_widget.dart';
import '../../../bird/bird_controller.dart';
import '../../../bird/bird_model.dart';
import '../cage_egg_controller.dart';
import '../cage_egg_model.dart';
part 'cage_egg_hatch_controller.g.dart';

class CageEggHatchController = _CageEggHatchController
    with _$CageEggHatchController;

abstract class _CageEggHatchController with Store {
  _CageEggHatchController({
    this.cageEggController,
    this.birdController,
  });

  final CageEggController cageEggController;
  final BirdController birdController;
  final TextEditingController birthDateController = TextEditingController();
  final TextEditingController candlingDateController = TextEditingController();

  @observable
  BirdModel father;

  @observable
  BirdModel mother;

  @action
  Future<void> onTapAddMother(
      CageEggModel egg, BirdModel mother, int type) async {
    if (mother.gender == 'fêmea'.toLowerCase()) {
      asuka.showSnackBar(
        SnackBar(
          content: AppMessageWidget(
            isLoading: true,
            message: 'A mãe deve ser do gênero fêmea',
          ),
          duration: Duration(
            milliseconds: 800,
          ),
        ),
      );
    }

    if (type == 0) {
      Modular.to.pop();
      egg.mother = -1;
    } else {
      egg.mother = mother.id;
    }
    asuka.showSnackBar(
      SnackBar(
        content: AppMessageWidget(
          isLoading: true,
          message: 'Por favor, aguarde ...',
        ),
        duration: Duration(
          milliseconds: 800,
        ),
      ),
    );
    await cageEggController.cageEggRepository.updateEgg(egg);
    readMother(egg.mother);
    asuka.showSnackBar(
      SnackBar(
        content: AppMessageWidget(
          message: 'O ovo foi atualizado com sucesso!',
        ),
        duration: Duration(
          milliseconds: 1000,
        ),
      ),
    );
  }

  @action
  Future<void> onTapAddFather(
      CageEggModel egg, BirdModel father, int type) async {
    if (father.gender == 'macho'.toLowerCase()) {
      asuka.showSnackBar(
        SnackBar(
          content: AppMessageWidget(
            isLoading: true,
            message: 'O pai deve ser do gênero macho',
          ),
          duration: Duration(
            milliseconds: 800,
          ),
        ),
      );
    }
    if (type == 0) {
      Modular.to.pop();
      egg.father = -1;
    } else {
      egg.father = father.id;
    }
    asuka.showSnackBar(
      SnackBar(
        content: AppMessageWidget(
          isLoading: true,
          message: 'Por favor, aguarde ...',
        ),
        duration: Duration(
          milliseconds: 800,
        ),
      ),
    );
    await cageEggController.cageEggRepository.updateEgg(egg);
    readFather(egg.father);
    asuka.showSnackBar(
      SnackBar(
        content: AppMessageWidget(
          message: 'O ovo foi atualizado com sucesso!',
        ),
        duration: Duration(
          milliseconds: 1000,
        ),
      ),
    );
  }

  @action
  Future<void> readMother(int motherId) async {
    if (motherId > 0) {
      var response = await birdController.birdRepository.readBird(motherId);
      mother = BirdModel.fromJson(json.decode(response['data']));
    }
  }

  @action
  Future<void> readFather(int fatherId) async {
    if (fatherId > 0) {
      var response = await birdController.birdRepository.readBird(fatherId);
      father = BirdModel.fromJson(json.decode(response['data']));
    }
  }

  @action
  Future<void> onTapHatch(CageEggModel egg, List<CageEggModel> eggs) async {
    Modular.to.pop();
    asuka.showSnackBar(
      SnackBar(
        content: AppMessageWidget(
          isLoading: true,
          message: 'Por favor, aguarde ...',
        ),
        duration: Duration(
          milliseconds: 800,
        ),
      ),
    );
    var response;
    var now = DateTime.now().millisecondsSinceEpoch;
    var cageElement;
    if (eggs == null) {
      egg.hatchingDate = now;
      var birthDate = int.tryParse(birthDateController.text) ?? 0;
      var birthDateDuration = Duration(days: birthDate);
      egg.birthDate = now + birthDateDuration.inMilliseconds;
      var candlingDate = int.tryParse(candlingDateController.text) ?? 0;
      var candlingDuration = Duration(days: candlingDate);
      egg.candlingDate = now + candlingDuration.inMilliseconds;
      egg.state = 'Fertilizar';
      response = await cageEggController.cageEggRepository.updateEgg(egg);
      cageEggController.firstAcessInFunction = true;
      cageEggController.eggs = null;
      cageEggController.readAllEggs(egg.cage);
    } else {
      await eggs.forEach((element) async {
        element.hatchingDate = now;
        var birthDate = int.tryParse(birthDateController.text) ?? 0;
        var birthDateDuration = Duration(days: birthDate);
        element.birthDate = now + birthDateDuration.inMilliseconds;
        var candlingDate = int.tryParse(candlingDateController.text) ?? 0;
        var candlingDuration = Duration(days: candlingDate);
        element.candlingDate = now + candlingDuration.inMilliseconds;
        element.state = 'Fertilizar';

        if (eggs[eggs.length - 1].id == element.id) {
          response =
              await cageEggController.cageEggRepository.updateEgg(element);
          cageEggController.firstAcessInFunction = true;
          cageEggController.eggs = null;
          await cageEggController.readAllEggs(eggs[0].cage);
          showAsukaSnackBar(response['message']);
        } else {
          await cageEggController.cageEggRepository.updateEgg(element);
        }
      });
    }

    Modular.to.pop();
  }

  void showAsukaSnackBar(String message) async {
    asuka.showSnackBar(
      SnackBar(
        content: AppMessageWidget(
          message: message,
        ),
        duration: Duration(
          milliseconds: 1000,
        ),
      ),
    );
  }
}
