// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cage_egg_hatch_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$CageEggHatchController on _CageEggHatchController, Store {
  final _$fatherAtom = Atom(name: '_CageEggHatchController.father');

  @override
  BirdModel get father {
    _$fatherAtom.reportRead();
    return super.father;
  }

  @override
  set father(BirdModel value) {
    _$fatherAtom.reportWrite(value, super.father, () {
      super.father = value;
    });
  }

  final _$motherAtom = Atom(name: '_CageEggHatchController.mother');

  @override
  BirdModel get mother {
    _$motherAtom.reportRead();
    return super.mother;
  }

  @override
  set mother(BirdModel value) {
    _$motherAtom.reportWrite(value, super.mother, () {
      super.mother = value;
    });
  }

  final _$onTapAddMotherAsyncAction =
      AsyncAction('_CageEggHatchController.onTapAddMother');

  @override
  Future<void> onTapAddMother(CageEggModel egg, BirdModel mother, int type) {
    return _$onTapAddMotherAsyncAction
        .run(() => super.onTapAddMother(egg, mother, type));
  }

  final _$onTapAddFatherAsyncAction =
      AsyncAction('_CageEggHatchController.onTapAddFather');

  @override
  Future<void> onTapAddFather(CageEggModel egg, BirdModel father, int type) {
    return _$onTapAddFatherAsyncAction
        .run(() => super.onTapAddFather(egg, father, type));
  }

  final _$readMotherAsyncAction =
      AsyncAction('_CageEggHatchController.readMother');

  @override
  Future<void> readMother(int motherId) {
    return _$readMotherAsyncAction.run(() => super.readMother(motherId));
  }

  final _$readFatherAsyncAction =
      AsyncAction('_CageEggHatchController.readFather');

  @override
  Future<void> readFather(int fatherId) {
    return _$readFatherAsyncAction.run(() => super.readFather(fatherId));
  }

  final _$onTapHatchAsyncAction =
      AsyncAction('_CageEggHatchController.onTapHatch');

  @override
  Future<void> onTapHatch(CageEggModel egg, List<CageEggModel> eggs) {
    return _$onTapHatchAsyncAction.run(() => super.onTapHatch(egg, eggs));
  }

  @override
  String toString() {
    return '''
father: ${father},
mother: ${mother}
    ''';
  }
}
