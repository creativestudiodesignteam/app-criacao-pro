import 'dart:convert';
import 'package:dio/dio.dart';
import '../../../constants.dart' as constants;
import 'cage_egg_model.dart';

class CageEggRepository {
  CageEggRepository({
    this.dio,
  });

  final Dio dio;

  final String path = '${constants.appUrl}/egg';
  final Options options = Options(
    headers: {
      'X-Token': constants.appSecret,
    },
  );

  Future<Map<String, dynamic>> readAllEggs(int cageId) async {
    var formData = FormData.fromMap({
      'cage': cageId,
    });
    var response = await dio.post(
      '${path}/read-all-by-cage',
      data: formData,
      options: options,
    );
    return json.decode(response.data);
  }

  Future<Map<String, dynamic>> readAllEggsPagination(
      int cageId, int lastId) async {
    var formData = FormData.fromMap({
      'cage': cageId,
      'lastId': lastId,
    });
    var response = await dio.post(
      '${path}/read-all-by-cage-pagination',
      data: formData,
      options: options,
    );
    return json.decode(response.data);
  }

  Future<Map<String, dynamic>> createEgg(CageEggModel egg) async {
    var formData = FormData.fromMap(egg.toJson());
    var response = await dio.post(
      '${path}/create',
      data: formData,
      options: options,
    );
    return json.decode(response.data);
  }

  Future<Map<String, dynamic>> updateEgg(CageEggModel egg) async {
    var formData = FormData.fromMap(egg.toJson());
    var response = await dio.post(
      '${path}/update',
      data: formData,
      options: options,
    );
    return json.decode(response.data);
  }

  Future<Map<String, dynamic>> deleteEgg(int eggId) async {
    var formData = FormData.fromMap(
      {
        'id': eggId,
      },
    );
    var response = await dio.post(
      '${path}/delete',
      data: formData,
      options: options,
    );
    return json.decode(response.data);
  }

  Future<Map<String, dynamic>> readAllBirdsCage(int cage) async {
    var formData = FormData.fromMap(
      {
        'cage': cage,
      },
    );
    var response = await dio.post(
      '${constants.appUrl}/bird/read-by-cage',
      data: formData,
      options: options,
    );
    return json.decode(response.data);
  }
}
