import 'package:criacao_pro/modules/cage/egg/cage_egg_item_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../app/app_action_button_widget.dart';
import '../../app/app_appbar_widget.dart';
import '../../app/app_header_widget.dart';
import '../../app/app_scaffold_widget_multi.dart';
import '../../app/app_sub_menu_widget.dart';
import '../../app/app_text_widget.dart';
import '../cage_model.dart';
import 'cage_egg_controller.dart';
import 'cage_egg_list_widget.dart';

class CageEggView extends StatefulWidget {
  const CageEggView({
    Key key,
    this.cage,
  }) : super(key: key);

  final CageModel cage;

  @override
  _CageEggViewState createState() => _CageEggViewState();
}

class _CageEggViewState extends ModularState<CageEggView, CageEggController> {
  ScrollController _sc;
  bool isLoading = true;

  @override
  void initState() {
    super.initState();
    _sc = new ScrollController()..addListener(_scrollListener);
  }

  @override
  void dispose() {
    _sc.removeListener(_scrollListener);
    super.dispose();
  }

  void _scrollListener() {
    if (_sc.position.maxScrollExtent == _sc.position.pixels) {
      if (int.parse(controller.totalEggs) > controller.eggs.length) {
        controller.readAllEggs(widget.cage.id);
      }
    }
  }

  Widget appBar(BuildContext context) {
    return AppAppBarWidget(
      titles: <Widget>[
        AppTextWidget(
          text: 'Ovos na gaiola',
          size: Theme.of(context).textTheme.subtitle2.fontSize,
        ),
      ],
      actions: <Widget>[
        IconButton(
          icon: Icon(
            Icons.chevron_left,
          ),
          onPressed: () => Navigator.pop(context),
        ),
      ],
    );
  }

  Widget body(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            AppSubMenuWidget(
              image: 'lib/assets/cage-sub-1.png',
              text: 'Detalhes',
              selected: false,
              onPressed: () {
                Navigator.pop(context);
                Navigator.pushNamed(
                  context,
                  '/cage/detail',
                  arguments: {'cage': widget.cage},
                );
              },
            ),
            AppSubMenuWidget(
              image: 'lib/assets/cage-sub-2.png',
              text: 'Aves',
              selected: false,
              onPressed: () {
                Navigator.pop(context);
                Navigator.pushNamed(
                  context,
                  '/cage/bird',
                  arguments: {'cage': widget.cage, 'isReport': false},
                );
              },
            ),
            AppSubMenuWidget(
              image: 'lib/assets/cage-sub-4.png',
              text: 'Ovos',
              selected: true,
            ),
            AppSubMenuWidget(
              image: 'lib/assets/cage-sub-3.png',
              text: 'Filhotes',
              selected: false,
              onPressed: () {
                Navigator.pop(context);
                Navigator.pushNamed(
                  context,
                  '/cage/baby',
                  arguments: {'cage': widget.cage},
                );
              },
            ),
          ],
        ),
        SizedBox(
          height: 20,
        ),
        Observer(
          builder: (_) {
            var isEmpty = controller.eggs == null;
            return AppHeaderWidget(
              title: 'Você tem um total',
              subtitle: 'de ',
              highlight: '${isEmpty ? 0 : controller.totalEggs} ovos na gaiola',
            );
          },
        ),
        SizedBox(
          height: 20,
        ),
        Observer(
          builder: (_) {
            return Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                !controller.isAction
                    ? RaisedButton.icon(
                        elevation: 0,
                        textColor: Colors.white,
                        color: Theme.of(context).primaryColor,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8),
                        ),
                        icon: Icon(
                          controller.isTransfer ? Icons.done : Icons.edit,
                        ),
                        onPressed: () async {
                          controller.eggs = null;
                          controller.lastId = 0;
                          controller.firstAcessInFunction = true;
                          await controller.readAllEggs(widget.cage.id);
                          if (controller.isTransfer) {
                            await controller.onPressedTransferDone(context);
                          } else {
                            await controller.onPressedTransfer();
                          }
                        },
                        label: AppTextWidget(
                          text: controller.isTransfer
                              ? 'Mover ovos'
                              : 'Selecionar ovos',
                        ),
                      )
                    : SizedBox(),
                Container(
                  width: 10,
                ),
                !controller.isTransfer
                    ? RaisedButton.icon(
                        elevation: 0,
                        textColor: Colors.white,
                        color: Theme.of(context).primaryColor,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8),
                        ),
                        icon: Icon(
                          controller.isAction ? Icons.done : Icons.edit,
                        ),
                        onPressed: () async {
                          if (controller.isAction) {
                            await controller.onPressedActionDone();
                          } else {
                            controller.eggs = null;
                            controller.lastId = 0;
                            controller.firstAcessInFunction = true;
                            await controller.readAllEggs(widget.cage.id);
                            await controller.onPressedAction();
                          }
                        },
                        label: AppTextWidget(
                          text: controller.isAction
                              ? 'Ação ${controller.stateEgg}'
                              : 'Ações em bloco',
                          // text: '${controller.stateEgg} Ovos'),
                        ),
                      )
                    : SizedBox(),
              ],
            );
          },
        ),
        SizedBox(
          height: 20,
        ),
        Observer(
          builder: (_) {
            if (controller.eggs == null) {
              return AppTextWidget(
                text: 'Buscando ovos...',
              );
            } else if (controller.eggs.isEmpty) {
              return AppTextWidget(
                text: 'Nenhum ovo encontrado.',
              );
            } else {
              return CageEggListWidget(
                  eggs: controller.eggs,
                  selectedEggs: controller.selectedEggs,
                  isTransfer: controller.isTransfer,
                  isAction: controller.isAction,
                  onChangedCheckbox: controller.onChangedCheckbox,
                  sc: _sc,
                  stateEgg: controller.stateEgg,
                  totalEggs: controller.totalEggs);
            }
          },
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    controller.readAllBirdsCage(widget.cage.id);
    controller.readAllEggs(widget.cage.id);
    return AppScaffoldWidgetMulti(
      appBar: appBar(context),
      body: body(context),
      sc: _sc,
      floatingActionButton: Observer(
        builder: (_) {
          return controller.isTransfer || controller.isAction
              ? SizedBox(
                  width: MediaQuery.of(context).size.width * 0.58,
                )
              : AppActionAppButtonWidget(
                  label: 'Adicionar ovo',
                  icon: Icons.add,
                  color: Colors.green,
                  onPressed: () => controller.onTapAddEgg(widget.cage.id),
                );
        },
      ),
    );
  }
}
