import 'package:criacao_pro/modules/cage/cage_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:dio/dio.dart';
import '../../account/account_controller.dart';
import 'birth/cage_egg_birth_module.dart';
import 'cage_egg_controller.dart';
import 'cage_egg_repository.dart';
import 'cage_egg_view.dart';
import 'fertilize/cage_egg_fertilize_module.dart';
import 'hatch/cage_egg_hatch_module.dart';

class CageEggModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind(
          (i) => CageEggController(
              cageEggRepository: i.get<CageEggRepository>(),
              accountController: i.get<AccountController>(),
              cageController: i.get<CageController>()),
        ),
        Bind(
          (i) => CageEggRepository(
            dio: i.get<Dio>(),
          ),
        ),
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(
          '/',
          child: (ctx, args) => CageEggView(
            cage: args.data['cage'],
          ),
        ),
        ModularRouter(
          '/hatch',
          module: CageEggHatchModule(),
        ),
        ModularRouter(
          '/fertilize',
          module: CageEggFertilizeModule(),
        ),
        ModularRouter(
          '/birth',
          module: CageEggBirthModule(),
        ),
      ];
}
