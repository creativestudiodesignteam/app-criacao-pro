import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import '../../../app/app_appbar_widget.dart';
import '../../../app/app_button_widget.dart';
import '../../../app/app_header_widget.dart';
import '../../../app/app_modal_widget.dart';
import '../../../app/app_scaffold_widget.dart';
import '../../../app/app_text_widget.dart';
import '../../../app/app_textfield_widget.dart';
import '../cage_egg_model.dart';
import 'cage_egg_birth_controller.dart';

class CageEggBirthView extends StatefulWidget {
  const CageEggBirthView({
    Key key,
    this.eggs,
    this.egg,
  }) : super(key: key);

  final CageEggModel egg;
  final List<CageEggModel> eggs;

  @override
  _CageEggBirthViewState createState() => _CageEggBirthViewState();
}

class _CageEggBirthViewState
    extends ModularState<CageEggBirthView, CageEggBirthController> {
  Widget appBar(BuildContext context) {
    return AppAppBarWidget(
      titles: <Widget>[
        AppTextWidget(
          text: 'Nascimento do filhote',
          size: Theme.of(context).textTheme.subtitle2.fontSize,
        ),
      ],
      actions: <Widget>[
        IconButton(
          icon: Icon(
            Icons.chevron_left,
          ),
          onPressed: () => Navigator.pop(context),
        ),
      ],
    );
  }

  Widget body(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        AppHeaderWidget(
          title: 'Confirme',
          subtitle: 'o ',
          highlight: 'nascimento',
        ),
        SizedBox(
          height: 20,
        ),
        AppTextFieldWidget(
          controller: controller.ringDateController,
          label: 'Quantos dias para anilhar?',
          keyboardType: TextInputType.number,
        ),
        AppTextFieldWidget(
          controller: controller.separateDateController,
          label: 'Dias para separacão?',
          keyboardType: TextInputType.number,
        ),
        SizedBox(
          height: 20,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            AppButtonWidget(
              text: 'Nascer',
              color: Theme.of(context).primaryColor,
              onPressed: () {
                showModalBottomSheet(
                  isDismissible: false,
                  context: context,
                  builder: (_) {
                    return AppModalWidget(
                      title: 'Confirma o nascimento?',
                      textYes: 'Confirmar',
                      textNo: 'Cancelar',
                      onPressedYes: () =>
                          controller.onTapBirth(widget.egg, widget.eggs),
                      onPressedNo: () => Navigator.pop(context),
                    );
                  },
                );
              },
            ),
            AppTextWidget(
              text: 'Voltar',
              onTap: () => Navigator.pop(context),
            ),
          ],
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    controller.ringDateController.text = '7';
    controller.separateDateController.text = '30';
    return AppScaffoldWidget(
      appBar: appBar(context),
      body: body(context),
    );
  }
}
