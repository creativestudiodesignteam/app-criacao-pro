// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cage_egg_birth_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$CageEggBirthController on _CageEggBirthController, Store {
  final _$onTapBirthAsyncAction =
      AsyncAction('_CageEggBirthController.onTapBirth');

  @override
  Future<void> onTapBirth(CageEggModel egg, List<CageEggModel> eggs) {
    return _$onTapBirthAsyncAction.run(() => super.onTapBirth(egg, eggs));
  }

  @override
  String toString() {
    return '''

    ''';
  }
}
