import 'package:flutter_modular/flutter_modular.dart';
import 'package:dio/dio.dart';
import '../../baby/cage_baby_controller.dart';
import '../../baby/cage_baby_repository.dart';
import '../cage_egg_controller.dart';
import '../cage_egg_repository.dart';
import 'cage_egg_birth_controller.dart';
import 'cage_egg_birth_view.dart';

class CageEggBirthModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind(
          (i) => CageEggBirthController(
            cageEggController: i.get<CageEggController>(),
            cageBabyController: i.get<CageBabyController>(),
          ),
        ),
        Bind(
          (i) => CageEggController(
            cageEggRepository: i.get<CageEggRepository>(),
          ),
        ),
        Bind(
          (i) => CageBabyController(
            cageBabyRepository: i.get<CageBabyRepository>(),
          ),
        ),
        Bind(
          (i) => CageBabyRepository(
            dio: i.get<Dio>(),
          ),
        ),
        Bind(
          (i) => CageEggRepository(
            dio: i.get<Dio>(),
          ),
        ),
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(
          '/',
          child: (ctx, args) => CageEggBirthView(
            egg: args.data['egg'],
            eggs: args.data['eggs'],
          ),
        ),
      ];
}
