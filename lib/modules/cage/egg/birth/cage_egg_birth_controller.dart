import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:asuka/asuka.dart' as asuka;
import '../../../app/app_message_widget.dart';
import '../../baby/cage_baby_controller.dart';
import '../../baby/cage_baby_model.dart';
import '../cage_egg_controller.dart';
import '../cage_egg_model.dart';
part 'cage_egg_birth_controller.g.dart';

class CageEggBirthController = _CageEggBirthController
    with _$CageEggBirthController;

abstract class _CageEggBirthController with Store {
  _CageEggBirthController({
    this.cageEggController,
    this.cageBabyController,
  });

  final CageEggController cageEggController;
  final CageBabyController cageBabyController;

  final TextEditingController ringDateController = TextEditingController();
  final TextEditingController separateDateController = TextEditingController();

  @action
  Future<void> onTapBirth(CageEggModel egg, List<CageEggModel> eggs) async {
    // Modular.to.pop();
    var response;
    var now = DateTime.now().millisecondsSinceEpoch;
    var ringDate = int.tryParse(ringDateController.text);
    var ringDateDuration = now + Duration(days: ringDate).inMilliseconds;
    var separateDate = int.tryParse(separateDateController.text);
    var separateDateDuration =
        now + Duration(days: separateDate).inMilliseconds;
    if (eggs == null) {
      asuka.showSnackBar(
        SnackBar(
          content: AppMessageWidget(
            isLoading: true,
            message: 'Por favor, aguarde ...',
          ),
          duration: Duration(
            milliseconds: 800,
          ),
        ),
      );
      var baby = CageBabyModel(
        account: egg.account,
        birthDate: egg.birthDate,
        ringDate: ringDateDuration,
        separateDate: separateDateDuration,
        cage: egg.cage,
        mother: egg.mother,
        father: egg.father,
      );
      response = await cageBabyController.cageBabyRepository.createBaby(baby);
      if (response['type'] == 'success') {
        await cageBabyController.readAllBabies(egg.cage);
        await cageEggController.onTapDeleteEgg(egg.cage, egg.id, true, false);
        cageEggController.firstAcessInFunction = true;
        cageEggController.eggs = null;
        await cageEggController.readAllEggs(baby.cage);
      }
    } else {
      eggs.forEach((element) async {
        var baby = CageBabyModel(
          account: element.account,
          birthDate: element.birthDate,
          ringDate: ringDateDuration,
          separateDate: separateDateDuration,
          cage: element.cage,
          mother: element.mother,
          father: element.father,
        );
        response = await cageBabyController.cageBabyRepository.createBaby(baby);
        if (eggs[eggs.length - 1].id == element.id) {
          await cageEggController.onTapDeleteEgg(
              element.cage, element.id, true, true);
          await cageBabyController.readAllBabies(eggs[0].cage);
          cageEggController.firstAcessInFunction = true;
          cageEggController.eggs = null;
          await cageEggController.readAllEggs(eggs[0].cage);
          asuka.showSnackBar(
            SnackBar(
              content: AppMessageWidget(
                message: response['message'],
              ),
              duration: Duration(
                milliseconds: 1000,
              ),
            ),
          );
        } else {
          await cageEggController.onTapDeleteEgg(
              element.cage, element.id, false, false);
        }
      });
    }

    Modular.to.pop();
  }
}
