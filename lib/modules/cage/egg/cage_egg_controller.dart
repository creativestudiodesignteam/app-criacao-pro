import 'dart:async';
import 'dart:convert';
import 'package:criacao_pro/modules/bird/bird_controller.dart';
import 'package:criacao_pro/modules/bird/bird_model.dart';
import 'package:criacao_pro/modules/cage/cage_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:asuka/asuka.dart' as asuka;
import '../../account/account_controller.dart';
import '../../app/app_message_widget.dart';
import '../cage_model.dart';
import 'cage_egg_model.dart';
import 'cage_egg_repository.dart';
part 'cage_egg_controller.g.dart';

class CageEggController = _CageEggController with _$CageEggController;

abstract class _CageEggController with Store {
  _CageEggController({
    this.cageEggRepository,
    this.accountController,
    this.cageController,
  });

  final AccountController accountController;
  final CageController cageController;
  final CageEggRepository cageEggRepository;

  @observable
  List<CageEggModel> eggs;

  @observable
  List<CageEggModel> selectedEggs = [];

  @observable
  String stateEgg = '';

  @observable
  bool isTransferEgg = false;

  @observable
  bool isTransfer = false;

  @observable
  bool isAction = false;

  @observable
  int lastId = 1;

  @observable
  List<BirdModel> birds;

  @observable
  String totalEggs = '0';

  bool firstAcessInFunction = true;
  List<CageEggModel> eggsList;

  @action
  Future<void> onChangedCheckbox(CageEggModel egg) async {
    if (isAction) {
      if (!selectedEggs.contains(egg)) {
        if (selectedEggs.length == 0) {
          // ignore: avoid_function_literals_in_foreach_calls
          stateEgg = egg.state;
          isTransferEgg = !isTransferEgg;
          eggs.forEach((element) {
            if (egg.state != element.state) {
              element.select = false;
            }
          });
          await selectedEggs.add(egg);
        } else {
          var length = selectedEggs.length;
          for (var i = 0; i < length; i++) {
            if (selectedEggs[i].state == egg.state) {
              // ignore: avoid_function_literals_in_foreach_calls
              eggs.forEach((element) {
                if (egg.state != element.state) {
                  element.select = false;
                }
              });
            }
          }
          await selectedEggs.add(egg);
        }
      } else {
        selectedEggs.remove(egg);
        if (selectedEggs.length == 0) {
          stateEgg = '';
          isTransferEgg = !isTransferEgg;
          // ignore: avoid_function_literals_in_foreach_calls
          eggs.forEach((element) {
            element.select = true;
          });
        }
      }
    } else {
      if (!selectedEggs.contains(egg)) {
        selectedEggs.add(egg);
      }
    }
  }

  @action
  Future<void> onPressedTransfer() async {
    eggs.forEach((element) {
      element.select = true;
    });
    isTransfer = !isTransfer;
    isTransferEgg = false;
    isAction = isAction ? !isAction : isAction;
    selectedEggs = [];
  }

  @action
  Future<void> onPressedAction() async {
    isTransfer = isTransfer ? !isTransfer : isTransfer;
    selectedEggs = [];
    if (!isTransferEgg) {
      isAction = !isAction;
      stateEgg = stateEgg;
    } else {
      stateEgg = stateEgg;
    }
  }

  @action
  Future<void> onPressedTransferDone(BuildContext context) async {
    isTransfer = !isTransfer;
    if (selectedEggs.isEmpty) {
      asuka.showSnackBar(
        SnackBar(
          content: AppMessageWidget(
            message: 'Você não selecionou nenhum ovo.',
          ),
          duration: Duration(
            milliseconds: 3000,
          ),
        ),
      );
      return;
    }
    var cage = await Modular.to.pushNamed(
      '/cage',
      arguments: {
        'isSelect': true,
      },
    ) as CageModel;
    if (cage != null) {
      for (var egg in selectedEggs) {
        egg.cage = cage.id;
        await cageEggRepository.updateEgg(egg);
      }
      selectedEggs = [];
      asuka.showSnackBar(
        SnackBar(
          content: AppMessageWidget(
            message: 'Os ovos foram movidos com sucesso!',
          ),
          duration: Duration(
            milliseconds: 800,
          ),
        ),
      );
      eggs = null;
      firstAcessInFunction = true;
      cageController.firstAcessInFunction = true;
      cageController.cages = null;
      cageController.readAllCages('', false, true);
    }
  }

  @action
  Future<void> onPressedActionDone() async {
    if (stateEgg == 'Chocar') {
      Modular.to.pushNamed(
        '/cage/egg/hatch',
        arguments: {
          'egg': null,
          'eggs': selectedEggs,
        },
      );
    }
    if (stateEgg == 'Fertilizar') {
      onTapFertilize(selectedEggs);
    }
    if (stateEgg == 'Nasceu') {
      print(selectedEggs.length);
      Modular.to.pushNamed(
        '/cage/egg/birth',
        arguments: {
          'egg': null,
          'eggs': selectedEggs,
        },
      );
    }
    isAction = !isAction;
    isTransfer = false;
    isTransferEgg = false;
    selectedEggs = [];
  }

  Future<void> onTapFertilize(List<CageEggModel> eggsSelect) async {
    if (eggsSelect.isEmpty) {
      asuka.showSnackBar(
        SnackBar(
          content: AppMessageWidget(
            message: 'Você não selecionou nenhum ovo.',
          ),
          duration: Duration(
            milliseconds: 3000,
          ),
        ),
      );
      return;
    }
    var response;
    eggsSelect.forEach((element) async {
      element.state = 'Nasceu';
      response = await cageEggRepository.updateEgg(element);
      if (element.id == eggsSelect[eggsSelect.length - 1].id) {
        firstAcessInFunction = true;
        eggs = null;
        await readAllEggs(eggsSelect[0].cage);
        asuka.showSnackBar(
          SnackBar(
            content: AppMessageWidget(
              message: response['message'],
            ),
            duration: Duration(
              milliseconds: 1000,
            ),
          ),
        );
      }
    });
    isTransferEgg = false;
  }

  @action
  Future<void> onTapDeleteEgg(
      int cageId, int eggId, bool message, bool readEgg) async {
    print("message ### message");
    print(message);
    if (message == true) {
      Modular.to.pop();
      asuka.showSnackBar(
        SnackBar(
          content: AppMessageWidget(
            isLoading: true,
            message: 'Por favor, aguarde ...',
          ),
          duration: Duration(
            milliseconds: 800,
          ),
        ),
      );
    }
    var response = await cageEggRepository.deleteEgg(eggId);
    if (message == true) {
      asuka.showSnackBar(
        SnackBar(
          content: AppMessageWidget(
            message: response['message'],
          ),
          duration: Duration(
            milliseconds: 1000,
          ),
        ),
      );
    }
    if (readEgg == true) {
      firstAcessInFunction = true;
      eggs = null;
      await readAllEggs(cageId);
    }
  }

  @action
  Future<void> onTapAddEgg(int cageId) async {
    var femea;
    var femeaCount = 0;
    var macho;
    var machoCount = 0;
    for (var bird in birds) {
      if (bird.gender == "Macho") {
        machoCount += 1;
        macho = bird.id;
      }
      if (bird.gender == "Fêmea") {
        femeaCount = 1;
        femea = bird.id;
      }
    }
    var egg;
    if (machoCount == 1 && femeaCount == 1) {
      egg = CageEggModel(
        account: accountController.account.id,
        state: 'Chocar',
        layingDate: DateTime.now().millisecondsSinceEpoch,
        cage: cageId,
        father: macho,
        mother: femea,
      );
    } else if (machoCount == 0 && femeaCount == 1) {
      egg = CageEggModel(
        account: accountController.account.id,
        state: 'Chocar',
        layingDate: DateTime.now().millisecondsSinceEpoch,
        cage: cageId,
        mother: femea,
      );
    } else {
      egg = CageEggModel(
        account: accountController.account.id,
        state: 'Chocar',
        layingDate: DateTime.now().millisecondsSinceEpoch,
        cage: cageId,
      );
    }

    var response = await cageEggRepository.createEgg(egg);
    asuka.showSnackBar(
      SnackBar(
        content: AppMessageWidget(
          message: response['message'],
        ),
        duration: Duration(
          milliseconds: 1000,
        ),
      ),
    );
    firstAcessInFunction = true;
    eggs = null;
    await readAllEggs(cageId);
  }

  Future<void> readAllBirdsCage(int cage) async {
    var response = await cageEggRepository.readAllBirdsCage(cage);
    var data = json.decode(response['data']);
    List<dynamic> list = data;
    list.sort((a, b) {
      return a['name'].toLowerCase().compareTo(b['name'].toLowerCase());
    });
    birds = List<BirdModel>.from(data.map((e) {
      return BirdModel.fromJson(e);
    }).toList());
    for (var item in birds) {
      print("item");
      print(item.gender);
    }
  }

  @action
  Future<void> readAllEggs(int cageId) async {
    if (firstAcessInFunction) {
      print('readAllEggs');
      eggs = null;
      eggsList = List<CageEggModel>();
      lastId = 1;
      firstAcessInFunction = false;
    }
    var response =
        await cageEggRepository.readAllEggsPagination(cageId, lastId);
    var data = json.decode(response['data']);
    totalEggs = response['total'] != '' ? response['total'] : '0';
    var egg = List<CageEggModel>.from(
      data.map((e) {
        return CageEggModel.fromJson(e);
      }).toList(),
    );
    await egg.forEach((element) async {
      var addCage = true;
      if (element.id > lastId) {
        lastId = element.id;
      }

      eggsList.forEach((elementCage) {
        if (element.id == elementCage.id) {
          addCage = false;
        }
      });
      if (addCage == true) {
        await eggsList.add(element);
      }
    });
    eggs = eggsList;
  }
}
