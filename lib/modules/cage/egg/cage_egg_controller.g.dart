// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cage_egg_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$CageEggController on _CageEggController, Store {
  final _$eggsAtom = Atom(name: '_CageEggController.eggs');

  @override
  List<CageEggModel> get eggs {
    _$eggsAtom.reportRead();
    return super.eggs;
  }

  @override
  set eggs(List<CageEggModel> value) {
    _$eggsAtom.reportWrite(value, super.eggs, () {
      super.eggs = value;
    });
  }

  final _$stateEggAtom = Atom(name: '_CageEggController.stateEgg');

  @override
  String get stateEgg {
    _$stateEggAtom.reportRead();
    return super.stateEgg;
  }

  @override
  set stateEgg(String value) {
    _$stateEggAtom.reportWrite(value, super.stateEgg, () {
      super.stateEgg = value;
    });
  }

  final _$selectedEggsAtom = Atom(name: '_CageEggController.selectedEggs');

  @override
  List<CageEggModel> get selectedEggs {
    _$selectedEggsAtom.reportRead();
    return super.selectedEggs;
  }

  @override
  set selectedEggs(List<CageEggModel> value) {
    _$selectedEggsAtom.reportWrite(value, super.selectedEggs, () {
      super.selectedEggs = value;
    });
  }

  final _$isTransferAtom = Atom(name: '_CageEggController.isTransfer');

  @override
  bool get isTransfer {
    _$isTransferAtom.reportRead();
    return super.isTransfer;
  }

  @override
  set isTransfer(bool value) {
    _$isTransferAtom.reportWrite(value, super.isTransfer, () {
      super.isTransfer = value;
    });
  }

  final _$totalEggsAtom = Atom(name: '_CageEggController.totalEggs');

  @override
  String get totalEggs {
    _$totalEggsAtom.reportRead();
    return super.totalEggs;
  }

  @override
  set totalEggs(String value) {
    _$totalEggsAtom.reportWrite(value, super.totalEggs, () {
      super.totalEggs = value;
    });
  }

  final _$isActionAtom = Atom(name: '_CageEggController.isAction');

  @override
  bool get isAction {
    _$isTransferAtom.reportRead();
    return super.isAction;
  }

  @override
  set isAction(bool value) {
    _$isActionAtom.reportWrite(value, super.isAction, () {
      super.isAction = value;
    });
  }

  final _$onChangedCheckboxAsyncAction =
      AsyncAction('_CageEggController.onChangedCheckbox');

  @override
  Future<void> onChangedCheckbox(CageEggModel egg) {
    return _$onChangedCheckboxAsyncAction
        .run(() => super.onChangedCheckbox(egg));
  }

  final _$onPressedTransferAsyncAction =
      AsyncAction('_CageEggController.onPressedTransfer');

  @override
  Future<void> onPressedTransfer() {
    return _$onPressedTransferAsyncAction.run(() => super.onPressedTransfer());
  }

  final _$onPressedTransferDoneAsyncAction =
      AsyncAction('_CageEggController.onPressedTransferDone');

  @override
  Future<void> onPressedTransferDone(BuildContext context) {
    return _$onPressedTransferDoneAsyncAction
        .run(() => super.onPressedTransferDone(context));
  }

  final _$onPressedActionAsyncAction =
      AsyncAction('_CageEggController.onPressedAction');

  @override
  Future<void> onPressedAction() {
    return _$onPressedActionAsyncAction.run(() => super.onPressedAction());
  }

  final _$onPressedActionDoneAsyncAction =
      AsyncAction('_CageEggController.onPressedActionDone');

  @override
  Future<void> onPressedActionDone() {
    return _$onPressedActionDoneAsyncAction
        .run(() => super.onPressedActionDone());
  }

  final _$onTapDeleteEggAsyncAction =
      AsyncAction('_CageEggController.onTapDeleteEgg');

  @override
  Future<void> onTapDeleteEgg(
      int cageId, int eggId, bool message, bool readEgg) {
    return _$onTapDeleteEggAsyncAction
        .run(() => super.onTapDeleteEgg(cageId, eggId, message, readEgg));
  }

  final _$onTapAddEggAsyncAction =
      AsyncAction('_CageEggController.onTapAddEgg');

  @override
  Future<void> onTapAddEgg(int cageId) {
    return _$onTapAddEggAsyncAction.run(() => super.onTapAddEgg(cageId));
  }

  final _$readAllEggsAsyncAction =
      AsyncAction('_CageEggController.readAllEggs');

  @override
  Future<void> readAllEggs(int cageId) {
    return _$readAllEggsAsyncAction.run(() => super.readAllEggs(cageId));
  }

  final _$readAllBirdsCageAsyncAction =
      AsyncAction('_CageEggController.readAllBirdsCage');

  @override
  Future<void> readAllBirdsCage(int cageId) {
    return _$readAllBirdsCageAsyncAction
        .run(() => super.readAllBirdsCage(cageId));
  }

  @override
  String toString() {
    return '''
eggs: ${eggs},
selectedEggs: ${selectedEggs},
isTransfer: ${isTransfer},
totalEggs: ${totalEggs},
    ''';
  }
}
