import 'package:criacao_pro/modules/app/app_card_widget_egg.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../../app/app_button_widget.dart';
import '../../app/app_card_widget.dart';
import '../../app/app_text_widget.dart';
import 'cage_egg_model.dart';

class CageEggItemWidget extends StatelessWidget {
  const CageEggItemWidget({
    Key key,
    this.egg,
    this.isTransfer = false,
    this.isAction = false,
    this.onChangedCheckbox,
    this.onPressedDelete,
    this.onPressedUpgrade,
    this.stateEgg,
  }) : super(key: key);

  final CageEggModel egg;
  final bool isTransfer;
  final bool isAction;
  final void Function(CageEggModel) onChangedCheckbox;
  final Function onPressedDelete;
  final Function onPressedUpgrade;
  final String stateEgg;

  @override
  Widget build(BuildContext context) {
    var layingDate = DateTime.fromMillisecondsSinceEpoch(egg.layingDate);
    var layingDateFormatted =
        DateFormat('dd/MM/yyyy').format(layingDate).toString();
    return AppCardWidgetEgg(
      stateEgg: stateEgg,
      egg: egg,
      isTransfer: isTransfer,
      isAction: isAction,
      onChangedCheckbox: (value) => onChangedCheckbox(egg),
      color: Colors.white,
      image: 'lib/assets/egg-${egg.state.toLowerCase()}.png',
      children: [
        AppTextWidget(
          text: 'Postura efetuada',
          size: Theme.of(context).textTheme.subtitle1.fontSize,
          weight: FontWeight.bold,
        ),
        AppTextWidget(
          text: '${layingDateFormatted}',
          size: Theme.of(context).textTheme.bodyText1.fontSize,
        ),
        SizedBox(
          height: 10,
        ),
        Builder(
          builder: (_) {
            if (egg.hatchingDate > 0) {
              var hatchingDate =
                  DateTime.fromMillisecondsSinceEpoch(egg.hatchingDate);
              var hatchingDateFormatted =
                  DateFormat('dd/MM/yyyy').format(hatchingDate).toString();
              var birthDate =
                  DateTime.fromMillisecondsSinceEpoch(egg.birthDate);
              var birthDateFormatted =
                  DateFormat('dd/MM/yyyy').format(birthDate).toString();
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  AppTextWidget(
                    text: 'Chôco iniciado em',
                    size: Theme.of(context).textTheme.subtitle1.fontSize,
                    weight: FontWeight.bold,
                  ),
                  AppTextWidget(
                    text: '${hatchingDateFormatted}',
                    size: Theme.of(context).textTheme.bodyText1.fontSize,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  AppTextWidget(
                    text: 'Nascimento previsto',
                    size: Theme.of(context).textTheme.subtitle1.fontSize,
                    weight: FontWeight.bold,
                  ),
                  AppTextWidget(
                    text: '${birthDateFormatted}',
                    size: Theme.of(context).textTheme.bodyText1.fontSize,
                  ),
                ],
              );
            } else {
              return SizedBox();
            }
          },
        ),
        SizedBox(
          height: 10,
        ),
        Row(
          children: <Widget>[
            AppButtonWidget(
              color: Colors.white,
              textColor: Colors.black,
              paddingX: 0,
              paddingY: 0,
              text: 'Deletar',
              onPressed: onPressedDelete,
            ),
            SizedBox(
              width: 10,
            ),
            AppButtonWidget(
              color: Colors.green,
              textColor: Colors.white,
              paddingX: 0,
              paddingY: 0,
              text: egg.state,
              onPressed: onPressedUpgrade,
            ),
          ],
        ),
      ],
    );
  }
}
