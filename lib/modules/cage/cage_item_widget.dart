import 'dart:async';

import 'package:asuka/asuka.dart' as asuka;
import 'package:criacao_pro/modules/app/app_message_widget.dart';
import 'package:flutter/material.dart';

import '../app/app_card_widget.dart';
import '../app/app_text_widget.dart';
import 'cage_model.dart';

class CageItemWidget extends StatefulWidget {
  CageItemWidget({
    Key key,
    this.cage,
    this.isReport = false,
    this.isSelect = false,
    this.function,
    this.readAllCages,
    this.firstAcessInFunction,
    this.isTwoOptions = false,
  }) : super(key: key);

  final CageModel cage;
  final bool isReport;
  final bool isSelect;
  final bool isTwoOptions;
  bool firstAcessInFunction;
  final Function function;
  final Function readAllCages;
  final StreamController<String> events = StreamController<String>();

  @override
  CageItemWidgetState createState() => CageItemWidgetState();
}

class CageItemWidgetState extends State<CageItemWidget> {
  String _readTitle() {
    var title = widget.cage.name.isEmpty ? 'Sem Número' : widget.cage.name;
    return title;
  }

  @override
  Widget build(BuildContext context) {
    return AppCardWidget(
      color: Colors.white,
      children: [
        AppTextWidget(
          text: _readTitle(),
          size: Theme.of(context).textTheme.subtitle1.fontSize,
          weight: FontWeight.bold,
        ),
        AppTextWidget(
          text: 'Aves: ${widget.cage.birds}',
          size: Theme.of(context).textTheme.bodyText1.fontSize,
        ),
        AppTextWidget(
          text: 'Filhotes: ${widget.cage.babies}',
          size: Theme.of(context).textTheme.bodyText1.fontSize,
        ),
        AppTextWidget(
          text: 'Ovos: ${widget.cage.eggs}',
          size: Theme.of(context).textTheme.bodyText1.fontSize,
        ),
      ],
      image: 'lib/assets/cage-${widget.cage.image}.png',
      onPressed: () async {
        if (widget.isReport) {
          // reportViewCages(context, widget.cage);
          Navigator.pushNamed(
            context,
            '/cage/bird',
            arguments: {
              'cage': widget.cage,
              'isReport': widget.isReport,
              'isTwoOptions': widget.isTwoOptions
            },
          ).then((value) {
            widget.function();
          });
          if (widget.isTwoOptions) {
            asuka.showSnackBar(
              SnackBar(
                content: AppMessageWidget(
                  message: 'Selecione o casal para o relatório!',
                ),
                duration: Duration(
                  milliseconds: 2000,
                ),
              ),
            );
          }
        } else if (widget.isSelect) {
          // widget.firstAcessInFunction = true;
          Navigator.pop(context, widget.cage);
        } else {
          Navigator.pushNamed(
            context,
            '/cage/detail',
            arguments: {
              'cage': widget.cage,
            },
          ).then((value) {
            widget.function();
          });
        }
      },
    );
  }
}
