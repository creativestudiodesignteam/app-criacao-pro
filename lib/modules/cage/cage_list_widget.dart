import 'package:flutter/material.dart';

import 'cage_item_widget.dart';
import 'cage_model.dart';

class CageListWidget extends StatefulWidget {
  const CageListWidget({
    Key key,
    this.cages,
    this.isReport = false,
    this.isSelect = false,
    @required this.function,
    this.readAllCages,
    this.totalEggs,
    this.sc,
    this.firstAcessInFunction,
    this.isTwoOptions = false,
  }) : super(key: key);

  final List<CageModel> cages;
  final bool isReport;
  final bool isSelect;
  final bool isTwoOptions;
  final bool firstAcessInFunction;
  final Function function;
  final Function readAllCages;
  final String totalEggs;
  final ScrollController sc;

  @override
  _CageListWidgetState createState() => _CageListWidgetState();
}

class _CageListWidgetState extends State<CageListWidget> {
  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.75,
      child: ListView.builder(
        shrinkWrap: true,
        controller: widget.sc,
        itemCount: widget.cages.length + 1,
        itemBuilder: (_, i) {
          if (i == widget.cages.length) {
            if (int.parse(widget.totalEggs) == widget.cages.length) {
              isLoading = false;
            } else {
              isLoading = true;
            }
            return _buildProgressIndicator();
          } else {
            isLoading = false;
            return CageItemWidget(
                cage: widget.cages.elementAt(i),
                isReport: widget.isReport,
                isSelect: widget.isSelect,
                function: widget.function,
                readAllCages: widget.readAllCages,
                firstAcessInFunction: widget.firstAcessInFunction,
                isTwoOptions: widget.isTwoOptions);
          }
        },
      ),
    );
  }

  Widget _buildProgressIndicator() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Center(
        child: Opacity(
          opacity: isLoading ? 1.0 : 00,
          child: CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(
              Color(0xffe9ae24),
            ),
          ),
        ),
      ),
    );
  }
}
