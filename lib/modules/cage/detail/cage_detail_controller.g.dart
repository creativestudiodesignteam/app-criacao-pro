// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cage_detail_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$CageDetailController on _CageDetailController, Store {
  final _$stateAtom = Atom(name: '_CageDetailController.state');

  @override
  String get state {
    _$stateAtom.reportRead();
    return super.state;
  }

  @override
  set state(String value) {
    _$stateAtom.reportWrite(value, super.state, () {
      super.state = value;
    });
  }

  final _$nameAtom = Atom(name: '_CageDetailController.name');

  @override
  String get name {
    _$nameAtom.reportRead();
    return super.name;
  }

  @override
  set name(String value) {
    _$nameAtom.reportWrite(value, super.name, () {
      super.name = value;
    });
  }

  final _$onChangedStateAsyncAction =
      AsyncAction('_CageDetailController.onChangedState');

  @override
  Future<void> onChangedState(String value) {
    return _$onChangedStateAsyncAction.run(() => super.onChangedState(value));
  }

  final _$onTapUpdateCageAsyncAction =
      AsyncAction('_CageDetailController.onTapUpdateCage');

  @override
  Future<void> onTapUpdateCage(CageModel cage, BuildContext context) {
    return _$onTapUpdateCageAsyncAction
        .run(() => super.onTapUpdateCage(cage, context));
  }

  final _$onTapDeleteCageAsyncAction =
      AsyncAction('_CageDetailController.onTapDeleteCage');

  @override
  Future<void> onTapDeleteCage(int cageId) {
    return _$onTapDeleteCageAsyncAction
        .run(() => super.onTapDeleteCage(cageId));
  }

  @override
  String toString() {
    return '''
state: ${state}
name: ${name}
    ''';
  }
}
