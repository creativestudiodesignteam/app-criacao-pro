import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:asuka/asuka.dart' as asuka;
import '../../app/app_message_widget.dart';
import '../cage_controller.dart';
import '../cage_model.dart';
part 'cage_detail_controller.g.dart';

class CageDetailController = _CageDetailController with _$CageDetailController;

abstract class _CageDetailController with Store {
  _CageDetailController({
    this.cageController,
  });

  final CageController cageController;
  final TextEditingController nameController = TextEditingController();

  @observable
  String state;

  @observable
  String name;

  @action
  Future<void> onChangedState(String value) async {
    state = value;
  }

  @action
  Future<void> onTapUpdateCage(CageModel cage, BuildContext context) async {
    var update = true;
    Modular.to.pop();

    if (name != nameController.text) {
      cageController.cagesAll.forEach((element) {
        if (element.name == nameController.text) {
          update = false;
        }
      });
    }

    if (update) {
      cage.name = nameController.text;
      cage.state = state == null ? cage.state : state;
      asuka.showSnackBar(
        SnackBar(
          content: AppMessageWidget(
            isLoading: true,
            message: 'Por favor, aguarde ...',
          ),
          duration: Duration(
            milliseconds: 800,
          ),
        ),
      );
      var response = await cageController.cageRepository.updateCage(cage);
      // cageController.readAllCages('', false, true);
      if (response['type'] == 'success') {
        Modular.to.pop();
      }
      asuka.showSnackBar(
        SnackBar(
          content: AppMessageWidget(
            message: response['message'],
          ),
          duration: Duration(
            milliseconds: 1000,
          ),
        ),
      );
    } else if (!update) {
      Flushbar(
        flushbarPosition: FlushbarPosition.TOP,
        message: 'Já existe este número de gaiola. Tente outro.',
        title: '',
        duration: Duration(
          milliseconds: 2000,
        ),
        icon: Image.asset(
          'lib/assets/menu-1.png',
          width: 48,
          height: 48,
        ),
      )..show(context);
    }
  }

  @action
  Future<void> onTapDeleteCage(int cageId) async {
    Modular.to.pop();
    asuka.showSnackBar(
      SnackBar(
        content: AppMessageWidget(
          isLoading: true,
          message: 'Por favor, aguarde ...',
        ),
        duration: Duration(
          milliseconds: 3000,
        ),
      ),
    );
    var response = await cageController.cageRepository.deleteCage(cageId);
    cageController.readAllCages('', false, true);
    Modular.to.pop();
    asuka.showSnackBar(
      SnackBar(
        content: AppMessageWidget(
          message: response['message'],
        ),
        duration: Duration(
          milliseconds: 1000,
        ),
      ),
    );
  }
}
