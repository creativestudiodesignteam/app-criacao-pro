import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../app/app_action_button_widget.dart';
import '../../app/app_appbar_widget.dart';
import '../../app/app_button_widget.dart';
import '../../app/app_dropdown_widget.dart';
import '../../app/app_header_widget.dart';
import '../../app/app_modal_widget.dart';
import '../../app/app_scaffold_widget.dart';
import '../../app/app_sub_menu_widget.dart';
import '../../app/app_text_widget.dart';
import '../../app/app_textfield_widget.dart';
import '../cage_model.dart';
import 'cage_detail_controller.dart';

class CageDetailView extends StatefulWidget {
  const CageDetailView({
    Key key,
    this.cage,
  }) : super(key: key);

  final CageModel cage;

  @override
  _CageDetailViewState createState() => _CageDetailViewState();
}

class _CageDetailViewState
    extends ModularState<CageDetailView, CageDetailController> {
  Widget appBar(BuildContext context) {
    return AppAppBarWidget(
      titles: <Widget>[
        AppTextWidget(
          text: 'Detalhes da gaiola',
          size: Theme.of(context).textTheme.subtitle2.fontSize,
        ),
      ],
      actions: <Widget>[
        IconButton(
          icon: Icon(
            Icons.chevron_left,
          ),
          onPressed: () => Navigator.pop(context),
        ),
      ],
    );
  }

  Widget body(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            AppSubMenuWidget(
              image: 'lib/assets/cage-sub-1.png',
              text: 'Detalhes',
              selected: true,
            ),
            AppSubMenuWidget(
              image: 'lib/assets/cage-sub-2.png',
              text: 'Aves',
              selected: false,
              onPressed: () {
                Navigator.pop(context);
                Navigator.pushNamed(
                  context,
                  '/cage/bird',
                  arguments: {'cage': widget.cage, 'isReport': false},
                );
              },
            ),
            AppSubMenuWidget(
              image: 'lib/assets/cage-sub-4.png',
              text: 'Ovos',
              selected: false,
              onPressed: () {
                Navigator.pop(context);
                Navigator.pushNamed(
                  context,
                  '/cage/egg',
                  arguments: {'cage': widget.cage},
                );
              },
            ),
            AppSubMenuWidget(
              image: 'lib/assets/cage-sub-3.png',
              text: 'Filhotes',
              selected: false,
              onPressed: () {
                Navigator.pop(context);
                Navigator.pushNamed(
                  context,
                  '/cage/baby',
                  arguments: {'cage': widget.cage},
                );
              },
            ),
          ],
        ),
        SizedBox(
          height: 20,
        ),
        AppHeaderWidget(
          title: 'Detalhes',
          subtitle: 'da sua ',
          highlight: 'gaiola',
        ),
        SizedBox(
          height: 20,
        ),
        AppDropdownWidget(
          label: 'Status',
          value: widget.cage.state,
          items: [
            'Ativa',
            'Inativa',
          ],
          onChanged: controller.onChangedState,
        ),
        AppTextFieldWidget(
          // keyboardType: TextInputType.number,
          label: 'Número',
          controller: controller.nameController,
        ),
        SizedBox(
          height: 20,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            AppButtonWidget(
              text: 'Salvar informacões',
              color: Theme.of(context).primaryColor,
              onPressed: () {
                showModalBottomSheet(
                  isDismissible: false,
                  context: context,
                  builder: (_) {
                    return AppModalWidget(
                      title: 'Quer mesmo atualizar a gaiola?',
                      textYes: 'Salvar',
                      textNo: 'Cancelar',
                      onPressedYes: () =>
                          controller.onTapUpdateCage(widget.cage, context),
                      onPressedNo: () => Modular.to.pop(),
                    );
                  },
                );
              },
            ),
            AppTextWidget(
              text: 'Voltar',
              onTap: () =>
                  Navigator.popUntil(context, ModalRoute.withName('/cage')),
            ),
          ],
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    controller.nameController.text = widget.cage.name;
    controller.name = widget.cage.name;
    controller.state = widget.cage.state;
    return AppScaffoldWidget(
      appBar: appBar(context),
      body: body(context),
      floatingActionButton: AppActionAppButtonWidget(
        color: Colors.redAccent,
        label: 'Deletar gaiola',
        icon: Icons.close,
        onPressed: () {
          showModalBottomSheet(
            isDismissible: false,
            context: context,
            builder: (_) {
              if (widget.cage.birds > 0 ||
                  widget.cage.eggs > 0 ||
                  widget.cage.babies > 0) {
                return AppModalWidget(
                    title: 'Para excluir a gaiola, ela precisa estar vazia.',
                    textYes: 'Ok, entendi.',
                    onPressedYes: () => Navigator.pop(context));
              } else {
                return AppModalWidget(
                  title: 'Quer mesmo deletar a gaiola?',
                  textYes: 'Deletar',
                  textNo: 'Cancelar',
                  onPressedYes: () =>
                      controller.onTapDeleteCage(widget.cage.id),
                  onPressedNo: () => Navigator.pop(context),
                );
              }
            },
          );
        },
      ),
    );
  }
}
