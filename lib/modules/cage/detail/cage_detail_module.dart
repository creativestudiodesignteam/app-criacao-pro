import 'package:flutter_modular/flutter_modular.dart';
import '../cage_controller.dart';
import 'cage_detail_controller.dart';
import 'cage_detail_view.dart';

class CageDetailModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind(
          (i) => CageDetailController(
            cageController: i.get<CageController>(),
          ),
        ),
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(
          '/',
          child: (ctx, args) => CageDetailView(
            cage: args.data['cage'],
          ),
        ),
      ];
}
