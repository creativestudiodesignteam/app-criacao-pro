import 'package:flutter_modular/flutter_modular.dart';
import 'account_view.dart';
import 'create/account_create_module.dart';
import 'menu/account_menu_module.dart';
import 'profile/account_profile_module.dart';
import 'recovery/account_recovery_module.dart';

class AccountModule extends ChildModule {
  @override
  List<Bind> get binds => [];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(
          '/',
          child: (ctx, args) => AccountView(),
        ),
        ModularRouter(
          '/create',
          module: AccountCreateModule(),
        ),
        ModularRouter(
          '/menu',
          module: AccountMenuModule(),
        ),
        ModularRouter(
          '/profile',
          module: AccountProfileModule(),
        ),
        ModularRouter(
          '/recovery',
          module: AccountRecoveryModule(),
        ),
      ];
}
