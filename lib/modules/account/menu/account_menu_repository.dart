import 'account_menu_model.dart';

class AccountMenuRepository {
  Future<List<AccountMenuModel>> readAllMenus() async {
    return [
      AccountMenuModel(
        id: 1,
        name: 'Aves',
        image: 'lib/assets/menu-1.png',
        route: '/bird',
      ),
      AccountMenuModel(
        id: 2,
        name: 'Gaiolas',
        image: 'lib/assets/menu-2.png',
        route: '/cage',
      ),
      AccountMenuModel(
        id: 3,
        name: 'Alertas',
        image: 'lib/assets/menu-3.png',
        route: '/alert',
      ),
      AccountMenuModel(
        id: 4,
        name: 'Relatórios',
        image: 'lib/assets/menu-4.png',
        route: '/report',
      ),
      AccountMenuModel(
        id: 5,
        name: 'Parcerias',
        image: 'lib/assets/menu-5.png',
        route: '/partner',
      ),
      AccountMenuModel(
        id: 6,
        name: 'Aves à venda',
        image: 'lib/assets/menu-6.png',
        route: null,
      ),
    ];
  }
}
