class AccountMenuModel {
  AccountMenuModel({
    this.id,
    this.name,
    this.image,
    this.route,
  });

  int id;
  String name;
  String image;
  String route;
}
