import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import '../../app/app_controller.dart';
import '../account_controller.dart';
import 'account_menu_model.dart';
import 'account_menu_repository.dart';
part 'account_menu_controller.g.dart';

class AccountMenuController = _AccountMenuController
    with _$AccountMenuController;

abstract class _AccountMenuController with Store {
  _AccountMenuController({
    this.accountMenuRepository,
    this.accountController,
    this.appController,
  });

  final AppController appController;
  final AccountMenuRepository accountMenuRepository;
  final AccountController accountController;

  @observable
  List<AccountMenuModel> menus;

  Future<void> onTapBack() async {
    Modular.to.pop();
  }

  Future<void> onTapExit() async {
    Modular.to.pushReplacementNamed('/');
  }

  Future<void> onTapAccount() async {
    Modular.to.pushNamed('/account/profile');
  }

  @action
  Future<void> readAllMenus() async {
    menus = await accountMenuRepository.readAllMenus();
  }
}
