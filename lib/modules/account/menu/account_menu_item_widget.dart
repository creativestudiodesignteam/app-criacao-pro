import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../app/app_modal_widget.dart';
import '../../app/app_text_widget.dart';
import 'account_menu_model.dart';

class AccountMenuItemWidget extends StatelessWidget {
  const AccountMenuItemWidget({
    Key key,
    this.menu,
  }) : super(key: key);

  final AccountMenuModel menu;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        if (menu.route != null) {
          Modular.to.pushNamed(
            menu.route,
            arguments: {
              'bird': null,
              'isReport': false,
              'isSelect': false,
            },
          );
        } else {
          showModalBottomSheet(
              context: context,
              isDismissible: true,
              builder: (_) {
                return AppModalWidget(
                    title: 'Em breve esse recurso estará disponível.',
                    textYes: 'Ok, entendi.',
                    onPressedYes: () => Navigator.pop(context));
              });
        }
      },
      child: Container(
        margin: EdgeInsets.all(10),
        decoration: BoxDecoration(
          boxShadow: <BoxShadow>[
            BoxShadow(
              offset: Offset(2, 2),
              spreadRadius: -3,
              blurRadius: 3,
              color: Colors.grey[700],
            ),
          ],
          color: Colors.white,
          borderRadius: BorderRadius.circular(20),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              menu.image,
              width: 80,
              height: 80,
            ),
            SizedBox(
              height: 5,
            ),
            AppTextWidget(
              text: menu.name,
              weight: FontWeight.bold,
            ),
          ],
        ),
      ),
    );
  }
}
