import 'package:flutter/material.dart';
import 'account_menu_item_widget.dart';
import 'account_menu_model.dart';

class AccountMenuListWidget extends StatelessWidget {
  const AccountMenuListWidget({
    Key key,
    this.menus,
  }) : super(key: key);

  final List<AccountMenuModel> menus;

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        childAspectRatio: 1,
      ),
      itemCount: menus.length,
      itemBuilder: (_, i) {
        return AccountMenuItemWidget(
          menu: menus.elementAt(i),
        );
      },
    );
  }
}
