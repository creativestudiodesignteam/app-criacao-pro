// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'account_menu_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$AccountMenuController on _AccountMenuController, Store {
  final _$menusAtom = Atom(name: '_AccountMenuController.menus');

  @override
  List<AccountMenuModel> get menus {
    _$menusAtom.reportRead();
    return super.menus;
  }

  @override
  set menus(List<AccountMenuModel> value) {
    _$menusAtom.reportWrite(value, super.menus, () {
      super.menus = value;
    });
  }

  final _$readAllMenusAsyncAction =
      AsyncAction('_AccountMenuController.readAllMenus');

  @override
  Future<void> readAllMenus() {
    return _$readAllMenusAsyncAction.run(() => super.readAllMenus());
  }

  @override
  String toString() {
    return '''
menus: ${menus}
    ''';
  }
}
