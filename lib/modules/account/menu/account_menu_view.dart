import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import '../../../constants.dart' as constants;
import '../../account/account_model.dart';
import '../../app/app_appbar_widget.dart';
import '../../app/app_header_widget.dart';
import '../../app/app_modal_widget.dart';
import '../../app/app_scaffold_widget.dart';
import '../../app/app_text_widget.dart';
import 'account_menu_controller.dart';
import 'account_menu_list_widget.dart';

class AccountMenuView extends StatefulWidget {
  const AccountMenuView({
    Key key,
    this.account,
  }) : super(key: key);

  final AccountModel account;

  @override
  _AccountMenuViewState createState() => _AccountMenuViewState();
}

class _AccountMenuViewState
    extends ModularState<AccountMenuView, AccountMenuController> {
  Widget appBar(BuildContext context) {
    var title = constants.appTitle.split(' ');
    return AppAppBarWidget(
      titles: <Widget>[
        AppTextWidget(
          text: title[0],
          size: Theme.of(context).textTheme.subtitle2.fontSize,
        ),
        SizedBox(
          width: 5,
        ),
        AppTextWidget(
          text: title[1],
          size: Theme.of(context).textTheme.subtitle2.fontSize,
          weight: FontWeight.bold,
        ),
      ],
      actions: <Widget>[
        IconButton(
          icon: Icon(
            Icons.exit_to_app,
          ),
          onPressed: () {
            showModalBottomSheet(
              isDismissible: false,
              context: context,
              builder: (_) {
                return AppModalWidget(
                  title: 'Quer mesmo sair da sua conta?',
                  textYes: 'Sair',
                  textNo: 'Voltar',
                  onPressedYes: controller.onTapExit,
                  onPressedNo: controller.onTapBack,
                );
              },
            );
          },
        ),
        IconButton(
          icon: Icon(
            Icons.person,
          ),
          onPressed: controller.onTapAccount,
        ),
      ],
    );
  }

  Widget body() {
    return Column(
      children: <Widget>[
        AppHeaderWidget(
          title: 'Seja bem vindo(a)!',
          subtitle: '',
          highlight: controller.accountController.account.name,
        ),
        SizedBox(
          height: 20,
        ),
        Observer(
          builder: (_) {
            if (controller.menus == null) {
              return AppTextWidget(
                text: 'Buscando menus...',
              );
            } else if (controller.menus.isEmpty) {
              return AppTextWidget(
                text: 'Nenhum menu encontrado.',
              );
            } else {
              return AccountMenuListWidget(
                menus: controller.menus,
              );
            }
          },
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    var babiesToRing = controller.accountController.account.babies
        .where((e) =>
            e.ringDate >=
            DateTime.now().subtract(Duration(days: 3)).millisecondsSinceEpoch)
        .toList();
    var babiesToSeparate = controller.accountController.account.babies
        .where((e) =>
            e.separateDate >=
            DateTime.now().subtract(Duration(days: 3)).millisecondsSinceEpoch)
        .toList();
    var eggsToBirth = controller.accountController.account.eggs
        .where((e) =>
            e.hatchingDate >=
            DateTime.now().subtract(Duration(days: 3)).millisecondsSinceEpoch)
        .toList();
    var eggsToCadling = controller.accountController.account.eggs
        .where((e) =>
            e.candlingDate >=
            DateTime.now().subtract(Duration(days: 3)).millisecondsSinceEpoch)
        .toList();
    var count = babiesToRing.length +
        babiesToSeparate.length +
        eggsToBirth.length +
        eggsToCadling.length;
    if (count > 0) {
      controller.appController.showNotificationWithDefaultSound(count);
    }
    controller.readAllMenus();
    return AppScaffoldWidget(
      appBar: appBar(context),
      body: body(),
    );
  }
}
