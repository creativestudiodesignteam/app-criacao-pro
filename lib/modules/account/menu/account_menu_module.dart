import 'package:flutter_modular/flutter_modular.dart';
import '../../app/app_controller.dart';
import '../account_controller.dart';
import 'account_menu_controller.dart';
import 'account_menu_repository.dart';
import 'account_menu_view.dart';

class AccountMenuModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind(
          (i) => AccountMenuController(
            accountMenuRepository: i.get<AccountMenuRepository>(),
            accountController: i.get<AccountController>(),
            appController: i.get<AppController>(),
          ),
        ),
        Bind(
          (i) => AccountMenuRepository(),
        ),
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(
          '/',
          child: (ctx, args) => AccountMenuView(),
        ),
      ];
}
