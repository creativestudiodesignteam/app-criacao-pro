import 'package:flutter_modular/flutter_modular.dart';
import '../../account_repository.dart';
import 'account_recovery_redefine_controller.dart';
import 'account_recovery_redefine_view.dart';

class AccountRecoveryRedefineModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind(
          (i) => AccountRecoveryRedefineController(
            accountRepository: i.get<AccountRepository>(),
          ),
        ),
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(
          '/',
          child: (ctx, args) => AccountRecoveryRedefineView(
            email: args.data['email'],
            code: args.data['code'],
          ),
        ),
      ];
}
