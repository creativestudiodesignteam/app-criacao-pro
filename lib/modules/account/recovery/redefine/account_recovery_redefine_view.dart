import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import '../../../app/app_appbar_widget.dart';
import '../../../app/app_button_widget.dart';
import '../../../app/app_logo_widget.dart';
import '../../../app/app_scaffold_widget.dart';
import '../../../app/app_text_widget.dart';
import '../../../app/app_textfield_widget.dart';
import 'account_recovery_redefine_controller.dart';

class AccountRecoveryRedefineView extends StatefulWidget {
  const AccountRecoveryRedefineView({
    Key key,
    this.email,
    this.code,
  }) : super(key: key);

  final String email;
  final String code;

  @override
  _AccountRecoveryRedefineViewState createState() =>
      _AccountRecoveryRedefineViewState();
}

class _AccountRecoveryRedefineViewState extends ModularState<
    AccountRecoveryRedefineView, AccountRecoveryRedefineController> {
  Widget appBar(BuildContext context) {
    return AppAppBarWidget(
      titles: <Widget>[
        AppTextWidget(
          text: 'Redefinir senha',
          size: Theme.of(context).textTheme.subtitle2.fontSize,
        ),
      ],
      actions: <Widget>[
        IconButton(
          icon: Icon(
            Icons.chevron_left,
          ),
          onPressed: controller.onTapBack,
        ),
      ],
    );
  }

  Widget body(BuildContext context) {
    return Column(
      children: <Widget>[
        AppLogoWidget(),
        AppTextWidget(
          text: 'Enviamos um código para seu email!',
          size: Theme.of(context).textTheme.subtitle1.fontSize,
          weight: FontWeight.bold,
        ),
        AppTextFieldWidget(
          controller: controller.emailController,
          keyboardType: TextInputType.emailAddress,
          label: 'E-mail',
        ),
        AppTextFieldWidget(
          controller: controller.codeController,
          keyboardType: TextInputType.text,
          label: 'Código',
        ),
        AppTextFieldWidget(
          controller: controller.passwordController,
          keyboardType: TextInputType.text,
          obscureText: true,
          label: 'Nova senha',
        ),
        AppTextFieldWidget(
          controller: controller.confirmPasswordController,
          keyboardType: TextInputType.text,
          obscureText: true,
          label: 'Confirme nova senha',
        ),
        SizedBox(
          height: 20,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            AppButtonWidget(
              text: 'Alterar senha',
              color: Theme.of(context).primaryColor,
              onPressed: () =>
                  controller.onTapUpdatePassword(widget.email, widget.code),
            ),
            AppTextWidget(
              text: 'Voltar',
              onTap: controller.onTapBack,
            ),
          ],
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return AppScaffoldWidget(
      appBar: appBar(context),
      body: body(context),
    );
  }
}
