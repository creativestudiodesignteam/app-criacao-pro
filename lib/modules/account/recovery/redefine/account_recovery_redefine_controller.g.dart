// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'account_recovery_redefine_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$AccountRecoveryRedefineController
    on _AccountRecoveryRedefineController, Store {
  final _$onTapBackAsyncAction =
      AsyncAction('_AccountRecoveryRedefineController.onTapBack');

  @override
  Future<void> onTapBack() {
    return _$onTapBackAsyncAction.run(() => super.onTapBack());
  }

  final _$onTapUpdatePasswordAsyncAction =
      AsyncAction('_AccountRecoveryRedefineController.onTapUpdatePassword');

  @override
  Future<void> onTapUpdatePassword(String sentEmail, String sentCode) {
    return _$onTapUpdatePasswordAsyncAction
        .run(() => super.onTapUpdatePassword(sentEmail, sentCode));
  }

  @override
  String toString() {
    return '''

    ''';
  }
}
