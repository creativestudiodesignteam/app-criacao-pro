import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:asuka/asuka.dart' as asuka;
import '../../../app/app_message_widget.dart';
import '../../account_repository.dart';
part 'account_recovery_redefine_controller.g.dart';

class AccountRecoveryRedefineController = _AccountRecoveryRedefineController
    with _$AccountRecoveryRedefineController;

abstract class _AccountRecoveryRedefineController with Store {
  _AccountRecoveryRedefineController({
    this.accountRepository,
  });

  final AccountRepository accountRepository;
  final TextEditingController emailController = TextEditingController();
  final TextEditingController codeController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final TextEditingController confirmPasswordController =
      TextEditingController();

  @action
  Future<void> onTapBack() async {
    Modular.to.pop();
  }

  @action
  Future<void> onTapUpdatePassword(String sentEmail, String sentCode) async {
    asuka.showSnackBar(
      SnackBar(
        content: AppMessageWidget(
          isLoading: true,
          message: 'Por favor, aguarde ...',
        ),
        duration: Duration(
          milliseconds: 800,
        ),
      ),
    );
    var email = emailController.text;
    var code = codeController.text;
    var password = passwordController.text;
    var confirmPassword = confirmPasswordController.text;
    if (password != confirmPassword) {
      asuka.showSnackBar(
        SnackBar(
          content: AppMessageWidget(
            message: 'A senha deve ser igual nos dois campos.',
          ),
          duration: Duration(
            milliseconds: 1000,
          ),
        ),
      );
      return;
    }
    if (email != sentEmail) {
      asuka.showSnackBar(
        SnackBar(
          content: AppMessageWidget(
            message: 'Email inválido.',
          ),
          duration: Duration(
            milliseconds: 1000,
          ),
        ),
      );
      return;
    }
    if (code != sentCode) {
      asuka.showSnackBar(
        SnackBar(
          content: AppMessageWidget(
            message: 'Codigo invalido.',
          ),
          duration: Duration(
            milliseconds: 1000,
          ),
        ),
      );
      return;
    }
    var response =
        await accountRepository.recoveryAccountChange(email, code, password);
    Modular.to.pop();
    Modular.to.pop();
    asuka.showSnackBar(
      SnackBar(
        content: AppMessageWidget(
          message: response['message'],
        ),
        duration: Duration(
          milliseconds: 1000,
        ),
      ),
    );
  }
}
