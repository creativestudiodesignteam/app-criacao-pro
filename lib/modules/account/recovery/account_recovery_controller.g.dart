// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'account_recovery_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$AccountRecoveryController on _AccountRecoveryController, Store {
  final _$onTapBackAsyncAction =
      AsyncAction('_AccountRecoveryController.onTapBack');

  @override
  Future<void> onTapBack() {
    return _$onTapBackAsyncAction.run(() => super.onTapBack());
  }

  final _$onTapRecoverAsyncAction =
      AsyncAction('_AccountRecoveryController.onTapRecover');

  @override
  Future<void> onTapRecover() {
    return _$onTapRecoverAsyncAction.run(() => super.onTapRecover());
  }

  @override
  String toString() {
    return '''

    ''';
  }
}
