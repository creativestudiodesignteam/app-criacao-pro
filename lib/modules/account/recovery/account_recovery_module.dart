import 'package:flutter_modular/flutter_modular.dart';
import '../account_repository.dart';
import 'account_recovery_controller.dart';
import 'account_recovery_view.dart';
import 'redefine/account_recovery_redefine_module.dart';

class AccountRecoveryModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind(
          (i) => AccountRecoveryController(
            accountRepository: i.get<AccountRepository>(),
          ),
        ),
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(
          '/',
          child: (ctx, args) => AccountRecoveryView(),
        ),
        ModularRouter(
          '/redefine',
          module: AccountRecoveryRedefineModule(),
        ),
      ];
}
