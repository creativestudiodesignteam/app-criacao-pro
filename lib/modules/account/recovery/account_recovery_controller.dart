import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:asuka/asuka.dart' as asuka;
import '../../app/app_message_widget.dart';
import '../account_repository.dart';
part 'account_recovery_controller.g.dart';

class AccountRecoveryController = _AccountRecoveryController
    with _$AccountRecoveryController;

abstract class _AccountRecoveryController with Store {
  _AccountRecoveryController({
    this.accountRepository,
  });

  final AccountRepository accountRepository;
  final TextEditingController emailController = TextEditingController();

  @action
  Future<void> onTapBack() async {
    Modular.to.pop();
  }

  @action
  Future<void> onTapRecover() async {
    var email = emailController.text;
    asuka.showSnackBar(
      SnackBar(
        content: AppMessageWidget(
          isLoading: true,
          message: 'Por favor, aguarde ...',
        ),
        duration: Duration(
          milliseconds: 800,
        ),
      ),
    );
    var response = await accountRepository.recoveryAccountSend(email);
    if (response['type'] != 'success') {
      asuka.showSnackBar(
        SnackBar(
          content: AppMessageWidget(
            message: response['message'],
          ),
          duration: Duration(
            milliseconds: 1000,
          ),
        ),
      );
      return;
    }
    var data = json.decode(response['data']);
    Modular.to.pushNamed('/account/recovery/redefine', arguments: {
      'email': email,
      'code': data['password'],
    });
  }
}
