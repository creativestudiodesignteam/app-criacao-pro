import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import '../../app/app_appbar_widget.dart';
import '../../app/app_button_widget.dart';
import '../../app/app_logo_widget.dart';
import '../../app/app_scaffold_widget.dart';
import '../../app/app_text_widget.dart';
import '../../app/app_textfield_widget.dart';
import 'account_recovery_controller.dart';

class AccountRecoveryView extends StatefulWidget {
  @override
  _AccountRecoveryViewState createState() => _AccountRecoveryViewState();
}

class _AccountRecoveryViewState
    extends ModularState<AccountRecoveryView, AccountRecoveryController> {
  Widget appBar(BuildContext context) {
    return AppAppBarWidget(
      titles: <Widget>[
        AppTextWidget(
          text: 'Esqueci minha senha',
          size: Theme.of(context).textTheme.subtitle2.fontSize,
        ),
      ],
      actions: <Widget>[
        IconButton(
          icon: Icon(
            Icons.chevron_left,
          ),
          onPressed: controller.onTapBack,
        ),
      ],
    );
  }

  Widget body(BuildContext context) {
    return Column(
      children: <Widget>[
        AppLogoWidget(),
        AppTextFieldWidget(
          controller: controller.emailController,
          keyboardType: TextInputType.emailAddress,
          label: 'E-mail',
        ),
        SizedBox(
          height: 20,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            AppButtonWidget(
              text: 'Recuperar senha',
              color: Theme.of(context).primaryColor,
              onPressed: controller.onTapRecover,
            ),
            AppTextWidget(
              text: 'Voltar',
              onTap: controller.onTapBack,
            ),
          ],
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return AppScaffoldWidget(
      appBar: appBar(context),
      body: body(context),
    );
  }
}
