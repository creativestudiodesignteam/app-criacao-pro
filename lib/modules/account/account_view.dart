import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import '../app/app_appbar_widget.dart';
import '../app/app_button_widget.dart';
import '../app/app_logo_widget.dart';
import '../app/app_scaffold_widget.dart';
import '../app/app_text_widget.dart';
import '../app/app_textfield_widget.dart';
import 'account_controller.dart';

class AccountView extends StatefulWidget {
  @override
  _AccountViewState createState() => _AccountViewState();
}

class _AccountViewState extends ModularState<AccountView, AccountController> {

  FocusNode fn = FocusNode();

  Widget appBar(BuildContext context) {
    return AppAppBarWidget(
      titles: <Widget>[
        AppTextWidget(
          text: 'Entrar',
          size: Theme.of(context).textTheme.subtitle2.fontSize,
        ),
      ],
      actions: <Widget>[
        IconButton(
          icon: Icon(
            Icons.chevron_left,
          ),
          onPressed: controller.onTapBack,
        ),
      ],
    );
  }

  Widget body(BuildContext context) {
    return Column(
      children: <Widget>[
        AppLogoWidget(),
        AppTextFieldWidget(
          controller: controller.emailController,
          keyboardType: TextInputType.emailAddress,
          label: 'E-mail',
          onSubmit: (){
            FocusScope.of(context).requestFocus(fn);
          },
        ),
        AppTextFieldWidget(
          focusNode: fn,
          controller: controller.passwordController,
          keyboardType: TextInputType.text,
          obscureText: true,
          label: 'Senha',
          onSubmit: controller.onTapAccessApp,
        ),
        SizedBox(
          height: 20,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            AppButtonWidget(
              text: 'Acessar app',
              color: Theme.of(context).primaryColor,
              onPressed: controller.onTapAccessApp,
            ),
            AppButtonWidget(
              text: 'Cadastrar',
              color: Theme.of(context).accentColor,
              onPressed: controller.onTapRegister,
            ),
          ],
        ),
        SizedBox(
          height: 30,
        ),
        AppTextWidget(
          text: 'Esqueci minha senha',
          size: Theme.of(context).textTheme.subtitle1.fontSize,
          onTap: controller.onTapForgotPassword,
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    controller.getAccountPrefs();
    return AppScaffoldWidget(
      appBar: appBar(context),
      body: body(context),
    );
  }
}
