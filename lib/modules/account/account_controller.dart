import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:asuka/asuka.dart' as asuka;
import 'package:shared_preferences/shared_preferences.dart';
import '../app/app_message_widget.dart';
import '../cage/baby/cage_baby_model.dart';
import '../cage/egg/cage_egg_model.dart';
import 'account_model.dart';
import 'account_repository.dart';
part 'account_controller.g.dart';

class AccountController = _AccountController with _$AccountController;

abstract class _AccountController with Store {
  _AccountController({
    this.accountRepository,
  });

  final AccountRepository accountRepository;
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();

  @observable
  AccountModel account;

  Future<void> setAccountPrefs(String email, String password) async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setString('email', email);
    prefs.setString('password', password);
  }

  Future<void> getAccountPrefs() async {
    var prefs = await SharedPreferences.getInstance();
    emailController.text = prefs.getString('email') ?? '';
    passwordController.text = prefs.getString('password') ?? '';
  }

  @action
  Future<void> onTapAccessApp() async {
    var email = emailController.text;
    var password = passwordController.text;
    setAccountPrefs(email, password);
    asuka.showSnackBar(
      SnackBar(
        content: AppMessageWidget(
          isLoading: true,
          message: 'Por favor, aguarde ...',
        ),
        duration: Duration(
          milliseconds: 800,
        ),
      ),
    );
    var response = await accountRepository.authAccount(email, password);
    if (response['type'] != 'success') {
      asuka.showSnackBar(
        SnackBar(
          content: AppMessageWidget(
            message: response['message'],
          ),
          duration: Duration(
            milliseconds: 1000,
          ),
        ),
      );
      return;
    } else {
      asuka.showSnackBar(
        SnackBar(
          content: AppMessageWidget(
            message: 'Você foi autenticado com sucesso!',
          ),
          duration: Duration(
            milliseconds: 800,
          ),
        ),
      );
    }
    var data = json.decode(response['data']);
    var babies = <CageBabyModel>[];
    var eggs = <CageEggModel>[];
    data['babies'].forEach((e) {
      var baby = CageBabyModel.fromJson(e);
      babies.add(baby);
    });
    data['eggs'].forEach((e) {
      var egg = CageEggModel.fromJson(e);
      eggs.add(egg);
    });
    account = AccountModel(
      id: int.parse(data['id']),
      cpf: data['cpf'],
      email: data['email'],
      name: data['name'],
      phone: data['phone'],
      password: data['password'],
      babies: babies,
      eggs: eggs,
    );
    Modular.to.pushNamed('/account/menu');
  }

  Future<void> onTapRegister() async {
    Modular.to.pushNamed('/account/create');
  }

  Future<void> onTapForgotPassword() async {
    Modular.to.pushNamed('/account/recovery');
  }

  Future<void> onTapBack() async {
    Modular.to.pop();
  }
}
