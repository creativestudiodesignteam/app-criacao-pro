import '../cage/baby/cage_baby_model.dart';
import '../cage/egg/cage_egg_model.dart';

class AccountModel {
  AccountModel({
    this.id,
    this.cpf,
    this.email,
    this.name,
    this.phone,
    this.password,
    this.babies,
    this.eggs,
  });

  final int id;
  final String cpf;
  final String email;
  final String name;
  final String phone;
  final String password;
  final List<CageBabyModel> babies;
  final List<CageEggModel> eggs;
}
