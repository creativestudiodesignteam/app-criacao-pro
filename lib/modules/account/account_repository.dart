import 'dart:convert';
import 'package:dio/dio.dart';
import '../../constants.dart' as constants;

class AccountRepository {
  AccountRepository({
    this.dio,
  });

  final Dio dio;

  final String path = '${constants.appUrl}/account';
  final Options options = Options(
    headers: {
      'X-Token': constants.appSecret,
    },
  );

  Future<Map<String, dynamic>> createAccount(String cpf, String email,
      String name, String phone, String password) async {
    var formData = FormData.fromMap(
      {
        'cpf': cpf,
        'email': email,
        'name': name,
        'phone': phone,
        'password': password,
      },
    );
    var response = await dio.post(
      '${path}/create',
      data: formData,
      options: options,
    );
    return json.decode(response.data);
  }

  Future<Map<String, dynamic>> readAccount(int id) async {
    var response = await dio.post(
      '${path}/read',
      data: {
        'id': id,
      },
      options: options,
    );
    return json.decode(response.data);
  }

  Future<Map<String, dynamic>> authAccount(
      String email, String password) async {
    var formData = FormData.fromMap(
      {
        'email': email,
        'password': password,
      },
    );
    var response = await dio.post(
      '${path}/auth',
      data: formData,
      options: options,
    );
    return json.decode(response.data);
  }

  Future<Map<String, dynamic>> recoveryAccountChange(
      String email, String code, String password) async {
    var formData = FormData.fromMap(
      {
        'email': email,
        'code': code,
        'password': password,
      },
    );
    var response = await dio.post(
      '${path}/recovery-change',
      data: formData,
      options: options,
    );
    return json.decode(response.data);
  }

  Future<Map<String, dynamic>> recoveryAccountSend(String email) async {
    var formData = FormData.fromMap(
      {
        'email': email,
      },
    );
    var response = await dio.post(
      '${path}/recovery-send',
      data: formData,
      options: options,
    );
    return json.decode(response.data);
  }
}
