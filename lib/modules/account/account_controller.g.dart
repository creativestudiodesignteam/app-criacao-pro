// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'account_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$AccountController on _AccountController, Store {
  final _$accountAtom = Atom(name: '_AccountController.account');

  @override
  AccountModel get account {
    _$accountAtom.reportRead();
    return super.account;
  }

  @override
  set account(AccountModel value) {
    _$accountAtom.reportWrite(value, super.account, () {
      super.account = value;
    });
  }

  final _$onTapAccessAppAsyncAction =
      AsyncAction('_AccountController.onTapAccessApp');

  @override
  Future<void> onTapAccessApp() {
    return _$onTapAccessAppAsyncAction.run(() => super.onTapAccessApp());
  }

  @override
  String toString() {
    return '''
account: ${account}
    ''';
  }
}
