import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:asuka/asuka.dart' as asuka;
import 'package:mobx/mobx.dart';
import '../../app/app_message_widget.dart';
import '../account_controller.dart';
part 'account_create_controller.g.dart';

class AccountCreateController = _AccountCreateController
    with _$AccountCreateController;

abstract class _AccountCreateController with Store {
  _AccountCreateController({
    this.accountController,
  });

  final AccountController accountController;
  final TextEditingController cpfController = TextEditingController();
  final TextEditingController emailController = TextEditingController();
  final TextEditingController nameController = TextEditingController();
  final TextEditingController phoneController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final TextEditingController confirmPasswordController =
      TextEditingController();

  @action
  Future<void> onTapBack() async {
    Modular.to.pop();
  }

  @action
  Future<void> onTapAddAccount() async {
    var cpf = cpfController.text;
    var email = emailController.text;
    var name = nameController.text;
    var phone = phoneController.text;
    var password = passwordController.text;
    var confirmPassword = confirmPasswordController.text;
    if (password != confirmPassword) {
      asuka.showSnackBar(
        SnackBar(
          content: AppMessageWidget(
            message: 'A senha deve ser igual nos dois campos.',
          ),
          duration: Duration(
            milliseconds: 1000,
          ),
        ),
      );
      return;
    }
    asuka.showSnackBar(
      SnackBar(
        content: AppMessageWidget(
          isLoading: true,
          message: 'Por favor, aguarde ...',
        ),
        duration: Duration(
          milliseconds: 800,
        ),
      ),
    );
    var response = await accountController.accountRepository
        .createAccount(cpf, email, name, phone, password);
    if (response['type'] == 'success') {
      Modular.to.pushReplacementNamed('/account');
    }
    asuka.showSnackBar(
      SnackBar(
        content: AppMessageWidget(
          message: response['message'],
        ),
        duration: Duration(
          milliseconds: 1000,
        ),
      ),
    );
  }
}
