import 'package:flutter_modular/flutter_modular.dart';
import '../account_controller.dart';
import 'account_create_controller.dart';
import 'account_create_view.dart';

class AccountCreateModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind(
          (i) => AccountCreateController(
            accountController: i.get<AccountController>(),
          ),
        ),
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(
          '/',
          child: (ctx, args) => AccountCreateView(),
        )
      ];
}
