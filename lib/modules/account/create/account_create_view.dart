import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import '../../app/app_appbar_widget.dart';
import '../../app/app_button_widget.dart';
import '../../app/app_logo_widget.dart';
import '../../app/app_scaffold_widget.dart';
import '../../app/app_text_widget.dart';
import '../../app/app_textfield_widget.dart';
import 'account_create_controller.dart';

class AccountCreateView extends StatefulWidget {
  @override
  _AccountCreateViewState createState() => _AccountCreateViewState();
}

class _AccountCreateViewState
    extends ModularState<AccountCreateView, AccountCreateController> {
  FocusNode fn2 = FocusNode();
  FocusNode fn3 = FocusNode();
  FocusNode fn4 = FocusNode();
  FocusNode fn5 = FocusNode();
  FocusNode fn6 = FocusNode();

  Widget appBar(BuildContext context) {
    return AppAppBarWidget(
      titles: <Widget>[
        AppTextWidget(
          text: 'Cadastrar',
          size: Theme.of(context).textTheme.subtitle2.fontSize,
        ),
      ],
      actions: <Widget>[
        IconButton(
          icon: Icon(
            Icons.chevron_left,
          ),
          onPressed: controller.onTapBack,
        ),
      ],
    );
  }

  Widget body(BuildContext context) {
    return Column(
      children: <Widget>[
        AppLogoWidget(),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            AppTextWidget(
              text: '* Campos obrigatórios',
              size: 10,
              weight: FontWeight.bold,
            ),
          ],
        ),
        AppTextFieldWidget(
          controller: controller.cpfController,
          keyboardType: TextInputType.numberWithOptions(),
          label: 'CPF / CNPJ',
          onSubmit: () {
            FocusScope.of(context).requestFocus(fn2);
          },
        ),
        AppTextFieldWidget(
          controller: controller.emailController,
          keyboardType: TextInputType.emailAddress,
          label: '* E-mail',
          focusNode: fn2,
          onSubmit: () {
            FocusScope.of(context).requestFocus(fn3);
          },
        ),
        AppTextFieldWidget(
          controller: controller.nameController,
          keyboardType: TextInputType.text,
          label: '* Nome',
          focusNode: fn3,
          onSubmit: () {
            FocusScope.of(context).requestFocus(fn4);
          },
        ),
        AppTextFieldWidget(
          controller: controller.phoneController,
          keyboardType: TextInputType.numberWithOptions(),
          label: '* Telefone',
          focusNode: fn4,
          onSubmit: () {
            FocusScope.of(context).requestFocus(fn5);
          },
        ),
        AppTextFieldWidget(
          controller: controller.passwordController,
          keyboardType: TextInputType.text,
          obscureText: true,
          label: '* Senha',
          focusNode: fn5,
          onSubmit: () {
            FocusScope.of(context).requestFocus(fn6);
          },
        ),
        AppTextFieldWidget(
          controller: controller.confirmPasswordController,
          keyboardType: TextInputType.text,
          obscureText: true,
          label: '* Confirmar senha',
          focusNode: fn6,
          onSubmit: controller.onTapAddAccount,
        ),
        SizedBox(
          height: 20,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            AppButtonWidget(
              text: 'Cadastrar',
              color: Theme.of(context).primaryColor,
              onPressed: controller.onTapAddAccount,
            ),
            AppTextWidget(
              text: 'Voltar',
              onTap: controller.onTapBack,
            ),
          ],
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return AppScaffoldWidget(
      appBar: appBar(context),
      body: body(context),
    );
  }
}
