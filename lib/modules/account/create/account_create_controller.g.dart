// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'account_create_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$AccountCreateController on _AccountCreateController, Store {
  final _$onTapBackAsyncAction =
      AsyncAction('_AccountCreateController.onTapBack');

  @override
  Future<void> onTapBack() {
    return _$onTapBackAsyncAction.run(() => super.onTapBack());
  }

  final _$onTapAddAccountAsyncAction =
      AsyncAction('_AccountCreateController.onTapAddAccount');

  @override
  Future<void> onTapAddAccount() {
    return _$onTapAddAccountAsyncAction.run(() => super.onTapAddAccount());
  }

  @override
  String toString() {
    return '''

    ''';
  }
}
