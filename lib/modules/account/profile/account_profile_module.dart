import 'package:flutter_modular/flutter_modular.dart';
import '../account_controller.dart';
import 'account_profile_controller.dart';
import 'account_profile_view.dart';

class AccountProfileModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind(
          (i) => AccountProfileController(
            accountController: i.get<AccountController>(),
          ),
        ),
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(
          '/',
          child: (ctx, args) => AccountProfileView(),
        ),
      ];
}
