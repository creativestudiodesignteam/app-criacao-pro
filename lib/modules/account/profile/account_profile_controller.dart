import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../../constants.dart' as constants;
import '../account_controller.dart';
part 'account_profile_controller.g.dart';

class AccountProfileController = _AccountProfileController
    with _$AccountProfileController;

abstract class _AccountProfileController with Store {
  _AccountProfileController({
    this.accountController,
  });

  final AccountController accountController;
  final TextEditingController cpfController = TextEditingController();
  final TextEditingController emailController = TextEditingController();
  final TextEditingController nameController = TextEditingController();
  final TextEditingController phoneController = TextEditingController();

  Future<void> onTapBack() async {
    Modular.to.pop();
  }

  Future<void> onTapChangePassword() async {
    Modular.to.pushNamed('/account/recovery');
  }

  Future<void> onTapNeedHelp() async {
    if (await canLaunch('${constants.appUrl}/help')) {
      await launch('${constants.appUrl}/help');
    }
  }
}
