import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import '../../app/app_appbar_widget.dart';
import '../../app/app_button_widget.dart';
import '../../app/app_logo_widget.dart';
import '../../app/app_scaffold_widget.dart';
import '../../app/app_text_widget.dart';
import '../../app/app_textfield_widget.dart';
import 'account_profile_controller.dart';

class AccountProfileView extends StatefulWidget {
  @override
  _AccountProfileViewState createState() => _AccountProfileViewState();
}

class _AccountProfileViewState
    extends ModularState<AccountProfileView, AccountProfileController> {
  Widget appBar(BuildContext context) {
    return AppAppBarWidget(
      titles: <Widget>[
        AppTextWidget(
          text: 'Perfil',
          size: Theme.of(context).textTheme.subtitle2.fontSize,
        ),
      ],
      actions: <Widget>[
        IconButton(
          icon: Icon(Icons.chevron_left),
          onPressed: controller.onTapBack,
        ),
      ],
    );
  }

  Widget body(BuildContext context) {
    return Column(
      children: <Widget>[
        AppLogoWidget(),
        AppTextFieldWidget(
          label: 'CPF',
          enabled: false,
          initialValue: controller.accountController.account.cpf,
        ),
        AppTextFieldWidget(
          label: 'E-mail',
          enabled: false,
          initialValue: controller.accountController.account.email,
        ),
        AppTextFieldWidget(
          label: 'Nome',
          enabled: false,
          initialValue: controller.accountController.account.name,
        ),
        AppTextFieldWidget(
          label: 'Telefone',
          enabled: false,
          initialValue: controller.accountController.account.phone,
        ),
        SizedBox(
          height: 20,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            AppButtonWidget(
              color: Theme.of(context).primaryColor,
              text: 'Alterar senha',
              onPressed: controller.onTapChangePassword,
            ),
            AppTextWidget(
              text: 'Quer ajuda?',
              onTap: controller.onTapNeedHelp,
            ),
          ],
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return AppScaffoldWidget(
      appBar: appBar(context),
      body: body(context),
    );
  }
}
