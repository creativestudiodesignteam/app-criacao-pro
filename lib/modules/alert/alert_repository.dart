import 'dart:convert';
import 'package:dio/dio.dart';
import '../../constants.dart' as constants;

class AlertRepository {
  AlertRepository({
    this.dio,
  });

  final Dio dio;

  final String path1 = '${constants.appUrl}/baby';
  final String path2 = '${constants.appUrl}/egg';
  final Options options = Options(
    headers: {
      'X-Token': constants.appSecret,
    },
  );

  Future<Map<String, dynamic>> readAllBabiesByAccount(int account) async {
    var formData = FormData.fromMap({
      'account': account,
    });
    var response = await dio.post(
      '${path1}/read-all-by-account',
      data: formData,
      options: options,
    );
    return json.decode(response.data);
  }

  Future<Map<String, dynamic>> readAllEggsByAccount(int account) async {
    var formData = FormData.fromMap({
      'account': account,
    });
    var response = await dio.post(
      '${path2}/read-all-by-account',
      data: formData,
      options: options,
    );
    return json.decode(response.data);
  }
}