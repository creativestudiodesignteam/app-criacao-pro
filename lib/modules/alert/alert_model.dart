import 'dart:ui';

import '../cage/cage_model.dart';

class AlertModel {
  AlertModel(
      {this.id,
      this.title,
      this.message,
      this.date,
      this.route,
      this.cage,
      this.color,
      this.type});

  int id;
  String title;
  String message;
  int date;
  String route;
  CageModel cage;
  Color color;
  int type;

  AlertModel.fromJson(Map<String, dynamic> json) {
    id = int.parse(json['id']);
    title = json['title'];
    message = json['message'];
    date = int.parse(json['id']);
    route = json['route'];
    cage = json['cage'];
    color = json['color'];
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    var data = {
      'id': id,
      'title': title,
      'message': message,
      'date': date,
      'route': route,
      'cage': cage,
      'color': color,
      'type': type,
    };
    return data;
  }
}
