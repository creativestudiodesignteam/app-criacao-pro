import 'package:dio/dio.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../account/account_controller.dart';
import '../cage/cage_controller.dart';
import '../cage/cage_repository.dart';
import 'alert_controller.dart';
import 'alert_repository.dart';
import 'alert_view.dart';

class AlertModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind(
          (i) => AlertController(
            accountController: i.get<AccountController>(),
            alertRepository: i.get<AlertRepository>(),
            cageController: i.get<CageController>(),
          ),
        ),
        Bind(
          (i) => AlertRepository(
            dio: i.get<Dio>(),
          ),
        ),
        Bind(
          (i) => CageController(
            accountController: i.get<AccountController>(),
            cageRepository: i.get<CageRepository>(),
          ),
        ),
        Bind(
          (i) => CageRepository(
            dio: i.get<Dio>(),
          ),
        ),
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(
          '/',
          child: (ctx, args) => AlertView(),
        ),
      ];
}
