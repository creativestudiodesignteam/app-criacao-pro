import 'dart:convert';
import 'dart:ui';

import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';

import '../account/account_controller.dart';
import '../cage/baby/cage_baby_model.dart';
import '../cage/cage_controller.dart';
import '../cage/egg/cage_egg_model.dart';
import 'alert_model.dart';
import 'alert_repository.dart';

part 'alert_controller.g.dart';

class AlertController = _AlertController with _$AlertController;

abstract class _AlertController with Store {
  _AlertController({
    this.accountController,
    this.alertRepository,
    this.cageController,
  });

  final AccountController accountController;
  final AlertRepository alertRepository;
  final CageController cageController;

  @observable
  List<AlertModel> alerts;

  Future<List<AlertModel>> readAlerts() async {
    var babies = await readAllBabiesByAccount(accountController.account.id);
    var eggs = await readAllEggsByAccount(accountController.account.id);
    var babiesToRing = babies
        .where((e) =>
            e.ringDate >=
            DateTime.now().subtract(Duration(days: 3)).millisecondsSinceEpoch)
        .toList();
    var babiesToSeparate = babies
        .where((e) =>
            e.separateDate >=
            DateTime.now().subtract(Duration(days: 3)).millisecondsSinceEpoch)
        .toList();
    var eggsToBirth = eggs
        .where((e) =>
            e.hatchingDate >=
            DateTime.now().subtract(Duration(days: 3)).millisecondsSinceEpoch)
        .toList();
    var eggsToCadling = eggs
        .where((e) =>
            e.candlingDate >=
            DateTime.now().subtract(Duration(days: 3)).millisecondsSinceEpoch)
        .toList();
    var newAlerts = <AlertModel>[];
    await cageController.readAllCagesForSelect('');
    newAlerts.addAll(
      eggsToCadling.map(
        (e) => AlertModel(
          title: 'Ovoscopia pendente',
          message: 'Data da ovoscopia: ',
          date: e.candlingDate,
          route: '/cage/egg',
          cage: cageController.cages
              .firstWhere((element) => element.id == e.cage),
          color: Color(0xFF7AE6B9),
          type: 1,
        ),
      ),
    );
    newAlerts.addAll(
      eggsToBirth.map(
        (e) => AlertModel(
          title: 'Nascimento pendente',
          message: 'Data do nascimento: ',
          date: e.birthDate,
          route: '/cage/egg',
          cage: cageController.cages
              .firstWhere((element) => element.id == e.cage),
          color: Color(0xFFFA8576),
          type: 2,
        ),
      ),
    );
    newAlerts.addAll(
      babiesToRing.map(
        (e) => AlertModel(
          title: 'Anilhagem pendente',
          message: 'Data da anilhagem: ',
          date: e.ringDate,
          route: '/cage/baby',
          cage: cageController.cages
              .firstWhere((element) => element.id == e.cage),
          color: Color(0xFFF5DA81),
          type: 3,
        ),
      ),
    );
    newAlerts.addAll(
      babiesToSeparate.map(
        (e) => AlertModel(
          title: 'Separacão pendente',
          message: 'Data da separacão: ',
          date: e.separateDate,
          route: '/cage/baby',
          cage: cageController.cages
              .firstWhere((element) => element.id == e.cage),
          color: Color(0xFF99D1FF),
          type: 4,
        ),
      ),
    );
    return alerts = newAlerts;
  }

  Future<void> onTapBack() async {
    Modular.to.pop();
  }

  @action
  Future<List<CageBabyModel>> readAllBabiesByAccount(int account) async {
    var response = await alertRepository.readAllBabiesByAccount(account);
    var data = json.decode(response['data']);
    var babyList = List<CageBabyModel>.from(data.map((e) {
      return CageBabyModel.fromJson(e);
    }).toList());
    return babyList;
  }

  @action
  Future<List<CageEggModel>> readAllEggsByAccount(int account) async {
    var response = await alertRepository.readAllEggsByAccount(account);
    var data = json.decode(response['data']);
    var eggList = List<CageEggModel>.from(data.map((e) {
      return CageEggModel.fromJson(e);
    }).toList());
    return eggList;
  }
}
