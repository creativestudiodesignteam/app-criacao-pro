import 'package:flutter/material.dart';

import 'alert_item_widget.dart';
import 'alert_model.dart';

class AlertListWidget extends StatelessWidget {
  const AlertListWidget({
    Key key,
    this.alerts,
    @required this.update,
  }) : super(key: key);

  final List<AlertModel> alerts;
  final Function update;

  @override
  Widget build(BuildContext context) {
    print("alerts.length");
    print(alerts.length);
    var ovoscopia = [];
    var nascimento = [];
    var anilhamento = [];
    var separacao = [];
    for (var element in alerts) {
      if (element.type == 1) {
        print("element.type");
        print(element.id);
        ovoscopia.add(element);
      } else if (element.type == 2) {
        print(element.id);
        nascimento.add(element);
      } else if (element.type == 3) {
        print(element.id);
        anilhamento.add(element);
      } else if (element.type == 4) {
        print(element.id);
        separacao.add(element);
      }
    }
    return Column(
      children: [
        ExpansionTile(
          title: Text('Ovoscopia'),
          children: [
            ListView.builder(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemCount: ovoscopia.length,
              itemBuilder: (_, i) {
                return AlertItemWidget(
                  alert: ovoscopia.elementAt(i),
                  update: update,
                );
              },
            ),
          ],
        ),
        ExpansionTile(
          title: Text('Nascimento'),
          children: [
            ListView.builder(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemCount: nascimento.length,
              itemBuilder: (_, i) {
                return AlertItemWidget(
                  alert: nascimento.elementAt(i),
                  update: update,
                );
              },
            ),
          ],
        ),
        ExpansionTile(
          title: Text('Anilhamento'),
          children: [
            ListView.builder(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemCount: anilhamento.length,
              itemBuilder: (_, i) {
                return AlertItemWidget(
                  alert: anilhamento.elementAt(i),
                  update: update,
                );
              },
            ),
          ],
        ),
        ExpansionTile(
          title: Text('Separação'),
          children: [
            ListView.builder(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemCount: separacao.length,
              itemBuilder: (_, i) {
                return AlertItemWidget(
                  alert: separacao.elementAt(i),
                  update: update,
                );
              },
            ),
          ],
        ),
      ],
    );
  }
}
