import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../app/app_card_widget.dart';
import '../app/app_text_widget.dart';
import 'alert_model.dart';

class AlertItemWidget extends StatelessWidget {
  const AlertItemWidget({
    Key key,
    this.alert,
    @required this.update,
  }) : super(key: key);

  final AlertModel alert;
  final Function update;

  @override
  Widget build(BuildContext context) {
    var date = DateTime.fromMillisecondsSinceEpoch(alert.date);
    var dateFormatted = DateFormat('dd/MM/yyyy').format(date).toString();
    return AppCardWidget(
      color: alert.color,
      children: [
        AppTextWidget(
          text: alert.title,
          size: 16,
          weight: FontWeight.bold,
        ),
        SizedBox(
          height: 5,
        ),
        FittedBox(
          fit: BoxFit.fitWidth,
          child: Row(
            children: <Widget>[
              AppTextWidget(
                text: alert.message,
                size: 15,
              ),
              AppTextWidget(
                text: dateFormatted,
                size: 15,
                weight: FontWeight.bold,
              ),
            ],
          ),
        ),
        Row(
          children: <Widget>[
            AppTextWidget(
              text: 'Gaiola: ',
              size: 15,
            ),
            AppTextWidget(
              text: alert.cage.name.length > 0
                  ? alert.cage.name : 'Sem Nome',
              size: 15,
              weight: FontWeight.bold,
            ),
          ],
        ),
      ],
      image: 'lib/assets/menu-3.png',
      onPressed: () {
        Navigator.pushNamed(context, alert.route,
          arguments: {
            'cage': alert.cage,
            'isSelect': false,
          },
        ).then((value) => update());
      },
    );
  }
}
