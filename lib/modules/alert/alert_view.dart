import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../app/app_appbar_widget.dart';
import '../app/app_header_widget.dart';
import '../app/app_scaffold_widget_multi.dart';
import '../app/app_text_widget.dart';
import 'alert_controller.dart';
import 'alert_list_widget.dart';

class AlertView extends StatefulWidget {
  @override
  _AlertViewState createState() => _AlertViewState();
}

class _AlertViewState extends ModularState<AlertView, AlertController> {
  Widget appBar(BuildContext context) {
    return AppAppBarWidget(
      titles: <Widget>[
        AppTextWidget(
          text: 'Alertas',
          size: Theme.of(context).textTheme.subtitle2.fontSize,
        ),
      ],
      actions: <Widget>[
        IconButton(
          icon: Icon(
            Icons.chevron_left,
          ),
          onPressed: controller.onTapBack,
        ),
      ],
    );
  }

  Widget body(BuildContext context) {
    return Column(
      children: <Widget>[
        AppHeaderWidget(
          title: 'Alertas',
          subtitle: 'do seu ',
          highlight: 'criatório',
        ),
        SizedBox(
          height: 20,
        ),
        Observer(
          builder: (_) {
            if (controller.alerts == null) {
              return AppTextWidget(
                text: 'Buscando alertas...',
              );
            } else if (controller.alerts.isEmpty) {
              return AppTextWidget(
                text: 'Nenhum alerta encontrado.',
              );
            } else {
              return AlertListWidget(
                alerts: controller.alerts,
                update: controller.readAlerts,
              );
            }
          },
        ),
      ],
    );
  }

  @override
  void initState() {
    super.initState();
    controller.readAlerts();
  }

  @override
  Widget build(BuildContext context) {
    return AppScaffoldWidgetMulti(
      mainAxisAlignment: MainAxisAlignment.end,
      floatingActionButton: Container(height: 0, width: 0),
      appBar: appBar(context),
      body: body(context),
    );
  }
}
