import 'package:flutter/material.dart';

class AppTextFieldWidget extends StatelessWidget {
  const AppTextFieldWidget({
    Key key,
    this.controller,
    this.keyboardType = TextInputType.text,
    this.label,
    this.obscureText = false,
    this.enabled = true,
    this.onTap,
    this.initialValue,
    this.onSubmit,
    this.focusNode,
  }) : super(key: key);

  final TextEditingController controller;
  final TextInputType keyboardType;
  final String label;
  final bool obscureText;
  final bool enabled;
  final Function onTap;
  final String initialValue;
  final Function onSubmit;
  final FocusNode focusNode;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 10),
      child: TextFormField(
        focusNode: focusNode,
        initialValue: initialValue,
        controller: controller,
        keyboardType: keyboardType,
        obscureText: obscureText,
        cursorColor: Theme.of(context).primaryColor,
        style: TextStyle(
          color: Colors.black,
          fontSize: Theme.of(context).textTheme.bodyText1.fontSize - 1,
          fontWeight: FontWeight.bold,
        ),
        decoration: InputDecoration(
          filled: !enabled,
          fillColor: Colors.grey[200],
          labelText: label,
          labelStyle: TextStyle(
            fontSize: Theme.of(context).textTheme.bodyText1.fontSize,
            color: Colors.black87,
            fontWeight: FontWeight.normal,
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: Theme.of(context).primaryColor,
            ),
          ),
          enabled: enabled,
        ),
        enabled: enabled,
        onTap: onTap,
        onFieldSubmitted: (_) {
          if (onSubmit != null) {
            onSubmit();
          }
        },
      ),
    );
  }
}
