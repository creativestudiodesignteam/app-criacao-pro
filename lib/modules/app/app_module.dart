import 'package:criacao_pro/modules/partner/partner_module.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../account/account_controller.dart';
import '../account/account_module.dart';
import '../account/account_repository.dart';
import '../alert/alert_module.dart';
import '../bird/bird_module.dart';
import '../cage/cage_module.dart';
import '../report/report_module.dart';
import 'app_controller.dart';
import 'app_view.dart';
import 'app_widget.dart';

class AppModule extends MainModule {
  @override
  List<Bind> get binds => [
        Bind(
          (i) => AppController(
            accountController: i.get<AccountController>(),
          ),
        ),
        Bind(
          (i) => AccountController(
            accountRepository: i.get<AccountRepository>(),
          ),
          singleton: true,
          lazy: true,
        ),
        Bind(
          (i) => AccountRepository(
            dio: i.get<Dio>(),
          ),
        ),
        Bind(
          (i) => Dio(),
        )
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(
          '/',
          child: (ctx, args) => AppView(),
          transition: TransitionType.fadeIn,
        ),
        ModularRouter(
          '/account',
          module: AccountModule(),
          transition: TransitionType.fadeIn,
        ),
        ModularRouter(
          '/bird',
          module: BirdModule(),
          transition: TransitionType.fadeIn,
        ),
        ModularRouter(
          '/cage',
          module: CageModule(),
          transition: TransitionType.fadeIn,
        ),
        ModularRouter(
          '/alert',
          module: AlertModule(),
          transition: TransitionType.fadeIn,
        ),
        ModularRouter(
          '/report',
          module: ReportModule(),
          transition: TransitionType.fadeIn,
        ),
        ModularRouter(
          '/partner',
          module: PartnerModule(),
          transition: TransitionType.fadeIn,
        ),
      ];

  @override
  Widget get bootstrap => AppWidget();
}
