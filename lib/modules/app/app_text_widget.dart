import 'package:flutter/material.dart';

class AppTextWidget extends StatelessWidget {
  const AppTextWidget({
    Key key,
    this.text,
    this.size,
    this.weight,
    this.color,
    this.onTap,
    this.textAlign = TextAlign.left,
  }) : super(key: key);

  final String text;
  final double size;
  final FontWeight weight;
  final Color color;
  final TextAlign textAlign;
  final Function onTap;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Text(
        text,
        style: TextStyle(
          fontSize: size,
          fontWeight: onTap == null ? weight : FontWeight.bold,
          color: color,
        ),
        textAlign: textAlign,
      ),
      onTap: onTap,
    );
  }
}
