import 'dart:ui';

import 'package:flutter/material.dart';

import '../cage/egg/cage_egg_model.dart';

class AppCardWidgetEgg extends StatefulWidget {
  const AppCardWidgetEgg({
    Key key,
    this.image,
    this.children,
    this.color,
    this.onPressed,
    this.isLocalImage = true,
    this.isTransfer = false,
    this.isAction = false,
    this.onChangedCheckbox,
    @required this.egg,
    this.stateEgg,
  }) : super(key: key);

  final String image;
  final List<Widget> children;
  final Color color;
  final Function onPressed;
  final bool isLocalImage;
  final bool isTransfer;
  final bool isAction;
  final CageEggModel egg;
  final void Function(bool) onChangedCheckbox;
  final String stateEgg;

  @override
  _AppCardWidgetEggState createState() => _AppCardWidgetEggState();
}

class _AppCardWidgetEggState extends State<AppCardWidgetEgg> {
  bool checkbox = false;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: widget.onPressed,
      child: Container(
        margin: EdgeInsets.only(
          bottom: 15,
        ),
        decoration: BoxDecoration(
          color: widget.color,
          boxShadow: [
            BoxShadow(
              color: Colors.black12,
              offset: Offset(2, 2),
              blurRadius: 4,
              spreadRadius: 0,
            ),
          ],
          borderRadius: BorderRadius.circular(8),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            widget.isTransfer || widget.isAction
                ? Checkbox(
                    value: checkbox,
                    onChanged: (value) {
                      setState(() {
                        widget.egg.select ? checkbox = value : null;
                      });
                      widget.egg.select
                          ? widget.onChangedCheckbox(value)
                          : showDialog(
                              context: this.context,
                              child: AlertDialog(
                                content: Text(
                                    "Só pode selecionar ovos do status ${widget.stateEgg}!"),
                              ),
                            );
                    },
                  )
                : Container(
                    padding: EdgeInsets.all(
                      widget.isLocalImage ? 10 : 0,
                    ),
                    width: widget.isLocalImage ? 80 : 100,
                    height: widget.isLocalImage ? 100 : 200,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(8),
                      child: widget.isLocalImage
                          ? Image.asset(
                              widget.image,
                              fit: BoxFit.contain,
                            )
                          : Image.network(
                              widget.image,
                              fit: BoxFit.cover,
                            ),
                    ),
                  ),
            Expanded(
              child: Container(
                padding: EdgeInsets.all(10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: widget.children,
                ),
              ),
            ),
            // Padding(
            //   padding: EdgeInsets.fromLTRB(0, 10, 10, 10),
            //   child: RotatedBox(
            //     child: Text("Ver mais", style: TextStyle(color: Colors.grey)),
            //     quarterTurns: 3,
            //   ),
            // ),
          ],
        ),
      ),
    );
  }
}
