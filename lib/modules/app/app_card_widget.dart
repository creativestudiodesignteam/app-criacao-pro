import 'package:criacao_pro/modules/app/app_message_widget.dart';
import 'package:criacao_pro/modules/bird/bird_model.dart';
import 'package:flutter/material.dart';
import 'package:asuka/asuka.dart' as asuka;

class AppCardWidget extends StatefulWidget {
  const AppCardWidget({
    Key key,
    this.image,
    this.children,
    this.bird,
    this.color,
    this.onPressed,
    this.isLocalImage = true,
    this.isTransfer = false,
    this.isTwoOptions = false,
    this.onChangedCheckbox,
    this.totalSelectBirds,
    this.selectGender,
  }) : super(key: key);

  final BirdModel bird;
  final String image;
  final List<Widget> children;
  final Color color;
  final Function onPressed;
  final bool isLocalImage;
  final bool isTransfer;
  final bool isTwoOptions;
  final int totalSelectBirds;
  final String selectGender;
  final void Function(bool) onChangedCheckbox;

  @override
  _AppCardWidgetState createState() => _AppCardWidgetState();
}

class _AppCardWidgetState extends State<AppCardWidget> {
  bool checkbox = false;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: widget.onPressed,
      child: Container(
        margin: EdgeInsets.only(
          bottom: 15,
        ),
        decoration: BoxDecoration(
          color: widget.color,
          boxShadow: [
            BoxShadow(
              color: Colors.black12,
              offset: Offset(2, 2),
              blurRadius: 4,
              spreadRadius: 0,
            ),
          ],
          borderRadius: BorderRadius.circular(8),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            widget.isTransfer
                ? Checkbox(
                    value: checkbox,
                    onChanged: (value) {
                      setState(() {
                        if (widget.image == 'lib/assets/baby.png') {
                          checkbox = value;
                        } else {
                          (widget.bird.gender == 'Indefinido' &&
                                      widget.isTwoOptions == true) ||
                                  (widget.totalSelectBirds == 2 &&
                                      value == true) ||
                                  (widget.selectGender == widget.bird.gender &&
                                      value == true)
                              ? null
                              : checkbox = value;
                        }
                      });
                      if (widget.image == 'lib/assets/baby.png') {
                        widget.onChangedCheckbox(value);
                      } else if (widget.bird.gender == 'Indefinido' &&
                          widget.isTwoOptions == true) {
                        asuka.showSnackBar(
                          SnackBar(
                            content: AppMessageWidget(
                              message: 'Selecione uma ave com genêro definido!',
                            ),
                            duration: Duration(
                              milliseconds: 2000,
                            ),
                          ),
                        );
                      } else if (widget.totalSelectBirds == 2 &&
                          value == true) {
                        asuka.showSnackBar(
                          SnackBar(
                            content: AppMessageWidget(
                              message: 'Só pode selecionar duas aves!',
                            ),
                            duration: Duration(
                              milliseconds: 2000,
                            ),
                          ),
                        );
                      } else if (widget.selectGender == widget.bird.gender &&
                          value == true) {
                        if (widget.selectGender == "Macho") {
                          asuka.showSnackBar(
                            SnackBar(
                              content: AppMessageWidget(
                                message: 'Selecione uma ave de gêneros Fêmea!',
                              ),
                              duration: Duration(
                                milliseconds: 2000,
                              ),
                            ),
                          );
                        } else {
                          asuka.showSnackBar(
                            SnackBar(
                              content: AppMessageWidget(
                                message: 'Selecione uma ave de gêneros Macho!',
                              ),
                              duration: Duration(
                                milliseconds: 2000,
                              ),
                            ),
                          );
                        }
                      } else {
                        widget.onChangedCheckbox(value);
                      }
                    },
                  )
                : Container(
                    padding: EdgeInsets.all(
                      widget.isLocalImage ? 10 : 0,
                    ),
                    width: widget.isLocalImage ? 80 : 100,
                    height: widget.isLocalImage ? 100 : 200,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(8),
                      child: widget.isLocalImage
                          ? Image.asset(
                              widget.image,
                              fit: BoxFit.contain,
                            )
                          : Image.network(
                              widget.image,
                              fit: BoxFit.cover,
                            ),
                    ),
                  ),
            Expanded(
              child: Container(
                padding: EdgeInsets.all(10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: widget.children,
                ),
              ),
            ),
            // Padding(
            //   padding: EdgeInsets.fromLTRB(0, 10, 10, 10),
            //   child: RotatedBox(
            //     child: Text("Ver mais", style: TextStyle(color: Colors.grey)),
            //     quarterTurns: 3,
            //   ),
            // ),
          ],
        ),
      ),
    );
  }
}
