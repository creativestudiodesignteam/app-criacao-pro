import 'package:flutter/material.dart';

class AppAppBarWidget extends StatelessWidget implements PreferredSizeWidget {
  const AppAppBarWidget({
    Key key,
    this.titles,
    this.actions,
    this.backgroundColor = Colors.transparent,
  }) : super(key: key);

  final List<Widget> titles;
  final List<Widget> actions;
  final Color backgroundColor;

  @override
  Size get preferredSize => Size.fromHeight(50);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      elevation: 0,
      automaticallyImplyLeading: false,
      backgroundColor: backgroundColor,
      centerTitle: true,
      title: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: titles,
      ),
      actions: actions,
    );
  }
}
