import 'package:flutter/material.dart';

class AppDropdownWidget extends StatelessWidget {
  const AppDropdownWidget({
    Key key,
    this.label,
    this.value,
    this.enabled = true,
    this.items,
    this.onChanged,
  }) : super(key: key);

  final String label;
  final String value;
  final bool enabled;
  final List<String> items;
  final Function(String) onChanged;

  List<DropdownMenuItem<String>> _dropdownItems() {
    return items
        .map((e) => DropdownMenuItem(
              child: Text(e, overflow: TextOverflow.ellipsis),
              value: e,
            ))
        .toList();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 10),
      child: DropdownButtonFormField(
        key: UniqueKey(),
        style: TextStyle(
          color: Colors.black,
          fontSize: Theme.of(context).textTheme.bodyText1.fontSize - 1,
          fontWeight: FontWeight.bold,
        ),
        decoration: InputDecoration(
          contentPadding: EdgeInsets.symmetric(
            vertical: 10,
            horizontal: 10,
          ),
          filled: !enabled,
          fillColor: Colors.grey[200],
          labelText: label,
          labelStyle: TextStyle(
            fontSize: Theme.of(context).textTheme.bodyText1.fontSize,
            color: Colors.black87,
            fontWeight: FontWeight.normal,
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: Theme.of(context).primaryColor,
            ),
          ),
          enabled: enabled,
        ),
        isExpanded: true,
        value: value,
        items: _dropdownItems(),
        onChanged: enabled ? onChanged : (value) {},
      ),
    );
  }
}
