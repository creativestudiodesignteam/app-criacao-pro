import 'package:flutter/material.dart';
import '../app/app_text_widget.dart';

class AppMessageWidget extends StatelessWidget {
  const AppMessageWidget({
    Key key,
    this.message,
    this.isLoading = false,
    this.sizeHeight = 50,
  }) : super(key: key);

  final String message;
  final bool isLoading;
  final double sizeHeight;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: sizeHeight,
      width: MediaQuery.of(context).size.width,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              isLoading
                  ? Container(
                      margin: EdgeInsets.only(
                        right: 15,
                      ),
                      width: 24,
                      height: 24,
                      child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation<Color>(
                          Color(0xffe9ae24),
                        ),
                      ),
                    )
                  : Image.asset(
                      'lib/assets/menu-1.png',
                      width: 48,
                      height: 48,
                    ),
              AppTextWidget(
                text: message,
                size: Theme.of(context).textTheme.subtitle2.fontSize,
                weight: FontWeight.bold,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
