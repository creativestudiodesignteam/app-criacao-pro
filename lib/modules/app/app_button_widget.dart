import 'package:flutter/material.dart';
import 'app_text_widget.dart';

class AppButtonWidget extends StatefulWidget {
  const AppButtonWidget({
    Key key,
    this.onPressed,
    this.color,
    this.text,
    this.textColor = Colors.white,
    this.paddingX = 16,
    this.paddingY = 8,
  }) : super(key: key);

  final Function onPressed;
  final Color color;
  final String text;
  final Color textColor;
  final double paddingX;
  final double paddingY;

  @override
  _AppButtonWidgetState createState() => _AppButtonWidgetState();
}

class _AppButtonWidgetState extends State<AppButtonWidget> {
  bool enabled = true;

  void _onPressed() async {
    enabled = false;
    await widget.onPressed();
    enabled = true;
  }

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      onPressed: enabled ? _onPressed : null,
      color: widget.color,
      child: Padding(
        padding: EdgeInsets.symmetric(
          vertical: widget.paddingY,
          horizontal: widget.paddingX,
        ),
        child: AppTextWidget(
          text: enabled ? widget.text : 'Aguarde ...',
          color: widget.textColor,
          weight: FontWeight.bold,
        ),
      ),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(7),
      ),
    );
  }
}
