import 'package:flutter/material.dart';

class AppScaffoldWidgetMulti extends StatelessWidget {
  const AppScaffoldWidgetMulti({
    Key key,
    this.appBar,
    this.body,
    this.floatingActionButton,
    this.mainAxisAlignment = MainAxisAlignment.spaceEvenly,
    this.sc,
  }) : super(key: key);

  final PreferredSizeWidget appBar;
  final Widget body;
  final Widget floatingActionButton;
  final MainAxisAlignment mainAxisAlignment;
  final ScrollController sc;

  @override
  Widget build(BuildContext context) {
    var scrollController = ScrollController();
    return Scaffold(
      resizeToAvoidBottomPadding: true,
      backgroundColor: Color(0xfff9f9f9),
      appBar: appBar,
      body: SingleChildScrollView(
        controller: scrollController,
        padding: EdgeInsets.fromLTRB(18, 5, 18, 18),
        physics: ScrollPhysics(),
        child: body,
      ),
      floatingActionButton: Row(
        mainAxisAlignment: mainAxisAlignment,
        children: [
          floatingActionButton,
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Container(
              height: 46,
              width: 46,
              child: FittedBox(
                child: FloatingActionButton(
                  heroTag: '',
                  child: Icon(Icons.keyboard_arrow_up, color: Colors.white),
                  onPressed: () {
                    sc.animateTo(
                      sc.position.minScrollExtent,
                      curve: Curves.easeOut,
                      duration: const Duration(milliseconds: 300),
                    );
                    scrollController.animateTo(
                      scrollController.position.minScrollExtent,
                      curve: Curves.easeOut,
                      duration: const Duration(milliseconds: 300),
                    );
                  },
                ),
              ),
            ),
          )
        ],
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}
