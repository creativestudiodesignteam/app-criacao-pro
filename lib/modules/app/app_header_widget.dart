import 'package:flutter/material.dart';
import 'app_text_widget.dart';

class AppHeaderWidget extends StatelessWidget {
  const AppHeaderWidget({
    Key key,
    this.title,
    this.subtitle,
    this.highlight,
    this.color,
    this.size,
  }) : super(key: key);

  final String title;
  final String subtitle;
  final String highlight;
  final Color color;
  final double size;

  @override
  Widget build(BuildContext context) {
    double sizefont;

    if (size == null) {
      sizefont = Theme.of(context).textTheme.headline5.fontSize;
    } else {
      sizefont = size;
    }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        AppTextWidget(
          text: title,
          size: sizefont,
          color: color,
          weight: FontWeight.bold,
        ),
        Row(
          children: <Widget>[
            AppTextWidget(
              text: subtitle,
              size: sizefont,
              color: color,
              weight: FontWeight.bold,
            ),
            Expanded(
              child: Text(
                highlight,
                style: TextStyle(
                  fontSize: sizefont,
                  fontWeight: FontWeight.bold,
                  color: Theme.of(context).primaryColor,
                ),
                textAlign: TextAlign.left,
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
              ),
            )
          ],
        ),
      ],
    );
  }
}
