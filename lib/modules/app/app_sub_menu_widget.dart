import 'package:flutter/material.dart';
import 'app_text_widget.dart';

class AppSubMenuWidget extends StatelessWidget {
  const AppSubMenuWidget({
    Key key,
    this.image,
    this.text,
    this.onPressed,
    this.selected = false,
  }) : super(key: key);

  final String image;
  final String text;
  final Function onPressed;
  final bool selected;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: Container(
        margin: EdgeInsets.only(
          bottom: selected ? 13 : 0,
        ),
        width: 70,
        height: 50,
        decoration: selected
            ? BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  stops: [0, 0.8],
                  colors: [
                    Color(0xffFADF6A),
                    Theme.of(context).primaryColor,
                  ],
                ),
                borderRadius: BorderRadius.circular(7),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black12,
                    offset: Offset(2, 2),
                    blurRadius: 4,
                    spreadRadius: 0,
                  ),
                ],
              )
            : BoxDecoration(
                color: Colors.grey[300],
                borderRadius: BorderRadius.circular(7),
              ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              image,
              width: 24,
              height: 24,
            ),
            SizedBox(
              height: 3,
            ),
            AppTextWidget(
              text: text,
              size: 12,
              weight: FontWeight.bold,
              color: selected ? Colors.white : Colors.black,
            ),
          ],
        ),
      ),
    );
  }
}
