import 'package:flutter/material.dart';
import '../app/app_button_widget.dart';
import '../app/app_text_widget.dart';

class AppModalWidget extends StatelessWidget {
  const AppModalWidget({
    Key key,
    this.title,
    this.textYes,
    this.textNo,
    this.onPressedYes,
    this.onPressedNo,
  }) : super(key: key);

  final String title;
  final String textYes;
  final String textNo;
  final Function onPressedYes;
  final Function onPressedNo;

  @override
  Widget build(BuildContext context) {
    var alignment = onPressedNo != null
    ? MainAxisAlignment.spaceEvenly
        : MainAxisAlignment.center;
    return Container(
      height: 150,
      width: MediaQuery.of(context).size.width,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: 20,
          ),
          AppTextWidget(
            text: title,
            size: Theme.of(context).textTheme.subtitle2.fontSize,
          ),
          SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: alignment,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              AppButtonWidget(
                text: textYes,
                color: Theme.of(context).primaryColor,
                onPressed: onPressedYes,
              ),
              onPressedNo != null
              ? AppTextWidget(
                text: textNo,
                onTap: onPressedNo,
              ) : Container()
            ],
          ),
        ],
      ),
    );
  }
}
