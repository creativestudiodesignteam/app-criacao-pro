import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import '../app/app_logo_widget.dart';
import '../app/app_scaffold_widget.dart';
import '../app/app_text_widget.dart';
import 'app_button_widget.dart';
import 'app_controller.dart';

class AppView extends StatefulWidget {
  @override
  _AppViewState createState() => _AppViewState();
}

class _AppViewState extends ModularState<AppView, AppController> {
  Widget body(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          AppLogoWidget(),
          AppButtonWidget(
            text: 'Li e concordo com os termos',
            onPressed: controller.onTapContinue,
            color: Theme.of(context).primaryColor,
          ),
          SizedBox(
            height: 15,
          ),
          AppTextWidget(
            text: 'Termos de uso',
            onTap: controller.onTapTerms,
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return AppScaffoldWidget(
      body: body(context),
    );
  }
}
