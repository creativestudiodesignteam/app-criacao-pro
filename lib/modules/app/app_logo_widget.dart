import 'package:flutter/material.dart';

class AppLogoWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Image.asset(
        'lib/assets/icon.png',
        width: 230,
        height: 230,
      ),
    );
  }
}
