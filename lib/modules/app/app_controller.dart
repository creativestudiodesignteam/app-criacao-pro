import 'dart:io';

import 'package:asuka/asuka.dart' as asuka;
import 'package:criacao_pro/modules/app/app_message_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../constants.dart' as constants;
import '../account/account_controller.dart';
part 'app_controller.g.dart';

class AppController = _AppController with _$AppController;

abstract class _AppController with Store {
  _AppController({
    this.accountController,
  }) {
    var initializationSettingsAndroid =
        AndroidInitializationSettings('@mipmap/ic_launcher');
    var initializationSettingsIOS = IOSInitializationSettings(
      onDidReceiveLocalNotification: onDidReceiveLocalNotification,
    );
    var initializationSettings = InitializationSettings(
      initializationSettingsAndroid,
      initializationSettingsIOS,
    );
    flutterLocalNotificationsPlugin.initialize(
      initializationSettings,
      onSelectNotification: onSelectNotification,
    );
  }

  final AccountController accountController;

  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();

  Future<void> showNotificationWithDefaultSound(int count) async {
    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
      '1',
      'alerts',
      'alerts',
    );
    var iOSPlatformChannelSpecifics = IOSNotificationDetails();
    var platformChannelSpecifics = NotificationDetails(
      androidPlatformChannelSpecifics,
      iOSPlatformChannelSpecifics,
    );
    await flutterLocalNotificationsPlugin.show(
      count,
      'Você tem ${count} alerta(s)',
      'Clique aqui para vê-los em seu criatório.',
      platformChannelSpecifics,
      payload: count.toString(),
    );
  }

  Future<void> onSelectNotification(String payload) async {
    if (accountController.account != null) {
      if (payload.isNotEmpty) {
        await Modular.to.pushNamed('/alert');
      }
    }
  }

  Future<void> onDidReceiveLocalNotification(
      int id, String title, String body, String payload) async {
    asuka.showDialog(
      builder: (_) => AlertDialog(
        title: Text(title),
        content: Text(body),
      ),
    );
  }

  Future<void> onTapContinue() async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        Modular.to.pushNamed('/account');
      }
    } on SocketException catch (_) {
      asuka.showSnackBar(
        SnackBar(
          content: AppMessageWidget(
            message: 'Sem internet. Verifique sua conexão!',
          ),
          duration: Duration(
            milliseconds: 3000,
          ),
        ),
      );
    }
  }

  Future<void> onTapTerms() async {
    if (await canLaunch('${constants.appUrl}/terms')) {
      await launch('${constants.appUrl}/terms');
    }
  }
}
