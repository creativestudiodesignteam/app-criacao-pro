import 'package:flutter/material.dart';

class AppScaffoldWidget extends StatelessWidget {
  const AppScaffoldWidget({
    Key key,
    this.appBar,
    this.body,
    this.floatingActionButton,
  }) : super(key: key);

  final PreferredSizeWidget appBar;
  final Widget body;
  final Widget floatingActionButton;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      backgroundColor: Color(0xfff9f9f9),
      appBar: appBar,
      body: Builder(
        builder: (context) => Stack(
          children: [
            ListView(
              children: [
                SafeArea(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 30.0),
                    child: body,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
      floatingActionButton: floatingActionButton,
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}
