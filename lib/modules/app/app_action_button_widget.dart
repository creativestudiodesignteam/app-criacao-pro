import 'package:flutter/material.dart';
import 'app_text_widget.dart';

class AppActionAppButtonWidget extends StatefulWidget {
  const AppActionAppButtonWidget({
    Key key,
    this.onPressed,
    this.color,
    this.icon,
    this.label,
  }) : super(key: key);

  final Function onPressed;
  final Color color;
  final IconData icon;
  final String label;

  @override
  _AppActionAppButtonWidgetState createState() =>
      _AppActionAppButtonWidgetState();
}

class _AppActionAppButtonWidgetState extends State<AppActionAppButtonWidget> {
  bool enabled = true;

  void _onPressed() async {
    setState(() {
      enabled = false;
    });
    await widget.onPressed();
    setState(() {
      enabled = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return RaisedButton.icon(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(25),
      ),
      padding: EdgeInsets.symmetric(
        horizontal: 65,
        vertical: 13,
      ),
      color: widget.color,
      label: AppTextWidget(
        text: enabled ? widget.label : 'Aguarde ...',
        color: Colors.white,
        weight: FontWeight.bold,
      ),
      icon: Icon(
        widget.icon,
        color: Colors.white,
        size: 14,
      ),
      onPressed: enabled ? _onPressed : null,
    );
  }
}
