import 'package:flutter/material.dart';
import '../app/app_text_widget.dart';

class BirdTagWidget extends StatelessWidget {
  const BirdTagWidget({
    Key key,
    this.title,
    this.subtitle,
  }) : super(key: key);

  final String title;
  final String subtitle;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 55,
      height: 72,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          stops: [0, 0.8],
          colors: [
            Color(0xffFADF6A),
            Theme.of(context).primaryColor,
          ],
        ),
        borderRadius: BorderRadius.circular(7),
        boxShadow: [
          BoxShadow(
            color: Colors.black12,
            offset: Offset(2, 2),
            blurRadius: 4,
            spreadRadius: 0,
          ),
        ],
      ),
      child: Padding(
        padding: const EdgeInsets.all(1.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            AppTextWidget(
              text: title,
              size: 12,
              weight: FontWeight.bold,
              color: Colors.white,
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 3,
            ),
            FittedBox(
              child: AppTextWidget(
                text: subtitle,
                size: 12,
                color: Colors.white,
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
