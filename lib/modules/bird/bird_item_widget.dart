import 'package:asuka/asuka.dart' as asuka;
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:intl/intl.dart';

import '../../report_pdf.dart';
import '../app/app_card_widget.dart';
import '../app/app_message_widget.dart';
import '../app/app_text_widget.dart';
import 'bird_model.dart';
import 'bird_tag_widget.dart';

class BirdItemWidget extends StatefulWidget {
  const BirdItemWidget({
    Key key,
    this.bird,
    this.isReport = false,
    this.isTwoOptions = false,
    this.isSelect = false,
    this.isTransfer = false,
    this.onChangedCheckbox,
    this.selectGender,
    this.function,
    this.totalSelectBirds,
  }) : super(key: key);

  final BirdModel bird;
  final bool isReport;
  final bool isTwoOptions;
  final bool isSelect;
  final bool isTransfer;
  final void Function(BirdModel, bool) onChangedCheckbox;
  final Function function;
  final int totalSelectBirds;
  final String selectGender;

  @override
  _BirdItemWidgetState createState() => _BirdItemWidgetState();
}

class _BirdItemWidgetState extends State<BirdItemWidget> {
  Color _readBgColor() {
    var gender = widget.bird.gender.toLowerCase();
    if (gender == 'indefinido') {
      return Colors.brown[50];
    } else if (gender == 'macho') {
      return Colors.lightBlue[50];
    } else if (gender == 'fêmea') {
      return Colors.pink[50];
    } else {
      return Colors.white;
    }
  }

  @override
  Widget build(BuildContext context) {
    var birthDate = DateTime.fromMillisecondsSinceEpoch(widget.bird.birthDate);
    var birthDateFormatted = DateFormat('yyyy').format(birthDate).toString();
    return AppCardWidget(
      isTransfer: widget.isTransfer,
      isTwoOptions: widget.isTwoOptions,
      totalSelectBirds: widget.totalSelectBirds,
      selectGender: widget.selectGender,
      onChangedCheckbox: (value) {
        widget.onChangedCheckbox(widget.bird, widget.isTwoOptions);
      },
      children: [
        AppTextWidget(
          text: '${widget.bird.name}',
          size: Theme.of(context).textTheme.subtitle1.fontSize,
          weight: FontWeight.bold,
        ),
        AppTextWidget(
          text: 'Categoria: ${widget.bird.category}',
          size: Theme.of(context).textTheme.bodyText1.fontSize,
        ),
        AppTextWidget(
          text: 'Raça: ${widget.bird.race}',
          size: Theme.of(context).textTheme.bodyText1.fontSize,
        ),
        AppTextWidget(
          text: 'Cor: ${widget.bird.color}',
          size: Theme.of(context).textTheme.bodyText1.fontSize,
        ),
        AppTextWidget(
          text: 'Gênero: ${widget.bird.gender}',
          size: Theme.of(context).textTheme.bodyText1.fontSize,
        ),
        Container(
          margin: EdgeInsets.only(
            top: 10,
          ),
          child: Row(
            children: <Widget>[
              BirdTagWidget(
                title: widget.bird.leftRing == ''
                    ? '--'
                    : '${widget.bird.leftRing}',
                subtitle: 'Anel\nEsq.',
              ),
              SizedBox(
                width: 5,
              ),
              BirdTagWidget(
                title: widget.bird.rightRing == ''
                    ? '--'
                    : '${widget.bird.rightRing}',
                subtitle: 'Anel\nDir.',
              ),
              SizedBox(
                width: 5,
              ),
              BirdTagWidget(
                title: birthDateFormatted,
                subtitle: 'Ano\nda Ave',
              ),
            ],
          ),
        ),
      ],
      isLocalImage: false,
      image: widget.bird.image,
      bird: widget.bird,
      color: _readBgColor(),
      onPressed: () async {
        if (widget.isSelect) {
          Modular.to.pop(widget.bird);
        } else {
          if (widget.isReport) {
            if (!widget.isTwoOptions) {
              asuka.showSnackBar(
                SnackBar(
                  content: AppMessageWidget(
                    message: 'Aguarde enquanto o relatório é gerado!',
                  ),
                  duration: Duration(
                    milliseconds: 2000,
                  ),
                ),
              );
              reportView(context, widget.bird);
            }
          } else {
            Navigator.pushNamed(
              context,
              '/bird/preview',
              arguments: {
                'id': widget.bird.id,
                'isReport': widget.isReport,
                'isSelect': widget.isSelect
              },
            ).then((value) {
              if (widget.function != null) {
                widget.function();
              }
            });
          }
        }
      },
    );
  }
}
