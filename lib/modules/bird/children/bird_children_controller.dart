import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:mobx/mobx.dart';
import 'package:asuka/asuka.dart' as asuka;
import '../../app/app_message_widget.dart';
import '../bird_model.dart';
import '../bird_repository.dart';
part 'bird_children_controller.g.dart';

class BirdChildrenController = _BirdChildrenController
    with _$BirdChildrenController;

abstract class _BirdChildrenController with Store {
  _BirdChildrenController({
    this.birdRepository,
  });

  final BirdRepository birdRepository;

  @observable
  List<BirdModel> children;

  @action
  Future<void> readChildren(int birdId) async {
    var response = await birdRepository.readChildren(birdId);
    var data = json.decode(response['data']);
    children = List<BirdModel>.from(data.map(
      (e) {
        return BirdModel.fromJson(e);
      },
    ).toList());
  }

  @action
  Future<void> onTapAddChildren(BirdModel bird, BirdModel child) async {
    if (bird.gender == 'Fêmea') {
      child.mother = bird.id;
    }
    if (bird.gender == 'Macho') {
      child.father = bird.id;
    }
    asuka.showSnackBar(
      SnackBar(
        content: AppMessageWidget(
          isLoading: true,
          message: 'Por favor, aguarde ...',
        ),
        duration: Duration(
          milliseconds: 800,
        ),
      ),
    );
    var response = await birdRepository.updateBird(child);
    readChildren(bird.id);
    asuka.showSnackBar(
      SnackBar(
        content: AppMessageWidget(
          message: response['message'],
        ),
        duration: Duration(
          milliseconds: 1000,
        ),
      ),
    );
  }
}
