import 'package:asuka/asuka.dart' as asuka;
import 'package:criacao_pro/modules/app/app_message_widget.dart';
import 'package:criacao_pro/modules/app/app_modal_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../app/app_action_button_widget.dart';
import '../../app/app_appbar_widget.dart';
import '../../app/app_header_widget.dart';
import '../../app/app_scaffold_widget.dart';
import '../../app/app_sub_menu_widget.dart';
import '../../app/app_text_widget.dart';
import '../bird_list_widget.dart';
import '../bird_model.dart';
import 'bird_children_controller.dart';

class BirdChildrenView extends StatefulWidget {
  const BirdChildrenView({
    Key key,
    this.bird,
  }) : super(key: key);

  final BirdModel bird;

  @override
  _BirdChildrenViewState createState() => _BirdChildrenViewState();
}

class _BirdChildrenViewState
    extends ModularState<BirdChildrenView, BirdChildrenController> {
  Widget appBar(BuildContext context) {
    return AppAppBarWidget(
      titles: <Widget>[
        AppTextWidget(
          text: 'Filhos da ave',
          size: Theme.of(context).textTheme.subtitle2.fontSize,
        ),
      ],
      actions: <Widget>[
        IconButton(
          icon: Icon(
            Icons.chevron_left,
          ),
          onPressed: () {
            Navigator.popUntil(context, ModalRoute.withName('/bird/preview'));
          } /*controller.onTapBack*/,
        ),
      ],
    );
  }

  Widget body(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            AppSubMenuWidget(
              image: 'lib/assets/bird-sub-1.png',
              text: 'Detalhes',
              selected: false,
              onPressed: () {
                Navigator.pushNamed(context, '/bird/detail',
                    arguments: {'bird': widget.bird, 'isReport': false});
              },
            ),
            AppSubMenuWidget(
              image: 'lib/assets/bird-sub-2.png',
              text: 'Pais',
              selected: false,
              onPressed: () {
                Navigator.pushNamed(
                  context,
                  '/bird/parent',
                  arguments: {'bird': widget.bird, 'isReport': false},
                );
              },
            ),
            AppSubMenuWidget(
              image: 'lib/assets/bird-sub-3.png',
              text: 'Filhos',
              selected: true,
            ),
            AppSubMenuWidget(
              image: 'lib/assets/bird-sub-4.png',
              text: 'Fotos',
              selected: false,
              onPressed: () {
                Navigator.pushNamed(
                  context,
                  '/bird/image',
                  arguments: {'bird': widget.bird, 'isReport': false},
                );
              },
            ),
          ],
        ),
        SizedBox(
          height: 20,
        ),
        Observer(
          builder: (_) {
            var isEmpty = controller.children == null;
            return AppHeaderWidget(
              title: 'A ave',
              subtitle: 'possui ',
              highlight: '${isEmpty ? 0 : controller.children.length} filhos',
            );
          },
        ),
        SizedBox(
          height: 20,
        ),
        Observer(
          builder: (_) {
            if (controller.children == null) {
              return AppTextWidget(
                text: 'Buscando filhos...',
              );
            } else if (controller.children.isEmpty) {
              return AppTextWidget(
                text: 'Nenhum filho encontrado.',
              );
            } else {
              return BirdListWidget(
                birds: controller.children,
                isReport: true,
                isSelect: false,
              );
            }
          },
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    controller.readChildren(widget.bird.id);
    return AppScaffoldWidget(
      appBar: appBar(context),
      body: body(context),
      floatingActionButton: AppActionAppButtonWidget(
          label: 'Adicionar filho',
          icon: Icons.add,
          color: Colors.green,
          onPressed: () async {
            if (widget.bird.gender == "Indefinido" ||
                widget.bird.gender == "") {
              showModalBottomSheet(
                isDismissible: true,
                context: context,
                builder: (_) {
                  return Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: AppModalWidget(
                      title:
                          'Não é possível adicionar uma ave filha em uma ave de gênero indefinido!',
                      textYes: 'Ok',
                      onPressedYes: () async {
                        Modular.to.pop();
                      },
                    ),
                  );
                },
              );
            } else {
              var child = await Modular.to.pushNamed(
                '/bird',
                arguments: {
                  'isReport': false,
                  'isSelect': true,
                  'id': widget.bird.id,
                  'children': true,
                  'gender': widget.bird.gender
                },
              );
              if (child != null) {
                controller.onTapAddChildren(widget.bird, child);
              }
            }
          }),
    );
  }
}
