// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'bird_children_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$BirdChildrenController on _BirdChildrenController, Store {
  final _$childrenAtom = Atom(name: '_BirdChildrenController.children');

  @override
  List<BirdModel> get children {
    _$childrenAtom.reportRead();
    return super.children;
  }

  @override
  set children(List<BirdModel> value) {
    _$childrenAtom.reportWrite(value, super.children, () {
      super.children = value;
    });
  }

  final _$readChildrenAsyncAction =
      AsyncAction('_BirdChildrenController.readChildren');

  @override
  Future<void> readChildren(int birdId) {
    return _$readChildrenAsyncAction.run(() => super.readChildren(birdId));
  }

  final _$onTapAddChildrenAsyncAction =
      AsyncAction('_BirdChildrenController.onTapAddChildren');

  @override
  Future<void> onTapAddChildren(BirdModel bird, BirdModel child) {
    return _$onTapAddChildrenAsyncAction
        .run(() => super.onTapAddChildren(bird, child));
  }

  @override
  String toString() {
    return '''
children: ${children}
    ''';
  }
}
