import 'package:flutter_modular/flutter_modular.dart';
import '../bird_repository.dart';
import 'bird_children_controller.dart';
import 'bird_children_view.dart';

class BirdChildrenModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind(
          (i) => BirdChildrenController(
            birdRepository: i.get<BirdRepository>(),
          ),
        ),
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(
          '/',
          child: (ctx, args) => BirdChildrenView(
            bird: args.data['bird'],
          ),
        ),
      ];
}
