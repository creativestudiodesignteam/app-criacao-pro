import 'bird_color_model.dart';

class BirdRaceModel {
  BirdRaceModel({
    this.name,
    this.colors,
  });

  BirdRaceModel.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    if (json['colors'] != null) {
      colors = <BirdColorModel>[];
      json['colors'].forEach((v) {
        colors.add(BirdColorModel.fromJson(v));
      });
      if(colors.length > 1){
        colors.sort((a, b) {
          return a.name.compareTo(b.name);
        });
      }
    }
  }

  String name;
  List<BirdColorModel> colors;

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['name'] = name;
    if (colors != null) {
      data['colors'] = colors.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
