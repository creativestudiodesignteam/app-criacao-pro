import 'package:flutter_modular/flutter_modular.dart';
import 'package:dio/dio.dart';
import '../account/account_controller.dart';
import '../cage/baby/cage_baby_controller.dart';
import '../cage/baby/cage_baby_repository.dart';
import '../cage/cage_controller.dart';
import '../cage/cage_repository.dart';
import 'bird_controller.dart';
import 'bird_repository.dart';
import 'bird_view.dart';
import 'children/bird_children_module.dart';
import 'create/bird_create_controller.dart';
import 'create/bird_create_module.dart';
import 'detail/bird_detail_module.dart';
import 'image/bird_image_module.dart';
import 'parent/bird_parent_module.dart';
import 'preview/bird_preview_module.dart';

class BirdModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind(
          (i) => BirdController(
            birdRepository: i.get<BirdRepository>(),
            accountController: i.get<AccountController>(),
            birdCreateController: i.get<BirdCreateController>(),
          ),
        ),
        Bind(
          (i) => BirdCreateController(
            birdRepository: i.get<BirdRepository>(),
            cageBabyController: i.get<CageBabyController>(),
            cageController: i.get<CageController>(),
            accountController: i.get<AccountController>(),
          ),
        ),
        Bind(
          (i) => CageBabyController(
            cageBabyRepository: i.get<CageBabyRepository>(),
          ),
        ),
        Bind(
          (i) => CageController(
            cageRepository: i.get<CageRepository>(),
            accountController: i.get<AccountController>(),
          ),
        ),
        Bind(
          (i) => BirdRepository(
            dio: i.get<Dio>(),
          ),
        ),
        Bind(
          (i) => CageBabyRepository(
            dio: i.get<Dio>(),
          ),
        ),
        Bind(
          (i) => CageRepository(
            dio: i.get<Dio>(),
          ),
        ),
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(
          '/',
          child: (ctx, args) => BirdView(
            isReport: args.data['isReport'],
            isSelect: args.data['isSelect'],
            gender: args.data['gender'],
            id: args.data['id'],
            children: args.data['children'],
          ),
        ),
        ModularRouter(
          '/children',
          module: BirdChildrenModule(),
        ),
        ModularRouter(
          '/create',
          module: BirdCreateModule(),
        ),
        ModularRouter(
          '/detail',
          module: BirdDetailModule(),
        ),
        ModularRouter(
          '/image',
          module: BirdImageModule(),
        ),
        ModularRouter(
          '/parent',
          module: BirdParentModule(),
        ),
        ModularRouter(
          '/preview',
          module: BirdPreviewModule(),
        ),
      ];
}
