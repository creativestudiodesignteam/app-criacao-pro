import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter/services.dart';
import '../../constants.dart' as constants;
import 'bird_category_model.dart';
import 'bird_gender_model.dart';
import 'bird_model.dart';
import 'bird_status_model.dart';

class BirdRepository {
  BirdRepository({
    this.dio,
  });

  final Dio dio;

  final String path = '${constants.appUrl}/bird';
  final Options options = Options(
    headers: {
      'X-Token': constants.appSecret,
    },
  );

  Future<Map<String, dynamic>> createBird(BirdModel bird) async {
    var formData = FormData.fromMap(bird.toJson());
    var response = await dio.post(
      '${path}/create',
      data: formData,
      options: options,
    );
    return json.decode(response.data);
  }

  Future<Map<String, dynamic>> readBird(int birdId) async {
    var formData = FormData.fromMap(
      {
        'id': birdId,
      },
    );
    var response = await dio.post(
      '${path}/read',
      data: formData,
      options: options,
    );
    return json.decode(response.data);
  }

  Future<Map<String, dynamic>> readAllBirds(
      int accountId, String gender, int id, int lastId) async {
    var formData = FormData.fromMap(
      {'account': accountId, 'gender': gender, 'id': id, 'lastId': lastId},
    );
    print("accountId");
    print(accountId);
    print("gender");
    print(gender);
    print("id");
    print(id);
    print("lastId");
    print(lastId);
    var response = await dio.post(
      '${path}/read-all-by-account-pagination',
      data: formData,
      options: options,
    );
    return json.decode(response.data);
  }

  Future<Map<String, dynamic>> readAllBirdsCage(
      int accountId, String gender, int id) async {
    var formData = FormData.fromMap(
      {
        'account': accountId,
        'gender': gender,
        'id': id,
      },
    );
    var response = await dio.post(
      '${path}/read-all-by-account',
      data: formData,
      options: options,
    );
    return json.decode(response.data);
  }

  Future<Map<String, dynamic>> readChildren(int birdId) async {
    var formData = FormData.fromMap(
      {
        'birdId': birdId,
      },
    );
    var response = await dio.post(
      '${path}/read-children',
      data: formData,
      options: options,
    );
    return json.decode(response.data);
  }

  Future<Map<String, dynamic>> createImage(
      int birdId, String base64, int account) async {
    var formData = FormData.fromMap(
      {
        'account': account,
        'bird': birdId,
        'base64': base64,
      },
    );
    var response = await dio.post(
      '${path}/create-image',
      data: formData,
      options: options,
    );
    return json.decode(response.data);
  }

  Future<Map<String, dynamic>> readImage(int birdId) async {
    var formData = FormData.fromMap(
      {
        'bird': birdId,
      },
    );
    var response = await dio.post(
      '${path}/read-image',
      data: formData,
      options: options,
    );
    return json.decode(response.data);
  }

  Future<Map<String, dynamic>> updateImage(int birdId, String image) async {
    var formData = FormData.fromMap(
      {
        'bird': birdId,
        'image': image,
      },
    );
    var response = await dio.post(
      '${path}/update-image',
      data: formData,
      options: options,
    );
    return json.decode(response.data);
  }

  Future<Map<String, dynamic>> deleteImage(String id) async {
    var formData = FormData.fromMap(
      {
        'url': id,
      },
    );
    var response = await dio.post(
      '${path}/delete-image',
      data: formData,
      options: options,
    );
    return json.decode(response.data);
  }

  Future<Map<String, dynamic>> updateBird(BirdModel bird) async {
    var formData = FormData.fromMap(bird.toJson());
    var response = await dio.post(
      '${path}/update',
      data: formData,
      options: options,
    );
    return json.decode(response.data);
  }

  Future<Map<String, dynamic>> deleteBird(int birdId) async {
    var formData = FormData.fromMap(
      {
        'id': birdId,
      },
    );
    var response = await dio.post(
      '${path}/delete',
      data: formData,
      options: options,
    );
    return json.decode(response.data);
  }

  Future<List<BirdGenderModel>> readAllGenders() async {
    return [
      BirdGenderModel(
        id: 0,
        name: '',
      ),
      BirdGenderModel(
        id: 1,
        name: 'Indefinido',
      ),
      BirdGenderModel(
        id: 2,
        name: 'Fêmea',
      ),
      BirdGenderModel(
        id: 3,
        name: 'Macho',
      ),
    ];
  }

  Future<List<BirdStatusModel>> readAllStatus() async {
    return [
      BirdStatusModel(
        id: 0,
        name: '',
      ),
      BirdStatusModel(
        id: 1,
        name: 'Vivo',
      ),
      BirdStatusModel(
        id: 2,
        name: 'Morto',
      ),
      BirdStatusModel(
        id: 3,
        name: 'Á venda',
      ),
      BirdStatusModel(
        id: 4,
        name: 'Vendido',
      ),
    ];
  }

  Future<List<BirdCategoryModel>> readAllCategories() async {
    var file = await rootBundle.loadString('lib/assets/categories.json');
    List<dynamic> data = json.decode(file);
    data.sort((a, b) {
      return a['name'].toLowerCase().compareTo(b['name'].toLowerCase());
    });
    return data.map((dado) => BirdCategoryModel.fromJson(dado)).toList();
  }
}
