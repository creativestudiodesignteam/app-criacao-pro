import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../app/app_action_button_widget.dart';
import '../app/app_appbar_widget.dart';
import '../app/app_button_widget.dart';
import '../app/app_dropdown_widget.dart';
import '../app/app_header_widget.dart';
import '../app/app_scaffold_widget_multi.dart';
import '../app/app_text_widget.dart';
import '../app/app_textfield_widget.dart';
import 'bird_controller.dart';
import 'bird_list_widget.dart';

class BirdView extends StatefulWidget {
  const BirdView({
    Key key,
    this.isReport = false,
    this.isSelect = false,
    @required this.gender,
    @required this.id,
    this.children = false,
  }) : super(key: key);

  final bool isReport;
  final bool isSelect;
  final String gender;
  final int id;
  final bool children;

  @override
  _BirdViewState createState() => _BirdViewState();
}

class _BirdViewState extends ModularState<BirdView, BirdController> {
  ScrollController _sc;
  bool isLoading = true;

  @override
  void initState() {
    super.initState();
    asyncInit();
    _sc = new ScrollController()..addListener(_scrollListener);
  }

  @override
  void dispose() {
    _sc.removeListener(_scrollListener);
    super.dispose();
  }

  void _scrollListener() {
    if (_sc.position.maxScrollExtent == _sc.position.pixels) {
      if (int.parse(controller.totalBirds) > controller.birds.length) {
        controller.readAllBirds(widget.gender, widget.id);
      }
    }
  }

  Widget appBar(BuildContext context) {
    return AppAppBarWidget(
      titles: <Widget>[
        AppTextWidget(
          text: widget.isSelect ? 'Selecione uma ave' : 'Aves',
          size: Theme.of(context).textTheme.subtitle2.fontSize,
        ),
      ],
      actions: <Widget>[
        IconButton(
          icon: Icon(
            Icons.sort,
          ),
          onPressed: () {
            showModalBottomSheet(
              context: context,
              builder: (_) {
                return SingleChildScrollView(
                  padding: EdgeInsets.all(30),
                  child: Container(
                    decoration: BoxDecoration(),
                    child: Column(
                      children: <Widget>[
                        AppTextWidget(
                          size: 15,
                          text: 'Filtros',
                          weight: FontWeight.bold,
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Row(
                          children: <Widget>[
                            Flexible(
                              child: AppTextFieldWidget(
                                label: 'Nome da ave',
                                controller: controller
                                    .birdCreateController.nameController,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Observer(
                          builder: (_) {
                            var loading =
                                controller.birdCreateController.categories ==
                                    null;
                            return AppDropdownWidget(
                              label: 'Categoria',
                              value: loading
                                  ? ' ... '
                                  : controller
                                      .birdCreateController.category.name,
                              items: loading
                                  ? [
                                      ' ... ',
                                    ]
                                  : controller.birdCreateController.categories
                                      .map((e) => e.name)
                                      .toList(),
                              onChanged: (value) {
                                controller.birdCreateController.category =
                                    controller.birdCreateController.categories
                                        .firstWhere((e) => e.name == value);
                                controller.birdCreateController.race =
                                    controller
                                        .birdCreateController.category.races
                                        .elementAt(0);
                                controller.birdCreateController.color =
                                    controller.birdCreateController.race.colors
                                        .elementAt(0);
                              },
                            );
                          },
                        ),
                        Observer(
                          builder: (_) {
                            var loading =
                                controller.birdCreateController.category ==
                                    null;
                            return AppDropdownWidget(
                              label: 'Raça',
                              value: loading
                                  ? ' ... '
                                  : controller.birdCreateController.race.name,
                              items: loading
                                  ? [
                                      ' ... ',
                                    ]
                                  : controller
                                      .birdCreateController.category.races
                                      .map((e) => e.name)
                                      .toList(),
                              onChanged: (value) {
                                controller.birdCreateController.race =
                                    controller
                                        .birdCreateController.category.races
                                        .firstWhere((e) => e.name == value);
                                controller.birdCreateController.color =
                                    controller.birdCreateController.race.colors
                                        .elementAt(0);
                              },
                            );
                          },
                        ),
                        Observer(
                          builder: (_) {
                            var loading =
                                controller.birdCreateController.race == null;
                            return AppDropdownWidget(
                              label: 'Cor',
                              value: loading
                                  ? ' ... '
                                  : controller.birdCreateController.color.name,
                              items: loading
                                  ? [
                                      ' ... ',
                                    ]
                                  : controller.birdCreateController.race.colors
                                      .map((e) => e.name)
                                      .toList(),
                              onChanged: (value) {
                                controller.birdCreateController.color =
                                    controller.birdCreateController.race.colors
                                        .firstWhere((e) => e.name == value);
                              },
                            );
                          },
                        ),
                        Row(
                          children: <Widget>[
                            Flexible(
                              child: Observer(
                                builder: (_) {
                                  var loading =
                                      controller.birdCreateController.genders ==
                                          null;
                                  return AppDropdownWidget(
                                    label: 'Gênero',
                                    value: loading
                                        ? ' ... '
                                        : controller
                                            .birdCreateController.gender.name,
                                    items: loading
                                        ? [
                                            ' ... ',
                                          ]
                                        : controller
                                            .birdCreateController.genders
                                            .map((e) => e.name)
                                            .toList(),
                                    onChanged: (value) {
                                      controller.birdCreateController.gender =
                                          controller
                                              .birdCreateController.genders
                                              .firstWhere(
                                                  (e) => e.name == value);
                                    },
                                  );
                                },
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Flexible(
                              child: Observer(
                                builder: (_) {
                                  var loading = controller
                                          .birdCreateController.statuses ==
                                      null;
                                  return AppDropdownWidget(
                                    label: 'Status',
                                    value: loading
                                        ? ' ... '
                                        : controller
                                            .birdCreateController.statuses
                                            .elementAt(0)
                                            .name,
                                    items: loading
                                        ? [
                                            ' ... ',
                                          ]
                                        : controller
                                            .birdCreateController.statuses
                                            .map((e) => e.name)
                                            .toList(),
                                    onChanged: (value) {
                                      controller.birdCreateController.status =
                                          controller
                                              .birdCreateController.statuses
                                              .firstWhere(
                                                  (e) => e.name == value);
                                    },
                                  );
                                },
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Flexible(
                              child: AppTextFieldWidget(
                                label: 'Nascimento',
                                controller: controller
                                    .birdCreateController.birthController,
                                keyboardType: TextInputType.datetime,
                                onTap: () async {
                                  var date = await showDatePicker(
                                    context: context,
                                    initialDate: DateTime.now(),
                                    firstDate: DateTime(2010),
                                    lastDate: DateTime.now(),
                                  );
                                  if (date != null) {
                                    controller.onTapBirthDate(date);
                                  }
                                },
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            AppButtonWidget(
                              text: 'Aplicar filtros',
                              color: Theme.of(context).primaryColor,
                              onPressed: controller.onTapApplyFilters,
                            ),
                            InkWell(
                                child: Text(
                                  'Limpar Filtro',
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                onTap: () async {
                                  controller.onTapCleanFilter(
                                      widget.gender, widget.id);
                                  controller.birdCreateController.nameController
                                      .text = '';
                                  controller.birdCreateController.status =
                                      controller
                                          .birdCreateController.statuses[0];
                                  controller.birdCreateController.gender =
                                      controller
                                          .birdCreateController.genders[0];
                                  controller.birdCreateController.categories =
                                      null;
                                  controller.birdCreateController.race = null;
                                  controller.birdCreateController.color = null;
                                  await controller.birdCreateController
                                      .readAllCategories();
                                  await controller.birdCreateController
                                      .readAllGenders();
                                  await controller.birdCreateController
                                      .readAllStatuses();
                                  controller.birdCreateController
                                      .birthController.text = '';
                                }),
                          ],
                        ),
                      ],
                    ),
                  ),
                );
              },
            );
          },
        ),
        IconButton(
          icon: Icon(
            Icons.chevron_left,
          ),
          onPressed: controller.onTapBack,
        ),
      ],
    );
  }

  Widget body(BuildContext context) {
    return Column(
      children: <Widget>[
        Observer(
          builder: (_) {
            if (controller.birds == null) {
              return AppHeaderWidget(
                title: 'Por favor',
                subtitle: 'aguarde ',
                highlight: ' ... ',
              );
            } else {
              return AppHeaderWidget(
                title: 'Você possui',
                subtitle: 'um total de ',
                highlight: '${controller.totalBirds} aves',
              );
            }
          },
        ),
        SizedBox(
          height: 20,
        ),
        !widget.isReport
            ? widget.isSelect
                ? SizedBox()
                : Observer(
                    builder: (_) {
                      return Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          RaisedButton.icon(
                            elevation: 0,
                            textColor: Colors.white,
                            color: Theme.of(context).primaryColor,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8),
                            ),
                            icon: Icon(
                              controller.isTransfer ? Icons.done : Icons.edit,
                            ),
                            onPressed: controller.isTransfer
                                ? controller.onPressedTransferDone
                                : controller.onPressedTransfer,
                            label: AppTextWidget(
                              text: controller.isTransfer
                                  ? 'Mover aves'
                                  : 'Selecionar aves',
                            ),
                          ),
                        ],
                      );
                    },
                  )
            : Container(),
        SizedBox(
          height: 20,
        ),
        Observer(
          builder: (_) {
            if (controller.birds == null) {
              return AppTextWidget(
                text: 'Buscando aves...',
              );
            } else if (controller.birds.isEmpty) {
              return AppTextWidget(
                text: 'Nenhuma ave encontrada.',
              );
            } else {
              return BirdListWidget(
                birds: controller.birds,
                isReport: widget.isReport,
                isSelect: widget.isSelect,
                isTransfer: controller.isTransfer,
                onChangedCheckbox: controller.onChangedCheckbox,
                function: asyncInit,
                readAllBirds: controller.readAllBirds,
                gender: widget.gender,
                id: widget.id,
                totalBirds: controller.totalBirds,
                sc: _sc,
              );
            }
          },
        ),
      ],
    );
  }

  void function() {
    setState(() {});
  }

  void asyncInit() async {
    controller.firstAcessInFunction = true;
    controller.birds = null;
    controller.children = widget.children;

    await controller.readAllBirds(widget.gender, widget.id);
    await controller.birdCreateController.readAllCategories();
    await controller.birdCreateController.readAllGenders();
    await controller.birdCreateController.readAllStatuses();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    controller.birdCreateController.birthController.text = '';
    controller.birdCreateController.nameController.text = '';
    return AppScaffoldWidgetMulti(
      appBar: appBar(context),
      body: body(context),
      mainAxisAlignment: widget.isReport
          ? MainAxisAlignment.end
          : MainAxisAlignment.spaceEvenly,
      sc: _sc,
      floatingActionButton: Observer(
        builder: (_) {
          return widget.isReport || controller.isTransfer
              ? SizedBox(
                  width: MediaQuery.of(context).size.width * 0.58,
                )
              : AppActionAppButtonWidget(
                  label: 'Adicionar ave',
                  icon: Icons.add,
                  color: Colors.green,
                  onPressed: () {
                    Navigator.pushNamed(
                      context,
                      '/bird/create',
                      arguments: {'baby': null},
                    ).then((value) => asyncInit());
                  },
                );
        },
      ),
    );
  }
}
