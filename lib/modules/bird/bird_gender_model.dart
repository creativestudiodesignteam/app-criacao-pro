class BirdGenderModel {
  BirdGenderModel({
    this.id,
    this.name,
  });

  final int id;
  final String name;
}
