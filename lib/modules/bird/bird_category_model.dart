import 'bird_race_model.dart';

class BirdCategoryModel {
  BirdCategoryModel({
    this.name,
    this.races,
  });

  BirdCategoryModel.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    if (json['races'] != null) {
      races = <BirdRaceModel>[];
      json['races'].forEach((v) {
        races.add(BirdRaceModel.fromJson(v));
      });
      if(races.length > 1){
        races.sort((a, b) {
          return a.name.compareTo(b.name);
        });
      }

    }
  }

  String name;
  List<BirdRaceModel> races;

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['name'] = name;
    if (races != null) {
      data['races'] = races.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
