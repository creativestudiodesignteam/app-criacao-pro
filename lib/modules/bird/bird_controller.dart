import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:intl/intl.dart';
import 'package:mobx/mobx.dart';
import 'package:asuka/asuka.dart' as asuka;
import '../account/account_controller.dart';
import '../app/app_message_widget.dart';
import '../cage/cage_model.dart';
import 'bird_model.dart';
import 'bird_repository.dart';
import 'create/bird_create_controller.dart';
part 'bird_controller.g.dart';

class BirdController = _BirdController with _$BirdController;

abstract class _BirdController with Store {
  _BirdController({
    this.birdRepository,
    this.accountController,
    this.birdCreateController,
  });

  final BirdRepository birdRepository;
  final AccountController accountController;
  final BirdCreateController birdCreateController;

  @observable
  bool isTransfer = false;

  @observable
  List<BirdModel> birds;

  @observable
  bool children = false;

  @observable
  List<BirdModel> selectedBirds = [];

  @observable
  int lastId = 1;

  @observable
  String totalBirds = '0';

  @observable
  bool clickReport = false;

  bool firstAcessInFunction = true;
  List<BirdModel> birdsList;
  int account;

  @action
  Future<void> onChangedCheckbox(BirdModel bird, bool isTwoOptions) async {
    if (isTwoOptions) {
      if (selectedBirds.length < 2) {
        if (!selectedBirds.contains(bird)) {
          selectedBirds.add(bird);
        }
      }
    } else {
      if (!selectedBirds.contains(bird)) {
        selectedBirds.add(bird);
      }
    }
  }

  @action
  Future<void> onPressedTransfer() async {
    isTransfer = !isTransfer;
    selectedBirds = [];
  }

  @action
  Future<void> onPressedTransferDone() async {
    isTransfer = !isTransfer;
    if (selectedBirds.isEmpty) {
      asuka.showSnackBar(
        SnackBar(
          content: AppMessageWidget(
            message: 'Você não selecionou nenhuma ave.',
          ),
          duration: Duration(
            milliseconds: 800,
          ),
        ),
      );
      return;
    }
    var cage = await Modular.to.pushNamed(
      '/cage',
      arguments: {
        'isSelect': true,
      },
    ) as CageModel;
    if (cage != null) {
      for (var bird in selectedBirds) {
        bird.cage = cage.id;
        await birdRepository.updateBird(bird);
      }
      selectedBirds = [];
      asuka.showSnackBar(
        SnackBar(
          content: AppMessageWidget(
            message: 'As aves foram movidas com sucesso!',
          ),
          duration: Duration(
            milliseconds: 800,
          ),
        ),
      );
    }
  }

  @action
  Future<void> onTapApplyFilters() async {
    var filtered = [];
    var birdsFiltered = [];
    var birdsFilter = [];
    await readAllBirdsCage(null, null);
    birdsList = birds;
    var nameBirds;
    var raceBirds;
    var colorBirds;
    var genderBirds;
    var statusBirds;
    var birthDateBirds;

    if (birdCreateController.nameController.text != '') {
      print('ASDASDSADSADSA');
      nameBirds = birdsList.where((b) {
        return b.name.indexOf(birdCreateController.nameController.text) == 0;
      });
      filtered.addAll(nameBirds);
    }

    if (birdCreateController.category.name != '') {
      var categoryBirds;
      var filteredTemp = filtered;
      filtered = [];
      if (filteredTemp.length > 0) {
        categoryBirds = filteredTemp.where((b) {
          return b.category == birdCreateController.category.name;
        });
        filtered.addAll(categoryBirds);
      } else {
        categoryBirds = birdsList.where((b) {
          return b.category == birdCreateController.category.name;
        });
        filtered.addAll(categoryBirds);
      }
    }

    if (birdCreateController.race.name != '') {
      var filteredTemp = filtered;
      filtered = [];
      if (filteredTemp.length > 0) {
        raceBirds = filteredTemp.where((b) {
          return b.race == birdCreateController.race.name;
        });
        filtered.addAll(raceBirds);
      } else {
        raceBirds = birdsList.where((b) {
          return b.race == birdCreateController.race.name;
        });
        filtered.addAll(raceBirds);
      }
    }

    if (birdCreateController.color.name != '') {
      var filteredTemp = filtered;
      filtered = [];
      if (filteredTemp.length > 0) {
        colorBirds = filteredTemp.where((b) {
          return b.color == birdCreateController.color.name;
        });
        filtered.addAll(colorBirds);
      } else {
        colorBirds = birdsList.where((b) {
          return b.color == birdCreateController.color.name;
        });
        filtered.addAll(colorBirds);
      }
    }

    if (birdCreateController.gender.name != '') {
      var filteredTemp = filtered;
      filtered = [];
      if (filteredTemp.length > 0) {
        genderBirds = filteredTemp.where((b) {
          return b.gender == birdCreateController.gender.name;
        });
        filtered.addAll(genderBirds);
      } else {
        genderBirds = birdsList.where((b) {
          return b.gender == birdCreateController.gender.name;
        });
        filtered.addAll(genderBirds);
      }
    }

    if (birdCreateController.status.name != '') {
      var filteredTemp = filtered;
      filtered = [];
      if (filteredTemp.length > 0) {
        statusBirds = filteredTemp.where((b) {
          return b.status == birdCreateController.status.name;
        });
        filtered.addAll(statusBirds);
      } else {
        statusBirds = birdsList.where((b) {
          return b.status == birdCreateController.status.name;
        });
        filtered.addAll(statusBirds);
      }
    }

    if (birdCreateController.birthController.text != '') {
      var filteredTemp = filtered;
      filtered = [];
      if (filteredTemp.length > 0) {
        birthDateBirds = filteredTemp.where((b) {
          return b.birthDate == birdCreateController.birthController.text;
        });
        filtered.addAll(birthDateBirds);
      } else {
        birthDateBirds = birdsList.where((b) {
          return b.birthDate == birdCreateController.birthController.text;
        });
        filtered.addAll(birthDateBirds);
      }
    }

    birdsFiltered = filtered;
    var addArray;
    var elementNumber;
    birdsFiltered.forEach((elementFiltered) {
      addArray = true;
      elementNumber = 0;
      birdsFiltered.forEach((element) {
        if (element.id == elementFiltered.id) {
          if (elementNumber == 0) {
            elementNumber = elementNumber + 1;
          } else if (elementNumber > 0) {
            addArray = false;
          }
        }
      });
      if (addArray) {
        birdsFilter.add(elementFiltered);
      }
    });
    birds = List<BirdModel>.from(birdsFilter);
    totalBirds = birds.length.toString();
    Modular.to.pop();
  }

  Future<void> onTapBirthDate(DateTime date) async {
    birdCreateController.birthController.text =
        DateFormat('dd/MM/yyyy').format(date).toString();
  }

  @action
  Future<void> onTapBack() async {
    Modular.to.pop();
  }

  @action
  Future<void> onTapCleanFilter(String gender, int id) async {
    firstAcessInFunction = true;
    birds = null;
    readAllBirds(gender, id);
  }

  @action
  Future<void> readAllBirds(String gender, int id) async {
    var birdsTemp = [];
    var genderTemp;
    if (firstAcessInFunction) {
      account = accountController.account.id;
      birdsList = List<BirdModel>();
      lastId = 0;
      firstAcessInFunction = false;
    }
    if (children == true) {
      genderTemp = gender;
      gender = null;
    }
    if (gender != null) {
      birdsList = List<BirdModel>();
    }

    if (birds == null || int.parse(totalBirds) > birds.length) {
      var response =
          await birdRepository.readAllBirds(account, gender, id, lastId);
      var data = json.decode(response['data']);
      totalBirds = response['total'] == '' ? '0' : response['total'];
      List<dynamic> list = data;
      list.sort((a, b) {
        return a['name'].toLowerCase().compareTo(b['name'].toLowerCase());
      });
      var bird = List<BirdModel>.from(
        data.map((e) {
          return BirdModel.fromJson(e);
        }).toList(),
      );
      // await bird.forEach((element) async {

      // });
      if (children == true) {
        // birds = birdsList;
        await bird.forEach((element) {
          if (element.id == id) {
            birdsTemp.add(element);
          } else {
            if (genderTemp == "Macho") {
              if (element.father == id) {
                birdsTemp.add(element);
              }
            } else if (genderTemp == "Fêmea") {
              if (element.mother == id) {
                birdsTemp.add(element);
              }
            }
          }
        });
        await bird.forEach((element) async {
          var addElement = true;
          if (element.id > lastId) {
            lastId = element.id;
          }

          if (genderTemp == "Macho") {
            await birdsTemp.forEach((elementbird) {
              if ((elementbird.father == element.id ||
                      elementbird.mother == element.id ||
                      elementbird.id == element.id) &&
                  (elementbird.mother != null || elementbird.mother != '')) {
                addElement = false;
              }
            });
          } else if (genderTemp == "Fêmea") {
            await birdsTemp.forEach((elementbird) {
              if ((elementbird.mother == element.id ||
                      elementbird.father == element.id ||
                      elementbird.id == element.id) &&
                  (elementbird.father != null || elementbird.father != '')) {
                addElement = false;
              }
            });
          }
          if (addElement) {
            birdsList.add(element);
          }
        });
        totalBirds = birdsList.length.toString();
      } else {
        if (gender != '' || gender != null) {
          await bird.forEach((element) {
            var addElement = true;
            if (gender == "Macho") {
              if (element.father == id) {
                addElement = false;
                birdsTemp.add(element);
              }
            } else if (gender == "Fêmea") {
              if (element.mother == id) {
                addElement = false;
                birdsTemp.add(element);
              }
            }
            if (addElement) {
              birdsList.add(element);
            }
          });
          totalBirds = birdsList.length.toString();
        } else {
          await bird.forEach((element) async {
            if (element.id > lastId) {
              lastId = element.id;
            }
            await birdsList.add(element);
          });
        }
      }
      birds = birdsList;
    }
  }

  @action
  Future<void> readAllBirdsCage(String gender, int id) async {
    var response = await birdRepository.readAllBirdsCage(
        accountController.account.id, gender, id);
    var data = json.decode(response['data']);
    List<dynamic> list = data;
    list.sort((a, b) {
      return a['name'].toLowerCase().compareTo(b['name'].toLowerCase());
    });
    birds = List<BirdModel>.from(data.map((e) {
      return BirdModel.fromJson(e);
    }).toList());
  }
}
