import 'package:flutter/material.dart';
import 'bird_image_item_widget.dart';

class BirdImageListWidget extends StatelessWidget {
  const BirdImageListWidget({
    Key key,
    this.images,
    @required this.function,
    @required this.deleteFunction,
    @required this.favorite,
  }) : super(key: key);

  final List<String> images;
  final Function(String) function;
  final Function(String) deleteFunction;
  final String favorite;

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        childAspectRatio: 1,
      ),
      itemCount: images.length,
      itemBuilder: (_, i) {
        return BirdImageItemWidget(
          image: images.elementAt(i),
          images: images,
          index: i,
          function: function,
          deleteFunction: deleteFunction,
          favorite: favorite,
        );
      },
    );
  }
}
