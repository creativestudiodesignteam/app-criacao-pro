import 'package:flutter/material.dart';

import 'bird_image_viewer.dart';

class BirdImageItemWidget extends StatelessWidget {
  const BirdImageItemWidget({
    Key key,
    this.image,
    this.images,
    this.index,
    @required this.function,
    @required this.deleteFunction,
    @required this.favorite,
  }) : super(key: key);

  final String image;
  final List<String> images;
  final int index;
  final Function(String) function;
  final Function(String) deleteFunction;
  final String favorite;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(30),
      ),
      child: GestureDetector(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => ImageViewer(
                images: images,
                index: index,
                function: function,
                deleteFunction: deleteFunction,
                favorite: favorite,
              ),
            ),
          );
        },
        child: Image.network(
          image,
          fit: BoxFit.fill,
        ),
      ),
    );
  }
}
