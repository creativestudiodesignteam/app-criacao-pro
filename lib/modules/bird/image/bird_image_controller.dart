import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:image_picker/image_picker.dart';
import 'package:asuka/asuka.dart' as asuka;
import '../../app/app_message_widget.dart';
import '../bird_model.dart';
import '../bird_repository.dart';
part 'bird_image_controller.g.dart';

class BirdImageController = _BirdImageController with _$BirdImageController;

abstract class _BirdImageController with Store {
  _BirdImageController({
    this.birdRepository,
  });

  final BirdRepository birdRepository;

  @observable
  List<String> images;

  @action
  Future<void> readImages(int birdId) async {
    var response = await birdRepository.readImage(birdId);
    var data = json.decode(response['data']);
    images = List<String>.from(data.map((e) {
      return e['url'];
    }).toList());
    if (images.length == 1) {
      updateImage(birdId, data[0]['url']);
    }
  }

  @action
  Future<void> updateImage(int birdId, String image) async {
    await birdRepository.updateImage(birdId, image);
  }

  @action
  Future<void> deleteImage(String url) async {
    await birdRepository.deleteImage(url);
  }

  @action
  Future<void> onTapBirdImage(BirdModel bird) async {
    Modular.to.pushReplacementNamed(
      '/bird/image',
      arguments: {
        'bird': bird,
        'isReport': false,
      },
    );
  }

  Future<void> onTapAddImage(BirdModel bird, int source) async {
    Modular.to.pop();
    var imagePicker = ImagePicker();
    var image = await imagePicker.getImage(
      imageQuality: 20,
      source: source == 1 ? ImageSource.camera : ImageSource.gallery,
    );
    asuka.showSnackBar(
      SnackBar(
        content: AppMessageWidget(
          isLoading: true,
          message: 'Por favor, aguarde ...',
        ),
        duration: Duration(
          milliseconds: 800,
        ),
      ),
    );
    var imageBytes = await image.readAsBytes();
    var base64 = base64Encode(imageBytes);
    var response =
        await birdRepository.createImage(bird.id, base64, bird.account);
    readImages(bird.id);
    Modular.to.pushNamed(
      '/bird/image',
      arguments: {
        'bird': bird,
      },
    );
    asuka.showSnackBar(
      SnackBar(
        content: AppMessageWidget(
          message: response['message'],
        ),
        duration: Duration(
          milliseconds: 1000,
        ),
      ),
    );
  }
}
