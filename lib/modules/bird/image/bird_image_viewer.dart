import 'dart:async';

import 'package:asuka/asuka.dart' as asuka;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:photo_view/photo_view_gallery.dart';

import '../../../constants.dart' as constants;
import '../../app/app_message_widget.dart';

class ImageViewer extends StatefulWidget {
  ImageViewer({
    @required this.images,
    @required this.index,
    @required this.function,
    @required this.deleteFunction,
    @required this.favorite,
  });

  final List<String> images;
  final int index;
  final Function(String) function;
  final Function(String) deleteFunction;
  final String favorite;

  @override
  _ImageViewerState createState() => _ImageViewerState();
}

class _ImageViewerState extends State<ImageViewer> {
  IconBloc iconBloc = IconBloc();
  int intPageController = 0;
  PageController pageController = PageController(initialPage: 0);
  List<PhotoViewGalleryPageOptions> galleryItems = [];
  TextEditingController controller = TextEditingController();
  String cacheDirectory;
  final String path = constants.appUrl;

  @override
  void initState() {
    super.initState();
    _init();
  }

  _init() async {
    pageController = PageController(initialPage: widget.index);
    intPageController = widget.index;
  }

  @override
  Widget build(BuildContext context) {
    for (var i = 0; i < widget.images.length; i++) {
      galleryItems.add(
        PhotoViewGalleryPageOptions(
            imageProvider: NetworkImage(
          widget.images[i],
        )),
      );
    }
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.navigate_before,
            color: Colors.white,
          ),
          onPressed: () => Navigator.of(context).pop(),
        ),
        centerTitle: true,
        brightness: Brightness.dark,
        backgroundColor: Colors.black,
        elevation: 0.0,
        title: Padding(
          padding: const EdgeInsets.only(top: 0.0),
          child: Row(
            children: <Widget>[
              Expanded(
                child: TextField(
                  controller: controller,
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                  enabled: false,
                ),
              ),
            ],
          ),
        ),
        actions: [
          IconButton(
            onPressed: () async {
              asuka.showSnackBar(
                SnackBar(
                  content: AppMessageWidget(
                    isLoading: true,
                    message: 'Por favor, aguarde ...',
                  ),
                  duration: Duration(
                    milliseconds: 800,
                  ),
                ),
              );
              if (widget.images.length > 1) {
                if (widget.images[intPageController] == widget.favorite) {
                  await widget.deleteFunction(widget.images[intPageController]);
                  String image;
                  for (var i in widget.images) {
                    if (i != widget.favorite) image = i;
                  }
                  await widget.function(image);
                } else {
                  await widget.deleteFunction(widget.images[intPageController]);
                }
              } else {
                await widget.deleteFunction(widget.images[intPageController]);
                await widget.function('$path/src/uploads/bird.png');
              }
              await widget.deleteFunction(widget.images[intPageController]);
              asuka.showSnackBar(
                SnackBar(
                  content: AppMessageWidget(
                    message: 'Imagem foi removida com sucesso',
                  ),
                  duration: Duration(
                    milliseconds: 1000,
                  ),
                ),
              );
              Navigator.pop(context);
            },
            icon: Icon(Icons.delete_forever),
            color: Colors.white,
          ),
          IconButton(
            onPressed: () async {
              asuka.showSnackBar(
                SnackBar(
                  content: AppMessageWidget(
                    isLoading: true,
                    message: 'Por favor, aguarde ...',
                  ),
                  duration: Duration(
                    milliseconds: 800,
                  ),
                ),
              );
              await widget.function(widget.images[intPageController]);
              asuka.showSnackBar(
                SnackBar(
                  content: AppMessageWidget(
                    message: 'Imagem favorita alterada com sucesso',
                  ),
                  duration: Duration(
                    milliseconds: 1000,
                  ),
                ),
              );
            },
            icon: Icon(Icons.favorite),
            color: widget.images[intPageController] == widget.favorite
                ? Colors.red
                : Colors.white,
          ),
        ],
      ),
      body: PhotoViewGallery(
        pageOptions: galleryItems,
        pageController: pageController,
        onPageChanged: (page) {
          intPageController = page;
        },
      ),
    );
  }
}

class IconBloc {
  final StreamController streamController = StreamController.broadcast();

  Sink get input => streamController.sink;

  Stream get output => streamController.stream;

  void iconShow() {
    input.add(1);
  }

  void iconHide() {
    input.add(2);
  }

  void dispose() {
    streamController.close();
  }
}
