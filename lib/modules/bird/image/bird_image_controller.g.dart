// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'bird_image_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$BirdImageController on _BirdImageController, Store {
  final _$imagesAtom = Atom(name: '_BirdImageController.images');

  @override
  List<String> get images {
    _$imagesAtom.reportRead();
    return super.images;
  }

  @override
  set images(List<String> value) {
    _$imagesAtom.reportWrite(value, super.images, () {
      super.images = value;
    });
  }

  final _$readImagesAsyncAction =
      AsyncAction('_BirdImageController.readImages');

  @override
  Future<void> readImages(int birdId) {
    return _$readImagesAsyncAction.run(() => super.readImages(birdId));
  }

  final _$updateImageAsyncAction =
  AsyncAction('_BirdImageController.updateImage');

  @override
  Future<void> updateImage(int birdId, String image) {
    return _$updateImageAsyncAction.run(() => super.updateImage(birdId, image));
  }

  final _$deleteImageAsyncAction =
  AsyncAction('_BirdImageController.deleteImage');

  @override
  Future<void> deleteImage(String url) {
    return _$deleteImageAsyncAction.run(() => super.deleteImage(url));
  }

  final _$onTapBirdImageAsyncAction =
      AsyncAction('_BirdImageController.onTapBirdImage');

  @override
  Future<void> onTapBirdImage(BirdModel bird) {
    return _$onTapBirdImageAsyncAction.run(() => super.onTapBirdImage(bird));
  }

  @override
  String toString() {
    return '''
images: ${images}
    ''';
  }
}
