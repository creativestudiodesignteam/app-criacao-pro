import 'package:flutter_modular/flutter_modular.dart';
import '../bird_repository.dart';
import 'bird_image_controller.dart';
import 'bird_image_view.dart';

class BirdImageModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind(
          (i) => BirdImageController(
            birdRepository: i.get<BirdRepository>(),
          ),
        ),
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(
          '/',
          child: (ctx, args) => BirdImageView(
            bird: args.data['bird'],
          ),
        ),
      ];
}
