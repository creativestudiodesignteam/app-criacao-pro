import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import '../../app/app_action_button_widget.dart';
import '../../app/app_appbar_widget.dart';
import '../../app/app_header_widget.dart';
import '../../app/app_modal_widget.dart';
import '../../app/app_scaffold_widget.dart';
import '../../app/app_sub_menu_widget.dart';
import '../../app/app_text_widget.dart';
import '../bird_model.dart';
import 'bird_image_controller.dart';
import 'bird_image_list_widget.dart';

class BirdImageView extends StatefulWidget {
  const BirdImageView({
    Key key,
    this.bird,
  }) : super(key: key);

  final BirdModel bird;

  @override
  _BirdImageViewState createState() => _BirdImageViewState();
}

class _BirdImageViewState
    extends ModularState<BirdImageView, BirdImageController> {
  Widget appBar(BuildContext context) {
    return AppAppBarWidget(
      titles: <Widget>[
        AppTextWidget(
          text: 'Fotos da ave',
          size: Theme.of(context).textTheme.subtitle2.fontSize,
        ),
      ],
      actions: <Widget>[
        IconButton(
          icon: Icon(
            Icons.chevron_left,
          ),
          onPressed: (){
            Navigator.popUntil(context, ModalRoute.withName('/bird/preview'));
          },
        ),
      ],
    );
  }

  Widget body(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            AppSubMenuWidget(
              image: 'lib/assets/bird-sub-1.png',
              text: 'Detalhes',
              selected: false,
              onPressed: () {
                Navigator.pushNamed(context, '/bird/detail',
                  arguments: {'bird': widget.bird, 'isReport': false},
                );
              },
            ),
            AppSubMenuWidget(
              image: 'lib/assets/bird-sub-2.png',
              text: 'Pais',
              selected: false,
              onPressed: () {
                Navigator.pushNamed(context, '/bird/parent',
                  arguments: {'bird': widget.bird, 'isReport': false},
                );
              },
            ),
            AppSubMenuWidget(
              image: 'lib/assets/bird-sub-3.png',
              text: 'Filhos',
              selected: false,
              onPressed: () {
                Navigator.pushNamed(context, '/bird/children',
                  arguments: {'bird': widget.bird, 'isReport': false},
                );
              },
            ),
            AppSubMenuWidget(
              image: 'lib/assets/bird-sub-4.png',
              text: 'Fotos',
              selected: true,
            ),
          ],
        ),
        SizedBox(
          height: 20,
        ),
        AppHeaderWidget(
          title: 'Galeria',
          subtitle: 'de ',
          highlight: 'fotos',
        ),
        SizedBox(
          height: 20,
        ),
        Observer(
          builder: (_) {
            if (controller.images == null) {
              return AppTextWidget(
                text: 'Buscando imagens...',
              );
            } else if (controller.images.isEmpty) {
              return AppTextWidget(
                text: 'Nenhuma imagem encontrada.',
              );
            } else {
              return BirdImageListWidget(
                images: controller.images,
                function: updateImage,
                deleteFunction: deleteImage,
                favorite: widget.bird.image,
              );
            }
          },
        ),
      ],
    );
  }

  void updateImage(String image){
    controller.updateImage(widget.bird.id, image);
  }

  void deleteImage(String url) async {
    await controller.deleteImage(url);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    controller.readImages(widget.bird.id);
    return AppScaffoldWidget(
      appBar: appBar(context),
      body: body(context),
      floatingActionButton: AppActionAppButtonWidget(
        label: 'Adicionar foto',
        icon: Icons.add_a_photo,
        color: Colors.green,
        onPressed: () {
          showModalBottomSheet(
            isDismissible: true,
            context: context,
            builder: (_) {
              return AppModalWidget(
                title: 'De onde deseja obter a foto?',
                textYes: 'Câmera',
                textNo: 'Galeria',
                onPressedYes: () => controller.onTapAddImage(widget.bird, 1),
                onPressedNo: () => controller.onTapAddImage(widget.bird, 0),
              );
            },
          );
        },
      ),
    );
  }
}
