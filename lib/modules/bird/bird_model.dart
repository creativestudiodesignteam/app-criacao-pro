class BirdModel {
  BirdModel({
    this.id,
    this.account,
    this.category,
    this.race,
    this.color,
    this.gender,
    this.status,
    this.birthDate,
    this.cage,
    this.rightRing,
    this.leftRing,
    this.registry,
    this.name,
    this.father,
    this.mother,
    this.image,
  });

  int id;
  int account;
  String category;
  String race;
  String color;
  String gender;
  String status;
  int birthDate;
  int cage;
  String rightRing;
  String leftRing;
  String registry;
  String name;
  int mother;
  int father;
  String image;

  BirdModel.fromJson(Map<String, dynamic> json) {
    id = int.parse(json['id']);
    account = int.parse(json['account']);
    category = json['category'];
    race = json['race'];
    color = json['color'];
    gender = json['gender'];
    status = json['status'];
    birthDate = int.parse(json['birth_date']);
    cage = int.parse(json['cage']);
    rightRing = json['right_ring'];
    leftRing = json['left_ring'];
    registry = json['registry'];
    name = json['name'];
    mother = int.parse(json['mother']);
    father = int.parse(json['father']);
    image = json['image'];
  }

  Map<String, dynamic> toJson() {
    var data = {
      'id': id,
      'account': account,
      'category': category,
      'race': race,
      'color': color,
      'gender': gender,
      'status': status,
      'birthDate': birthDate,
      'cage': cage,
      'rightRing': rightRing,
      'leftRing': leftRing,
      'registry': registry,
      'name': name,
      'father': father,
      'mother': mother,
      'image': image,
    };
    return data;
  }
}
