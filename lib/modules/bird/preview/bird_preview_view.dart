import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import '../../app/app_appbar_widget.dart';
import '../../app/app_modal_widget.dart';
import '../../app/app_text_widget.dart';
import 'bird_preview_controller.dart';
import 'bird_preview_widget.dart';

class BirdPreviewView extends StatefulWidget {
  const BirdPreviewView({
    Key key,
    this.id,
    this.isReport = false,
  }) : super(key: key);

  final int id;
  final bool isReport;

  @override
  _BirdPreviewViewState createState() => _BirdPreviewViewState();
}

class _BirdPreviewViewState
    extends ModularState<BirdPreviewView, BirdPreviewController> {
  Widget appBar(BuildContext context) {
    return AppAppBarWidget(
      titles: <Widget>[
        AppTextWidget(
          text: 'Detalhe da ave',
          size: Theme.of(context).textTheme.subtitle2.fontSize,
          color: Colors.white,
        ),
      ],
      actions: <Widget>[
        IconButton(
          icon: Icon(
            Icons.chevron_left,
            color: Colors.white,
          ),
          onPressed: controller.onTapBack,
        ),
      ],
    );
  }

  Widget body(BuildContext context, String name) {
    return Observer(
      builder: (_) {
        if (controller.bird == null) {
          return Center(
            child: Container(
              margin: EdgeInsets.only(
                right: 15,
              ),
              width: 24,
              height: 24,
              child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(
                  Color(0xffe9ae24),
                ),
              ),
            ),
          );
        } else {
          return BirdPreviewWidget(
            cageName: name,
            bird: controller.bird,
            isReport: widget.isReport,
            onDelete: () {
              showModalBottomSheet(
                isDismissible: false,
                context: context,
                builder: (_) {
                  return AppModalWidget(
                    title: 'Quer mesmo deletar a ave?',
                    textYes: 'Deletar',
                    textNo: 'Cancelar',
                    onPressedYes: () => controller.onTapDelete(controller.bird),
                    onPressedNo: controller.onTapBack,
                  );
                },
              );
            },
          );
        }
      },
    );
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _asyncInit();
    });
  }

  void _asyncInit() async {
    await controller.readBird(widget.id);
    cageName = _cageName();
    setState(() {});
  }

  Future<String> _cageName() async {
    var t = await controller.readAllCages(controller.bird.cage);
    return t;
  }

  Future<String> cageName;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<String>(
        future: cageName,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Scaffold(
              extendBodyBehindAppBar: true,
              appBar: appBar(context),
              body: body(context, snapshot.data),
              floatingActionButton: widget.isReport
                  ? SizedBox()
                  : GestureDetector(
                      onTap: () {
                        Navigator.pushNamed(
                          context,
                          '/bird/detail',
                          arguments: {'bird': controller.bird},
                        ).then((value) {
                          _asyncInit();
                        });
                      },
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Icon(
                            Icons.edit,
                            color: Colors.white,
                          ),
                          AppTextWidget(
                            text: 'Ver mais',
                            color: Colors.white,
                            size:
                                Theme.of(context).textTheme.subtitle1.fontSize,
                          ),
                        ],
                      ),
                    ),
              floatingActionButtonLocation:
                  FloatingActionButtonLocation.centerFloat,
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        });
  }
}
