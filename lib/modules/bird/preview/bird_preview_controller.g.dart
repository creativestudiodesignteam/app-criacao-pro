// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'bird_preview_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$BirdPreviewController on _BirdPreviewController, Store {
  final _$birdAtom = Atom(name: '_BirdPreviewController.bird');

  @override
  BirdModel get bird {
    _$birdAtom.reportRead();
    return super.bird;
  }

  @override
  set bird(BirdModel value) {
    _$birdAtom.reportWrite(value, super.bird, () {
      super.bird = value;
    });
  }

  final _$onTapBackAsyncAction =
      AsyncAction('_BirdPreviewController.onTapBack');

  @override
  Future<void> onTapBack() {
    return _$onTapBackAsyncAction.run(() => super.onTapBack());
  }

  final _$onTapDeleteAsyncAction =
      AsyncAction('_BirdPreviewController.onTapDelete');

  @override
  Future<void> onTapDelete(BirdModel bird) {
    return _$onTapDeleteAsyncAction.run(() => super.onTapDelete(bird));
  }

  final _$onTapEditAsyncAction =
      AsyncAction('_BirdPreviewController.onTapEdit');

  @override
  Future<void> onTapEdit(BirdModel bird) {
    return _$onTapEditAsyncAction.run(() => super.onTapEdit(bird));
  }

  final _$readBirdAsyncAction = AsyncAction('_BirdPreviewController.readBird');

  @override
  Future<void> readBird(int id) {
    return _$readBirdAsyncAction.run(() => super.readBird(id));
  }

  final _$readAllCagesAsyncAction =
      AsyncAction('_BirdPreviewController.readAllCages');

  @override
  Future<String> readAllCages(int id) {
    return _$readAllCagesAsyncAction.run(() => super.readAllCages(id));
  }

  @override
  String toString() {
    return '''

    ''';
  }
}
