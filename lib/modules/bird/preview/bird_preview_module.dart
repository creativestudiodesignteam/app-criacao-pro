import 'package:flutter_modular/flutter_modular.dart';

import '../../cage/cage_controller.dart';
import '../bird_controller.dart';
import '../bird_repository.dart';
import 'bird_preview_controller.dart';
import 'bird_preview_view.dart';

class BirdPreviewModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind(
          (i) => BirdPreviewController(
            birdController: i.get<BirdController>(),
            birdRepository: i.get<BirdRepository>(),
            cageController: i.get<CageController>(),
          ),
        ),
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(
          '/',
          child: (ctx, args) => BirdPreviewView(
            id: args.data['id'],
            isReport: args.data['isReport'],
          ),
        ),
      ];
}
