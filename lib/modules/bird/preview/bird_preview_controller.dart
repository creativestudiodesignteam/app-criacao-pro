import 'dart:convert';

import 'package:asuka/asuka.dart' as asuka;
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';

import '../../app/app_message_widget.dart';
import '../../cage/cage_controller.dart';
import '../bird_controller.dart';
import '../bird_model.dart';
import '../bird_repository.dart';

part 'bird_preview_controller.g.dart';

class BirdPreviewController = _BirdPreviewController
    with _$BirdPreviewController;

abstract class _BirdPreviewController with Store {
  _BirdPreviewController({
    this.birdController,
    this.birdRepository,
    this.cageController,
  });

  final CageController cageController;
  final BirdRepository birdRepository;
  final BirdController birdController;

  @action
  Future<void> onTapBack() async {
    Modular.to.pop();
  }

  @observable
  BirdModel bird;

  @action
  Future<void> readBird(int id) async {
    var response = await birdRepository.readBird(id);
    var data = json.decode(response['data']);
    bird = BirdModel.fromJson(data);
  }

  @action
  Future<void> onTapDelete(BirdModel bird) async {
    Modular.to.pop();
    asuka.showSnackBar(
      SnackBar(
        content: AppMessageWidget(
          isLoading: true,
          message: 'Por favor, aguarde ...',
        ),
        duration: Duration(
          milliseconds: 800,
        ),
      ),
    );
    var response = await birdController.birdRepository.deleteBird(bird.id);
    birdController.readAllBirds(null, null);
    Modular.to.pop();
    asuka.showSnackBar(
      SnackBar(
        content: AppMessageWidget(
          message: response['message'],
        ),
        duration: Duration(
          milliseconds: 1000,
        ),
      ),
    );
  }

  @action
  Future<void> onTapEdit(BirdModel bird) async {
    Modular.to.pushNamed('/bird/detail', arguments: {
      'bird': bird,
    });
  }

  @action
  Future<String> readAllCages(int id) async {
    await cageController.readAllCages('', false, false);
    var s;
    for (var i in cageController.cages) {
      if (i.id == id) {
        s = i.name;
      }
    }
    if (s == null) {
      s = '';
    }
    return s;
  }
}
