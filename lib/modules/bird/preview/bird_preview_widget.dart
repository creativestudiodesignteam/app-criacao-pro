import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../../app/app_text_widget.dart';
import '../bird_model.dart';

class BirdPreviewWidget extends StatelessWidget {
  const BirdPreviewWidget({
    Key key,
    this.bird,
    this.isReport = false,
    this.onDelete,
    @required this.cageName,
  }) : super(key: key);

  final BirdModel bird;
  final bool isReport;
  final Function onDelete;
  final String cageName;

  @override
  Widget build(BuildContext context) {
    var birthDate = DateTime.fromMillisecondsSinceEpoch(bird.birthDate);
    var birthDateFormatted = DateFormat('yyyy').format(birthDate).toString();
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: 30,
      ),
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: NetworkImage(bird.image),
          fit: BoxFit.cover,
        ),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                child: Text(
                  bird.name,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: Theme.of(context).textTheme.headline4.fontSize,
                    color: Colors.white,
                  ),
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              isReport
                  ? SizedBox()
                  : FloatingActionButton(
                      child: Icon(
                        Icons.delete_forever_outlined,
                      ),
                      foregroundColor: Colors.white,
                      backgroundColor: Colors.redAccent,
                      onPressed: onDelete,
                    ),
            ],
          ),
          AppTextWidget(
            text: cageName == '' ? 'Gaiola: Sem gaiola' : 'Gaiola: $cageName',
            color: Colors.white,
            size: Theme.of(context).textTheme.subtitle1.fontSize,
          ),
          AppTextWidget(
            text: 'Categoria: ${bird.category}',
            color: Colors.white,
            size: Theme.of(context).textTheme.subtitle1.fontSize,
          ),
          AppTextWidget(
            text: 'Raça: ${bird.race}',
            color: Colors.white,
            size: Theme.of(context).textTheme.subtitle1.fontSize,
          ),
          AppTextWidget(
            text: 'Cor: ${bird.color}',
            color: Colors.white,
            size: Theme.of(context).textTheme.subtitle1.fontSize,
          ),
          AppTextWidget(
            text: 'Gênero: ${bird.gender}',
            color: Colors.white,
            size: Theme.of(context).textTheme.subtitle1.fontSize,
          ),
          AppTextWidget(
            text: 'Status: ${bird.status}',
            color: Colors.white,
            size: Theme.of(context).textTheme.subtitle1.fontSize,
          ),
          SizedBox(
            height: 20,
          ),
          AppTextWidget(
            text: 'Registro IBAMA:',
            color: Colors.white,
            weight: FontWeight.bold,
            size: Theme.of(context).textTheme.subtitle1.fontSize,
          ),
          AppTextWidget(
            text: '${bird.registry}',
            color: Colors.white,
            weight: FontWeight.bold,
            size: Theme.of(context).textTheme.headline4.fontSize,
          ),
          SizedBox(
            height: 60,
          ),
          Stack(
            children: <Widget>[
              Positioned(
                top: 6,
                child: Image.asset(
                  'lib/assets/menu-1.png',
                  width: 48,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(
                  top: 80,
                ),
                child: Container(
                  padding: EdgeInsets.only(
                    top: 20,
                  ),
                  decoration: BoxDecoration(
                    border: Border(
                      top: BorderSide(
                        width: 1,
                        color: Colors.white.withOpacity(0.3),
                      ),
                    ),
                  ),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          AppTextWidget(
                            textAlign: TextAlign.center,
                            text: bird.leftRing.length == 0
                                ? '--'
                                : '${bird.leftRing}',
                            color: Colors.white,
                            size:
                                Theme.of(context).textTheme.headline6.fontSize /
                                    1.1,
                            weight: FontWeight.bold,
                          ),
                          AppTextWidget(
                            textAlign: TextAlign.center,
                            text: 'Anel esquerdo',
                            color: Colors.white,
                            size:
                                Theme.of(context).textTheme.bodyText2.fontSize /
                                    1.1,
                          ),
                        ],
                      ),
                      Column(
                        children: <Widget>[
                          AppTextWidget(
                            textAlign: TextAlign.center,
                            text: bird.rightRing.length == 0
                                ? '--'
                                : '${bird.rightRing}',
                            color: Colors.white,
                            size:
                                Theme.of(context).textTheme.headline6.fontSize /
                                    1.1,
                            weight: FontWeight.bold,
                          ),
                          AppTextWidget(
                            textAlign: TextAlign.center,
                            text: 'Anel direito',
                            color: Colors.white,
                            size:
                                Theme.of(context).textTheme.bodyText2.fontSize /
                                    1.1,
                          ),
                        ],
                      ),
                      Column(
                        children: <Widget>[
                          AppTextWidget(
                            textAlign: TextAlign.center,
                            text: birthDateFormatted,
                            color: Colors.white,
                            size:
                                Theme.of(context).textTheme.headline6.fontSize /
                                    1.1,
                            weight: FontWeight.bold,
                          ),
                          AppTextWidget(
                            textAlign: TextAlign.center,
                            text: 'Ano da ave',
                            color: Colors.white,
                            size:
                                Theme.of(context).textTheme.bodyText2.fontSize /
                                    1.1,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
