import 'package:dio/dio.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../account/account_controller.dart';
import '../../cage/baby/cage_baby_controller.dart';
import '../../cage/cage_controller.dart';
import '../bird_controller.dart';
import '../bird_repository.dart';
import 'bird_detail_controller.dart';
import 'bird_detail_view.dart';

class BirdDetailModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind(
          (i) => BirdDetailController(
            birdController: i.get<BirdController>(),
            birdRepository: i.get<BirdRepository>(),
            cageBabyController: i.get<CageBabyController>(),
            cageController: i.get<CageController>(),
            accountController: i.get<AccountController>(),
          ),
        ),
        Bind(
          (i) => BirdRepository(
          dio: i.get<Dio>(),
        ),
    ),
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(
          '/',
          child: (ctx, args) => BirdDetailView(
            bird: args.data['bird'],
          ),
        ),
      ];
}
