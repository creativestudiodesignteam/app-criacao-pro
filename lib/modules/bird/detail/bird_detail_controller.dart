import 'package:asuka/asuka.dart' as asuka;
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';

import '../../account/account_controller.dart';
import '../../app/app_message_widget.dart';
import '../../cage/baby/cage_baby_controller.dart';
import '../../cage/cage_controller.dart';
import '../../cage/cage_model.dart';
import '../bird_category_model.dart';
import '../bird_color_model.dart';
import '../bird_controller.dart';
import '../bird_gender_model.dart';
import '../bird_model.dart';
import '../bird_race_model.dart';
import '../bird_repository.dart';
import '../bird_status_model.dart';

part 'bird_detail_controller.g.dart';

class BirdDetailController = _BirdDetailController with _$BirdDetailController;

abstract class _BirdDetailController with Store {
  _BirdDetailController({
    this.birdController,
    this.accountController,
    this.birdRepository,
    this.cageController,
    this.cageBabyController,
  });

  final BirdController birdController;
  final BirdRepository birdRepository;
  final CageBabyController cageBabyController;
  final CageController cageController;
  final AccountController accountController;
  final TextEditingController rightRingController = TextEditingController();
  final TextEditingController leftRingController = TextEditingController();
  final TextEditingController birthController = TextEditingController();
  final TextEditingController registryController = TextEditingController();
  final TextEditingController nameController = TextEditingController();

  @observable
  List<BirdCategoryModel> categories;

  @observable
  List<BirdGenderModel> genders;

  @observable
  List<BirdStatusModel> statuses;

  @observable
  BirdCategoryModel category;

  @observable
  BirdRaceModel race;

  @observable
  BirdColorModel color;

  @observable
  BirdGenderModel gender;

  @observable
  BirdStatusModel status;

  @observable
  CageModel cage;

  @action
  Future<void> onTapBirdDetail(BirdModel bird) async {
    Modular.to.pushReplacementNamed(
      '/bird/detail',
      arguments: {
        'bird': bird,
        'isReport': false,
      },
    );
  }

  @action
  Future<bool> onTapUpdateBird(BirdModel bird) async {
    var success = false;
    Modular.to.pop();
    asuka.showSnackBar(
      SnackBar(
        content: AppMessageWidget(
          isLoading: true,
          message: 'Por favor, aguarde ...',
        ),
        duration: Duration(
          milliseconds: 800,
        ),
      ),
    );
    print("bird.account");
    print(bird.account);
    print("bird.birthDate");
    print(bird.birthDate);
    print("bird.cage");
    print(bird.cage);
    print("bird.category");
    print(bird.category);
    print("bird.color");
    print(bird.color);
    print("bird.father");
    print(bird.father);
    print("bird.gender");
    print(bird.gender);
    print("bird.id");
    print(bird.id);
    print("bird.image");
    print(bird.image);
    print("bird.leftRing");
    print(bird.leftRing);
    print("bird.mother");
    print(bird.mother);
    print("bird.name");
    print(bird.name);
    print("bird.race");
    print(bird.race);
    print("bird.registry");
    print(bird.registry);
    print("bird.rightRing");
    print(bird.rightRing);
    print("bird.status");
    print(bird.status);
    var response = await birdRepository.updateBird(bird);
    birdRepository.readAllBirds(accountController.account.id, null, null, null);
    if (response['type'] == 'success') {
      success = true;
    }
    asuka.showSnackBar(
      SnackBar(
        content: AppMessageWidget(
          message: response['message'],
        ),
        duration: Duration(
          milliseconds: 1000,
        ),
      ),
    );
    return success;
  }

  @action
  Future<void> readAllCategories(
      String cCategory, String cRace, String cColor) async {
    categories = await birdRepository.readAllCategories();
    category = categories.firstWhere((e) => e.name == cCategory);
    race = category.races.firstWhere((e) => e.name == cRace);
    color = race.colors.firstWhere((e) => e.name == cColor);
    // color = race.colors[1];
  }

  @action
  Future<void> readAllGenders(String cGender) async {
    genders = await birdRepository.readAllGenders();
    gender = genders.firstWhere((e) => e.name == cGender);
  }

  @action
  Future<void> readAllStatuses(String cStatus) async {
    statuses = await birdRepository.readAllStatus();
    status = statuses.firstWhere((e) => e.name == cStatus);
  }

  @action
  Future<void> readAllCages() async {
    await cageController.readAllCagesForSelect('');
  }
}
