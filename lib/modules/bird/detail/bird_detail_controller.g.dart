// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'bird_detail_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$BirdDetailController on _BirdDetailController, Store {
  final _$categoriesAtom = Atom(name: '_BirdDetailController.categories');

  @override
  List<BirdCategoryModel> get categories {
    _$categoriesAtom.reportRead();
    return super.categories;
  }

  @override
  set categories(List<BirdCategoryModel> value) {
    _$categoriesAtom.reportWrite(value, super.categories, () {
      super.categories = value;
    });
  }

  final _$gendersAtom = Atom(name: '_BirdDetailController.genders');

  @override
  List<BirdGenderModel> get genders {
    _$gendersAtom.reportRead();
    return super.genders;
  }

  @override
  set genders(List<BirdGenderModel> value) {
    _$gendersAtom.reportWrite(value, super.genders, () {
      super.genders = value;
    });
  }

  final _$statusesAtom = Atom(name: '_BirdDetailController.statuses');

  @override
  List<BirdStatusModel> get statuses {
    _$statusesAtom.reportRead();
    return super.statuses;
  }

  @override
  set statuses(List<BirdStatusModel> value) {
    _$statusesAtom.reportWrite(value, super.statuses, () {
      super.statuses = value;
    });
  }

  final _$categoryAtom = Atom(name: '_BirdDetailController.category');

  @override
  BirdCategoryModel get category {
    _$categoryAtom.reportRead();
    return super.category;
  }

  @override
  set category(BirdCategoryModel value) {
    _$categoryAtom.reportWrite(value, super.category, () {
      super.category = value;
    });
  }

  final _$raceAtom = Atom(name: '_BirdDetailController.race');

  @override
  BirdRaceModel get race {
    _$raceAtom.reportRead();
    return super.race;
  }

  @override
  set race(BirdRaceModel value) {
    _$raceAtom.reportWrite(value, super.race, () {
      super.race = value;
    });
  }

  final _$colorAtom = Atom(name: '_BirdDetailController.color');

  @override
  BirdColorModel get color {
    _$colorAtom.reportRead();
    return super.color;
  }

  @override
  set color(BirdColorModel value) {
    _$colorAtom.reportWrite(value, super.color, () {
      super.color = value;
    });
  }

  final _$genderAtom = Atom(name: '_BirdDetailController.gender');

  @override
  BirdGenderModel get gender {
    _$genderAtom.reportRead();
    return super.gender;
  }

  @override
  set gender(BirdGenderModel value) {
    _$genderAtom.reportWrite(value, super.gender, () {
      super.gender = value;
    });
  }

  final _$statusAtom = Atom(name: '_BirdDetailController.status');

  @override
  BirdStatusModel get status {
    _$statusAtom.reportRead();
    return super.status;
  }

  @override
  set status(BirdStatusModel value) {
    _$statusAtom.reportWrite(value, super.status, () {
      super.status = value;
    });
  }

  final _$cageAtom = Atom(name: '_BirdCreateController.cage');

  @override
  CageModel get cage {
    _$cageAtom.reportRead();
    return super.cage;
  }

  @override
  set cage(CageModel value) {
    _$cageAtom.reportWrite(value, super.cage, () {
      super.cage = value;
    });
  }

  final _$onTapBirdDetailAsyncAction =
      AsyncAction('_BirdDetailController.onTapBirdDetail');

  @override
  Future<void> onTapBirdDetail(BirdModel bird) {
    return _$onTapBirdDetailAsyncAction.run(() => super.onTapBirdDetail(bird));
  }

  final _$readAllCategoriesAsyncAction =
      AsyncAction('_BirdDetailController.readAllCategories');

  @override
  Future<void> readAllCategories(String category, String race, String color) {
    return _$readAllCategoriesAsyncAction
        .run(() => super.readAllCategories(category, race, color));
  }

  final _$readAllGendersAsyncAction =
      AsyncAction('_BirdDetailController.readAllGenders');

  @override
  Future<void> readAllGenders(String gender) {
    return _$readAllGendersAsyncAction.run(() => super.readAllGenders(gender));
  }

  final _$readAllStatusesAsyncAction =
      AsyncAction('_BirdDetailController.readAllStatuses');

  @override
  Future<void> readAllStatuses(String status) {
    return _$readAllStatusesAsyncAction
        .run(() => super.readAllStatuses(status));
  }

  final _$readAllCagesAsyncAction =
      AsyncAction('_BirdDetailController.readAllCages');

  @override
  Future<void> readAllCages() {
    return _$readAllCagesAsyncAction.run(() => super.readAllCages());
  }

  @override
  String toString() {
    return '''

    ''';
  }
}
