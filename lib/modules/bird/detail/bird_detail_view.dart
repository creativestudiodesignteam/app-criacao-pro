import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:intl/intl.dart';

import '../../app/app_appbar_widget.dart';
import '../../app/app_button_widget.dart';
import '../../app/app_dropdown_widget.dart';
import '../../app/app_header_widget.dart';
import '../../app/app_modal_widget.dart';
import '../../app/app_scaffold_widget.dart';
import '../../app/app_sub_menu_widget.dart';
import '../../app/app_text_widget.dart';
import '../../app/app_textfield_widget.dart';
import '../../cage/cage_model.dart';
import '../bird_model.dart';
import 'bird_detail_controller.dart';

class BirdDetailView extends StatefulWidget {
  const BirdDetailView({
    Key key,
    this.bird,
  }) : super(key: key);

  final BirdModel bird;

  @override
  _BirdDetailViewState createState() => _BirdDetailViewState();
}

class _BirdDetailViewState
    extends ModularState<BirdDetailView, BirdDetailController> {
  String _renderCageId(CageModel cage) {
    return cage.name.isEmpty ? cage.id.toString().padLeft(3, '0') : cage.name;
  }

  Widget appBar(BuildContext context) {
    return AppAppBarWidget(
      titles: <Widget>[
        AppTextWidget(
          text: 'Detalhes da ave',
          size: Theme.of(context).textTheme.subtitle2.fontSize,
        ),
      ],
      actions: <Widget>[
        IconButton(
          icon: Icon(
            Icons.chevron_left,
          ),
          onPressed: () {
            Navigator.popUntil(context, ModalRoute.withName('/bird/preview'));
          },
        ),
      ],
    );
  }

  Widget body(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            AppSubMenuWidget(
              image: 'lib/assets/bird-sub-1.png',
              text: 'Detalhes',
              selected: true,
            ),
            AppSubMenuWidget(
              image: 'lib/assets/bird-sub-2.png',
              text: 'Pais',
              selected: false,
              onPressed: () {
                Navigator.pushNamed(
                  context,
                  '/bird/parent',
                  arguments: {'bird': widget.bird, 'isReport': false},
                );
              },
            ),
            AppSubMenuWidget(
              image: 'lib/assets/bird-sub-3.png',
              text: 'Filhos',
              selected: false,
              onPressed: () {
                Navigator.pushNamed(
                  context,
                  '/bird/children',
                  arguments: {'bird': widget.bird, 'isReport': false},
                );
              },
            ),
            AppSubMenuWidget(
              image: 'lib/assets/bird-sub-4.png',
              text: 'Fotos',
              selected: false,
              onPressed: () {
                Navigator.pushNamed(
                  context,
                  '/bird/image',
                  arguments: {'bird': widget.bird, 'isReport': false},
                );
              },
            ),
          ],
        ),
        SizedBox(
          height: 20,
        ),
        AppHeaderWidget(
          title: 'Detalhes',
          subtitle: 'da sua ',
          highlight: 'ave',
        ),
        SizedBox(
          height: 20,
        ),
        Observer(
          builder: (_) {
            var loading = controller.categories == null;
            return AppDropdownWidget(
              label: 'Categoria',
              value: loading ? ' ... ' : widget.bird.category,
              items: loading
                  ? [
                      ' ... ',
                    ]
                  : controller.categories.map((e) => e.name).toList(),
              onChanged: (value) {
                controller.category =
                    controller.categories.firstWhere((e) => e.name == value);
                controller.race = controller.category.races.elementAt(0);
                controller.color = controller.race.colors.elementAt(0);
              },
            );
          },
        ),
        Observer(
          builder: (_) {
            var loading = controller.category == null;
            return AppDropdownWidget(
              label: 'Raça',
              value: loading ? ' ... ' : widget.bird.race,
              items: loading
                  ? [
                      ' ... ',
                    ]
                  : controller.category.races.map((e) => e.name).toList(),
              onChanged: (value) {
                controller.race = controller.category.races
                    .firstWhere((e) => e.name == value);
                controller.color = controller.race.colors.elementAt(0);
              },
            );
          },
        ),
        Observer(
          builder: (_) {
            var loading = controller.race == null;
            return AppDropdownWidget(
              label: 'Cor',
              value: loading ? ' ... ' : widget.bird.color,
              items: loading
                  ? [
                      ' ... ',
                    ]
                  : controller.race.colors.map((e) => e.name).toList(),
              onChanged: (value) {
                controller.color =
                    controller.race.colors.firstWhere((e) => e.name == value);
              },
            );
          },
        ),
        Row(
          children: <Widget>[
            Flexible(
              child: Observer(
                builder: (_) {
                  if (controller.cageController.cages != null) {
                    var loading = controller.cageController.cages == null;
                    var list1 = <String>['Sem gaiola'];
                    var list2 = controller.cageController.cages
                        .map(_renderCageId)
                        .toList();
                    List<String> newList;
                    newList = List.from(list1)..addAll(list2);
                    String cageName;
                    for (var i in controller.cageController.cages) {
                      if (i.id == widget.bird.cage) {
                        if (i.name == '') {
                          controller.cage = i;
                          cageName = i.id.toString().padLeft(3, '0');
                        } else {
                          controller.cage = i;
                          cageName = i.name;
                        }
                      }
                    }
                    ;
                    return AppDropdownWidget(
                      label: 'Gaiola',
                      value: loading
                          ? ' ... '
                          : widget.bird.cage == 0
                              ? 'Sem gaiola'
                              : cageName,
                      items: loading
                          ? [
                              ' ... ',
                            ]
                          : newList,
                      onChanged: (value) {
                        if (value != ' ... ' && value != 'Sem gaiola') {
                          controller.cage = controller.cageController.cages
                              .firstWhere((e) => _renderCageId(e) == value);
                          // print(controller.cage.)
                        } else if (value == 'Sem gaiola') {
                          controller.cage = null;
                        }
                      },
                    );
                  } else {
                    return CircularProgressIndicator();
                  }
                },
              ),
            ),
          ],
        ),
        Row(
          children: <Widget>[
            Flexible(
              child: AppTextFieldWidget(
                label: 'Nascimento',
                controller: controller.birthController,
                keyboardType: TextInputType.datetime,
                onTap: () async {
                  var date = await showDatePicker(
                    context: context,
                    initialDate: DateTime.now(),
                    firstDate: DateTime(2010),
                    lastDate: DateTime.now(),
                  );
                  if (date != null) {
                    controller.birthController.text =
                        DateFormat('dd/MM/yyyy').format(date).toString();
                  }
                },
              ),
            ),
          ],
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Flexible(
              child: Observer(
                builder: (_) {
                  var loading = controller.genders == null;
                  return AppDropdownWidget(
                    label: 'Gênero',
                    value: loading ? ' ... ' : controller.gender.name,
                    items: loading
                        ? [
                            ' ... ',
                          ]
                        : controller.genders.map((e) => e.name).toList(),
                    onChanged: (value) {
                      controller.gender =
                          controller.genders.firstWhere((e) => e.name == value);
                    },
                  );
                },
              ),
            ),
            SizedBox(
              width: 10,
            ),
            Flexible(
              child: Observer(
                builder: (_) {
                  var loading = controller.statuses == null;
                  return AppDropdownWidget(
                    label: 'Status',
                    value: loading
                        ? ' ... '
                        : widget.bird.status == null ||
                                widget.bird.status == 'NULL'
                            ? 'Vivo'
                            : widget.bird.status,
                    items: loading
                        ? [
                            ' ... ',
                          ]
                        : controller.statuses.map((e) => e.name).toList(),
                    onChanged: (value) {
                      controller.status = controller.statuses
                          .firstWhere((e) => e.name == value);
                    },
                  );
                },
              ),
            ),
          ],
        ),
        Row(
          children: <Widget>[
            Flexible(
              child: AppTextFieldWidget(
                label: 'Anel Esquerdo',
                controller: controller.leftRingController,
                keyboardType: TextInputType.text,
              ),
            ),
            SizedBox(
              width: 10,
            ),
            Flexible(
              child: AppTextFieldWidget(
                label: 'Registro IBAMA',
                controller: controller.registryController,
                keyboardType: TextInputType.text,
              ),
            ),
            SizedBox(
              width: 10,
            ),
            Flexible(
              child: AppTextFieldWidget(
                label: 'Anel Direito',
                controller: controller.rightRingController,
                keyboardType: TextInputType.text,
              ),
            ),
          ],
        ),
        AppTextFieldWidget(
          label: 'Nome',
          controller: controller.nameController,
          keyboardType: TextInputType.text,
        ),
        SizedBox(height: 20),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            AppButtonWidget(
              text: 'Salvar',
              color: Theme.of(context).primaryColor,
              onPressed: () {
                showModalBottomSheet(
                  isDismissible: false,
                  context: context,
                  builder: (_) {
                    var convertedBirthDate = DateFormat("dd/MM/yyyy")
                        .parse(controller.birthController.text);
                    return AppModalWidget(
                      title: 'Quer mesmo editar a ave?',
                      textYes: 'Sim',
                      textNo: 'Cancelar',
                      onPressedYes: () async {
                        var bird = BirdModel(
                          id: widget.bird.id,
                          account: widget.bird.account,
                          category: controller.category.name,
                          race: controller.race.name,
                          color: controller.color.name,
                          gender: controller.gender.name,
                          status: controller.status.name,
                          birthDate: convertedBirthDate.millisecondsSinceEpoch,
                          cage: controller.cage != null
                              ? controller.cage.id
                              : null,
                          rightRing: controller.rightRingController.text,
                          leftRing: controller.leftRingController.text,
                          registry: controller.registryController.text,
                          name: controller.nameController.text,
                          mother: widget.bird.mother,
                          father: widget.bird.father,
                          image: widget.bird.image,
                        );
                        var success = await controller.onTapUpdateBird(bird);
                        if (success) {
                          Navigator.popUntil(
                              context, ModalRoute.withName('/bird/preview'));
                        }
                      },
                      onPressedNo: () {
                        Navigator.popUntil(
                            context, ModalRoute.withName('/bird/preview'));
                      },
                    );
                  },
                );
              },
            ),
            AppTextWidget(
              text: 'Voltar',
              onTap: () {
                Navigator.popUntil(
                    context, ModalRoute.withName('/bird/preview'));
              },
            ),
          ],
        ),
      ],
    );
  }

  @override
  void initState() {
    var birthDate = DateTime.fromMillisecondsSinceEpoch(widget.bird.birthDate);
    var birthDateFormatted =
        DateFormat('dd/MM/yyyy').format(birthDate).toString();
    super.initState();
    controller.birthController.text = '${birthDateFormatted}';
    controller.registryController.text = '${widget.bird.registry}';
    controller.leftRingController.text = '${widget.bird.leftRing}';
    controller.rightRingController.text = '${widget.bird.rightRing}';
    controller.nameController.text = '${widget.bird.name}';
    _asyncInit();
  }

  void _asyncInit() async {
    await controller.readAllCategories(
        widget.bird.category, widget.bird.race, widget.bird.color);
    await controller.readAllGenders(widget.bird.gender);
    await controller.readAllStatuses(widget.bird.status);
    await controller.readAllCages();
    // setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return AppScaffoldWidget(
      appBar: appBar(context),
      body: body(context),
    );
  }
}
