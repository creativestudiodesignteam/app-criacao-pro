class BirdStatusModel {
  BirdStatusModel({
    this.id,
    this.name,
  });

  final int id;
  final String name;
}
