import 'package:criacao_pro/modules/bird/bird_controller.dart';
import 'package:flutter/material.dart';
import 'bird_item_widget.dart';
import 'bird_model.dart';

class BirdListWidget extends StatefulWidget {
  const BirdListWidget({
    Key key,
    this.birds,
    this.isReport = false,
    this.isTwoOptions = false,
    this.isSelect = false,
    this.isTransfer = false,
    this.onChangedCheckbox,
    this.function,
    this.readAllBirds,
    this.gender,
    this.selectGender,
    this.id,
    this.totalBirds,
    this.totalSelectBirds,
    this.sc,
  }) : super(key: key);

  final List<BirdModel> birds;
  final bool isReport;
  final bool isSelect;
  final bool isTransfer;
  final bool isTwoOptions;
  final void Function(BirdModel, bool) onChangedCheckbox;
  final Function function;
  final Function readAllBirds;
  final String gender;
  final String selectGender;
  final int id;
  final int totalSelectBirds;
  final String totalBirds;
  final ScrollController sc;

  @override
  _BirdListWidgetState createState() => _BirdListWidgetState();
}

class _BirdListWidgetState extends State<BirdListWidget> {
  bool isLoading = true;

  BirdController controller;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var sizeScreen = widget.isReport ? 0.68 : 0.75;
    return Container(
      height: MediaQuery.of(context).size.height * sizeScreen,
      child: ListView.builder(
        controller: widget.sc,
        shrinkWrap: true,
        itemCount: widget.birds.length + 1,
        itemBuilder: (_, i) {
          if (widget.birds.length == i) {
            if (widget.totalBirds != null) {
              if (widget.birds.length == int.parse(widget.totalBirds)) {
                isLoading = false;
              } else {
                isLoading = true;
              }
            }
            return _buildProgressIndicator();
          } else {
            isLoading = false;
            return BirdItemWidget(
              bird: widget.birds.elementAt(i),
              isReport: widget.isReport,
              isTwoOptions: widget.isTwoOptions,
              isSelect: widget.isSelect,
              isTransfer: widget.isTransfer,
              onChangedCheckbox: widget.onChangedCheckbox,
              function: widget.function,
              totalSelectBirds: widget.totalSelectBirds,
              selectGender: widget.selectGender,
            );
          }
        },
      ),
    );
  }

  Widget _buildProgressIndicator() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Center(
        child: Opacity(
          opacity: isLoading ? 1.0 : 00,
          child: CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(
              Color(0xffe9ae24),
            ),
          ),
        ),
      ),
    );
  }
}
