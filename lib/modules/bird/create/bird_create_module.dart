import 'package:flutter_modular/flutter_modular.dart';
import 'package:dio/dio.dart';
import '../../account/account_controller.dart';
import '../../cage/baby/cage_baby_controller.dart';
import '../../cage/baby/cage_baby_repository.dart';
import '../../cage/cage_controller.dart';
import '../bird_repository.dart';
import 'bird_create_controller.dart';
import 'bird_create_view.dart';

class BirdCreateModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind(
          (i) => BirdCreateController(
            birdRepository: i.get<BirdRepository>(),
            cageBabyController: i.get<CageBabyController>(),
            cageController: i.get<CageController>(),
            accountController: i.get<AccountController>(),
          ),
        ),
        Bind(
          (i) => CageBabyController(
            cageBabyRepository: i.get<CageBabyRepository>(),
          ),
        ),
        Bind(
          (i) => CageBabyRepository(
            dio: i.get<Dio>(),
          ),
        ),
        Bind(
          (i) => BirdRepository(
            dio: i.get<Dio>(),
          ),
        )
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(
          '/',
          child: (ctx, args) => BirdCreateView(
            baby: args.data['baby'],
            cage: args.data['cage'],
            ring: args.data['ring'],
          ),
        ),
      ];
}
