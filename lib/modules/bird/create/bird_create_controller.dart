import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:intl/intl.dart';
import 'package:mobx/mobx.dart';
import 'package:asuka/asuka.dart' as asuka;
import '../../account/account_controller.dart';
import '../../app/app_message_widget.dart';
import '../../cage/baby/cage_baby_controller.dart';
import '../../cage/baby/cage_baby_model.dart';
import '../../cage/cage_controller.dart';
import '../../cage/cage_model.dart';
import '../bird_category_model.dart';
import '../bird_color_model.dart';
import '../bird_gender_model.dart';
import '../bird_model.dart';
import '../bird_race_model.dart';
import '../bird_repository.dart';
import '../bird_status_model.dart';
part 'bird_create_controller.g.dart';

class BirdCreateController = _BirdCreateController with _$BirdCreateController;

abstract class _BirdCreateController with Store {
  _BirdCreateController({
    this.birdRepository,
    this.cageBabyController,
    this.cageController,
    this.accountController,
  });

  final BirdRepository birdRepository;
  final CageBabyController cageBabyController;
  final CageController cageController;
  final AccountController accountController;
  final TextEditingController rightRingController = TextEditingController();
  final TextEditingController leftRingController = TextEditingController();
  final TextEditingController birthController = TextEditingController();
  final TextEditingController registryController = TextEditingController();
  final TextEditingController nameController = TextEditingController();

  @observable
  List<BirdCategoryModel> categories;

  @observable
  List<BirdGenderModel> genders;

  @observable
  List<BirdStatusModel> statuses;

  @observable
  BirdCategoryModel category;

  @observable
  BirdRaceModel race;

  @observable
  BirdColorModel color;

  @observable
  BirdGenderModel gender;

  @observable
  BirdStatusModel status;

  @observable
  CageModel cage;

  @action
  Future<void> setCage(int cageId) async {
    var response = await cageController.cageRepository.readCage(cageId);
    if (response['type'] == 'success') {
      cage = CageModel.fromJson(json.decode(response['data']));
    }
  }

  @action
  Future<void> readAllCages() async {
    await cageController.readAllCagesForSelect('');
  }

  @action
  Future<void> readAllCategories() async {
    categories = await birdRepository.readAllCategories();
    category = categories.elementAt(0);
    race = category.races.elementAt(0);
    color = race.colors[0];
  }

  @action
  Future<void> readAllGenders() async {
    genders = await birdRepository.readAllGenders();
    gender = genders[0];
  }

  @action
  Future<void> readAllStatuses() async {
    statuses = await birdRepository.readAllStatus();
    status = statuses[0];
  }

  @action
  Future<void> onTapAddBird(CageBabyModel baby) async {
    int father;
    int mother;
    if (baby != null) {
      father = baby.father;
      mother = baby.mother;
    }
    //var convertedBirthDate = DateTime.parse(birthController.text);
    var convertedBirthDate =
        DateFormat("dd/MM/yyyy").parse(birthController.text);
    var bird = BirdModel(
      account: accountController.account.id,
      category: category.name,
      race: race.name,
      color: color.name,
      gender: gender.name,
      status: status.name,
      birthDate: convertedBirthDate.millisecondsSinceEpoch,
      cage: cage != null ? cage.id : null,
      rightRing: rightRingController.text,
      leftRing: leftRingController.text,
      registry: registryController.text,
      name: nameController.text == '' ? 'Sem nome' : nameController.text,
      father: father,
      mother: mother,
    );
    Modular.to.pop();
    asuka.showSnackBar(
      SnackBar(
        content: AppMessageWidget(
          isLoading: true,
          message: 'Por favor, aguarde ...',
        ),
        duration: Duration(
          milliseconds: 800,
        ),
      ),
    );
    var response = await birdRepository.createBird(bird);
    // birdRepository.readAllBirds(accountController.account.id, null, null, null);
    if (response['type'] == 'success') {
      Modular.to.pop();
    }
    if (baby != null) {
      await cageBabyController.cageBabyRepository.deleteBaby(baby.id);
      cageBabyController.readAllBabies(bird.cage);
    }
    asuka.showSnackBar(
      SnackBar(
        content: FittedBox(
          fit: BoxFit.fitWidth,
          child: AppMessageWidget(
            message: response['message'],
          ),
        ),
        duration: Duration(
          milliseconds: 1000,
        ),
      ),
    );
  }

  @action
  Future<void> onTapBack() async {
    Modular.to.pop();
  }
}
