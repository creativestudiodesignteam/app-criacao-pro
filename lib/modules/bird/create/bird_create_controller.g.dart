// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'bird_create_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$BirdCreateController on _BirdCreateController, Store {
  final _$categoriesAtom = Atom(name: '_BirdCreateController.categories');

  @override
  List<BirdCategoryModel> get categories {
    _$categoriesAtom.reportRead();
    return super.categories;
  }

  @override
  set categories(List<BirdCategoryModel> value) {
    _$categoriesAtom.reportWrite(value, super.categories, () {
      super.categories = value;
    });
  }

  final _$gendersAtom = Atom(name: '_BirdCreateController.genders');

  @override
  List<BirdGenderModel> get genders {
    _$gendersAtom.reportRead();
    return super.genders;
  }

  @override
  set genders(List<BirdGenderModel> value) {
    _$gendersAtom.reportWrite(value, super.genders, () {
      super.genders = value;
    });
  }

  final _$statusesAtom = Atom(name: '_BirdCreateController.statuses');

  @override
  List<BirdStatusModel> get statuses {
    _$statusesAtom.reportRead();
    return super.statuses;
  }

  @override
  set statuses(List<BirdStatusModel> value) {
    _$statusesAtom.reportWrite(value, super.statuses, () {
      super.statuses = value;
    });
  }

  final _$categoryAtom = Atom(name: '_BirdCreateController.category');

  @override
  BirdCategoryModel get category {
    _$categoryAtom.reportRead();
    return super.category;
  }

  @override
  set category(BirdCategoryModel value) {
    _$categoryAtom.reportWrite(value, super.category, () {
      super.category = value;
    });
  }

  final _$raceAtom = Atom(name: '_BirdCreateController.race');

  @override
  BirdRaceModel get race {
    _$raceAtom.reportRead();
    return super.race;
  }

  @override
  set race(BirdRaceModel value) {
    _$raceAtom.reportWrite(value, super.race, () {
      super.race = value;
    });
  }

  final _$colorAtom = Atom(name: '_BirdCreateController.color');

  @override
  BirdColorModel get color {
    _$colorAtom.reportRead();
    return super.color;
  }

  @override
  set color(BirdColorModel value) {
    _$colorAtom.reportWrite(value, super.color, () {
      super.color = value;
    });
  }

  final _$genderAtom = Atom(name: '_BirdCreateController.gender');

  @override
  BirdGenderModel get gender {
    _$genderAtom.reportRead();
    return super.gender;
  }

  @override
  set gender(BirdGenderModel value) {
    _$genderAtom.reportWrite(value, super.gender, () {
      super.gender = value;
    });
  }

  final _$statusAtom = Atom(name: '_BirdCreateController.status');

  @override
  BirdStatusModel get status {
    _$statusAtom.reportRead();
    return super.status;
  }

  @override
  set status(BirdStatusModel value) {
    _$statusAtom.reportWrite(value, super.status, () {
      super.status = value;
    });
  }

  final _$cageAtom = Atom(name: '_BirdCreateController.cage');

  @override
  CageModel get cage {
    _$cageAtom.reportRead();
    return super.cage;
  }

  @override
  set cage(CageModel value) {
    _$cageAtom.reportWrite(value, super.cage, () {
      super.cage = value;
    });
  }

  final _$setCageAsyncAction = AsyncAction('_BirdCreateController.setCage');

  @override
  Future<void> setCage(int cageId) {
    return _$setCageAsyncAction.run(() => super.setCage(cageId));
  }

  final _$readAllCagesAsyncAction =
      AsyncAction('_BirdCreateController.readAllCages');

  @override
  Future<void> readAllCages() {
    return _$readAllCagesAsyncAction.run(() => super.readAllCages());
  }

  final _$readAllCategoriesAsyncAction =
      AsyncAction('_BirdCreateController.readAllCategories');

  @override
  Future<void> readAllCategories() {
    return _$readAllCategoriesAsyncAction.run(() => super.readAllCategories());
  }

  final _$readAllGendersAsyncAction =
      AsyncAction('_BirdCreateController.readAllGenders');

  @override
  Future<void> readAllGenders() {
    return _$readAllGendersAsyncAction.run(() => super.readAllGenders());
  }

  final _$readAllStatusesAsyncAction =
      AsyncAction('_BirdCreateController.readAllStatuses');

  @override
  Future<void> readAllStatuses() {
    return _$readAllStatusesAsyncAction.run(() => super.readAllStatuses());
  }

  final _$onTapAddBirdAsyncAction =
      AsyncAction('_BirdCreateController.onTapAddBird');

  @override
  Future<void> onTapAddBird(CageBabyModel baby) {
    return _$onTapAddBirdAsyncAction.run(() => super.onTapAddBird(baby));
  }

  final _$onTapBackAsyncAction = AsyncAction('_BirdCreateController.onTapBack');

  @override
  Future<void> onTapBack() {
    return _$onTapBackAsyncAction.run(() => super.onTapBack());
  }

  @override
  String toString() {
    return '''
categories: ${categories},
genders: ${genders},
statuses: ${statuses},
category: ${category},
race: ${race},
color: ${color},
gender: ${gender},
status: ${status},
cage: ${cage}
    ''';
  }
}
