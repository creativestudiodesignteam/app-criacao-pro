import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../../modules/cage/cage_model.dart';
import '../../app/app_appbar_widget.dart';
import '../../app/app_button_widget.dart';
import '../../app/app_dropdown_widget.dart';
import '../../app/app_header_widget.dart';
import '../../app/app_modal_widget.dart';
import '../../app/app_scaffold_widget.dart';
import '../../app/app_text_widget.dart';
import '../../app/app_textfield_widget.dart';
import '../../cage/baby/cage_baby_model.dart';
import 'bird_create_controller.dart';

class BirdCreateView extends StatefulWidget {
  const BirdCreateView({
    Key key,
    this.baby,
    this.cage,
    this.ring = false,
  }) : super(key: key);

  final CageBabyModel baby;
  final int cage;
  final dynamic ring;

  @override
  _BirdCreateViewState createState() => _BirdCreateViewState();
}

class _BirdCreateViewState
    extends ModularState<BirdCreateView, BirdCreateController> {
  SharedPreferences prefs;
  String category;
  String race;
  String _renderCageId(CageModel cage) {
    print('AAAAAAAAAAAAAAAAAAAAAAAAA');
    return cage.name.isEmpty ? cage.id.toString().padLeft(3, '0') : cage.name;
  }

  Widget appBar(BuildContext context) {
    return AppAppBarWidget(
      titles: <Widget>[
        AppTextWidget(
          text: 'Adicionar ave',
          size: Theme.of(context).textTheme.subtitle2.fontSize,
        ),
      ],
      actions: <Widget>[
        IconButton(
          icon: Icon(
            Icons.chevron_left,
          ),
          onPressed: controller.onTapBack,
        ),
      ],
    );
  }

  Widget body(BuildContext context) {
    controller.birthController.text =
        DateFormat('dd/MM/yyyy').format(DateTime.now()).toString();
    return Column(
      children: <Widget>[
        AppHeaderWidget(
          title: 'Aqui você pode',
          subtitle: 'adicionar a sua ',
          highlight: 'ave',
        ),
        SizedBox(
          height: 20,
        ),
        Observer(
          builder: (_) {
            var loading = controller.categories == null;
            return controller.cageController.cages != null
                ? AppDropdownWidget(
                    label: 'Categoria',
                    value: loading
                        ? ' ... '
                        : category == null
                            ? controller.category.name
                            : category,
                    items: loading
                        ? [
                            ' ... ',
                          ]
                        : controller.categories.map((e) => e.name).toList(),
                    onChanged: (value) {
                      prefs.setString('category', value);
                      controller.category = controller.categories
                          .firstWhere((e) => e.name == value);
                      controller.race = controller.category.races.elementAt(0);
                      prefs.setString('race', controller.race.name);
                      controller.color = controller.race.colors.elementAt(0);
                    },
                  )
                : Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Center(
                      child: Opacity(
                        opacity: 1.0,
                        child: CircularProgressIndicator(
                          valueColor: AlwaysStoppedAnimation<Color>(
                            Color(0xffe9ae24),
                          ),
                        ),
                      ),
                    ),
                  );
            ;
          },
        ),
        Observer(
          builder: (_) {
            var loading = controller.category == null;
            return controller.cageController.cages != null
                ? AppDropdownWidget(
                    label: 'Raça',
                    value: loading ? ' ... ' : controller.race.name,
                    items: loading
                        ? [
                            ' ... ',
                          ]
                        : controller.category.races.map((e) => e.name).toList(),
                    onChanged: (value) {
                      prefs.setString('race', value);
                      controller.race = controller.category.races
                          .firstWhere((e) => e.name == value);
                      controller.color = controller.race.colors.elementAt(0);
                    },
                  )
                : Container();
          },
        ),
        Observer(
          builder: (_) {
            var loading = controller.race == null;
            return controller.cageController.cages != null
                ? AppDropdownWidget(
                    label: 'Cor',
                    value: loading ? ' ... ' : controller.color.name,
                    items: loading
                        ? [
                            ' ... ',
                          ]
                        : controller.race.colors.map((e) => e.name).toList(),
                    onChanged: (value) {
                      controller.color = controller.race.colors
                          .firstWhere((e) => e.name == value);
                    },
                  )
                : Container();
          },
        ),
        Row(
          children: <Widget>[
            Flexible(
              child: Observer(
                builder: (_) {
                  List<String> newList;
                  var loading;
                  var list1;
                  var list2;
                  if (controller.cageController.cages != null) {
                    loading = controller.cageController.cages == null;
                    list1 = <String>['Sem gaiola'];
                    list2 = controller.cageController.cages
                        .map(_renderCageId)
                        .toList();

                    newList = List.from(list1)..addAll(list2);
                  }
                  return controller.cageController.cages != null
                      ? AppDropdownWidget(
                          label: 'Gaiola',
                          value: loading
                              ? ' ... '
                              : controller.cage != null
                                  ? controller.cage.name
                                  : 'Sem gaiola',
                          items: loading
                              ? [
                                  ' ... ',
                                ]
                              : newList,
                          onChanged: (value) {
                            if (value != ' ... ' && value != 'Sem gaiola') {
                              controller.cage = controller.cageController.cages
                                  .firstWhere((e) => _renderCageId(e) == value);
                            } else if (value == 'Sem gaiola') {
                              controller.cage = null;
                            }
                          },
                        )
                      : Container();
                },
              ),
            ),
            SizedBox(
              width: 10,
            ),
            Flexible(
              child: Observer(
                builder: (_) {
                  var loading = controller.genders == null;
                  return controller.cageController.cages != null
                      ? AppDropdownWidget(
                          label: 'Gênero',
                          value: loading ? ' ... ' : controller.gender.name,
                          items: loading
                              ? [
                                  ' ... ',
                                ]
                              : controller.genders.map((e) => e.name).toList(),
                          onChanged: (value) {
                            controller.gender = controller.genders
                                .firstWhere((e) => e.name == value);
                          },
                        )
                      : Container();
                },
              ),
            ),
            SizedBox(
              width: 10,
            ),
            Flexible(
              child: Observer(
                builder: (_) {
                  var loading = controller.statuses == null;
                  return controller.cageController.cages != null
                      ? AppDropdownWidget(
                          label: 'Status',
                          value: loading
                              ? ' ... '
                              : controller.statuses.elementAt(0).name,
                          items: loading
                              ? [
                                  ' ... ',
                                ]
                              : controller.statuses.map((e) => e.name).toList(),
                          onChanged: (value) {
                            controller.status = controller.statuses
                                .firstWhere((e) => e.name == value);
                          },
                        )
                      : Container();
                },
              ),
            ),
          ],
        ),
        Row(
          children: <Widget>[
            Flexible(
              child: Observer(
                builder: (_) {
                  return controller.cageController.cages != null
                      ? AppTextFieldWidget(
                          label: 'Nascimento',
                          controller: controller.birthController,
                          keyboardType: TextInputType.datetime,
                          onTap: () async {
                            var date = await showDatePicker(
                              context: context,
                              initialDate: DateTime.now(),
                              firstDate: DateTime(2010),
                              lastDate: DateTime.now(),
                            );
                            if (date != null) {
                              controller.birthController.text =
                                  DateFormat('dd/MM/yyyy')
                                      .format(date)
                                      .toString();
                            }
                          },
                        )
                      : Container();
                },
              ),
            ),
            SizedBox(
              width: 10,
            ),
            Flexible(
              child: Observer(
                builder: (_) {
                  return controller.cageController.cages != null
                      ? AppTextFieldWidget(
                          label: 'Registro IBAMA',
                          controller: controller.registryController,
                          keyboardType: TextInputType.text,
                        )
                      : Container();
                },
              ),
            ),
          ],
        ),
        Row(
          children: <Widget>[
            Flexible(
              child: Observer(
                builder: (_) {
                  return controller.cageController.cages != null
                      ? AppTextFieldWidget(
                          label: 'Anel Esquerdo',
                          controller: controller.leftRingController,
                          keyboardType: TextInputType.text,
                        )
                      : Container();
                },
              ),
            ),
            SizedBox(
              width: 10,
            ),
            Flexible(
              child: Observer(
                builder: (_) {
                  return controller.cageController.cages != null
                      ? AppTextFieldWidget(
                          label: 'Anel Direito',
                          controller: controller.rightRingController,
                          keyboardType: TextInputType.text,
                        )
                      : Container();
                },
              ),
            ),
          ],
        ),
        Observer(
          builder: (_) {
            return controller.cageController.cages != null
                ? AppTextFieldWidget(
                    label: 'Nome',
                    controller: controller.nameController,
                    keyboardType: TextInputType.text,
                  )
                : Container();
          },
        ),
        SizedBox(
          height: 20,
        ),
        Observer(
          builder: (_) {
            return controller.cageController.cages != null
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      AppButtonWidget(
                        text: 'Adicionar',
                        color: Theme.of(context).primaryColor,
                        onPressed: () {
                          showModalBottomSheet(
                            isDismissible: false,
                            context: context,
                            builder: (_) {
                              return AppModalWidget(
                                title: 'Quer mesmo adicionar a ave?',
                                textYes: 'Adicionar',
                                textNo: 'Cancelar',
                                onPressedYes: () =>
                                    controller.onTapAddBird(widget.baby),
                                onPressedNo: controller.onTapBack,
                              );
                            },
                          );
                        },
                      ),
                      AppTextWidget(
                        text: 'Voltar',
                        onTap: controller.onTapBack,
                      ),
                    ],
                  )
                : Container();
          },
        ),
      ],
    );
  }

  @override
  void initState() {
    super.initState();
    _asyncInit();
    if (widget.ring != null) {
      controller.leftRingController.text = widget.ring['left'];
      controller.rightRingController.text = widget.ring['right'];
    }
  }

  void _asyncInit() async {
    await controller.readAllCategories();
    await controller.readAllGenders();
    await controller.readAllStatuses();
    await controller.readAllCages();

    print(controller.cageController.cages.length);
    prefs = await SharedPreferences.getInstance();
    category = await prefs.getString('category');
    if (category != null || category != '') {
      controller.category =
          await controller.categories.firstWhere((e) => e.name == category);
    }
    race = await prefs.getString('race');
    if (race != null || race != '') {
      controller.race =
          await controller.category.races.firstWhere((e) => e.name == race);
      controller.color = await controller.race.colors.elementAt(0);
    }
    if (widget.cage != null) {
      controller.cage = controller.cageController.cages
          .firstWhere((e) => e.id == widget.cage);
    }
  }

  @override
  Widget build(BuildContext context) {
    if (widget.baby != null) {
      var babyBirthDate = widget.baby.birthDate;
      var birthDate = DateTime.fromMillisecondsSinceEpoch(babyBirthDate);
      var birthDateFormatted =
          DateFormat('dd/MM/yyy').format(birthDate).toString();
      controller.birthController.text = birthDateFormatted;
      controller.setCage(widget.baby.cage);
    }
    return AppScaffoldWidget(
      appBar: appBar(context),
      body: body(context),
    );
  }
}
