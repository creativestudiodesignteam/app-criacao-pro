// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'bird_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$BirdController on _BirdController, Store {
  final _$isTransferAtom = Atom(name: '_BirdController.isTransfer');

  @override
  bool get isTransfer {
    _$isTransferAtom.reportRead();
    return super.isTransfer;
  }

  @override
  set isTransfer(bool value) {
    _$isTransferAtom.reportWrite(value, super.isTransfer, () {
      super.isTransfer = value;
    });
  }

  final _$clickReportAtom = Atom(name: '_BirdController.clickReport');

  @override
  bool get clickReport {
    _$clickReportAtom.reportRead();
    return super.clickReport;
  }

  @override
  set clickReport(bool value) {
    _$clickReportAtom.reportWrite(value, super.clickReport, () {
      super.clickReport = value;
    });
  }

  final _$totalBirdsAtom = Atom(name: '_CageEggController.totalBirds');

  @override
  String get totalBirds {
    _$totalBirdsAtom.reportRead();
    return super.totalBirds;
  }

  @override
  set totalBirds(String value) {
    _$totalBirdsAtom.reportWrite(value, super.totalBirds, () {
      super.totalBirds = value;
    });
  }

  final _$birdsAtom = Atom(name: '_BirdController.birds');

  @override
  List<BirdModel> get birds {
    _$birdsAtom.reportRead();
    return super.birds;
  }

  @override
  set birds(List<BirdModel> value) {
    _$birdsAtom.reportWrite(value, super.birds, () {
      super.birds = value;
    });
  }

  final _$selectedBirdsAtom = Atom(name: '_BirdController.selectedBirds');

  @override
  List<BirdModel> get selectedBirds {
    _$selectedBirdsAtom.reportRead();
    return super.selectedBirds;
  }

  @override
  set selectedBirds(List<BirdModel> value) {
    _$selectedBirdsAtom.reportWrite(value, super.selectedBirds, () {
      super.selectedBirds = value;
    });
  }

  final _$onChangedCheckboxAsyncAction =
      AsyncAction('_BirdController.onChangedCheckbox');

  @override
  Future<void> onChangedCheckbox(BirdModel bird, bool isTwoOptions) {
    return _$onChangedCheckboxAsyncAction
        .run(() => super.onChangedCheckbox(bird, isTwoOptions));
  }

  final _$onPressedTransferAsyncAction =
      AsyncAction('_BirdController.onPressedTransfer');

  @override
  Future<void> onPressedTransfer() {
    return _$onPressedTransferAsyncAction.run(() => super.onPressedTransfer());
  }

  final _$onPressedTransferDoneAsyncAction =
      AsyncAction('_BirdController.onPressedTransferDone');

  @override
  Future<void> onPressedTransferDone() {
    return _$onPressedTransferDoneAsyncAction
        .run(() => super.onPressedTransferDone());
  }

  final _$onTapApplyFiltersAsyncAction =
      AsyncAction('_BirdController.onTapApplyFilters');

  @override
  Future<void> onTapApplyFilters() {
    return _$onTapApplyFiltersAsyncAction.run(() => super.onTapApplyFilters());
  }

  final _$onTapBackAsyncAction = AsyncAction('_BirdController.onTapBack');

  @override
  Future<void> onTapBack() {
    return _$onTapBackAsyncAction.run(() => super.onTapBack());
  }

  final _$onTapCleanFilterAsyncAction =
      AsyncAction('_BirdController.onTapCleanFilter');

  @override
  Future<void> onTapCleanFilter(String gender, int id) {
    return _$onTapCleanFilterAsyncAction
        .run(() => super.onTapCleanFilter(gender, id));
  }

  final _$readAllBirdsAsyncAction = AsyncAction('_BirdController.readAllBirds');

  @override
  Future<void> readAllBirds(String gender, int id) {
    return _$readAllBirdsAsyncAction.run(() => super.readAllBirds(gender, id));
  }

  final _$readAllBirdsCageAsyncAction =
      AsyncAction('_BirdController.readAllBirdsCage');

  @override
  Future<void> readAllBirdsCage(String gender, int id) {
    return _$readAllBirdsCageAsyncAction
        .run(() => super.readAllBirdsCage(gender, id));
  }

  @override
  String toString() {
    return '''
isTransfer: ${isTransfer},
birds: ${birds},
selectedBirds: ${selectedBirds},
totalBirds: ${totalBirds}
    ''';
  }
}
