import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import '../../app/app_appbar_widget.dart';
import '../../app/app_button_widget.dart';
import '../../app/app_header_widget.dart';
import '../../app/app_modal_widget.dart';
import '../../app/app_scaffold_widget.dart';
import '../../app/app_sub_menu_widget.dart';
import '../../app/app_text_widget.dart';
import '../bird_item_widget.dart';
import '../bird_model.dart';
import 'bird_parent_controller.dart';

class BirdParentView extends StatefulWidget {
  const BirdParentView({
    Key key,
    this.bird,
  }) : super(key: key);

  final BirdModel bird;

  @override
  _BirdParentViewState createState() => _BirdParentViewState();
}

class _BirdParentViewState
    extends ModularState<BirdParentView, BirdParentController> {
  Widget appBar(BuildContext context) {
    return AppAppBarWidget(
      titles: <Widget>[
        AppTextWidget(
          text: 'Pais da ave',
          size: Theme.of(context).textTheme.subtitle2.fontSize,
        ),
      ],
      actions: <Widget>[
        IconButton(
          icon: Icon(
            Icons.chevron_left,
          ),
          onPressed: () {
            Navigator.popUntil(context, ModalRoute.withName('/bird/preview'));
          },
        ),
      ],
    );
  }

  Widget body(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            AppSubMenuWidget(
              image: 'lib/assets/bird-sub-1.png',
              text: 'Detalhes',
              selected: false,
              onPressed: () {
                Navigator.pushNamed(
                  context,
                  '/bird/detail',
                  arguments: {'bird': widget.bird, 'isReport': false},
                );
              },
            ),
            AppSubMenuWidget(
              image: 'lib/assets/bird-sub-2.png',
              text: 'Pais',
              selected: true,
            ),
            AppSubMenuWidget(
              image: 'lib/assets/bird-sub-3.png',
              text: 'Filhos',
              selected: false,
              onPressed: () {
                Navigator.pushNamed(
                  context,
                  '/bird/children',
                  arguments: {'bird': widget.bird, 'isReport': false},
                );
              },
            ),
            AppSubMenuWidget(
              image: 'lib/assets/bird-sub-4.png',
              text: 'Fotos',
              selected: false,
              onPressed: () {
                Navigator.pushNamed(
                  context,
                  '/bird/image',
                  arguments: {'bird': widget.bird, 'isReport': false},
                );
              },
            ),
          ],
        ),
        SizedBox(
          height: 20,
        ),
        Column(
          children: <Widget>[
            AppHeaderWidget(
              title: 'Quem é ',
              subtitle: 'a ',
              highlight: 'Mãe?',
            ),
            SizedBox(
              height: 20,
            ),
            Observer(
              builder: (_) {
                var isEmpty = controller.mother == null;
                if (isEmpty) {
                  return AppTextWidget(
                    text: 'Selecione a mãe da ave',
                  );
                } else {
                  return BirdItemWidget(
                    bird: controller.mother,
                    isReport: true,
                    isSelect: true,
                  );
                }
              },
            ),
            Observer(
              builder: (_) {
                var isEmpty = controller.mother == null;
                if (isEmpty) {
                  return AppButtonWidget(
                    onPressed: () async {
                      showModalBottomSheet(
                        isDismissible: true,
                        context: context,
                        builder: (_) {
                          return Padding(
                            padding: const EdgeInsets.all(20.0),
                            child: AppModalWidget(
                              title:
                                  'Você tem certeza que quer cadastrar essa ave como mãe? Você não poderá editar isso depois!',
                              textYes: 'Adicionar',
                              textNo: 'Cancelar',
                              onPressedYes: () async {
                                Modular.to.pop();
                                var mother = await Modular.to.pushNamed(
                                  '/bird',
                                  arguments: {
                                    'isReport': true,
                                    'isSelect': true,
                                    'gender': 'Fêmea',
                                    'id': widget.bird.id,
                                  },
                                );
                                if (mother != null) {
                                  controller.onTapAddMother(
                                      widget.bird, mother, 1);
                                }
                              },
                              onPressedNo: () {
                                if (isEmpty) {
                                  Modular.to.pop();
                                } else {
                                  controller.onTapAddMother(
                                      widget.bird, widget.bird, 0);
                                }
                              },
                            ),
                          );
                        },
                      );
                    },
                    color: Colors.white,
                    text: 'Adicionar mãe',
                    textColor: Colors.black,
                  );
                } else {
                  return Container();
                }
              },
            ),
            SizedBox(
              height: 20,
            ),
            AppHeaderWidget(
              title: 'Quem é ',
              subtitle: 'o ',
              highlight: 'Pai?',
            ),
            SizedBox(
              height: 20,
            ),
            Observer(
              builder: (_) {
                var isEmpty = controller.father == null;
                if (isEmpty) {
                  return AppTextWidget(
                    text: 'Selecione o pai da ave',
                  );
                } else {
                  return BirdItemWidget(
                    bird: controller.father,
                    isReport: true,
                    isSelect: true,
                  );
                }
              },
            ),
            Observer(
              builder: (_) {
                var isEmpty = controller.father == null;
                if (isEmpty) {
                  return AppButtonWidget(
                    onPressed: () async {
                      showModalBottomSheet(
                        isDismissible: true,
                        context: context,
                        builder: (_) {
                          return Padding(
                            padding: const EdgeInsets.all(20.0),
                            child: AppModalWidget(
                              title:
                                  'Você tem certeza que quer cadastrar essa ave como pai? Você não poderá editar isso depois!',
                              textYes: 'Adicionar',
                              textNo: 'Cancelar',
                              onPressedYes: () async {
                                Modular.to.pop();
                                var father = await Modular.to.pushNamed(
                                  '/bird',
                                  arguments: {
                                    'isReport': true,
                                    'isSelect': true,
                                    'gender': 'Macho',
                                    'id': widget.bird.id,
                                  },
                                );
                                if (father != null) {
                                  controller.onTapAddFather(
                                      widget.bird, father, 1);
                                }
                              },
                              onPressedNo: () {
                                if (isEmpty) {
                                  Modular.to.pop();
                                } else {
                                  controller.onTapAddFather(
                                      widget.bird, widget.bird, 0);
                                }
                              },
                            ),
                          );
                        },
                      );
                    },
                    color: Colors.white,
                    text: 'Adicionar pai',
                    textColor: Colors.black,
                  );
                } else {
                  return Container();
                }
              },
            ),
          ],
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    controller.readMother(widget.bird.mother);
    controller.readFather(widget.bird.father);
    return AppScaffoldWidget(
      appBar: appBar(context),
      body: body(context),
    );
  }
}
