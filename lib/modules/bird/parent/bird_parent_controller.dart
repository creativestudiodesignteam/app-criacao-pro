import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:asuka/asuka.dart' as asuka;
import '../../app/app_message_widget.dart';
import '../bird_model.dart';
import '../bird_repository.dart';
part 'bird_parent_controller.g.dart';

class BirdParentController = _BirdParentController with _$BirdParentController;

abstract class _BirdParentController with Store {
  _BirdParentController({
    this.birdRepository,
  });

  final BirdRepository birdRepository;

  @observable
  BirdModel father;

  @observable
  BirdModel mother;

  @action
  Future<void> readMother(int motherId) async {
    if (motherId > 0) {
      var response = await birdRepository.readBird(motherId);
      mother = BirdModel.fromJson(json.decode(response['data']));
    }
  }

  @action
  Future<void> readFather(int fatherId) async {
    if (fatherId > 0) {
      var response = await birdRepository.readBird(fatherId);
      father = BirdModel.fromJson(json.decode(response['data']));
    }
  }

  @action
  Future<void> onTapAddMother(
      BirdModel bird, BirdModel mother, int type) async {
    if (mother.gender == 'fêmea'.toLowerCase()) {
      asuka.showSnackBar(
        SnackBar(
          content: AppMessageWidget(
            isLoading: true,
            message: 'A mãe deve ser do gênero fêmea',
          ),
          duration: Duration(
            milliseconds: 800,
          ),
        ),
      );
    }
    if (type == 0) {
      Modular.to.pop();
      bird.mother = -1;
    } else {
      bird.mother = mother.id;
    }
    asuka.showSnackBar(
      SnackBar(
        content: AppMessageWidget(
          isLoading: true,
          message: 'Por favor, aguarde ...',
        ),
        duration: Duration(
          milliseconds: 800,
        ),
      ),
    );
    await birdRepository.updateBird(bird);
    readMother(bird.mother);
    asuka.showSnackBar(
      SnackBar(
        content: AppMessageWidget(
          message: 'A ave foi atualizada com sucesso!',
        ),
        duration: Duration(
          milliseconds: 1000,
        ),
      ),
    );
  }

  @action
  Future<void> onTapAddFather(
      BirdModel bird, BirdModel father, int type) async {
    if (father.gender == 'macho'.toLowerCase()) {
      asuka.showSnackBar(
        SnackBar(
          content: AppMessageWidget(
            isLoading: true,
            message: 'O pai deve ser do gênero macho',
          ),
          duration: Duration(
            milliseconds: 800,
          ),
        ),
      );
    }
    if (type == 0) {
      Modular.to.pop();
      bird.father = -1;
    } else {
      bird.father = father.id;
    }
    asuka.showSnackBar(
      SnackBar(
        content: AppMessageWidget(
          isLoading: true,
          message: 'Por favor, aguarde ...',
        ),
        duration: Duration(
          milliseconds: 800,
        ),
      ),
    );
    await birdRepository.updateBird(bird);
    readFather(bird.father);
    asuka.showSnackBar(
      SnackBar(
        content: AppMessageWidget(
          message: 'A ave foi atualizada com sucesso!',
        ),
        duration: Duration(
          milliseconds: 1000,
        ),
      ),
    );
  }
}
