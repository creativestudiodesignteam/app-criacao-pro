// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'bird_parent_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$BirdParentController on _BirdParentController, Store {
  final _$fatherAtom = Atom(name: '_BirdParentController.father');

  @override
  BirdModel get father {
    _$fatherAtom.reportRead();
    return super.father;
  }

  @override
  set father(BirdModel value) {
    _$fatherAtom.reportWrite(value, super.father, () {
      super.father = value;
    });
  }

  final _$motherAtom = Atom(name: '_BirdParentController.mother');

  @override
  BirdModel get mother {
    _$motherAtom.reportRead();
    return super.mother;
  }

  @override
  set mother(BirdModel value) {
    _$motherAtom.reportWrite(value, super.mother, () {
      super.mother = value;
    });
  }

  final _$readMotherAsyncAction =
      AsyncAction('_BirdParentController.readMother');

  @override
  Future<void> readMother(int motherId) {
    return _$readMotherAsyncAction.run(() => super.readMother(motherId));
  }

  final _$readFatherAsyncAction =
      AsyncAction('_BirdParentController.readFather');

  @override
  Future<void> readFather(int fatherId) {
    return _$readFatherAsyncAction.run(() => super.readFather(fatherId));
  }

  final _$onTapAddMotherAsyncAction =
      AsyncAction('_BirdParentController.onTapAddMother');

  @override
  Future<void> onTapAddMother(BirdModel bird, BirdModel mother, int type) {
    return _$onTapAddMotherAsyncAction
        .run(() => super.onTapAddMother(bird, mother, type));
  }

  final _$onTapAddFatherAsyncAction =
      AsyncAction('_BirdParentController.onTapAddFather');

  @override
  Future<void> onTapAddFather(BirdModel bird, BirdModel father, int type) {
    return _$onTapAddFatherAsyncAction
        .run(() => super.onTapAddFather(bird, father, type));
  }

  @override
  String toString() {
    return '''
father: ${father},
mother: ${mother}
    ''';
  }
}
