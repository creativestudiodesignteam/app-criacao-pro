import 'package:flutter_modular/flutter_modular.dart';
import '../bird_repository.dart';
import 'bird_parent_controller.dart';
import 'bird_parent_view.dart';

class BirdParentModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind(
          (i) => BirdParentController(
            birdRepository: i.get<BirdRepository>(),
          ),
        ),
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(
          '/',
          child: (ctx, args) => BirdParentView(
            bird: args.data['bird'],
          ),
        ),
      ];
}
