class BirdColorModel {
  BirdColorModel({
    this.name,
  });

  BirdColorModel.fromJson(Map<String, dynamic> json) {
    name = json['name'];
  }

  String name;

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['name'] = name;
    return data;
  }
}

class YearModel {
  YearModel({
    this.name,
  });

  YearModel.fromJson(Map<String, dynamic> json) {
    name = json['name'];
  }

  String name;

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['name'] = name;
    return data;
  }
}
