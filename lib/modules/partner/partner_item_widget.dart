import 'package:flutter/material.dart';
import '../app/app_card_widget.dart';
import '../app/app_text_widget.dart';
import 'partner_model.dart';

class PartnerItemWidget extends StatelessWidget {
  const PartnerItemWidget({
    Key key,
    this.partner,
  }) : super(key: key);

  final PartnerModel partner;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(5),
      child: Container(
        child: GestureDetector(
          onTap: () async {
            await showDialog(context: context, builder: (_) => ImageDialog());
          },
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          image: DecorationImage(
              image: NetworkImage(
                  'http://198.199.90.249:2048/src/uploads/bird.png'),
              fit: BoxFit.cover),
        ),
      ),
    );
  }
}

class ImageDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: Colors.transparent,
      child: Container(
        width: 400,
        height: 600,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(50),
        ),
        child: Stack(
          children: [
            Container(
              width: 400,
              height: 600,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30),
                  image: DecorationImage(
                      image: NetworkImage(
                          'http://198.199.90.249:2048/src/uploads/bird.png'),
                      fit: BoxFit.cover)),
            ),
            Align(
              alignment: Alignment(1.05, -1.05),
              child: InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Container(
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors
                          .red[200], //                   <--- border color
                      width: 5.0,
                    ),
                    color: Colors.red,
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: Icon(
                    Icons.close,
                    color: Colors.black,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
