class PartnerModel {
  PartnerModel({
    this.id,
    this.image,
    this.title,
    this.subtitle,
  });

  int id;
  String image;
  String title;
  String subtitle;

  PartnerModel.fromJson(Map<String, dynamic> json) {
    id = int.parse(json['id'].toString());
    image = json['father'];
    title = json['father'];
    subtitle = json['image'];
  }

  Map<String, dynamic> toJson() {
    var data = {
      'id': id,
      'account': image,
      'category': title,
      'race': subtitle,
    };
    return data;
  }
}
