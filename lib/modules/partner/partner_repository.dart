import 'dart:convert';

import 'package:flutter/services.dart';

import 'partner_model.dart';

class PartnerRepository {
  Future<List<PartnerModel>> readAllPartners() async {
    var file = await rootBundle.loadString('lib/assets/partners.json');
    List<dynamic> data = json.decode(file);
    return data.map((dado) => PartnerModel.fromJson(dado)).toList();
  }
}
