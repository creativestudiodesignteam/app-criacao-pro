import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'partner_model.dart';
import 'partner_repository.dart';
part 'partner_controller.g.dart';

class PartnerController = _PartnerController with _$PartnerController;

abstract class _PartnerController with Store {
  _PartnerController({
    this.partnerRepository,
  });

  final PartnerRepository partnerRepository;

  @observable
  List<PartnerModel> partners;

  @action
  Future<void> onTapBack() async {
    Modular.to.pop();
  }

  @action
  Future<void> readAllPartners() async {
    partners = await partnerRepository.readAllPartners();
  }
}
