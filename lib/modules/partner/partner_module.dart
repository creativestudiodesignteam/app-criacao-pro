import 'package:flutter_modular/flutter_modular.dart';
import '../bird/bird_module.dart';
// import 'cage/partner_cage_module.dart';
// import 'filter/partner_filter_module.dart';
import 'partner_controller.dart';
import 'partner_repository.dart';
import 'partner_view.dart';

class PartnerModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind(
          (i) => PartnerController(
            partnerRepository: i.get<PartnerRepository>(),
          ),
        ),
        Bind(
          (i) => PartnerRepository(),
        ),
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(
          '/',
          child: (ctx, args) => PartnerView(),
        ),
        ModularRouter(
          '/bird',
          module: BirdModule(),
        ),
        // ModularRouter(
        //   '/filter',
        //   module: PartnerFilterModule(),
        // ),
        // ModularRouter(
        //   '/cage',
        //   module: PartnerCageModule(),
        // ),
      ];
}
