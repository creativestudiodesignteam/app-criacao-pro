import 'package:criacao_pro/modules/partner/partner_item_widget.dart';
import 'package:flutter/material.dart';
import 'partner_model.dart';

class PartnerListWidget extends StatelessWidget {
  const PartnerListWidget({
    Key key,
    this.partners,
  }) : super(key: key);

  final List<PartnerModel> partners;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 600,
      width: 400,
      child: GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
        ),
        itemCount: partners.length,
        itemBuilder: (_, i) {
          return PartnerItemWidget(
            partner: partners.elementAt(i),
          );
        },
      ),
    );
  }
}
