import 'package:criacao_pro/modules/app/app_action_button_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:url_launcher/url_launcher.dart';
import '../app/app_appbar_widget.dart';
import '../app/app_header_widget.dart';
import '../app/app_scaffold_widget.dart';
import '../app/app_text_widget.dart';
import 'partner_controller.dart';
import 'partner_list_widget.dart';

class PartnerView extends StatefulWidget {
  @override
  _PartnerViewState createState() => _PartnerViewState();
}

class _PartnerViewState extends ModularState<PartnerView, PartnerController> {
  Widget appBar(BuildContext context) {
    return AppAppBarWidget(
      titles: <Widget>[
        AppTextWidget(
          text: 'Parcerias',
          size: Theme.of(context).textTheme.subtitle2.fontSize,
        ),
      ],
      actions: <Widget>[
        IconButton(
          icon: Icon(
            Icons.chevron_left,
          ),
          onPressed: controller.onTapBack,
        ),
      ],
    );
  }

  Widget body(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Column(
            children: <Widget>[
              AppHeaderWidget(
                title: 'Parcerias',
                subtitle: '',
                highlight: 'Criação Pró',
              ),
              SizedBox(
                height: 20,
              ),
              AppHeaderWidget(
                title: 'Quer ser um parceiro Criação Pró?',
                subtitle:
                    'Para saber como anunciar aqui\n entre em contato conosco!',
                highlight: '',
                size: Theme.of(context).textTheme.subtitle1.fontSize,
              ),
              SizedBox(
                height: 20,
              ),
              AppActionAppButtonWidget(
                label: 'Quero anunciar aqui',
                icon: Icons.message,
                color: Colors.green,
                onPressed: () async {
                  if (await canLaunch(
                      'https://api.whatsapp.com/send?1=pt_br&phone=5511963466313')) {
                    await launch(
                        'https://api.whatsapp.com/send?1=pt_br&phone=5511963466313');
                  }
                },
              ),
              SizedBox(
                height: 20,
              ),
              Observer(
                builder: (_) {
                  if (controller.partners == null) {
                    return AppTextWidget(
                      text: 'Buscando parcerias...',
                    );
                  } else if (controller.partners.isEmpty) {
                    return AppTextWidget(
                      text: 'Nenhum relatório encontrado.',
                    );
                  } else {
                    return PartnerListWidget(
                      partners: controller.partners,
                    );
                  }
                },
              ),
            ],
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    controller.readAllPartners();
    return AppScaffoldWidget(
      appBar: appBar(context),
      body: body(context),
    );
  }
}
