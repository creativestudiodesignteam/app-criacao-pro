import 'package:flutter/material.dart';

import 'modules/bird/bird_model.dart';
import 'report_pdf.dart';

class PDFCreator extends StatefulWidget {
  PDFCreator({
    this.bird,
  });

  final BirdModel bird;

  @override
  _PDFCreatorState createState() => _PDFCreatorState();
}

class _PDFCreatorState extends State<PDFCreator> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          margin: EdgeInsets.only(top: 30),
          height: 40,
          child: RaisedButton(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5.0),
            ),
            child: Text(
              'Get Report',
              style:
                  TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
            ),
            color: Colors.blue,
            onPressed: () {
              reportView(context, widget.bird);
            },
          ),
        ),
      ),
    );
  }
}
