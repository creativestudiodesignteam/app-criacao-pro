import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart' as material;
import 'constants.dart' as constants;
import 'pdf_viewer_page.dart';

final String pathBird = '${constants.appUrl}/bird';

// ignore: type_annotate_public_apis
reportViewFilters(material.BuildContext context, dynamic birds) async {
  var resultReport = await pairReportFunction(birds);

  print("resultReport['data']");
  print(resultReport['data']);

  var link = json.decode(resultReport['data']);
  var linkPdf = link.replaceAll('\\', '');

  material.Navigator.of(context).push(
    material.MaterialPageRoute(
      builder: (_) => PdfViewerPage(
        linkPdf: linkPdf,
      ),
    ),
  );
}

final Options options = Options(
  headers: {
    'X-Token': constants.appSecret,
  },
);

Future<Map<String, dynamic>> pairReportFunction(dynamic birds) async {
  var formData = FormData.fromMap({'birds': birds});
  var response = await Dio().post(
    '${pathBird}/report-filters',
    data: formData,
    options: options,
  );
  return json.decode(response.data);
}
