// import 'package:app/utils/share/share_bloc.dart';
// import 'package:app/utils/share/share_state.dart';
// import 'package:flutter/material.dart';

// class ShareButton extends StatelessWidget {
//   const ShareButton({
//     Key key,
//     @required this.fileUrl,
//     @required this.fileType,
//   }) : super(key: key);

//   final String fileUrl;
//   final FileType fileType;

//   @override
//   Widget build(BuildContext context) {
//     var shareState = Provider.of<ShareState>(context, listen: false);
//     return IconButton(
//       onPressed: () async {
//         shareState.shareFile(
//           fileUrl: fileUrl,
//           fileType: fileType,
//         );
//       },
//       icon: Icon(
//         Icons.share,
//         color: Colors.white,
//       ),
//     );
//   }
// }
