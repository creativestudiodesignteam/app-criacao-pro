import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart' as material;
import 'package:intl/intl.dart';
import 'constants.dart' as constants;
import 'modules/bird/bird_model.dart';
import 'pdf_viewer_page.dart';

final String path = '${constants.appUrl}/egg';
final String pathBird = '${constants.appUrl}/bird';

// ignore: type_annotate_public_apis
reportViewCages(material.BuildContext context, List<BirdModel> birds) async {
  var birdFather;
  var birdMother;

  for (var bird in birds) {
    if (bird.gender == "Macho") {
      birdFather = bird;
    }
    if (bird.gender == "Fêmea") {
      birdMother = bird;
    }
  }

  // String endpoint = bird.gender == 'Macho'
  //     ? '${constants.appUrl}/egg/read-all-by-father'
  //     : '${constants.appUrl}/egg/read-all-by-mother';

  // dynamic mother = null;
  // dynamic father = null;
  int totalOvosBotados = 0;
  int totalOvosCheios = 0;
  int totalNascimentos = 0;
  int totalObitos = 0;
  int totalVivos = 0;

  final dateNow = DateTime.now();
  DateFormat dateFormat = DateFormat("dd/MM/yyyy");

  var birthDateMother =
      DateTime.fromMillisecondsSinceEpoch(birdMother.birthDate);
  var birthDateFormattedMother =
      DateFormat('dd/MM/yyyy').format(birthDateMother).toString();
  final daysLifeMother =
      dateNow.difference(dateFormat.parse(birthDateFormattedMother)).inDays;
  var birthDateFather =
      DateTime.fromMillisecondsSinceEpoch(birdFather.birthDate);
  var birthDateFormattedFather =
      DateFormat('dd/MM/yyyy').format(birthDateFather).toString();
  final daysLifeFather =
      dateNow.difference(dateFormat.parse(birthDateFormattedFather)).inDays;

  // var responseMother = await readBird(bird.mother);
  // bird.mother == 0 || bird.mother == ''
  //     ? mother = null
  //     : mother = BirdModel.fromJson(json.decode(responseMother['data']));

  // var responseFather = await readBird(bird.father);
  // bird.father == 0 || bird.father == ''
  //     ? father = null
  //     : father = BirdModel.fromJson(json.decode(responseFather['data']));

  var responseEggs = await readEggs(birdMother.id, birdFather.id);
  var jsonEgg = json.decode(responseEggs['data']);
  for (var egg in jsonEgg) {
    if (egg['state'] == "Chocar") {
      totalOvosBotados += 1;
    }
    if (egg['state'] == "Fertilizar") {
      totalOvosCheios += 1;
    }
    if (egg['state'] == "Nasceu") {
      totalOvosCheios += 1;
    }
  }

  var responseBirds = await readChildren(birdMother.id, birdFather.id);
  var jsonBirds = json.decode(responseBirds['data']);
  for (var forBird in jsonBirds) {
    if (forBird['status'] == "Vivo") {
      totalVivos += 1;
    }
    if (forBird['status'] == "Morto") {
      totalObitos += 1;
    }
  }

  var pairReport = {
    'imageMale': birdFather.image,
    'leftRingMale': birdFather.rightRing != '' ? birdFather.rightRing : null,
    'rightRingMale': birdFather.leftRing != '' ? birdFather.leftRing : null,
    'registryMale': birdFather.registry != '' ? birdFather.registry : null,
    'birthDateMale': birthDateFormattedFather,
    'daysLifeMale': daysLifeFather,
    'genderMale': birdFather.gender,
    'categoryMale': birdFather.category,
    'raceMale': birdFather.race,
    'colorMale': birdFather.color,
    'imageFemale': birdFather.image,
    'leftRingFemale': birdMother.rightRing != '' ? birdMother.rightRing : null,
    'rightRingFemale': birdMother.leftRing != '' ? birdMother.leftRing : null,
    'registryFemale': birdMother.registry != '' ? birdMother.registry : null,
    'birthDateFemale': birthDateFormattedMother,
    'daysLifeFemale': daysLifeMother,
    'genderFemale': birdMother.gender,
    'categoryFemale': birdMother.category,
    'raceFemale': birdMother.race,
    'colorFemale': birdMother.color,
    'eggsLaid': totalOvosBotados,
    'eggsFull': totalOvosCheios,
    'numberBirths': totalNascimentos,
    'puppyDeaths': totalObitos,
    'liveChicks': totalVivos,
  };

  var resultReport = await pairReportFunction(pairReport);

  var link = json.decode(resultReport['data']);
  var linkPdf = link.replaceAll('\\', '');

  material.Navigator.of(context).push(
    material.MaterialPageRoute(
      builder: (_) => PdfViewerPage(
        linkPdf: linkPdf,
      ),
    ),
  );
}

final Options options = Options(
  headers: {
    'X-Token': constants.appSecret,
  },
);

Future<Map<String, dynamic>> pairReportFunction(dataReport) async {
  var formData = FormData.fromMap(dataReport);
  var response = await Dio().post(
    '${pathBird}/report-pair',
    data: formData,
    options: options,
  );
  return json.decode(response.data);
}

Future<Map<String, dynamic>> readEggs(int mother, int father) async {
  var formData = FormData.fromMap(
    {
      'mother': mother,
      'father': father,
    },
  );
  var response = await Dio().post(
    '${constants.appUrl}/egg/read-all-by-parents',
    data: formData,
    options: options,
  );
  return json.decode(response.data);
}

Future<Map<String, dynamic>> readChildren(int mother, int father) async {
  var formData = FormData.fromMap(
    {
      'mother': mother,
      'father': father,
    },
  );
  var response = await Dio().post(
    '${constants.appUrl}/bird/read-by-parents',
    data: formData,
    options: options,
  );
  return json.decode(response.data);
}
