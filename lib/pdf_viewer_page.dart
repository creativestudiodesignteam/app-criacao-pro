import 'dart:io';
import 'package:advance_pdf_viewer/advance_pdf_viewer.dart';
import 'package:criacao_pro/modules/app/app_appbar_widget.dart';
import 'package:criacao_pro/modules/app/app_message_widget.dart';
import 'package:criacao_pro/modules/app/app_text_widget.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:ext_storage/ext_storage.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:share/share.dart';
import 'package:asuka/asuka.dart' as asuka;

class PdfViewerPage extends StatefulWidget {
  const PdfViewerPage({
    Key key,
    this.linkPdf,
  }) : super(key: key);

  final String linkPdf;

  @override
  _PdfViewerPageState createState() => _PdfViewerPageState();
}

class _PdfViewerPageState extends State<PdfViewerPage> {
  @override
  void initState() {
    super.initState();
    changePDF();
  }

  bool _isLoading = true;
  PDFDocument document;

  changePDF() async {
    setState(() => _isLoading = true);
    document = await PDFDocument.fromURL(widget.linkPdf);
    setState(() => _isLoading = false);
  }

  @override
  Widget build(BuildContext context) {
    getPermission();
    return Scaffold(
      appBar: AppAppBarWidget(
        titles: <Widget>[
          AppTextWidget(
            text: 'Relatórios',
            size: Theme.of(context).textTheme.subtitle2.fontSize,
          ),
        ],
        backgroundColor: Colors.blue,
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.download_outlined,
            ),
            onPressed: () async {
              downloadFile('Download efetuado com sucesso!');
              asuka.showSnackBar(
                SnackBar(
                  content: AppMessageWidget(
                    isLoading: true,
                    message: 'Por favor, aguarde ...',
                  ),
                  duration: Duration(
                    milliseconds: 800,
                  ),
                ),
              );
            },
          ),
          IconButton(
            icon: Icon(
              Icons.share,
            ),
            onPressed: () async {
              var pathDownload =
                  await downloadFile('Compartilhamento em andamento!');
              await Share.shareFiles(['${pathDownload}'],
                  text: 'Compartilhar Relatório');
            },
          ),
          IconButton(
            icon: Icon(
              Icons.chevron_left,
            ),
            onPressed: () => Navigator.pop(context),
          ),
        ],
      ),
      body: Center(
        child: _isLoading
            ? CircularProgressIndicator()
            : PDFViewer(document: document),
      ),
    );
  }

  var imageUrl = "http://52.165.220.225/src/uploads/pdf_uplo_24600.pdf";
  bool downloading = true;
  String downloadingStr = "No data";
  double download = 0.0;
  File f;

  void getPermission() async {
    Map<PermissionGroup, PermissionStatus> permissions =
        await PermissionHandler().requestPermissions([PermissionGroup.storage]);
  }

  Future<String> downloadFile(String message) async {
    HttpClient httpClient = new HttpClient();
    File file;
    String filePath = '';
    String myUrl = '';

    final pathPdf = await ExtStorage.getExternalStoragePublicDirectory(
        ExtStorage.DIRECTORY_DOWNLOADS);
    var namePdf = DateTime.now().toString();
    var fullPath = "$pathPdf/relatório-$namePdf.pdf";

    try {
      myUrl = widget.linkPdf;
      var request = await httpClient.getUrl(Uri.parse(myUrl));
      var response = await request.close();
      if (response.statusCode == 200) {
        var bytes = await consolidateHttpClientResponseBytes(response);
        file = File(fullPath);
        await file.writeAsBytes(bytes);
        asuka.showSnackBar(
          SnackBar(
            content: AppMessageWidget(
              isLoading: false,
              message: message,
            ),
            duration: Duration(
              milliseconds: 2000,
            ),
          ),
        );
      } else {
        filePath = 'Error code: ' + response.statusCode.toString();
      }
    } catch (ex) {
      filePath = 'Can not fetch url';
    }

    return fullPath;
  }
}
