import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart' as material;
import 'package:intl/intl.dart';
import './constants.dart' as constants;
import 'modules/bird/bird_model.dart';
import 'pdf_viewer_page.dart';

final String path = '${constants.appUrl}/bird';

// ignore: type_annotate_public_apis
reportView(context, bird) async {
  String endpoint = bird.gender == 'Macho'
      ? '${constants.appUrl}/egg/read-all-by-father'
      : '${constants.appUrl}/egg/read-all-by-mother';

  dynamic mother = null;
  dynamic father = null;
  String eggs = null;

  var responseMother = await readBird(bird.mother);
  bird.mother == 0 || bird.mother == ''
      ? mother = null
      : mother = BirdModel.fromJson(json.decode(responseMother['data']));

  var responseFather = await readBird(bird.father);
  bird.father == 0 || bird.father == ''
      ? father = null
      : father = BirdModel.fromJson(json.decode(responseFather['data']));

  var responseEggs = await readEggs(bird.id, endpoint);
  var jsonEgg = json.decode(responseEggs['data']);
  eggs = jsonEgg.length > 0 ? jsonEgg[0]['eggs'] : null;

  final dateNow = DateTime.now();
  DateFormat dateFormat = DateFormat("dd/MM/yyyy");

  var birthDateBird = DateTime.fromMillisecondsSinceEpoch(bird.birthDate);
  var birthDateFormattedBird =
      DateFormat('dd/MM/yyyy').format(birthDateBird).toString();
  final daysLifeBird =
      dateNow.difference(dateFormat.parse(birthDateFormattedBird)).inDays;

  var invidualReport = {
    'birdId': bird.id,
    'imageBird': bird.image,
    'leftRingBird': bird.leftRing != '' ? bird.leftRing : "Sem número",
    'rightRingBird': bird.rightRing != '' ? bird.rightRing : "Sem número",
    'registryBird': bird.registry != '' ? bird.registry : "Sem número",
    'birthDateBird': birthDateFormattedBird,
    'daysLifeBird': daysLifeBird,
    'genderBird': bird.gender,
    'categoryBird': bird.category,
    'raceBird': bird.race,
    'colorBird': bird.color,
    'imageBirdMother': mother != null
        ? mother.image != ''
            ? mother.image
            : null
        : null,
    'leftRingBirdMother': mother != null
        ? mother.leftRing != ''
            ? mother.leftRing
            : "Sem número"
        : '',
    'rightRingBirdMother': mother != null
        ? mother.rightRing != ''
            ? mother.rightRing
            : "Sem número"
        : '',
    'categoryBirdMother': mother != null
        ? mother.category != ''
            ? mother.category
            : "Sem número"
        : '',
    'raceBirdMother': mother != null
        ? mother.race != ''
            ? mother.race
            : "Sem número"
        : '',
    'colorBirdMother': mother != null
        ? mother.color != ''
            ? mother.color
            : "Sem número"
        : '',
    'imageBirdFather': father != null
        ? father.image != ''
            ? father.image
            : null
        : null,
    'leftRingBirdFather': father != null
        ? father.leftRing != ''
            ? father.leftRing
            : "Sem número"
        : '',
    'rightRingBirdFather': father != null
        ? father.rightRing != ''
            ? father.rightRing
            : "Sem número"
        : '',
    'categoryBirdFather': father != null
        ? father.category != ''
            ? father.category
            : "Sem número"
        : '',
    'raceBirdFather': father != null
        ? father.race != ''
            ? father.race
            : "Sem número"
        : '',
    'colorBirdFather': father != null
        ? father.color != ''
            ? father.color
            : "Sem número"
        : '',
    'eggs': eggs
  };

  var resultReport = await indiviudalReport(invidualReport);

  var link = json.decode(resultReport['data']);
  var linkPdf = link.replaceAll('\\', '');

  material.Navigator.of(context).push(
    material.MaterialPageRoute(
      builder: (_) => PdfViewerPage(
        linkPdf: linkPdf,
      ),
    ),
  );
}

final Options options = Options(
  headers: {
    'X-Token': constants.appSecret,
  },
);

Future<Map<String, dynamic>> readBird(int birdId) async {
  var formData = FormData.fromMap(
    {
      'id': birdId,
    },
  );
  var response = await Dio().post(
    '${path}/read',
    data: formData,
    options: options,
  );
  return json.decode(response.data);
}

Future<Map<String, dynamic>> indiviudalReport(dataReport) async {
  var formData = FormData.fromMap(dataReport);
  var response = await Dio().post(
    '${path}/report-individual',
    data: formData,
    options: options,
  );
  return json.decode(response.data);
}

Future<Map<String, dynamic>> readEggs(int cageId, String path) async {
  var formData = FormData.fromMap({
    'cage': cageId,
  });
  var response = await Dio().post(
    path,
    data: formData,
    options: options,
  );
  return json.decode(response.data);
}
