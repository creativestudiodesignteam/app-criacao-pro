<?php

define('APP_PATH', str_replace('\\', '/', __DIR__));
define('APP_HOST', 'http' . ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') ? 's' : '') . '://' . $_SERVER['HTTP_HOST']);

include APP_PATH . '/vendor/autoload.php';
include APP_PATH . '/src/configs.php';
include APP_PATH . '/src/functions.php';

ini_set('max_execution_time', 15);
ini_set('date.timezone', 'America/Sao_Paulo');
ini_set('default_charset', 'UTF-8');
ini_set('display_errors', APP_DEBUG);
ini_set('display_startup_errors', APP_DEBUG);
ini_set('error_reporting', APP_DEBUG ? E_ALL : 0);
ini_set('session.save_path', path('/src/sessions/'));
ini_set('session.name', encode(encrypt(APP_SECRET)));

try {
    if(APP_OFFLINE) {
        http_response_code(503);
        exit('503 Service Unavailable. We are under maintenance but dont worry, well be back soon.');
    }
    $routes = include path('/src/routes.php');
    $router = new \AltoRouter($routes, APP_DIR, []);
    $match = $router->match();
    if(!is_array($match) || !is_callable($match['target'])) {
        http_response_code(404);
        exit('404 Not Found. The page you requested does not exist or has been moved.');
    }
    $params['url'] = host($_SERVER['REQUEST_URI']);
    $params['method'] = $_SERVER['REQUEST_METHOD'];
    $params['get'] = array_merge(isset($match['params']) ? $match['params'] : [], $_GET);
    $params['post'] = $_POST;
    $params['file'] = $_FILES;
    call_user_func_array($match['target'], [$params]);
} catch(Exception $e) {
    exit($e->getMessage());
}
