<?php

namespace mthsena\src\controllers\bird;

defined('APP_PATH') or exit('No direct script access allowed.');

class ReadImage {

    public function __construct($params) {
        $isPost = $params['method'] == 'POST';
        $isSigned = getHeaderKey() == APP_SECRET;
        if($isPost && $isSigned) {
            $this->post($params);
        } else {
            http_response_code(404);
            exit('404 Not Found. The page you requested does not exist or has been moved.');
        }
    }

    private function post($params) {
        $imageRepository = new \mthsena\src\repositories\Images();
        $bird = isset($params['post']['bird']) ? $params['post']['bird'] : false;
        if(!$bird) {
            exit(response('warning', 'Preencha todos os campos corretamente.'));
        }
        $images = $imageRepository->readAllByBird($bird);
        if(empty($images)) {
            exit(response('danger', 'As imagens não foram encontradas.'));
        }
        exit(response('success', 'As imagens foram obtidas com sucesso!', $images));
    }

}
