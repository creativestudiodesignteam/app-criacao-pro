<?php

namespace mthsena\src\controllers\cage;

defined('APP_PATH') or exit('No direct script access allowed.');

class Update {

    public function __construct($params) {
        $isPost = $params['method'] == 'POST';
        $isSigned = getHeaderKey() == APP_SECRET;
        if($isPost && $isSigned) {
            $this->post($params);
        } else {
            http_response_code(404);
            exit('404 Not Found. The page you requested does not exist or has been moved.');
        }
    }

    private function post($params) {
        $cageRepository = new \mthsena\src\repositories\Cages();
        $account = isset($params['post']['account']) ? $params['post']['account'] : false;
        $state = isset($params['post']['state']) ? $params['post']['state'] : false;
        $name = isset($params['post']['name']) ? $params['post']['name'] : '';
        $id = isset($params['post']['id']) ? $params['post']['id'] : false;
        if(!$account || !$state || !$id) {
            exit(response('warning', 'Preencha todos os campos corretamente.'));
        }
        $cageRepository->update($account, $state, $name, $id);
        exit(response('success', 'A gaiola foi atualizada com sucesso!'));
    }

}
