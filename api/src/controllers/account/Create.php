<?php

namespace mthsena\src\controllers\account;

defined('APP_PATH') or exit('No direct script access allowed.');

class Create {

    public function __construct($params) {
        $isPost = $params['method'] == 'POST';
        $isSigned = getHeaderKey() == APP_SECRET;
        if($isPost && $isSigned) {
            $this->post($params);
        } else {
            http_response_code(404);
            exit('404 Not Found. The page you requested does not exist or has been moved.');
        }
    }

    private function post($params) {
        $accountRepository = new \mthsena\src\repositories\Accounts();
        $cpf = isset($params['post']['cpf']) ? $params['post']['cpf'] : '';
        $email = isset($params['post']['email']) ? strtolower($params['post']['email']) : false;
        $name = isset($params['post']['name']) ? $params['post']['name'] : false;
        $phone = isset($params['post']['phone']) ? $params['post']['phone'] : false;
        $password = isset($params['post']['password']) ? encrypt($email . $params['post']['password']) : false;
        if(!$email || !$name || !$phone || !$password) {
            exit(response('warning', 'Preencha todos os campos corretamente.'));
        }
        $account = $accountRepository->read($email);
        if(!empty($account)) {
            exit(response('danger', 'Esse e-mail já foi registrado.'));
        }
        $accountRepository->create($cpf, $email, $name, $phone, $password);
        exit(response('success', 'A conta foi criada com sucesso!', $account));
    }

}
