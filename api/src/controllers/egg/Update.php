<?php

namespace mthsena\src\controllers\egg;

defined('APP_PATH') or exit('No direct script access allowed.');

class Update {

    public function __construct($params) {
        $isPost = $params['method'] == 'POST';
        $isSigned = getHeaderKey() == APP_SECRET;
        if($isPost && $isSigned) {
            $this->post($params);
        } else {
            http_response_code(404);
            exit('404 Not Found. The page you requested does not exist or has been moved.');
        }
    }

    private function post($params) {
        $eggRepository = new \mthsena\src\repositories\Eggs();
        $account = isset($params['post']['account']) ? $params['post']['account'] : false;
        $state = isset($params['post']['state']) ? $params['post']['state'] : false;
        $layingDate = isset($params['post']['layingDate']) ? $params['post']['layingDate'] : false;
        $hatchingDate = isset($params['post']['hatchingDate']) ? $params['post']['hatchingDate'] : -1;
        $birthDate = isset($params['post']['birthDate']) ? $params['post']['birthDate'] : -1;
        $candlingDate = isset($params['post']['candlingDate']) ? $params['post']['candlingDate'] : -1;
        $cage = isset($params['post']['cage']) ? $params['post']['cage'] : false;
        $mother = isset($params['post']['mother']) ? $params['post']['mother'] : -1;
        $father = isset($params['post']['father']) ? $params['post']['father'] : -1;
        $id = isset($params['post']['id']) ? $params['post']['id'] : false;
        if(!$account || !$state || !$layingDate || !$cage || !$id) {
            exit(response('warning', 'Preencha todos os campos corretamente.'));
        }
        $eggRepository->update($account, $state, $layingDate, $hatchingDate, $birthDate, $candlingDate, $cage, $mother, $father, $id);
        exit(response('success', 'O ovo foi atualizado com sucesso!'));
    }

}
