<?php

namespace mthsena\src\repositories;

defined('APP_PATH') or exit('No direct script access allowed.');

class Accounts {

    private $table = 'accounts';

    public function auth($email, $password) {
        $query = 'select id from %s where email = ? AND password = ?';
        $result = database($query, $this->table, func_get_args());
        return empty($result->rowCount()) ? [] : $result->fetch(\PDO::FETCH_ASSOC);
    }

    public function create($cpf, $email, $name, $phone, $password) {
        $query = 'insert into %s (cpf, email, name, phone, password) values (?, ?, ?, ?, ?)';
        $result = database($query, $this->table, func_get_args());
        return empty($result->rowCount()) ? false : true;
    }

    public function read($email) {
        $query = 'select * from %s where email = ?';
        $result = database($query, $this->table, func_get_args());
        return empty($result->rowCount()) ? [] : $result->fetch(\PDO::FETCH_ASSOC);
    }

    public function updatePassword($password, $email) {
        $query = 'update %s set password = ? where email = ?';
        $result = database($query, $this->table, func_get_args());
        return empty($result->rowCount()) ? false : true;
    }
}
