<?php

namespace mthsena\src\repositories;

defined('APP_PATH') or exit('No direct script access allowed.');

class Babies {

    private $table = 'babies';

    public function create($account, $birthDate, $ringDate, $separateDate, $cage, $mother, $father) {
        $query = 'insert into %s (account, birth_date, ring_date, separate_date, cage, mother, father) values (?, ?, ?, ?, ?, ?, ?)';
        $result = database($query, $this->table, func_get_args());
        return empty($result->rowCount()) ? false : true;
    }

    public function read($id) {
        $query = 'select * from %s where id = ?';
        $result = database($query, $this->table, func_get_args());
        return empty($result->rowCount()) ? [] : $result->fetch(\PDO::FETCH_ASSOC);
    }

    public function readAllByCage($cage) {
        $query = 'select * from %s where cage = ?';
        $result = database($query, $this->table, func_get_args());
        return empty($result->rowCount()) ? [] : $result->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function readAllByAccount($account) {
        $query = 'select * from %s where account = ?';
        $result = database($query, $this->table, func_get_args());
        return empty($result->rowCount()) ? [] : $result->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function update($account, $birthDate, $ringDate, $separateDate, $cage, $mother, $father, $id) {
        $query = 'update %s set set account = ?, birth_date = ?, ring_date = ?, separate_date = ?, cage = ?, mother = ?, father = ? where id = ?';
        $result = database($query, $this->table, func_get_args());
        return empty($result->rowCount()) ? false : true;
    }

    public function delete($id) {
        $query = 'delete from %s where id = ?';
        $result = database($query, $this->table, func_get_args());
        return empty($result->rowCount()) ? false : true;
    }
}
